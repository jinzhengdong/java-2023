package me.ereach.hr;

public class Employee {
    private Integer id;
    private String name;
    private Boolean gender;
    private String phone;
    private Double gz;
    private Integer jbsj;
    private Double jbgz;


    public Employee(Integer id, String name, Boolean gender, String phone, Double gz, Integer jbsj, Double jbgz) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.gz = gz;
        this.jbsj = jbsj;
        this.jbgz = jbgz;



    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getGz() {
        return gz;
    }

    public void setGz(Double gz) {
        this.gz = gz;
    }

    public Integer getJbsj() {
        return jbsj;
    }

    public void setJbsj(Integer jbsj) {
        this.jbsj = jbsj;
    }

    public Double getJbgz() {
        return jbgz;
    }

    public void setJbgz(Double jbgz) {
        this.jbgz = jbgz;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", phone='" + phone + '\'' +
                ", gz=" + gz +
                ", jbsj=" + jbsj +
                ", jbgz=" + jbgz +
                '}';
    }

    public double sjgz(Employee emp){
        double sj = gz + jbgz * jbsj;
        if (sj <= 5000){
            return sj;
        }else if(sj > 5000 && sj <=12000){
            return sj - ((sj-5000)*0.03);
        }else
            return sj - ((sj-5000)*0.1)+210;
    }
}
