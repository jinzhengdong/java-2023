package me.ereach;

public class Teacher extends Person {
    private String major;

    public Teacher(String name, String gender, String major) {
        super(name, gender);
        this.major = major;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "person=" + super.toString() + ", " +
                "major='" + major + '\'' +
                '}';
    }
}
