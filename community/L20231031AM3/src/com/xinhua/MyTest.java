package com.xinhua;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyTest {
    @Test
    public void test08() {
        String str = null;

        // 尝试
        try {
            System.out.println(str.toLowerCase());
        }
        catch (NullPointerException e) {    // 抓住 NullPointerException 这个错误
            System.out.println(e.getMessage());
        }
        finally {   // 最后
            System.out.println("finished.");
        }
    }

    @Test
    public void test07() {
        String str = null;

        try {
            System.out.println(str.toUpperCase());
        }
        catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("finished.");
        }

    }

    @Test
    public void test06() {
        String str = "Hello, World";
        // 转化为大写
        System.out.println(str.toUpperCase());
    }

    @Test
    public void test05() {
//        FileReader fileReader = new FileReader("d:\\readme.tt");
    }

    @Test
    public void test04() {
        try (FileReader fileReader = new FileReader("d:\\readme.txt")) {
            System.out.println(fileReader);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void test03() {
        try {
            FileReader fileReader = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        finally {
            System.out.println("finished.");
        }
    }

    @Test
    public void test02() {
        FileReader fileReader = null;

        try {
            fileReader = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Test
    public void test01() throws FileNotFoundException {
        // FileReader 文件读取类
        // d:\readme.txt    转义字符 \\ => \
        FileReader fileReader = new FileReader("d:\\readme.txt");
    }

}
