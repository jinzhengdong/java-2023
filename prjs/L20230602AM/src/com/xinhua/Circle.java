package com.xinhua;

public class Circle {
    private float r;
    private Point o;

    public Circle(Point o, float r) {
        this.o = o;
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public void setO(Point o) {
        this.o = o;
    }

    public Point getO() {
        return o;
    }

    public String toString() {
        return "Circle{" +
                "o=" + o.toString() +
                ", r=" + r +
                '}';
    }

    public double getDistance(Point p) {
        return o.getDistance(p);
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
