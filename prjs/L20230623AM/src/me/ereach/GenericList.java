package me.ereach;

import java.util.Arrays;

public class GenericList<T extends Comparable<T>> {
    private T[] items = (T[]) new Comparable[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "GenericList{" +
                "items=" + Arrays.toString(items) +
                ", count=" + count +
                '}';
    }
}
