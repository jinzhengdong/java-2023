package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyTest {
    @Test
    public void test05(){
        double a = 20e10;
    }
    @Test
    public void test04() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        // 使用并行流对数字进行处理
        numbers.parallelStream()
                .forEach(System.out::println);
    }

    @Test
    public void test03() {
        List<Integer> numbers = Arrays.asList(1, 2, null);

        numbers.stream().forEach(System.out::println);
        System.out.println("===");
        int max = numbers.stream()
                .max(Integer::compare)
                .orElse(0);
        System.out.println("===");
        System.out.println(max);

    }

    @Test
    public void test02() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        // 使用forEach打印每个元素
        numbers.stream()
                .forEach(System.out::println);

        // 使用collect将元素收集到一个List中
        List<Integer> evenNumbers = numbers.stream()
                .filter(number -> number % 2 == 0)
                .collect(Collectors.toList());
        System.out.println("===");
        for (var x : evenNumbers)
            System.out.println(x);
        System.out.println("===");
        evenNumbers.stream().forEach(System.out::println);

        // 使用count统计元素个数
        long count = numbers.stream()
                .count();
        System.out.println("===");
        System.out.println(count);

        // 使用max找到最大元素
        int max = numbers.stream()
                .max(Integer::compare)
                .orElse(0);
        System.out.println("===");
        System.out.println(max);

        // 使用min找到最小元素
        int min = numbers.stream()
                .min(Integer::compare)
                .orElse(0);
        System.out.println("===");
        System.out.println(min);
    }

    @Test
    public void test01() {
        List<Integer> numbers = Arrays.asList(5, 2, 2, 1, 1);

        // 使用filter过滤偶数
        // select * from numbers where number % 2 = 1
        Stream<Integer> evenNumbers = numbers.stream()
                .filter(number -> number % 2 == 1);

//        evenNumbers.forEach(x -> System.out.println(x));
//        System.out.println("===");
        evenNumbers.forEach(x -> System.out.println(x * x));
        System.out.println("===");

        // 使用map将数字映射为它们的平方
        Stream<Integer> squares = numbers.stream()
                .map(number -> number * number);
        squares.forEach(System.out::println);

        // 使用sorted对数字进行排序
        Stream<Integer> sortedNumbers = numbers.stream()
                .sorted();
        System.out.println("===");
        sortedNumbers.forEach(System.out::println);

        // 使用distinct去除重复的数字
        Stream<Integer> distinctNumbers = numbers.stream()
                .distinct();
        System.out.println("===");
        distinctNumbers.forEach(System.out::println);
    }
}
