package com.xinhua;

public class Main {
    public static void main(String[] args) {
        int a = 12;
        int b = 18;

        int c = a + b;

        System.out.println(c);
        System.out.println("===");

        float f1 = 12.34f;
        float f2 = 28.37F;

        float f3 = f1 * f2;
        System.out.println(f3);

        System.out.println("===");

        System.out.println(12 / 4); // => 3
        System.out.println(1 / 3);  // => 0.3333
        System.out.println(17 / 3); // => 5.6666

        System.out.println("===");
        System.out.println(1 % 3);  // => 1
        System.out.println(17 % 3); // => 2

        System.out.println("===");
        System.out.println(12f / 4f);
        System.out.println(1f / 3f);
        System.out.println(17f / 3f);

        System.out.println("===");
        System.out.println(12d / 4d);
        System.out.println(1d / 3d);
        System.out.println(17d / 3d);

        System.out.println("===");
        System.out.println(12.0 / 4.0);
        System.out.println(1. / 3.);
        System.out.println(17. / 3.);

        System.out.println("===");
        System.out.println(17.0 / 4.0);
        System.out.println(17.0 % 4.0);

        System.out.println("===");
//        System.out.println(2 ^ 3);  // 2 * 2 * 2
        System.out.println(Math.pow(2, 3));
    }
}
