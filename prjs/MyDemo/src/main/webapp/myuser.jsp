<%@ page import="java.sql.*" %>
<%@ page import="javax.naming.*, javax.sql.*" %>
<%@ page import="javax.servlet.http.*, javax.servlet.*" %>
<%@ page import="java.io.*, java.util.*" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>JSP极简站点示例</title>
  <link rel="stylesheet" href="https://cdn.staticfile.org/layui/2.5.7/css/layui.min.css">
</head>

<body>

<table class="layui-table">
  <thead>
  <tr>
    <th>ID</th>
    <th>姓名</th>
    <th>Password</th>
  </tr>
  </thead>

  <%
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;

    try {
      // 获取数据库连接

      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      String url = "jdbc:sqlserver://127.0.0.1:1433;databaseName=xhedu";
//      String username = "sa";
//      String passwordStr = "123456";

      Properties props = new Properties();
      props.setProperty("user", "sa");
      props.setProperty("password", "123456");
      props.setProperty("encrypt", "false"); // 禁用加密
      props.setProperty("trustServerCertificate", "true"); // 信任服务器证书

      conn = DriverManager.getConnection(url, props);


//      conn = DriverManager.getConnection(url, username, passwordStr);

      stmt = conn.createStatement();

      // 执行查询语句
      rs=stmt.executeQuery("SELECT user_id, user_name, password FROM sys_user");

      while (rs.next()) {
        int id=rs.getInt(1);
        String name=rs.getString(2);
        String password=rs.getString(3);

  %>
  <!-- 显示查询结果 -->
  <tbody><tr><td><%=id%></td><td><%=name%></td><td><%=password%></td></tr>
    <%
         }
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        // 关闭数据库连接和资源
        try { if(rs!=null) rs.close(); } catch (SQLException se1) {}
        try { if(stmt!=null) stmt.close(); } catch (SQLException se2) {}
        try { if(conn!=null) conn.close(); } catch (SQLException se3) {}
    }
  %>

</table>

<script src="https://cdn.staticfile.org/layui/2.5.7/layui.min.js"></script>

</body>
</html>
