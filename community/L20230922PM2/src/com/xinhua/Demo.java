package com.xinhua;

public class Demo {
    public static void main(String[] args) {
        int i = 2;
        i += 3;     // i = i + 3
        System.out.println(i);  //  5

        i -= 3;     // i = i - 3
        System.out.println(i);  // 2

        i *= 7;     // i = i * 7
        System.out.println(i);  // 14

        i /= 3;     // i = i / 3
        System.out.println(i);  // 4

        System.out.println("===");

        double d1 = 3;
        double d2 = 7;
        d1 /= d2;
        System.out.println(d1);
    }
}
