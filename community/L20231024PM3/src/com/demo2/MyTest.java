package com.demo2;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(5, 7);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println("===");
        System.out.println(p1.getDistance(p2));
        System.out.println(p2.getDistance(p1));
    }
}
