import java.awt.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //                     1
        //            12345678901   长度
        //            01234567890   位置
        String str = "Hello World";
        int len = str.length();
        System.out.println(len);

        System.out.println(str.charAt(9));  // l

        System.out.println(str.substring(3));   // lo World
        System.out.println(str.substring(1, 8));    // ello Wo [顾头不顾尾]

        System.out.println(str.indexOf("l"));   // 2
        System.out.println(str.indexOf("l", 5));    // 9

        System.out.println(str.lastIndexOf("l"));   // 9
        System.out.println(str.lastIndexOf("l", 5));    // 3

        int a = 5;
        int b = 7;
        System.out.println(a == b);     // false
        int c = 7;
        System.out.println(b == c);     // true

        Point p1 = new Point(2, 3);
        Point p2 = new Point(2, 3);
        System.out.println(p1 == p2);   // false
        System.out.println(p1.equals(p2));  // true

        String str1 = "Hello, World!";
        String str2 = "Hello, World!";
        String str3 = "hello, world!";

        System.out.println(str1 == str2);   // true
        System.out.println(str1.equals(str2));  // true
        // 忽略大小写
        System.out.println(str1.equalsIgnoreCase(str3));    // true

        System.out.println(str1.startsWith("ll"));  // false
        System.out.println(str1.startsWith("He"));  // true

        System.out.println(str1.endsWith("ll"));  // false
        System.out.println(str1.endsWith("ld!"));  // true

        System.out.println(str1.toUpperCase()); // HELLO, WORLD!
        System.out.println(str1.toLowerCase()); // hello, world!

        String str4 = "Hello, World!";
        String str5 = "      Hello, World!   ";
        System.out.println(str4);
        System.out.println(str5);
        System.out.println(str5.trim());

        System.out.println(str4.replace('l', 'w'));

        String[] s = str4.split(",");
        System.out.println(s.toString());
        System.out.println(Arrays.toString(s));
        String[] s2 = str4.split(", ");
        System.out.println(Arrays.toString(s2));

        System.out.println(String.format("He\'s name is %s, he\'s %d years old.","Tod", 5));
    }
}