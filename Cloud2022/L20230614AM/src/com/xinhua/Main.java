package com.xinhua;

public class Main {
    public static void main(String[] args) {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(5, 7);
        Point p3 = new Point(2, 3);
        Point p4 = p2;

        String str = "Hello, World";

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p1.getDistance(p2));
        System.out.println(p2.getDistance(p1));

        System.out.println(p1 == p3);   // false
        System.out.println(p2 == p4);   // true
        System.out.println(p2.equals(p4));  // true
        System.out.println(p1.equals(str));  // false
    }
}
