package me.ereach.l20231219am3spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L20231219Am3SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(L20231219Am3SpringApplication.class, args);
    }

}
