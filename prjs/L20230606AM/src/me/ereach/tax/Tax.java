package me.ereach.tax;

public class Tax implements ITax {
    @Override
    public float getTax(float income) {
        if (income <= 5000)
            return 0;
        else
            return (income - 5000) * 0.1f;
    }
}
