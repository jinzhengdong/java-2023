package com.xinhua.l20231222pm2springmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L20231222Pm2SpringMvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(L20231222Pm2SpringMvcApplication.class, args);
    }

}
