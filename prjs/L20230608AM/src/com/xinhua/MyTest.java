package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        Cube c1 = new Cube(7);
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getVolume());
    }

    @Test
    public void test01() {
        Square s1 = new Square(7);
        System.out.println(s1);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
    }
}
