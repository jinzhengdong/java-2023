package me.ereach.sms20230818.controller;

import me.ereach.sms20230818.mapper.SubjectMapper;
import me.ereach.sms20230818.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CtrlSubject {
    @Autowired
    private SubjectMapper subjectMapper;

    @GetMapping("/subject")
    public List<Subject> getSubject() {
        return subjectMapper.selectList(null);
    }

    @PostMapping("/subject")
    public String insertSubject(
            @RequestParam("subjectName") String subjectName
    ) {
        Subject subject = new Subject();
        subject.setSubjectName(subjectName);

        int retvar = subjectMapper.insert(subject);

        if (retvar > 0)
            return "insert operation is OK.";
        else
            return "insert operation is failed.";
    }

    @PostMapping("/subjectjson")
    public String insertSubject(@RequestBody Map<String, String> payload) {
        Subject subject = new Subject();
        subject.setSubjectName(payload.get("subjectName"));

        int retvar = subjectMapper.insert(subject);

        if (retvar > 0)
            return "insert operation is OK.";
        else
            return "insert operation is failed.";
    }

}
