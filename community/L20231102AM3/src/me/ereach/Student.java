package me.ereach;

public class Student {
    private String name;
    private String gender;
    private String birthDate;
    private String mbphone;

    public Student(String name, String gender, String birthDate, String mbphone) {
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
        this.mbphone = mbphone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMbphone() {
        return mbphone;
    }

    public void setMbphone(String mbphone) {
        this.mbphone = mbphone;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", mbphone='" + mbphone + '\'' +
                '}';
    }
}
