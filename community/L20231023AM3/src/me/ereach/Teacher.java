package me.ereach;

public class Teacher extends Person {
    private String school;
    private String major;

    public Teacher(String name, int age, String school, String major) {
        super(name, age);
        this.school = school;
        this.major = major;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "school='" + school + '\'' +
                ", major='" + major + '\'' +
                '}';
    }
}
