package com.xinhua;

public class Lesson1 {
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            if (i == 3)
                break;

            System.out.println("计数器：" + i);
        }
        System.out.println("完成！");

        for (int i = 1; i <= 5; i++) {
            if (i == 3) {
                System.out.println("continue");
                continue;
            }

            System.out.println("计数器：" + i);
        }
        System.out.println("完成");

    }
}
