package com.ynxinhua;

public class Student extends Person {
    private String className;

    public Student(String name, String gender, String className) {
        super(name, gender);
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "Student{" +
                "person=" + super.toString() + ", " +
                "className='" + className + '\'' +
                '}';
    }
}
