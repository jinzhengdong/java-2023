```sql
select * from city; -- 城市
select * from country; -- 国家
select * from countrylanguage; -- 国家语言

select				-- 选择
	id,				-- 标识符
    name,			-- 国家名称
    CountryCode,	-- 国家代码
    District,		-- 行政区
    Population		-- 人口
from				-- 来自
	city;			-- 城市表
	
select
	code, -- 国家代码
    name, -- 国家名称
    Continent -- 大陆
from
	country -- 国家表
where -- 在那里
	Continent = 'Europe' -- 大陆 = 欧洲
	
select
	code, -- 国家代码
    name, -- 国家名称
    Continent -- 大陆
from
	country -- 国家表
where -- 在那里
	Continent = 'Asia' -- 大陆 = 亚洲 Asia
	
select
	code as 国家代码,
    name as 国家名称,
    Continent as 大陆,
    Region as 区域,
    SurfaceArea as 表面积,
    Population as 人口,
    LifeExpectancy as 预期寿命,
    Capital as 首都
from
	country -- 国家表
where -- 在那里
	name = 'China'

select
	code as 国家代码,
    name as 国家名称,
    Continent as 大陆,
    Region as 区域,
    SurfaceArea as 表面积,
    Population as 人口,
    LifeExpectancy as 预期寿命,
    Capital as 首都
from
	country -- 国家表
where -- 在那里
	LifeExpectancy >= 80

select
	code as 国家代码,
    name as 国家名称,
    Continent as 大陆,
    Region as 区域,
    SurfaceArea as 表面积,
    Population as 人口,
    LifeExpectancy as 预期寿命,
    Capital as 首都
from
	country -- 国家表
where -- 在那里
	SurfaceArea >= 9500000

-- 请把国家换为日本 Japan
-- 请把预期寿命大于 80 岁的国家找出来
-- 表面积大于950万平方公里

select
	code as 国家代码,
    name as 国家名称,
    Continent as 大陆,
    Region as 区域,
    SurfaceArea as 表面积,
    Population as 人口,
    LifeExpectancy as 预期寿命,
    Capital as 首都
from
	country -- 国家表
where -- 在那里
	SurfaceArea >= 9500000

-- 请把国家换为日本 Japan
-- 请把预期寿命大于 80 岁的国家找出来
-- 表面积大于950万平方公里
-- 区域 = 'Asia'



```

