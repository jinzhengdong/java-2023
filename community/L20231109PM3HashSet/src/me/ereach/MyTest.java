package me.ereach;

import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

public class MyTest {
    @Test
    public void test01() {
        Set<Circle> s1 = new LinkedHashSet<>();

        s1.add(new Circle(5));
        s1.add(new Circle(7));

        s1.stream().forEach(c -> System.out.println(c + ", " + c.getArea()));

        System.out.println("===");

        Set<Square> sq = new LinkedHashSet<>();
        sq.add(new Square(5));
        sq.add(new Square(7));

        sq.stream().forEach(y -> System.out.println(y + ", " + y.getPerimeter()));
    }
}
