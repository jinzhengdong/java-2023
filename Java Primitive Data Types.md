Java的基本数据类型包括以下8种：

1. `byte`：1个字节，取值范围为-128到127，用于表示整数。
2. `short`：2个字节，取值范围为-32768到32767，用于表示整数。
3. `int`：4个字节，取值范围为-2147483648到2147483647，用于表示整数。
4. `long`：8个字节，取值范围为-9223372036854775808到9223372036854775807，用于表示整数。
5. `float`：4个字节，取值范围为1.4E-45到3.4028235E38，用于表示浮点数。
6. `double`：8个字节，取值范围为4.9E-324到1.7976931348623157E308，用于表示浮点数。
7. `char`：2个字节，取值范围为0到65535，用于表示字符。
8. `boolean`：1个字节，取值范围为true或false，用于表示布尔值。

其中，`byte`、`short`、`int`、`long`、`float`、`double`、`char`都是基本数据类型，而`boolean`是Java中的逻辑类型，只能表示`true`或`false`。在Java中，基本数据类型都有对应的包装类，用来处理基本数据类型的变量。例如，`int`对应的包装类为`Integer`，`float`对应的包装类为`Float`，以此类推。

在早期版本的Java中，`char`是一个字节，取值范围为0到255。但是，随着Java的发展，`char`的取值范围扩展到了0到65535，因此现在的Java中，`char`占用的内存空间已经变成了2个字节，而不是一个字节。

需要注意的是，在某些特殊情况下，`char`的长度可能会受到影响。例如，在使用Java虚拟机的某些特定平台上，`char`可能会被压缩为一个字节，这是为了节省内存空间。但是，这种情况不是Java的标准行为，也不是Java开发中应该依赖的特性。因此，在编写Java程序时，应该始终将`char`视为占用2个字节的数据类型。

Java基本数据类型运算特例：

在Java中，字符型`char`的值可以转换为整型`int`的值，因此在下面的代码中，'4'被转换为整数52（ASCII码）：

```java
byte x3 = '4';
char x4 = '4';
```

在Java中，当两个不同类型的数据进行运算时，它们会自动转换为同一类型的数据，转换的规则是：将两个数据类型中较小的数据类型自动转换为较大的数据类型。在下面的代码中，`x3`的类型是`byte`，`x4`的类型是`char`。`byte`的取值范围为-128到127，而char的取值范围为0到65535，因此`byte`是较小的数据类型，它会被自动转换为较大的数据类型`int`。因此，下面的代码等价于`System.out.println((int)x3 + (int)x4);`。

```java
System.out.println(x3 + x4);
```

将'4'转换为整数52后，x3的值为52，x4的值也为52，因此x3 + x4的值为104。最后，System.out.println(x3 + x4);语句输出104。

Java获取ASCII码：

可以通过Java中的强制类型转换将字符转换为对应的ASCII码。具体的做法是将字符类型(char)转换为整数类型(int)，例如：

```java
char c = 'a';
int ascii = (int) c;
System.out.println("ASCII码为：" + ascii);
```

上面的代码中，将字符'a'转换为整数类型(int)，并将结果赋值给变量ascii。最后，使用System.out.println()方法输出结果。输出结果为：ASCII码为：97。

需要注意的是，在Java中，字符类型(char)是一个16位的Unicode字符，而不是一个8位的ASCII码。因此，如果需要获取一个字符的ASCII码，需要将其强制转换为整数类型(int)。
