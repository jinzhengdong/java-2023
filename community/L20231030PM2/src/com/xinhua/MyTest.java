package com.xinhua;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class MyTest {

    @Test
    public void test07() {
        try {
            FileReader reader = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        finally {
            System.out.println("finished.");
        }
    }

    @Test
    public void test06() {
        try {
            var reader = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        finally {
            System.out.println("finished.");
        }
    }

    @Test
    public void test05() {
        try {
            var reader = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void test04() throws FileNotFoundException {
        var reader = new FileReader("d:\\readme.txt");
        System.out.println(reader);
    }

    @Test
    public void test03() {
        // f 表示浮点数
        System.out.println(5f / 3);

        System.out.println(5 / 3f);

        // d 表示双精度浮点数
        System.out.println(5d / 3);

        System.out.println(5 / 3d);

        System.out.println(7d / 3d);
    }

    @Test
    public void test02() {
        int[] intArr = {1, 2, 3, 4, 5};

        for (int i = 0; i < intArr.length; i++)
            System.out.println(intArr[i]);
    }

    @Test
    public void test01() {
        int[] intArr = new int[5];

        intArr[0] = 1;
        intArr[1] = 2;
        intArr[2] = 3;
        intArr[3] = 4;
        intArr[4] = 5;

        for (int i = 0; i < intArr.length; i++)
            System.out.println(intArr[i]);
    }
}
