package com.LEL;

public class Sjx {
    private Point a;
    private Point b;
    private Point c;

    public Sjx(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        this.a = a;
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        this.b = b;
    }

    public Point getC() {
        return c;
    }

    public void setC(Point c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "Sjx{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

    public double s(){
        return a.jl(b) + a.jl(c) + b.jl(c);
    }

    public double l(){
        double n = a.jl(b);
        double m = a.jl(c);
        double z = b.jl(c);

        double p = s() / 2;

        return  Math.sqrt(p*(p-n)*(p-m)*(p-z));
    }
}
