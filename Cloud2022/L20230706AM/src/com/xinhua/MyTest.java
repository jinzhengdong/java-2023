package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        Animal animal1 = new Dog();
        Animal animal2 = new Cat();

        animal1.makeSount();
        animal2.makeSount();

        System.out.println("===");

        System.out.println(animal1 instanceof Dog);
        System.out.println(animal1 instanceof Cat);
        System.out.println(animal1 instanceof Animal);
    }

    @Test
    public void test02() {
        Dog dog = new Dog();
        dog.bark();
        dog.eat();
    }
    @Test
    public void test01() {
        Person p1 = new Person();
        p1.displayInfo();
        p1.setName("Jerry");
        p1.setAge(10);
        p1.displayInfo();
        String str = p1.toString();
        System.out.println(str);
        System.out.println(p1.getName());
        System.out.println(p1.getAge());

        System.out.println("===");

        Person p2 = new Person("Tom", 8);
        System.out.println(p2);
    }
}
