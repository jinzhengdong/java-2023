package me.ereach;

public interface IGeometry {
    double getArea();
    double getPerimeter();
}
