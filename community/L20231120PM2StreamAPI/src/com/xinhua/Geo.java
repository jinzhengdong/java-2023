package com.xinhua;

public interface Geo {
    // 计算面积
    double getArea();

    // 计算周长
    double getPerimeter();
}
