package me.ereach.l005p01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L005P01Application {

    public static void main(String[] args) {
        SpringApplication.run(L005P01Application.class, args);
    }

}
