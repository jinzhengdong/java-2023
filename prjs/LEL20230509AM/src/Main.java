import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("请输入一个正整数：");
            String str = scanner.next();
            str = str.toLowerCase();
            if (str.equals("exit"))
                break;
            long num = Long.parseLong(str);
            System.out.println(zhishu(num));
        }
        System.out.println("完成！");
    }
    public static String zhishu(long num){

        if (num == 2){
            return num + "是质数";}

        boolean flag = true;
        if (num % 2 == 0)
            return num + "不是质数";


        for (int i = 2; i < num/2; i++) {
            if (num % i == 0){
                flag = false;
                break;
            }
        }
        if (flag){
            return num + "是质数";
        }else
            return num + "不是质数";
    }

}