package me.ereach;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class MyTest {
    @Test
    public void test02() {
        System.out.println("---collection compare---");
        Collection<String> first = new ArrayList<>();
        Collections.addAll(first, "a", "b", "c");

        Collection<String> second = new ArrayList<>();
        Collections.addAll(second, "a", "b", "c");

        System.out.println(first == second);        // false
        System.out.println(first.equals(second));   // true

    }

    @Test
    public void test01() {
        GenericList<String> list = new GenericList<>();

        list.add("Hello");
        list.add("World");

        for (int i = 0; i < list.getCount(); i++)
            System.out.println(list.get(i));

        System.out.println("===");

        for (var item : list)
            System.out.println(item);

        System.out.println("===");

        var iterator = list.iterator();

        while (iterator.hasNext()) {
            var current = iterator.next();
            System.out.println(current);
        }
    }
}
