package com.demo;

import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        Person s1 = new Student("Tom", 12, "xinhua");
        Person t1 = new Teacher("Jerry", 18, "Java");

        System.out.println(s1);
        System.out.println(s1.sayHello());
        System.out.println("===");
        System.out.println(t1);
        System.out.println(t1.sayHello());
    }

    @Test
    public void test01() {
        Student s1 = new Student("Tom", 12, "xinhua");
        Teacher t1 = new Teacher("Jerry", 18, "Java");

        System.out.println(s1);
        System.out.println(s1.sayHello());
        System.out.println("===");
        System.out.println(t1);
        System.out.println(t1.sayHello());
    }
}
