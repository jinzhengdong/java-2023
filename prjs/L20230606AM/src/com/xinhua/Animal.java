package com.xinhua;

abstract class Animal {
    abstract void sound();

    public void eating() {
        System.out.println("I'm eating.");
    }
}
