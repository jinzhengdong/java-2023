package me.ereach.demo202312211;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo202312211Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo202312211Application.class, args);
    }

}
