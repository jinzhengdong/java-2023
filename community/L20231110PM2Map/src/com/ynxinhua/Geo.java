package com.ynxinhua;

public interface Geo {
    double getArea();
    double getPerimeter();
}
