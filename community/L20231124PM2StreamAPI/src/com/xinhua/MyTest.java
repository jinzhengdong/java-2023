package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyTest {

    List<Class> classList = Arrays.asList(
            new Class(1, "202213", "2022(2+1)云计算班", 2022, 2025, 1),
            new Class(2, "202108", "2021(2+2)云计算班", 2021, 2025, 1),
            new Class(3, "202301", "2022计算机应用1班", 2023, 2026, 1),
            new Class(4, "202302", "2022计算机应用2班", 2023, 2026, 1)
    );

    List<Student> studentList = new ArrayList<>(Arrays.asList(
            new Student(1, "Zhou ShuangYang", "male", "2004-05-16", 1, 1, 2022, "13888000000"),
            new Student(2, "Yu Yu", "male", "2005-01-16", 1, 1, 2022, "13888000001"),
            new Student(3, "Chen RongLai", "male", "2007-01-04", 1, 1, 2022, "13888000002")
    ));

    @Test
    public void test02() {
        Student student = new Student();

        student.setId(4);
        student.setName("Wan HaiChuan");
        student.setGender("male");
        student.setBirthDate("2006-12-31");
        student.setEmployeeId(1);
        student.setClassId(1);
        student.setEnterYear(2022);
        student.setMobilePhone("13888000004");

        System.out.println(student);
        System.out.println("===");
        studentList.add(student);
        studentList.stream().forEach(System.out::println);
    }

    @Test
    public void test01() {
        System.out.println("Total classes: " + classList.stream().count());
        classList.stream().forEach(System.out::println);
        classList.stream().mapToInt(Class::getEnterYear).forEach(System.out::println);
        System.out.println("Total 2023: " + classList.stream().filter(x -> x.getEnterYear() == 2023).count());
    }

}
