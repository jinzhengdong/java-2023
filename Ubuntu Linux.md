# Ubuntu

Ubuntu Linux 是一种流行的开源操作系统，它基于 Debian Linux 发行版，具有易用性、安全性和稳定性。本教程将介绍 Ubuntu Linux 的使用和维护，以帮助学生更好地使用 Ubuntu 系统。

一、Ubuntu Linux 的安装

1. 下载 Ubuntu Linux

首先，需要从 Ubuntu 官网下载 Ubuntu Linux 的 ISO 镜像文件。可以选择适合自己的版本，如 18.04 LTS 或 20.04 LTS。

2. 制作启动盘

将下载的 ISO 镜像文件写入 USB 启动盘或 DVD 光盘。可以使用 Rufus 或 Etcher 等工具来制作启动盘。

3. 安装 Ubuntu Linux

将启动盘插入计算机，重启计算机并进入 BIOS 设置，将启动顺序设置为 USB 或 DVD。然后选择“安装 Ubuntu”选项，按照提示进行安装。

二、Ubuntu Linux 的基本使用

1. 桌面环境

Ubuntu Linux 默认使用 GNOME 桌面环境，它提供了一个现代化的用户界面和易于使用的应用程序。

2. 应用程序

Ubuntu Linux 提供了许多应用程序，如 Firefox 网络浏览器、LibreOffice 办公套件、Rhythmbox 音乐播放器等。可以通过 Ubuntu 软件中心或命令行安装其他应用程序。

3. 终端

Ubuntu Linux 提供了一个强大的终端，可以使用命令行来执行各种任务。例如，可以使用 apt-get 命令来安装软件包，使用 ls 命令来列出目录中的文件等。

4. 文件管理器

Ubuntu Linux 的文件管理器称为 Nautilus，它提供了一个直观的界面来管理文件和文件夹。可以使用它来复制、移动、删除和重命名文件。

5. 系统设置

Ubuntu Linux 提供了一个系统设置应用程序，可以使用它来更改各种系统设置，如网络、声音、显示器、用户账户等。

三、Ubuntu Linux 的维护

1. 更新系统

Ubuntu Linux 提供了一个软件更新应用程序，可以使用它来更新系统和安装新的软件包。可以使用以下命令在终端中更新系统：

sudo apt-get update
sudo apt-get upgrade

2. 安装防病毒软件

虽然 Linux 系统相对较安全，但仍然需要安装防病毒软件来保护系统。可以使用 ClamAV 或 Sophos 等防病毒软件来保护系统。

3. 备份数据

备份数据是非常重要的，可以使用 Ubuntu Linux 提供的备份应用程序来备份数据。可以将备份保存在外部硬盘、云存储或其他存储设备中。

4. 清理系统

Ubuntu Linux 提供了一个系统清理应用程序，可以使用它来清理系统中的临时文件、缓存和其他不需要的文件。可以使用以下命令在终端中清理系统：

sudo apt-get autoclean
sudo apt-get autoremove

总结

本教程介绍了 Ubuntu Linux 的安装、基本使用和维护。学生可以使用这些知识来更好地使用 Ubuntu 系统，并保护系统的安全和稳定性。

# 附录

## ssh

SSH（Secure Shell）是一种加密的网络协议，用于在不安全的网络中安全地传输数据。在 Linux 系统中，SSH 可以用于远程登录和执行命令。本文将介绍如何使用 SSH 远程登录 Linux 系统，并提供可能遇到的问题及处理办法。

### 远程登录 Linux 系统

#### 步骤一：安装 SSH 服务

在 Linux 系统中，SSH 服务通常已经安装好了。如果没有安装，可以使用以下命令安装：

```
sudo apt-get install openssh-server
```

#### 步骤二：启动 SSH 服务

使用以下命令启动 SSH 服务：

```
sudo service ssh start
```

#### 步骤三：远程登录

使用以下命令远程登录 Linux 系统：

```
ssh username@ip_address
```

其中，`username` 是要登录的用户名，`ip_address` 是要登录的 Linux 系统的 IP 地址。如果要登录本地系统，可以使用 `localhost` 或 `127.0.0.1` 作为 IP 地址。

#### 步骤四：输入密码

输入要登录的用户的密码，然后按 Enter 键。

#### 步骤五：执行命令

成功登录后，可以在远程终端中执行命令。

### 可能遇到的问题及处理办法

#### 问题一：连接被拒绝

如果在远程登录时出现连接被拒绝的错误，可能是因为 SSH 服务没有启动或防火墙阻止了连接。可以使用以下命令检查 SSH 服务是否已启动：

```
sudo service ssh status
```

如果 SSH 服务没有启动，可以使用以下命令启动 SSH 服务：

```
sudo service ssh start
```

如果防火墙阻止了连接，可以使用以下命令打开 SSH 端口：

```
sudo ufw allow ssh
```

#### 问题二：密码错误

如果在远程登录时出现密码错误的错误，可能是因为输入的密码不正确。请确保输入的密码与要登录的用户的密码相同。

#### 问题三：连接超时

如果在远程登录时出现连接超时的错误，可能是因为网络连接不稳定或 SSH 服务响应缓慢。可以尝试重新连接或等待一段时间后再次尝试连接。

#### 问题四：远程登录

```bash
ssh ubuntu@<ip address>
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ED25519 key sent by the remote host is
SHA256:FPWt/THdc+WqQ+oYiajcoENm8RB1b1O9DTuy31h1peY.
Please contact your system administrator.
Add correct host key in C:\\Users\\jinzd/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in C:\\Users\\jinzd/.ssh/known_hosts:12
Host key for <ip address> has changed and you have requested strict checking.
Host key verification failed.
```

```bash
ssh-keygen -R <ip address>
```