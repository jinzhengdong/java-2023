package me.ereach.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import me.ereach.sms.model.Class;

@Repository
public interface ClassMapper extends BaseMapper<Class> {
}