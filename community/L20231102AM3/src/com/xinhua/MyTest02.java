package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MyTest02 {
    @Test
    public void test03() {
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(7);
//        list.add("hello");
        System.out.println(list);

        List<String> slist = new ArrayList<>();
        slist.add("Hello");
        slist.add("world");
        System.out.println(slist);
    }

    @Test
    public void test02() {
        Integer[] items = new Integer[5];

        items[0] = 12;
        items[1] = 17;
//        items[2] = "hello";

        System.out.println(items);

        for (int i = 0; i < items.length; i++)
            System.out.println(items[i]);
    }

    @Test
    public void test01() {
        Object[] items = new Object[5];

        items[0] = "Hello";
        items[1] = 12;
        items[2] = new java.awt.Point(3, 5);

        System.out.println(items);

        for (int i = 0; i < items.length; i++)
            System.out.println(items[i]);
    }
}
