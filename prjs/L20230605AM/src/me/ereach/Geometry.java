package me.ereach;

public interface Geometry {
    public double getArea();
    public double getPerimeter();
}
