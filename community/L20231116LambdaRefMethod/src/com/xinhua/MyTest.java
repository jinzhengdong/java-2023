package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MyTest {
    @Test
    public void test05() {
        List<Person> list = Arrays.asList(
                new Person("Jerry", "male"),
                new Student("Tom", "male", "2022 Cloud"),
                new Student("Tod", "male", "2021 Cloud"),
                new Teacher("Wendy", "female", "Web Page"),
                new Student("Cherry", "female", "2022 Cloud")
        );

        list.forEach(System.out::println);
        System.out.println("===");
        list.stream().filter(x -> x.getName().startsWith("T"))
                .forEach(System.out::println);
        System.out.println("===");
        List<Person> list2 = list.stream().filter(x-> x.getGender() == "female")
                .collect(Collectors.toList());

        System.out.println(list2);
        System.out.println("===");
        list2.forEach(System.out::println);
    }

    @Test
    public void test04() {
        List<Geo> list = Arrays.asList(
                new Circle(5),
                new Circle(7),
                new Square(3),
                new Square(11)
        );

        list.forEach(System.out::println);
    }

    @Test
    public void test03() {
        List<String> list = Arrays.asList("Apple", "Banana", "Orange", "Pear");
        list.forEach(System.out::println);

        System.out.println("===");

        list.stream()
                .filter(x -> x.startsWith("A"))
                .forEach(System.out::println);
    }

    @Test
    public void test02() {
        List<Integer> list1 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        list1.forEach(System.out::println);

        System.out.println("===");

        List<Integer> list2 = list1.stream()
                .filter(x -> x % 2 == 0)
                .collect(Collectors.toList());

        list2.forEach(System.out::println);

        System.out.println("===");

        List<Integer> list3 = list1.stream()
                .map(x -> x * x)
                .collect(Collectors.toList());

        list3.forEach(System.out::println);
    }

    @Test
    public void test01() {
        List<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Banana");
        list.add("Cherry");

        list.forEach(x -> System.out.println(x));

        System.out.println("===");

        list.forEach(System.out::println);
    }
}
