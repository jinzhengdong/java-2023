package com.xinhua;

public class Lesson3 {
    public static void main(String[] args) {
        int sum = 0;
        int count = 1;

        while (count <= 100) {
            sum += count;
            count += 2;
        }

        System.out.println(sum);

        sum = 0;
        count = 0;

        while (count <= 100) {
            sum += count;
            count += 2;
        }

        System.out.println(sum);
    }
}
