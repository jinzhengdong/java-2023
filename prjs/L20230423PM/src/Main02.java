public class Main02 {
    public static void main(String[] args) {
        // 从命令行接受一个整数
        int num = Integer.parseInt(args[0]);
        // 判断是否为偶数
        if (num % 2 == 0) {
            System.out.println(num + " 是偶数.");
        } else {
            System.out.println(num + " 不是偶数.");
        }
    }
}
