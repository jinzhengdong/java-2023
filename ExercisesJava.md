# Java语言基础练习

### 温度及汇率

* 温度、汇率转换：
  * 摄氏温度 = (f - 32) / 1.8f
  * 华氏温度 = (c * 1.8f) + 32
  * 人民币和美元之间的兑换率为6.5

```java
package me.ereach.exercises;

import org.junit.Test;
import java.util.Scanner;

public class Exercises {
    public static void main(String[] args) {
        // 扫描控制台输入
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input 23C 89F $100 ¥100: ");
        String instr = scanner.next();

        // 摄氏温度输入
        if (instr.toLowerCase().endsWith("c")) {
            float c = Float.parseFloat(instr.substring(0, instr.length() - 1));
            float f = c * 1.8f + 32;
            System.out.println(f + "F");
        }
        // 华氏温度
        else if (instr.toLowerCase().endsWith("f")) {
            float f = Float.parseFloat(instr.substring(0, instr.length() - 1));
            float c = (f - 32) / 1.8f;
            System.out.println(c + "C");
        }
        else if (instr.startsWith("$")) {
            float usd = Float.parseFloat(instr.substring(1));
            float rmb = usd * 6.5f;
            System.out.println("¥" + rmb);
        }
        else if (instr.startsWith("¥")) {
            float rmb = Float.parseFloat(instr.substring(1));
            float usd = rmb / 6.5f;
            System.out.println("$" + usd);
        }
        else
            System.out.println("Input error!");
    }
}
```

* 继续上面的练习，把整个转换过程添加到循环中，并且：
  * 键入"quit"退出操作
  * 键入"pass"返回循环

```java
package me.ereach.exercises;

import org.junit.Test;

public class Exercises {
    public static void main(String[] args) {
        while (true) {
            // 扫描控制台输入
            Scanner scanner = new Scanner(System.in);
            System.out.print("Input 23C 89F $100 ¥100: ");
            String instr = scanner.next();

            if (instr.toLowerCase().trim().equals("quit"))
                break;
            else if (instr.toLowerCase().trim().equals("pass"))
                continue;
            // 摄氏温度输入
            else if (instr.toLowerCase().endsWith("c")) {
                float c = Float.parseFloat(instr.substring(0, instr.length() - 1));
                float f = c * 1.8f + 32;
                System.out.println(f + "F");
            }
            // 华氏温度
            else if (instr.toLowerCase().endsWith("f")) {
                float f = Float.parseFloat(instr.substring(0, instr.length() - 1));
                float c = (f - 32) / 1.8f;
                System.out.println(c + "C");
            } else if (instr.startsWith("$")) {
                float usd = Float.parseFloat(instr.substring(1));
                float rmb = usd * 6.5f;
                System.out.println("¥" + rmb);
            } else if (instr.startsWith("¥")) {
                float rmb = Float.parseFloat(instr.substring(1));
                float usd = rmb / 6.5f;
                System.out.println("$" + usd);
            } else
                System.out.println("Input error!");
        }
    }

    @Test
    public void consolePrint99() {
        for (int i = 1; i < 10; i++)
            for (int j = 1; j < i + 1; j++) {
                
            }
    }
}
```

### 九九乘法表

* 输出如下形式的99表

```
1*1=1	
2*1=2	2*2=4	
3*1=3	3*2=6	3*3=9	
4*1=4	4*2=8	4*3=12	4*4=16	
5*1=5	5*2=10	5*3=15	5*4=20	5*5=25	
6*1=6	6*2=12	6*3=18	6*4=24	6*5=30	6*6=36	
7*1=7	7*2=14	7*3=21	7*4=28	7*5=35	7*6=42	7*7=49	
8*1=8	8*2=16	8*3=24	8*4=32	8*5=40	8*6=48	8*7=56	8*8=64	
9*1=9	9*2=18	9*3=27	9*4=36	9*5=45	9*6=54	9*7=63	9*8=72	9*9=81
```

代码：

```java
package me.ereach.exercises;

import org.junit.Test;

public class Exercises {
    @Test
    public void consolePrint99() {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < i + 1; j++) {
                System.out.print(i + "*" + j + "=" + i * j + "\t");
            }
            System.out.println();
        }
    }
}
```

### 水仙花数

* 找出三位数的水仙花数
  * 例如：153 = 1^3 + 5^3 + 3^3

```java
package me.ereach.exercises;

import org.junit.Test;

public class Exercises {
    @Test
    public void getFlowers3() {
        for (int i = 100; i <= 999; i++) {
            int n1 = i / 10 / 10 % 10;
            int n2 = i / 10 % 10;
            int n3 = i % 10;

            if (i == (int)(Math.pow(n1, 3) + Math.pow(n2, 3) + Math.pow(n3, 3)))
                System.out.println(i);
        }
    }
}
```

### 找出100以内的质数

```java
package me.ereach.exercises;

import org.junit.Test;
import java.util.Scanner;

public class Exercises {
    // 质数
    @Test
    public void getPrime() {
        for (int i = 2; i <= 100; i++)
            if (checkPrime(i))
                System.out.println(i);
    }

    private boolean checkPrime(int num) {
        boolean primeFlag = true;

        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                primeFlag = false;
                break;
            }
        }

        return primeFlag;
    }
}
```

### 斐波那契

给定两个种子数，按斐波那契生成规则计算找出第n项是多少？n > 2

```java
package me.ereach.exercises;

import org.junit.Test;
import java.util.Scanner;

public class Exercises {
    // 斐波那契
    @Test
    public void getFib() {
        System.out.println(fib(1, 2, 10));
    }

    public long fib(long a, long b, long n) {
        long total = 0;

        if (n <= 2)
            throw new IllegalArgumentException("数据项数必须大于2");

        for (int i = 3; i <= n; i++) {
            total = a + b;
            a = b;
            b = total;
        }

        return total;
    }
}
```

### 圆周率近似公式

圆周率近似公式为：pi = 4 * (1 - 1/3 + 1/5 - 1/7 + 1/9 - ...)

```java
package me.ereach.exercises;

import org.junit.Test;

public class Exercises {
  // 圆周率
  @Test
  public void calculatePi() {
    System.out.println(getPi(100000));
  }

  public double getPi(long n) {
    int sn = 0;
    double pi = 0;

    for (long i = 1; i <= n; i += 2) {
      sn++;
      if (sn % 2 == 0)
        pi -= 1 / (double)i;
      else
        pi += 1 / (double)i;
    }

    return pi * 4;
  }
}
```

### 分解质因数一

```java
package me.ereach.exercises;

import org.junit.Test;

public class Exercises {
  // 分解质因数一
  @Test
  public void fact1() {
    System.out.println(getFact1(126));
  }

  public String getFact1(int n) {
    String str = n + "=";

    int i = 2;

    while (i <= n) {
      if (n % i == 0) {
        str += i + "*";
        n = n / i;
      }
      else {
        i++;
      }
    }

    return str.substring(0, str.length()-1);
  }
}
```

### 分解质因数二

```java
package me.ereach.exercises;

import org.junit.Test;

public class Exercises {
  // 分解质因数二
  @Test
  public void fact1() {
    System.out.println(getFact2(126));
  }

  public static String getFact2(int n) {
    StringBuilder sb = new StringBuilder(n + "=");

    int i = 2;

    while (i <= n) {
      if (n % i == 0) {
        sb.append(i + "*");
        n = n / i;
      }
      else {
        i++;
      }
    }

    return sb.toString().substring(0, sb.toString().length() - 1);
  }
}
```
