public class Main02 {
    public static void main(String[] args) {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(2, 3);
        Point p3 = p1;

        System.out.println(p1 == p2);   // false
        System.out.println(p1);
        System.out.println(p2);
        System.out.println("=====");
        System.out.println(p1 == p3);   // true
        System.out.println(p1);
        System.out.println(p3);


    }
}
