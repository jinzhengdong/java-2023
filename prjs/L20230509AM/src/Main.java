import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("请输入一个正整数：");
            String str = scanner.next();

            // toLowerCase() 把大写转换为小写 Exit => exit
            str = str.toLowerCase();

            if (str.equals("exit")) {
                System.out.println(str);
                break;
            }

            // parse 解析, parseInt 把输入的字符串转换为一个整数。
            long num = Long.parseLong(str);

            System.out.println(checkPrime(num));
        }

        System.out.println("完成.");
    }

    public static String checkPrime(long num) {
        boolean flag = true;

        if (num % 2 == 0)
            return num + " 不是质数.";

        for (long i = 2; i < num / 2; i++) {
            if (num % i == 0) {
                flag = false;
                break;
            }
        }

        if (flag)
            return num + " 是质数.";
        else
            return num + " 不是质数.";
    }
}