package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Dog dog = new Dog();
        dog.eating();
        dog.sound();
        dog.walk();

        Cat cat = new Cat();
        cat.eating();
        cat.sound();
        cat.walk();
    }
}
