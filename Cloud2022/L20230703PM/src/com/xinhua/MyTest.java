package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        Emp emp = new Emp("Jerry", "male", 5000, 20, 30);
        System.out.println(emp);
        System.out.println(emp.getTotalSalary());

        Tax tax = new Tax();
        System.out.println(tax.getTax(emp.getTotalSalary()));
    }

    @Test
    public void test02() {
        Square s1 = new Square(5);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        System.out.println(s1.sayHello("Square"));
    }

    @Test
    public void test01() {
        Circle c1 = new Circle(7);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.sayHello("Circle"));
    }
}
