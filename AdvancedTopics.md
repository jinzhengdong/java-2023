# 高级主题

在开始学校课程之前，你应该了解以下知识：

* 类(Classes)
* 接口(Interfaces)
* 面向对象编程(Object-Oriented Programming)
* 继承(Inheritance)
* 多态(Polymorphism)

本课程的内容包括：

* 异常(Exceptions)
* 泛型(Generics)
* 集合(Collections)
* 拉姆达表达式和函数式接口(Lambda Expressions & Functional Interfaces)
* 流(Streams)
* 多线程(Multi-Threading)
* 异步编程

## 异常

### 简介

这一节我们将介绍：

* Exceptions
* Handling Exceptions
* Types of Exceptions
* Custom Exceptions
* Chaining Exceptions

### 什么是异常

* 打开我们之前创建的项目，在“me.ereach”包下创建新的包“exceptions”
* 在"exceptions"包下添加类"ExceptionsDemo"类，并对代码进行如下更新：

```java
package me.ereach.exceptions;

public class ExceptionsDemo {
    public static void show() {
        sayHello(null);
    }

    public static void sayHello(String name) {
        System.out.println(name.toUpperCase());
    }
}
```

* 返回项目Main类并对入口函数做如下修改：

```java
package me.ereach;

import me.ereach.exceptions.ExceptionsDemo;

public class Main {

    public static void main(String[] args) {
        ExceptionsDemo.show();
    }
}
```

* 运行该入口函数，我们将看到如下异常信息，显示“java.lang.NullPointerException”：

```script
"C:\Program Files\Java\jdk-17.0.1\bin\java.exe" "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA 2021.3.2\lib\idea_rt.jar=55550:C:\Program Files\JetBrains\IntelliJ IDEA 2021.3.2\bin" -Dfile.encoding=UTF-8 -classpath D:\xgitee\java2021\prjs\AdvancedCode\out\production\AdvancedCode me.ereach.Main
Exception in thread "main" java.lang.NullPointerException: Cannot invoke "String.toUpperCase()" because "name" is null
 at me.ereach.exceptions.ExceptionsDemo.sayHello(ExceptionsDemo.java:11)
 at me.ereach.exceptions.ExceptionsDemo.show(ExceptionsDemo.java:7)
 at me.ereach.Main.main(Main.java:8)

Process finished with exit code 1
```

如下图，点击相关的连接可以跟进到相关的异常代码：

<img src="./images/img046.png" width="500px" />

### 异常类型

在Java中，我们有三种异常类型：

* Checked Exception，必须在代码钟由开发人员进行处理，否则编译器不会通过
* Unchecked (Runtime) Exception
* Error

下面我们将针对这类异常进行分别描述和处理：

* Checked Exception，看下面类ExceptionsDemo：

```java
package me.ereach.exceptions;

import java.io.FileReader;

public class ExceptionsDemo {
    public static void show() {
        var reader = new FileReader("file.txt");
    }
}
```

* 上面的例子在IDEA开发环境钟显示为下图，它是必须在代码钟进行处理的异常（编译器强制要求开发人员进行处理，否则编译不会通过）：

<img src="./images/img047.png" />

* 针对上述异常的处理可以用下面类似代码完成：

```java
package me.ereach.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExceptionsDemo {
    public static void show() throws FileNotFoundException {
        var reader = new FileReader("file.txt");
    }
}
```

* Unchecked Exception在Java中也叫做Runtime Exception，根据其字面意思，此类错误不在编译期间由编译器检查，下面代码引发的错误就属于此类情况，要避免此类错误可能需要大量测试或自动测试：

```java
package me.ereach.exceptions;

public class ExceptionsDemo {
    public static void show() {
        sayHello(null);
    }

    public static void sayHello(String name) {
        System.out.println(name.toUpperCase());
    }
}
```

```java
package me.ereach;

import me.ereach.exceptions.ExceptionsDemo;

public class Main {

    public static void main(String[] args) {
        ExceptionsDemo.show();
    }
}
```

上面Runtime Exceptions如果再细分可以分为：

1. NullPointerException，空指针异常操作，相对比较难调试
2. ArithmeticException，运算异常操作，例如被零除
3. IllegalArgumentException，非法参数异常，传递不合法或不正确的参数发生
4. IndexOutOfBoundsException，索引超出边界异常，例如访问数组下标时的异常
5. IllegalStateException，非法状态异常

* Error Exception，这类异常通常指堆栈溢出（stack overflow）或内存溢出（out of memory）

### 异常层次结构

Java异常的层次结构可用下图描述：

<img src="./images/img048.png" />

* 上图中，Throwable是Error和Exception的超类；
* 上图中，Exception如果是checked则由开发人员在代码中直径处理否则不能编译；
* Error通常指堆栈溢出或内存溢出相对也容易处理；
* RuntimeException（Unchecked Exception）则最不容易处理。

下面我们看看Calss NullPointerException

* java.lang.Object
  * java.lang.Throwable
    * java.lang.RuntimeException
      * [java.lang.NullPointerException](https://docs.oracle.com/javase/7/docs/api/java/lang/RuntimeException.html)

点击上面链接可以看懂相关的子类

### 捕捉异常

* 创建Java项目Demo
* 在src下创建me.ereach.advanced.exceptions包，并在该包下创建ExceptionDemo类，并添加如下代码：

```java
package me.ereach.advanced.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            System.out.println("file opened");
        }
        catch (FileNotFoundException ex) {
            System.out.println("file does not exist.");
        }
    }
}
```

* 运行该代码我们将不再看到程序崩溃的情况。
* 我们也可以直接使用FileNotFoundException的实例输出异常信息，如下：

```java
package me.ereach.advanced.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            System.out.println("file opened");
        }
        catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
```

* 上例中，移除try-catch后，光标停留于FileReader红线处，按下Alt-Enter可以添加try-catch代码块
* 运行上面代码可以看到下面的异常信息：

```script
java.io.FileNotFoundException: file.txt (系统找不到指定的文件。)
 at java.base/java.io.FileInputStream.open0(Native Method)
 at java.base/java.io.FileInputStream.open(FileInputStream.java:216)
 at java.base/java.io.FileInputStream.<init>(FileInputStream.java:157)
 at java.base/java.io.FileInputStream.<init>(FileInputStream.java:111)
 at java.base/java.io.FileReader.<init>(FileReader.java:60)
 at me.ereach.advanced.exceptions.ExceptionDemo.main(ExceptionDemo.java:9)
```

### 捕捉多种类型的异常

* 继续修改```ExceptionDemo```类为下面代码捕获多个错误：

```java
package me.ereach.advanced.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            System.out.println("could not read file.");
        }
    }
}
```

* 上例中如果我们移动IOException到FileNotFoundException上面，如下：

```java
package me.ereach.advanced.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (IOException e) {
            System.out.println("could not read file.");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```

* 上面修改我们可以看到集成开发环境中显示如下错误，原因是```FileNotFoundException extends IOException```：

<img src="./images/img049.png" />

* 这种情况，我们仍应恢复之前的代码顺序，如下：

```java
package me.ereach.advanced.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            System.out.println("could not read file.");
        }
    }
}
```

* 再次修改上面代码如下：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
            new SimpleDateFormat().parse("");
        }
        catch (IOException e) {
            System.out.println("could not read file.");
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
```

* 上面代码我们可以进一步简化为：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
            new SimpleDateFormat().parse("");
        }
        catch (IOException | ParseException e) {
            System.out.println(e.getMessage());
        }
    }
}
```

### finally代码块

* 回到上面的代码，添加如下修改，文件完成修改后我们将关闭该文件：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            var reader = new FileReader("file.txt");
            var value = reader.read();
            reader.close();
        }
        catch (IOException e) {
            System.out.println("Could not read data.");
        }
    }
}
```

* 上面代码的问题是：如果读文件时发生错误将导致```reader.close()```不会被执行，解决的办法就是使用finally代码块

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            FileReader reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (IOException e) {
            System.out.println("Could not read data.");
        }
        finally {
            reader.close();
        }

    }
}
```

* 上面代码的finally代码块中reader是报错的，因为reader看不到，解决办法如下：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        FileReader reader;

        try {
            reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (IOException e) {
            System.out.println("Could not read data.");
        }
        finally {
            reader.close();
        }

    }
}
```

* 完成上面修改后我们仍然看到如下错误信息，提示reader没有被初始化：

<img src="./images/img050.png" />

* 要解决上面问题可以按下面代码修改：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        FileReader reader = null;

        try {
            reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (IOException e) {
            System.out.println("Could not read data.");
        }
        finally {
            reader.close();
        }
    }
}
```

* 上面代码仍然提示下图的错误信息：

<img src="./images/img051.png" />

* 针对上图情况我们可以按下面进一步修改代码，虽然还是有点丑但它是我们目前修改的最终版本：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        FileReader reader = null;

        try {
            reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (IOException e) {
            System.out.println("Could not read data.");
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
```

### try-with-resources语句

* 上一级我们完成了try-catch-finally但整个代码看起来有些丑，这一节我们将做一些修改，首先我们移除finally代码块：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        FileReader reader = null;

        try {
            reader = new FileReader("file.txt");
            var value = reader.read();
        }
        catch (IOException e) {
            System.out.println("Could not read data.");
        }
    }
}
```

* 基于上面代码我们添加相关的资源语句如下，该修改将自动完成相关资源的关闭：

```java
package me.ereach.advanced.exceptions;

import java.io.FileReader;
import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        try (var reader = new FileReader("file.txt")) {
            var value = reader.read();
        }
        catch (IOException e) {
            System.out.println("Could not read data.");
        }
    }
}
```

* 针对上面的代码修改，请参考FileReader有关[AutoCloseable](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/io/FileReader.html)部分

### 抛出异常

* 这一节我们学习异常的抛出，首先我们添加一个类"Account"，如下：

```java
package me.ereach.advanced.exceptions;

public class Account {
    public void deposit(float value) {
        // 防御性编程 defensive programming
        if (value <= 0)
            throw new IllegalArgumentException();
    }
}
```

* 返回到类ExceptionDemo并做如下修改：

```java
package me.ereach.advanced.exceptions;

public class ExceptionDemo {
    public static void main(String[] args) {
        var account = new Account();
        account.deposit(-1);
    }
}
```

* 上面代码当存款为-1时运行该类我们看到如下结果：

```script
Exception in thread "main" java.lang.IllegalArgumentException
 at me.ereach.advanced.exceptions.Account.deposit(Account.java:6)
 at me.ereach.advanced.exceptions.ExceptionDemo.main(ExceptionDemo.java:6)

Process finished with exit code 1
```

* 上面的例子显示了一个RunTimeException（UncheckedException）的情况，类似的防御性编程我们常常用于工具库的开发。
* 针对上面Account类，我们尝试做如下修改，让它抛出IOException：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class Account {
    public void deposit(float value) {
        if (value <= 0)
            throw new IOException();
    }
}
```

* 完成上面的修改后我们看到如下错误信息：

<img src="./images/img052.png" />

* 针对上面的错误信息，如果我们执意要抛出IOException异常则可采用下面的方式：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class Account {
    public void deposit(float value) throws IOException {
        if (value <= 0)
            throw new IOException();
    }
}
```

* 完成上面代码修改后我们返回主类看到如下错误信息：

<img src="./images/img053.png" />

* 对于上面情况可以按下面代码修改：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        var account = new Account();
        try {
            account.deposit(-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

### 重新抛出异常

* 继续上面一节的例子，我们返回到ExceptionDemo类，并进行如下修改：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        var account = new Account();
        try {
            account.deposit(-1);
        } catch (IOException e) {
            System.out.println("Logging");
        }
    }
}
```

* 运行上面代码我们看到如下信息，相关的异常信息将不再抛出：

```script
Logging

Process finished with exit code 0
```

* 为了解决上述问题我们可以用下面代码重新抛出异常信息：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) throws IOException {
        var account = new Account();
        try {
            account.deposit(-1);
        } catch (IOException e) {
            System.out.println("Logging");
            throw e;
        }
    }
}
```

* 在进一步修改ExceptionDemo类为如下d代码：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class ExceptionDemo {
    public static void show() throws IOException {
        var account = new Account();
        try {
            account.deposit(-1);
        } catch (IOException e) {
            System.out.println("Logging");
            throw e;
        }
    }
}
```

* 在包下添加主类Main并更新为如下代码：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            ExceptionDemo.show();
        } catch (Throwable e) {
            System.out.println("An unexpected error occurred");
        }
    }
}
```

* 运行该主类我们看到如下输出：

```script
Logging
An unexpected error occurred

Process finished with exit code 0
```

### 自定义异常

* 返回到Account类，并做如下更新：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class Account {
    private float balance;

    public void deposit(float value) throws IOException {
        if (value <= 0)
            throw new IOException();

        balance += value;
    }

    public void withdraw(float value) {
        if (value > balance)
            throw new IllegalArgumentException();

        balance -= value;
    }
}
```

* 在exceptions包下添加一个新类InsufficientFundsException，如下：

```java
package me.ereach.advanced.exceptions;

// Checked => Exception
// Unchecked (runtime) => RuntimeException

public class InsufficientFundsException extends Exception {
    public InsufficientFundsException() {
        super("Insufficient funds in your account.");
    }

    public InsufficientFundsException(String message) {
        super(message);
    }
}
```

* 返回Account代码，再次更新其代码如下：

```java
package me.ereach.advanced.exceptions;

import java.io.IOException;

public class Account {
    private float balance;

    public void deposit(float value) throws IOException {
        if (value <= 0)
            throw new IOException();

        balance += value;
    }

    public void withdraw(float value) throws InsufficientFundsException {
        if (value > balance)
            throw new InsufficientFundsException();

        balance -= value;
    }
}
```

* 返回ExceptionDemo类，并做如下修改：

```java
package me.ereach.advanced.exceptions;

public class ExceptionDemo {
    public static void show() {
        var account = new Account();
        try {
            account.withdraw(10);
        } catch (InsufficientFundsException e) {
            System.out.println(e.getMessage());
        }
    }
}
```

* 返回主类运行看到如下结果：

```script
Insufficient funds in your account.

Process finished with exit code 0
```

### 链式异常

* 首先返回Account类，并恢复其代码如下：

```java
package me.ereach.advanced.exceptions;

public class Account {
    private float balance;

    public void deposit(float value) {
        if (value <= 0)
            throw new IllegalArgumentException();

        balance += value;
    }

    public void withdraw(float value) throws InsufficientFundsException {
        if (value > balance)
            throw new InsufficientFundsException();

        balance -= value;
    }
}
```

* 在exceptions包下添加一个类AccountException如下：

```java
package me.ereach.advanced.exceptions;

public class AccountException extends Exception {

}
```

* 返回Account类并做如下修改：

```java
package me.ereach.advanced.exceptions;

public class Account {
    private float balance;

    public void deposit(float value) {
        if (value <= 0)
            throw new IllegalArgumentException();

        balance += value;
    }

    public void withdraw(float value) throws AccountException {
        if (value > balance) {
            var fundsException = new InsufficientFundsException();
            var accountException = new AccountException();
            accountException.initCause(fundsException);
            throw accountException;
        }
        
        balance -= value;
    }
}
```

* 返回AccountException类并做如下修改：

```java
package me.ereach.advanced.exceptions;

public class AccountException extends Exception {
    public AccountException(Exception cause) {
        super(cause);
    }
}
```

* 再次返回Account类我们发现如下错误信息：

<img src="./images/img054.png" />

* 根据信息，针对Account类我们做如下修改即可：

```java
package me.ereach.advanced.exceptions;

public class Account {
    private float balance;

    public void deposit(float value) {
        if (value <= 0)
            throw new IllegalArgumentException();

        balance += value;
    }

    public void withdraw(float value) throws AccountException {
        if (value > balance)
            throw new AccountException(new InsufficientFundsException());

        balance -= value;
    }
}
```

* 返回ExceptionDemo类，我们可看到如下代码，并看到withdraw方法报错：

```
package me.ereach.advanced.exceptions;

public class ExceptionDemo {
    public static void show() {
        var account = new Account();
        try {
            account.withdraw(10);
        } catch (InsufficientFundsException e) {
            System.out.println(e.getMessage());
        }
    }
}
```

* 报错信息如下图示：

<img src="./images/img055.png" />

* 根据提示我们修改ExceptionsDemo类为如下代码：

```java
package me.ereach.advanced.exceptions;

public class ExceptionDemo {
    public static void show() {
        var account = new Account();
        try {
            account.withdraw(10);
        } catch (AccountException e) {
            e.printStackTrace();
        }
    }
}
```

* 返回Main类，如下：

```java
package me.ereach.advanced.exceptions;

public class Main {
    public static void main(String[] args) {
        try {
            ExceptionDemo.show();
        } catch (Throwable e) {
            var cause = e.getCause();
            System.out.println(cause.getMessage());
        }
    }
}
```

* 运行Main我们可以看到如下提示：

```script
me.ereach.advanced.exceptions.AccountException: me.ereach.advanced.exceptions.InsufficientFundsException: Insufficient funds in your account.
 at me.ereach.advanced.exceptions.Account.withdraw(Account.java:15)
 at me.ereach.advanced.exceptions.ExceptionDemo.show(ExceptionDemo.java:7)
 at me.ereach.advanced.exceptions.Main.main(Main.java:6)
Caused by: me.ereach.advanced.exceptions.InsufficientFundsException: Insufficient funds in your account.
 ... 3 more

Process finished with exit code 0
```

* 再次返回ExceptionDemo类并做如下修改：

```java
package me.ereach.advanced.exceptions;

public class ExceptionDemo {
    public static void show() {
        var account = new Account();
        try {
            account.withdraw(10);
        } catch (AccountException e) {
            var cause = e.getCause();
            System.out.println(cause.getMessage());
        }
    }
}
```

* 再次运行Main类入口函数可看到如下结果:

```java
Insufficient funds in your account.

Process finished with exit code 0
```

### 总结

* Exceptions (checked, runtime, error)
* Try/catch blocks
* The throw keyword
* Custom execptions
* Chaining exceptions

## 泛型

### 简介

泛型（generics）是Java的JDK 5中引入的一个重要概念，它提供了编译时类型安全检测机制，它运行程序员在编译时发现非法的类型，它的本质时参数化类型，即数据类型被指定为参数。

### 泛型的需求

* 在开始前我们先添加一个包"me.ereach.generics"到项目中
* 完成后我们继续在该包中添加一个List类，并更新其代码如下：

```java
package me.ereach.generics;

import java.util.Arrays;

public class List {
    private int[] items = new int[10];
    private int count;

    public void add(int item) {
        items[count++] = item;
    }

    public int get(int index) {
        return items[index];
    }

    @Override
    public String toString() {
        return Arrays.toString(items);
    }
}
```

* 继续在该包下添加一个Main类如下：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        var list = new List();
        list.add(1);
        list.add(2);
        System.out.println(list);
    }
}
```

* 运行该Main类可以看到如下输出：

```script
[1, 2, 3, 0, 0, 0, 0, 0, 0, 0]

Process finished with exit code 0
```

* 完成后我们继续在generics包下添加一个类User如下：

```java
package me.ereach.generics;

public class User {
    private String name;
    private String gender;
    private int age;

    public User() {
        this.name = "";
        this.gender = "male";
        this.age = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User { name: '" + name + "', gender: '" + gender + "', age: " + age + " }";
    }
}
```

* 完成后我们将添加一个类UserList以存放User对象的列表：

```java
package me.ereach.generics;

public class UserList {
    private User[] items = new User[10];
    private int count;

    public void add(User user) {
        items[count++] = user;
    }

    public User get(int index) {
        return items[index];
    }

    public int getCount() {
        return count;
    }
}
```

* 返回Main并修改其代码如下：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        var userList = new UserList();

        User user1 = new User();
        user1.setName("Tom");
        user1.setAge(10);
        userList.add(user1);

        User user2 = new User();
        user2.setName("Jerry");
        user2.setAge(6);
        userList.add(user2);

        for (int i = 0; i < userList.getCount(); i++)
            System.out.println(i + " " + userList.get(i));
    }
}
```

* 运行上面例子我们看到如下结果：

```script
0 User { name: 'Tom', gender: 'male', age: 10}
1 User { name: 'Jerry', gender: 'male', age: 6}

Process finished with exit code 0
```

* 为了让两个例子更相似，可如下修改User类，增加一个带参数的构造函数：

```java
package me.ereach.generics;

public class User {
    private String name;
    private String gender;
    private int age;

    public User() {
        this.name = "";
        this.gender = "male";
        this.age = 0;
    }

    public User(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User { name: '" + name + "', gender: '" + gender + "', age: " + age + " }";
    }
}
```

* 返回Main类进行如下修改，运行该主类可看到相同的结果：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        var userList = new UserList();

        User user1 = new User("Tom", "male", 10);
        userList.add(user1);

        User user2 = new User("Jerry", "male", 6);
        userList.add(user2);

        for (int i = 0; i < userList.getCount(); i++)
            System.out.println(i + " " + userList.get(i));
    }
}
```

上面的两个例子在形式和操作方式上是极其相似的，我们希望以相似的形式来实现对多种不同类型对象的操作，这就是泛型的需求。

### 一个不好的解决方案

* 继续上一节，对于List类我们做如下修改：

```java
package me.ereach.generics;

public class List {
    private Object[] items = new Object[10];
    private int count;

    public void add(Object item) {
        items[count++] = item;
    }

    public Object get(int index) {
        return items[index];
    }

    public int getCount() {
        return count;
    }
}
```

* 返回该包下的Main类，我们进行如下修改：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        var list = new List();

        list.add(1);
        list.add("abc");
        list.add(new User("Tom", "male", 10));

        for (int i = 0; i < list.getCount(); i++)
            System.out.println(i + " " +list.get(i));
    }
}
```

* 运行上面Main类我们看到下面结果：

```script
0 1
1 abc
2 User { name: 'Tom', gender: 'male', age: 10 }

Process finished with exit code 0
```

* 上面例子运行似乎运行得不错，似乎解决了我们提出的问题，但是列表中每个元素的类型可能是完全不同的，这可能使后续处理带来麻烦，看下面例子，这也是我们需要泛型类的原因。

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        var list = new List();

        list.add(Integer.valueOf(1));
        list.add("abc");
        list.add(new User("Tom", "male", 10));

        for (int i = 0; i < list.getCount(); i++)
            System.out.println(list.get(i).getClass());
    }
}
```

* 运行上面代码看到如下结果：

```script
class java.lang.Integer
class java.lang.String
class me.ereach.generics.User

Process finished with exit code 0
```

### 泛型类

* 继续上节，我们在generics包下添加一个新类GenericList如下：

```java
package me.ereach.generics;

import java.util.Arrays;

public class GenericList<T> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }

    @Override
    public String toString() {
        return Arrays.toString(items);
    }
}
```

* 上面例子中```<T>```用于对GenericList类的类型描述：
  * ```<T>```通常表示类型
  * ```<E>```通常表示元素
* 返回Main类并做如下更新：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        GenericList<String> list1 = new GenericList<>();

        list1.add("a");
        list1.add("b");

        System.out.println(list1);

        GenericList<Integer> list2 = new GenericList<>();

        list2.add(1);
        list2.add(2);

        System.out.println(list2);
    }
}
```

* 运行上面Main类看到如下输出结果：

```script
[a, b, null, null, null, null, null, null, null, null]
[1, 2, null, null, null, null, null, null, null, null]

Process finished with exit code 0
```

* 再次修改Main类为如下代码：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        GenericList<User> list = new GenericList<>();
        list.add(new User("Tom", "male", 10));
        list.add(new User("Jerry", "male", 9));
        System.out.println(list);
    }
}
```

* 运行看到如下输出：

```script
[
    User { name: 'Tom', gender: 'male', age: 10 }, 
    User { name: 'Jerry', gender: 'male', age: 9 }, 
    null, null, null, null, null, null, null, null
]

Process finished with exit code 0
```

### 泛型和原生类型

在Java中每一个原生数据类型都对应一个包装类型：

* ```int => Integer```
* ```float => Float```
* ```boolean => Boolean```

在泛型中我们使用包装类型而非原生数据类型，看下面例子：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        GenericList<Integer> numbers = new GenericList<>();
        numbers.add(1);                 // Boxing, 打包
        int number = numbers.get(0);    // Unboxing, 解包
    }
}
```

### 约束

* 在某些情况下，泛型类针对某些情况特别有意义，而其他一些情况这无意义的，对于这种情况我们通常会使用约束条件，例如下面针对GenericList类的代码修改，其中的泛型继承了Number类：

```java
package me.ereach.generics;

import java.util.Arrays;

public class GenericListConstraint<T extends Number> {
    private T[] items = (T[])new Number[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return Arrays.toString(items);
    }
}
```

* 关于上面Number类我们可以访问下面地址：
  * [java.lang.Number](https://docs.oracle.com/javase/8/docs/api/java/lang/Number.html)并参看直接子类（Direct Known Subclasses）一节
* 完成上面的约束条件后，返回Main类并修改入口方法为下面内容：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        var intList = new IntList();
        intList.add(1);
        intList.add(2);
        System.out.println(intList);

        var userList = new UserList();
        userList.add(new User("Tom", "male", 10));
        userList.add(new User("Jerry", "male", 9));
        System.out.println(userList);

        var objList = new ObjList();
        objList.add(1);
        objList.add("abc");
        objList.add(new User("Tom", "male", 10));
        System.out.println(objList);

        var genericList1 = new GenericList<Integer>();
        genericList1.add(1);
        genericList1.add(2);
        System.out.println(genericList1);

        var genericList2 = new GenericList<String>();
        genericList2.add("abc");
        genericList2.add("cde");
        System.out.println(genericList2);

        var genericList3 = new GenericList<User>();
        genericList3.add(new User("Tom", "male", 10));
        genericList3.add(new User("Jerry", "male", 12));
        System.out.println(genericList3);

        var gList4 = new GenericListConstraint<Integer>();
        gList4.add(1);
        gList4.add(2);
        System.out.println(gList4);

        var user1 = new User("Tom", "male", 10);
        var user2 = new User("Tom", "male", 12);
        System.out.println(user1.compareTo(user2));
    }
}
```

* 针对上面情况，如果鼠标移动到String上可以看到如下提示信息(IDEA)，提示String的泛型用法是错误的：

<img src="./images/img056.png" />

* 针对Number的约束条件下面的用法都是正确的：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        new GenericList<Number>();
        new GenericList<Float>();
        new GenericList<Double>();
        new GenericList<Short>();
    }
}
```

* 针对泛型的另外一种用法是Java中非常流行的可比较接口```Comparable```，我们使用此接口并非实现实例对象之间的相互比较，而是实现可比较的约束，如下：

```java
package me.ereach.advanced.generics;

public class GenericList<T extends Comparable> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }
}
```

* 完成上面修改后我们返回Main类并做如下修改：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        new GenericList<Float>();
        new GenericList<Double>();
        new GenericList<Short>();
        new GenericList<User>();
    }
}
```

* 上面的例子我们看到```new GenericList<User>();```提示错误信息，如下图，提示User没有实现Comparable接口：

<img src="./images/img057.png" />

* 返回User类我们加入下面代码：

```java
package me.ereach.advanced.generics;

public class User implements Comparable {
    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
```

* 完成上面修改后，返回Main类原有错误被消除
* 作为约束条件的另外一种用法就是泛型可以继承多个接口，如下：

```java
package me.ereach.advanced.generics;

public class GenericList<T extends Comparable & Cloneable> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }
}
```

* 这种情况，返回User类并实现Cloneable接口，如下：

```java
package me.ereach.advanced.generics;

public class User implements Comparable, Cloneable {
    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
```

* 返回Main类，修改代码如下，相关错误信息被消除：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        new GenericList<User>();
    }
}
```

### 类型擦除(Type Erasure)

这一节我们将学习泛型中的类型擦除概念：

* 返回GenericList类移除相关的约束并恢复其原始代码如下：

```java
package me.ereach.advanced.generics;

public class GenericList<T> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }
}
```

* 返回User类，并恢复其代码为如下空类：

```java
package me.ereach.advanced.generics;

public class User {
}
```

* 返回Main类并恢复其入口函数为如下：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {

    }
}
```

* 从IDEA的工具菜单选择```Build >> Build Project```
* IDEA左侧的Project面板，选择GenericList类
* 从IDEA的工具菜单选择```View >> Show Bytecode```，可以看到如下图示的字节码，它示平台独立的编译代码，Java虚拟机用字节码生成相关硬件平台的本地代码：

<img src="./images/img058.png" />

* 从字节码可看到item的类型是Object类型的，再次修改GenericList类的代码如下：

```java
package me.ereach.advanced.generics;

public class GenericList<T extends Number> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }
}
```

* 再次构建项目并查看字节码发现item的类型是Number

结论：

* 没有指定约束条件时都是Object
* Java编译器依赖泛型的约束条件来编译泛型的相关类型
* 如果泛型T继承了可比较接口，其类型也会是可比较接口
* 如果泛型T继承了俩个可比较接口，则最左侧的那个类型将被用作字节码的类型，如下例中的Comparable:

```java
package me.ereach.advanced.generics;

public class GenericList<T extends Comparable & Cloneable> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }
}
```

### 可比较接口

本节我们介绍可比较接口Comparable:

* 返回User类我们做如下代码更新：

```java
package me.ereach.generics;

public class User implements Comparable<User> {
    private String name;
    private String gender;
    private Integer age;

    public User(String name, String gender, Integer age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public int compareTo(User o) {
        return age - o.age;
    }

    @Override
    public String toString() {
        return "User { name: " + name + ", gender: " + gender + ", age: " + age + " }";
    }
}
```

* 上面的方法还不够简单，我们继续修改为如下代码：

```java
package me.ereach.generics;

public class User implements Comparable<User>{
    private int points;

    public User(int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public int compareTo(User other) {
        return points - other.points;
    }
}
```

* 返回Main类修改其代码如下：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        var user1 = new User(10);
        var user2 = new User(20);

        int rst = user1.compareTo(user2);

        if (rst < 0)
            System.out.println("user1 < user2");
        else if (rst == 0)
            System.out.println("user1 == user2");
        else
            System.out.println("user1 > user2");
    }
}
```

### 泛型方法

* 返回GenericList类，下面的add和get方法相应的泛型方法

```java
package me.ereach.generics;

public class GenericList<T> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }
}
```

* 在项目中添加Utils类，并在其中实现如下代码：

```java
package me.ereach.generics;

public class Utils {
    public static int max(int first, int second) {
        return (first > second) ? first : second;
    }
}
```

* 返回到Main并做如下修改：

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        System.out.println(Utils.max(4, 7));
    }
}
```

* 上面实现的方法只能比较整数，有很大的局限性，通过泛型我们可以极大的提升代码的适用范围，返回Utils类

```java
package me.ereach.generics;

public class Utils {
    public static <T extends Comparable<T>> T max(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }
}
```

* 更新User如下：

```java
package me.ereach.generics;

public class User implements Comparable<User>{
    private int points;

    public User(int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public int compareTo(User other) {
        return points - other.points;
    }

    @Override
    public String toString() {
        return "Points = " + points;
    }
}
```

* 返回到Main类并做如下修改

```java
package me.ereach.generics;

public class Main {
    public static void main(String[] args) {
        System.out.println(Utils.max(4, 7));
        System.out.println(Utils.max(4.7F, 7.8F));
        System.out.println(Utils.max("Hello", "Max"));
        System.out.println(Utils.max(new User(10), new User(20)));
    }
}
```

* 运行Main类的入口程序看到如下结果：

```script
7
7.8
Max
Points = 20

Process finished with exit code 0
```

### 多类型参数（泛型参数）

泛型支持多类型的参数，参看下面例子：

* 返回Utils类并做如下代码更新：

```java
package me.ereach.advanced.generics;

public class Utils {
    public static <T extends Comparable<T>> T max(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }

    public static void print(int key, int value) {
        System.out.println(key + "=" + value);
    }
}
```

* 上面的例子，静态方法print的参数支持int key和int value，但很多时候key可能需要支持String类型，此中情况我们就可能需要泛型的表示方法，如下：

```java
package me.ereach.advanced.generics;

public class Utils {
    public static <T extends Comparable<T>> T max(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }

    public static <T> void print(T key, int value) {
        System.out.println(key + "=" + value);
    }
}
```

* 通过上面的泛型多类型支持，使得key参数支持多类型，但是value参数仍然只支持int类型，如果value也需要支持多类型，则我们需要如下更新：

```java
package me.ereach.advanced.generics;

public class Utils {
    public static <T extends Comparable<T>> T max(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }

    public static <K, V> void print(K key, V value) {
        System.out.println(key + "=" + value);
    }
}
```

* 完成上面修改后我们返回Main类的main函数进行如下测试:

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        Utils.print(1, "jinzd");
        Utils.print("name", "Tom");
        Utils.print("gender", true);
    }
}
```

* 运行上面代码我们可以看到如下输出结果：

```script
1=jinzd
name=Tom
gender=true

Process finished with exit code 0
```

* 继续我们的泛型实验，添加一个类KeyValuePair并更新其代码如下：

```java
package me.ereach.advanced.generics;

public class KeyValuePair<K, V> {
    private K key;
    private V value;

    public KeyValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
```

* 完成后，返回Main类对KeyValuePair进行如下测试：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        KeyValuePair<String, Float> kv1 = new KeyValuePair<>("age", 25.6F);
        System.out.println(kv1);

        KeyValuePair<Boolean, String> kv2 = new KeyValuePair<>(true, "Jinzd");
        System.out.println(kv2);
    }
}
```

* 运行上面代码则输出下面结果：

```script
Key = age, Value = 25.6
Key = true, Value = Jinzd

Process finished with exit code 0
```

### 泛型类的继承

本小节我们讨论泛型类的继承：

* 添加一个指导员类Instructor，它继承User类如下，并且它必须实现父类的构造，如下：

```java
package me.ereach.advanced.generics;

public class Instructor extends User {
    public Instructor(int points) {
        super(points);
    }
}
```

* 为方便起见，我们把User类代码列出如下：

```java
package me.ereach.advanced.generics;

public class User implements Comparable<User>{
    private int points;

    public User(int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public int compareTo(User other) {
        return points - other.points;
    }
}
```

* 返回Main主类并更新main方法如下：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        User user = new Instructor(10);
    }
}
```

* 再次返回Utils类并更新其内容为如下：

```java
package me.ereach.advanced.generics;

public class Utils {
    public static <T extends Comparable<T>> T max(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }

    public static <K, V> void print(K key, V value) {
        System.out.println(key + "=" + value);
    }

    public static void printUser(User user) {
        System.out.println(user);
    }
}
```

* 再次返回Main类，并作如下更新：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        User user = new Instructor(10);
        Utils.printUser(new Instructor(10));
    }
}
```

* 再次返回Utils类并作如下修改：

```java
package me.ereach.advanced.generics;

public class Utils {
    public static <T extends Comparable<T>> T max(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }

    public static <K, V> void print(K key, V value) {
        System.out.println(key + "=" + value);
    }

    public static void printUser(User user) {
        System.out.println(user);
    }

    public static void printUsers(GenericList<User> users) {

    }
}
```

* 回到Main类并更新如下：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        var users = new GenericList<User>();
        Utils.printUsers(users);
    }
}
```

### 通配符

* 为演示通配符，请返回Utils类并做下面修改(printUsers)：

```java
package me.ereach.advanced.generics;

public class Utils {
    public static <T extends Comparable<T>> T max(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }

    public static <K, V> void print(K key, V value) {
        System.out.println(key + "=" + value);
    }

    public static void printUser(User user) {
        System.out.println(user);
    }

    public static void printUsers(GenericList<? extends User> users) {
        User x = users.get(0);
    }
}
```

* 完成上面修改后返回Main主类并完成下面修改：

```java
package me.ereach.advanced.generics;

public class Main {
    public static void main(String[] args) {
        var users = new GenericList<User>();
        Utils.printUsers(users);
        Utils.printUsers(new GenericList<User>());
        Utils.printUsers(new GenericList<Instructor>());
    }
}
```

### 总结

泛型在Java中非常常见，这一节我们学习了以下几个部分：

* 泛型类及泛型方法（Generic classes/methods）
* 类型擦除（Type erasure）
* 约束条件，泛型继承（Constraints, extends）
* 通配符（Wildcards）

## 集合

### 简介

这一节我们讨论集合框架，它是Java的一个标准库。

### 集合框架概要

* 关于集合框架请参看下图：

<img src="./images/img059.png" />

* 上图中绿框表示接口，蓝框是类并实现了上面的绿色接口
  * Iterable是可迭代接口，support for-each-loop
  * Collection集合接口继承了Iterable接口
  * List, Queue, Set实现了Collection接口并加入了自己特有的方法
  * ArrayList 和 LinkedList分别实现了List接口但也有自己的特有实现方法
  * PriorityQueue则实现了Queue
  * HashSet则实现了Set

### 可迭代的需求

* 演示本案例之前，我们先返回"me.ereach.generics"并修改"GenericList"为如下代码：

```java
package me.ereach.generics;

public class GenericList<T> {
    public T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }
}
```

* 返回项目，创建包"me.ereach.collections"，该包下创建Main类并添加入口函数main并做如下改动：

```java
package me.ereach.set;

import me.ereach.generics.GenericList;

public class Main {
    public static void main(String[] args) {
        var list = new GenericList<String>();

        list.add("a");
        list.add("b");

        for (var item : list.items)
            System.out.println(item);
    }
}
```

* 上面代码是有问题的，它不支持迭代因此foreach部分会报错误，我们将在下面一节处理这些问题

### 可迭代接口

* 返回“me.ereach.generics”下的“GenericList”类，我们对它进行如下修改：

```java
package me.ereach.generics;

import java.util.Iterator;

public class GenericList<T> implements Iterable<T> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
```

* 返回Main类并做如下修改：

```java
package me.ereach.collections;

import me.ereach.generics.GenericList;

public class Main {
    public static void main(String[] args) {
        var list = new GenericList<String>();

        list.add("a");
        list.add("b");

        var iterator = list.iterator();

        while (iterator.hasNext()) {
            var current = iterator.next();
            System.out.println(current);
        }
    }
}
```

* 或者也可以把代码修改为如下：

```java
package me.ereach.collections;

import me.ereach.generics.GenericList;

public class Main {
    public static void main(String[] args) {
        var list = new GenericList<String>();

        list.add("a");
        list.add("b");

        for (String item : list)
            System.out.println(item);
    }
}
```

* 返回GenericList类，并如下修改：

```java
package me.ereach.generics;

import java.util.Iterator;

public class GenericList<T> implements Iterable<T> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator(this);
    }

    private class ListIterator implements Iterator<T> {
        private GenericList<T> list;
        private int index;

        public ListIterator(GenericList<T> list) {
            this.list = list;
        }

        @Override
        public boolean hasNext() {
            return (index < list.count);
        }

        @Override
        public T next() {
            return list.items[index++];
        }
    }
}
```

* 运行主类main方法我们得到如下结果：

```script
a
b

Process finished with exit code 0
```

### 集合接口 Collection Interface

* 返回项目，创建包```me.ereach.collections```，在该包下创建类```CollectionsDemo```并更新其代码如下：

```java
package me.ereach.collections;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionsDemo {
    public static void show() {
        Collection<String> collection = new ArrayList<>();
        collection.add("a");
        collection.add("b");
        collection.add("c");

        for (var x : collection)
            System.out.println(x);

        System.out.println(collection);
    }
}
```

* 在项目包```me.ereach.collections```下创建Main并更新其代码如下：

```java
package me.ereach.collections;

public class Main {
    public static void main(String[] args) {
        CollectionsDemo.show();
    }
}
```

* 运行该入口函数我们看到如下输出：

```script
a
b
c
[a, b, c]

Process finished with exit code 0
```

* 再次返回类CollectionsDemo并完成如下修改：

```java
package me.ereach.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CollectionsDemo {
    public static void show() {
        Collection<String> collection = new ArrayList<>();

        Collections.addAll(collection, "a", "b", "c");

        for (var x : collection)
            System.out.println(x);

        System.out.println(collection);
    }
}
```

* 完成上面修改后再次返回Main类运行我们将看到如前一致的输出结果
* 继续修改CollectionsDemo的代码如下，完成相关的测试：

```java
package me.ereach.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CollectionsDemo {
    public static void show() {
        Collection<String> collection = new ArrayList<>();

        Collections.addAll(collection, "a", "b", "c");

        for (var x : collection)
            System.out.println(x);

        System.out.println("---println(collection)---");
        System.out.println(collection);

        System.out.println("---println(collection.size())---");
        System.out.println(collection.size());  // show the size of the collection

        System.out.println("---objArray -> collection.toArray()---");
        var objArray = collection.toArray();
        System.out.println(objArray);

        System.out.println("---strArray -> collection.toArray(new String[3])---");
        var strArray = collection.toArray(new String[0]);
        System.out.println(strArray);

        System.out.println("---for each iterator---");
        for (var x : strArray)
            System.out.println(x);

        System.out.println("---remove operation---");
        collection.remove("a");     // remove "a" from the collection
        System.out.println(collection);

        System.out.println("---contains operation---");
        System.out.println(collection.contains("c"));   // if collection contains "c"

        System.out.println("---clear operation---");
        collection.clear();     // clear the collection
        System.out.println(collection);

        System.out.println("---isEmpty testing---");
        System.out.println(collection.isEmpty());   // if collection is empty

        System.out.println("---collection compare---");
        Collection<String> first = new ArrayList<>();
        Collections.addAll(first, "a", "b", "c");

        Collection<String> second = new ArrayList<>();
        Collections.addAll(second, "a", "b", "c");
        System.out.println(first == second);
        System.out.println(first.equals(second));

    }
}
```

### 列表接口

List也叫顺序表，它是集合框架中的列表接口，列表可以包含重复元素，也可以存储多个空值，它从零位置开始存放元素，我们可以依次添加元素到列表，也可按索引位置添加元素到列表，参考下面的例子。

* 返回项目，并添加包```me.ereach.list```，在该包下添加类```ListDemo```并更新其为如下代码：

```java
package me.ereach.list;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {
    public static void show() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        System.out.println(list);
        list.add(0, "!");
        System.out.println(list);
    }
}
````

* 继续在```me.ereach.list```包下添加Main类，并更新为以下代码：

```java
package me.ereach.list;

public class Main {
    public static void main(String[] args) {
        ListDemo.show();
    }
}
```

* 运行该Main类看到如下结果：

```script
[a, b, c]
[!, a, b, c]

Process finished with exit code 0
```

注意：

这里，我们需要注意的是，如果我们关心列表中的索引位置，则应该用List接口，否则可以用集合接口。

* 上面ListDemo类，我们也可以用下面的方法实现：

```java
package me.ereach.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListDemo {
    public static void show() {
        List<String> list = new ArrayList<>();

        Collections.addAll(list,"a", "b", "c", "e", "b");
        list.set(0, "a+");

        System.out.println(list.get(0));
        System.out.println(list);

        list.remove(0);
        System.out.println(list);

        System.out.println(list.indexOf("b"));  // print 0
        System.out.println(list.indexOf("a+")); // print -1
        System.out.println(list.lastIndexOf("b"));  // print 3
        System.out.println(list.subList(1, 3)); // [c, e]
    }
}
```

* 运行上面的代码可看到如下输出：

```script
a+
[a+, b, c, e, b]
[b, c, e, b]
0
-1
3
[c, e]

Process finished with exit code 0
```

### 可比较接口

* 之前我们已经实现了User类的可比较接口，代码如下：

```java
package me.ereach.comparable;

public class User implements Comparable<User> {
    private String name;
    private String gender;
    private int age;

    public User(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User { name: " + name + ", gender: " + gender + ", age: " + age + " }";
    }

    @Override
    public int compareTo(User o) {
        return this.age - o.age;
    }
}
```

* 下面的例子是使用User类的可比较接口进行排序的例子：

```java
package me.ereach.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();

        users.add(new User("Tom", "male", 82));
        users.add(new User("Jerry", "male", 30));
        users.add(new User("Cindy", "female", 18));

        for (var x : users)
            System.out.println(x);

        System.out.println("===");

        Collections.sort(users);

        for (var x : users)
            System.out.println(x);
    }
}
```

* 运行上面代码可看到下面输出：

```script
User { name: Tom, gender: male, age: 82 }
User { name: Jerry, gender: male, age: 30 }
User { name: Cindy, gender: female, age: 18 }
===
User { name: Cindy, gender: female, age: 18 }
User { name: Jerry, gender: male, age: 30 }
User { name: Tom, gender: male, age: 82 }

Process finished with exit code 0
```

### 比较器接口

* 上面例子我们实现了一个简单的可比较接口，并实现了用户的比较及排序，这一节我们将介绍比较器接口Comparator，返回User类并做如下修改（增加了email和getEmail）：

```java
package me.ereach.comparable;

public class User implements Comparable<User> {
    private String name;
    private String email;
    private String gender;
    private int age;

    public String getEmail() {
        return email;
    }

    public User(String name, String email, String gender, int age) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User { name: " + name + ", email: " + email + ", gender: " + gender + ", age: " + age + " }";
    }

    @Override
    public int compareTo(User o) {
        return this.age - o.age;
    }
}
```

* 添加一个新类EmailComparator并实现如下代码：

```java
package me.ereach.collections;

import me.ereach.comparable.User;
import java.util.Comparator;

public class EmailComparator implements Comparator<User> {
    @Override
    public int compare(User o1, User o2) {
        return o1.getEmail().compareTo(o2.getEmail());
    }
}
```

* 返回Main类并作如下修改：

```java
package me.ereach.comparable;

import me.ereach.collections.EmailComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();

        users.add(new User("Tom", "e3", "male", 82));
        users.add(new User("Jerry", "e1", "male", 30));
        users.add(new User("Cindy", "e2", "female", 18));

        for (var x : users)
            System.out.println(x);

        System.out.println("===");

        Collections.sort(users);

        for (var x : users)
            System.out.println(x);

        System.out.println("===");

        Collections.sort(users, new EmailComparator());
        for (var x : users)
            System.out.println(x);
    }
}
```

* 运行Main类我们看到下面结果：

```script
User { name: Tom, email: e3, gender: male, age: 82 }
User { name: Jerry, email: e1, gender: male, age: 30 }
User { name: Cindy, email: e2, gender: female, age: 18 }
===
User { name: Cindy, email: e2, gender: female, age: 18 }
User { name: Jerry, email: e1, gender: male, age: 30 }
User { name: Tom, email: e3, gender: male, age: 82 }
===
User { name: Jerry, email: e1, gender: male, age: 30 }
User { name: Cindy, email: e2, gender: female, age: 18 }
User { name: Tom, email: e3, gender: male, age: 82 }

Process finished with exit code 0
```

总结：

* 在完成了User类的可比较实现后，我们可能发现比较User类的其他属性也是必须的，这种情况下我们就需要实现该属性的比较器接口
* 完成比较器接口后我们就可以通过不同属性的比较

### 队列接口

这一节我们介绍队列接口，它也是基于集合接口实现的，队列通常可以用打印任务做类比，多个打印任务必须顺序完成。

### 集合接口

这一节我们讨论Set接口，它的特点是元素没有重复值，我们看下面的例子：

* 创建包“me.ereach.set”，并在包下创建主类“Main”然后如下修改代码：

```java
package me.ereach.set;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("sky");
        set.add("is");
        set.add("blue");
        set.add("blue");
        System.out.println(set);
    }
}
```

* 运行上面代码可看到如下结果，可以看到重复项被剔除，同时我们也看到Set并不担保元素的顺序：

```script
[sky, blue, is]

Process finished with exit code 0
```

* 再次返回Main类并更新代码如下：

```java
package me.ereach.set;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();
        Collections.addAll(collection, "a", "b", "c", "c");
        System.out.println(collection);
        Set<String> set = new HashSet<>(collection);
        System.out.println(set);
    }
}
```

* 再次运行代码我们看到如下输出，重复元素被自动剔除。

```script
[a, b, c, c]
[a, b, c]

Process finished with exit code 0
```

* 下面我们看集合的Union操作，返回Main类并更新代码如下：

```java
package me.ereach.set;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>(Arrays.asList("a", "b", "c"));
        Set<String> set2 = new HashSet<>(Arrays.asList("b", "c", "d"));

        set1.addAll(set2);

        System.out.println(set1);
    }
}
```

* 运行上面代码我们看到如下输出：

```script
[a, b, c, d]

Process finished with exit code 0
```

* 再次修改Main类我们看Intersection运算：

```java
package me.ereach.set;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>(Arrays.asList("a", "b", "c"));
        Set<String> set2 = new HashSet<>(Arrays.asList("b", "c", "d"));

        set1.retainAll(set2);

        System.out.println(set1);
    }
}
```

* 运行上面代码我们看到如下输出：

```script
[b, c]

Process finished with exit code 0
```

* 再次修改Main我们看Difference运算

```java
package me.ereach.set;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>(Arrays.asList("a", "b", "c"));
        Set<String> set2 = new HashSet<>(Arrays.asList("b", "c", "d"));

        set1.removeAll(set2);

        System.out.println(set1);
    }
}
```

* 运行上面代码看到如下输出：

```script
[a]

Process finished with exit code 0
```

### 哈希表

* 创建包me.ereach.hashtable，并在包下添加类User，然后更新其代码如下：

```java
package me.ereach.hashtable;

public class User implements Comparable<User> {
    private String name;
    private String gender;
    private String email;
    private Integer age;

    public User(String name, String gender, String email, Integer age) {
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int compareTo(User o) {
        return this.age - o.age;
    }
}
```

* 继续在该包下创建Main类并添加如下代码：

```java
package me.ereach.hashtable;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<User> users = new ArrayList<>();

        users.add(new User("Tom", "male", "e0", 17));
        users.add(new User("Jerry", "male", "e1", 19));
        users.add(new User("Cherry", "female", "e2", 16));
        users.add(new User("Wendy", "female", "e3", 18));
        users.add(new User("Rollin", "female", "e4", 22));

        for (var user : users)
            if (user.getEmail() == "e1")
                System.out.println("Found!");
    }
}
```

* 运行上面代码我们得到下面结果：

```script
Found!

Process finished with exit code 0
```

* 上面代码的空间复杂度是O(n)，具体说就是最坏情况，有几个用户就要找几次，这样的效率是不够好的，因此我们引入HashTable的实现，它的复杂度是O(1)
* 所谓哈希表在不同的语言中有不同的表述，比如：
  * Java: Maps，也叫键值对
  * C#: Dictionary
  * Python: Dictionary
  * JavaScript: Objects
* 在Map之前我们看下一个例子：

```java
package me.ereach.hashtable;

import java.util.Enumeration;
import java.util.Hashtable;

public class Main {
    public static void main(String[] args) {
        Hashtable balance = new Hashtable();

        balance.put("Tom", 1000);
        balance.put("Jerry", 1200);
        balance.put("Cherry", 1600);
        balance.put("Wendy", 1500);

        Enumeration<String> names = balance.keys();

        while (names.hasMoreElements()) {
            String nm = names.nextElement();
            System.out.println(nm + "'s balance is " + balance.get(nm));
        }
    }
}
```

### Map接口

* Map是键值对的结构，创建mapinterface包，在包下创建Main类并更新如下代码：

```java
package me.ereach.mapinterface;

import me.ereach.hashtable.User;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, User> map = new HashMap<>();

        var u1 = new User("Tom", "male", "e1", 19);
        var u2 = new User("Jerry", "male", "e2", 21);
        var u3 = new User("Cherry", "female", "e3", 16);
        var u4 = new User("Wendy", "female", "e4", 18);

        map.put(u1.getEmail(), u1);
        map.put(u2.getEmail(), u2);
        map.put(u3.getEmail(), u3);
        map.put(u4.getEmail(), u4);

        var u = map.get("e1");
        System.out.println(u);

        var flag = map.containsKey("e3");
        System.out.println(flag);

        map.replace("e1", new User("Tod", "male", "e5", 22));
        System.out.println(map.get("e1"));
    }
}
```

### 总结

## Lambda表达式和函数式接口

### 简介

本节我们讲介绍以下内容：

* Lambda Expressions
* Functional Interfaces
* Consumers
* Suppliers
* Functions
* Predicates

### 函数式接口

所谓函数式接口就是一个接口伴随一个抽象方法；就比如之前学习过的Comparable接口，它只有一个方法compareTo(T o)，这就是所谓的函数式接口。

* 创建一个项目，并在项目下创建包"me.ereach.lambdas"并在包下创建接口Printer如下：

```java
package me.ereach.lambdas;

public interface Printer {
    void print(String message);

    default void printTwice(String message) {
        System.out.println(message);
        System.out.println(message);
    }
}
```

* 如上面的代码，函数式接口要求一个抽象方法，而上面的接口包含了其他缺省方法，因此不是函数式接口，所以我修改上面代码如下：

```java
package me.ereach.lambdas;

public interface Printer {
    void print(String message);
}
```

* 完成上面接口后，我们创建一个ConsolePrinter类来实现该接口，如下：

```java
package me.ereach.lambdas;

public class ConsolePrinter implements Printer {
    @Override
    public void print(String message) {
        System.out.println(message);
    }
}
```

* 创建主类Main01并添加以下内容：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        // ConsolePrinter is the implementation used as paramter
        greet(new ConsolePrinter());
    }

    // printer is the interface type used as parameter
    public static void greet(Printer printer) {
        printer.print("Hello, World");
    }
}
```

* 运行上面主类我们看到如下输出：

```script
Hello, World

Process finished with exit code 0
```

### 匿名内联类

上一节中，我们把实现类“ConsolePrinter”的实例作为参数传递到“greet”方法，很多时候，我们这样做的目的只有一个就是使用这个类，而非这个类的一个实现类，这就是我们使用匿名类目的。

* 返回“Main01”类，在IDEA中，如下图修改“greet”方法的参数：

<img src="./images/img060.png" />

* 完成上面操作后我们可看到下面代码，它实际就是那个匿名的实现类：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new Printer() {
            @Override
            public void print(String message) {
                
            }
        });
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
```

* 继续修改上面的代码如下，并运行该代码我们看到完全一样的结果，这就是匿名内联类，这种方式可以有效的减少代码：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println(message);
            }
        });
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
```

* 接上面例子我们仍然可以使用之前的实现类“ConsolePrinter”，如下:

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new ConsolePrinter());
        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("===" + message + "===");
            }
        });
    }

    public static void greet(Printer printer) {
        printer.print("Hello My World.");
    }
}
```

* 运行上面代码输出如下：

```script
Hello My World.
===Hello My World.===

Process finished with exit code 0
```

### Lambda表达式

* 继续上面的例子，我们返回“Main01”类并加入下面代码：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new ConsolePrinter());

        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("===" + message + "===");
            }
        });

        // lambda expression
        greet((String message) -> {
            System.out.println("+++" + message + "+++");
        });
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
```

* 运行该住类可看到如下输出：

```java
Hello, World!
===Hello, World!===
+++Hello, World!+++

Process finished with exit code 0
```

* 由于接口类定义了接口方法，因此我们可进一步简化Lambda表达式为如下：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new ConsolePrinter());

        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("===" + message + "===");
            }
        });

        // lambda expression
        greet((message) -> {
            System.out.println("+++" + message + "+++");
        });
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
```

* 针对Lambda表达式，如果如下新增参数则会报错，因为它式接口方法的实现：

<img src="./images/img061.png" />

* 针对上面Lambda表达式，如果只有一个参数我们甚至可以进一步移除参数括号简化如下：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new ConsolePrinter());

        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("===" + message + "===");
            }
        });

        // lambda expression
        greet(message -> {
            System.out.println("+++" + message + "+++");
        });
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
```

* 如果Lambda表达式只有一行代码，我们还可简化代码如下：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new ConsolePrinter());

        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("===" + message + "===");
            }
        });

        // lambda expression
        greet(message -> System.out.println("+++" + message + "+++"));
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
```

* 我们还可进一步如下玩Lambda表达式：

```java
package me.ereach.lambdas;

public class Main01 {
    public static void main(String[] args) {
        greet(new ConsolePrinter());

        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("===" + message + "===");
            }
        });

        // lambda expression
        greet(message -> System.out.println("+++" + message + "+++"));

        Printer printer = message -> System.out.println("@@@" + message + "@@@");
        greet(printer);
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
```

* 简化的最后一步：

```java
package me.ereach.lambdas;

public class Main {
    public static void main(String[] args) {
        greet(new ConsolePrinter());

        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("===" + message + "===");
            }
        });

        greet((String message) -> {
            System.out.println("@@@" + message + "@@@");
        });

        greet(message -> System.out.println("+++" + message + "+++"));

        Printer printer = message -> System.out.println("---" + message + "---");
        greet(printer);

        greet(message -> System.out.println("***" + message + "***"));
    }

    public static void greet(Printer printer) {
        printer.print("Hello, World.");
    }
}
```

### 变量捕获

* 重新回到lambdas包下，删除所有的类和接口，创建接口Printer并添加下面唯一的接口方法：

```java
package me.ereach.lambdas;

public interface Printer {
    void print(String message);
}
```

* 添加主类Main为如下代码：

```java
package me.ereach.lambdas;

public class Main {
    public static String str2 = "###";

    public static void main(String[] args) {
        String str1 = "===";
        
        greet(message -> System.out.println(str1 + message + str2));
    }

    public static void greet(Printer printer) {
        printer.print("Hello, My World!");
    }
}
```

* 运行上面代码输出如下，它反映了Lambda方法访问变量的作用域：

```script
===Hello, My World!###

Process finished with exit code 0
```

### 方法引用

* 要引用类方法可用形式：```Class/Object::method```，续接我们上一节的例子，如下：

```java
package me.ereach.lambdas;

public class Main {
    public static void main(String[] args) {
        greet(message -> System.out.println(message));
        // 下面为引用方法
        greet(System.out::println);
    }

    public static void greet(Printer printer) {
        printer.print("Hello, My World!");
    }
}
```

* 使用idea我们可以方便的把方法转换为引用方法，如下图：

<img src="./images/img062.png" />

* 上图点击“Replace Lambda with method reference”，代码将被修改为如下：

```java
package me.ereach.lambdas;

public class Main {
    public static void main(String[] args) {
        greet(System.out::println);
        greet(System.out::println);
    }

    public static void greet(Printer printer) {
        printer.print("Hello, My World!");
    }
}
```

* 运行上面代码看到如下输出：

```java
Hello, My World!
Hello, My World!

Process finished with exit code 0
```

* 我们还可引用接口外部的方法，如下：

```java
package me.ereach.lambdas;

public class Main {
    public static void print(String message) {
        System.out.println(message + " ref method");
    }

    public static void main(String[] args) {
        greet(Main::print);
    }

    public static void greet(Printer printer) {
        printer.print("Hello, My World!");
    }
}
```

* 运行上面代码可看到如下输出：

```script
Hello, My World! ref method

Process finished with exit code 0
```

### 内置函数接口

Java提供了很多内置的函数式接口以便我们执行一些常用的任务，它们之所以被提供也是为了方便使用Lambda表达式和方法的引用，它们可以在java.util.function包下找到，它们通常可以分为以下几类：

* Consumer（消费者），它的形式是：“void consume(obj)”例如前面用到的例子：“void print(String message);”，该例子消费message字符串信息。
* Supplier（提供者），它的形式是：“obj supply()”，它执行某些操作并返回相关的数据对象。
* Function（功能），它的形式是：“obj map(obj)”，它针对某些对象执行操作并返回相关的数据对象。
* Predicate（断言），它的形式是：“bool test(condition)”，它针对某些条件进行测试并返回布尔值测试结果。

下面几节课的内容我们分别描述它们。

### 消费接口

消费接口接收单个泛型参数并行相关操作而不返回任何结果，参考下面的例子：

```java
package me.ereach.lambdas;

import java.util.List;

public class Main02 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3);

        // Imperative Programming (指令式编程)
        //      for, if/else, switch/case
        //      关注如何让事情发生
        for (var item : list)
            System.out.println(item);

        System.out.println("===");

        // Declarative Programming (声明式编程)
        //      关注将要发生的事情
        list.forEach(item -> System.out.println(item));
    }
}
```

输出：

```script
1
2
3
===
1
2
3

Process finished with exit code 0
```

上面例子，分割线下面的部分是用Lambda表达式实现的消费型函数式接口

### 链式消费

访问[Java文档](https://docs.oracle.com/javase/8/docs/api/java/util/function/Consumer.html)我们可以看到"andThen"这样一个接口类型，它就是链式消费接口，它返回一个组合的消费接口并按此组合执行，参看下面例子：

* 返回项目创建一个Main03主类，并更新其代码如下：

```java
package me.ereach.lambdas;

import java.util.List;
import java.util.function.Consumer;

public class Main03 {
    public static void main(String[] args) {
        List<String> list = List.of("a", "b", "c");

        Consumer<String> print = item -> System.out.println(item);
        Consumer<String> printUpperCase = item -> System.out.println(item.toUpperCase());

        list.forEach(print.andThen(printUpperCase));
    }
}
```

* 完成后运行该代码看到如下输出：

```script
a
A
b
B
c
C

Process finished with exit code 0
```

### 供应商接口

如前述，供应商接口不需要提供参数，执行该接口方法返回相应类型的数据对象，如下例子：

* 创建主类Main04并按下更新代码：

```java
package me.ereach.lambdas;

import java.util.function.Supplier;

public class Main04 {
    public static void main(String[] args) {
        Supplier<Double> getRandom = () -> Math.random();
        var random = getRandom.get();
        System.out.println(random);
    }
}
```

* 执行该主类输出如下内容：

```script
0.7941356553958611

Process finished with exit code 0
```

要注意的是上面的接口方法直到调用"get()"时才被执行。

### 函数接口

基本形式```Interface Function<T, R>```表述了函数的一般行为：T表示函数的输入类型，R表示函数结果的类型，还有其它一些形式：

* ```Interface BiFunction<T, U, R>```，T表示函数第一个参数的类型；U表示函数第二个参数的类型；R表示函数的结果的类型
* ```Interface IntFunction<R>```，R表示函数的结果类型为整数型，以此类推我们还有```LongFunction```和```DoubleFunction```
* ```Interface ToIntFunction<T>```，T表示函数的输入类型，同样我们还有```ToLongFunction```和```ToDoubleFunction```

参看下面例子：

```java
package me.ereach.lambdas;

import java.util.function.Function;

public class Main05 {
    public static void main(String[] args) {
        Function<String, Integer> map = str -> str.length();
        var length = map.apply("Blue Sky");
        System.out.println(length);
    }
}
```

运行上面代码我们看到如下输出：

```script
8

Process finished with exit code 0
```

### 组合函数(Composing Functions)

下面一节我们将演示组合函数构建更复杂和有意思的操作，这些操作包括：

* 传递一个字符串：“key:value”
* 处理这个字符串成为：“key=value”
* 把这个处理后的字符串加上花括号：“{key=value}”

看下面例子：

```java
package me.ereach.lambdas;

import java.util.function.Function;

public class Main06 {
    public static void main(String[] args) {
        Function<String, String> replaceColon = str -> str.replace(":", "=");
        Function<String, String> addBraces = str -> "{" + str + "}";

        var rst = replaceColon.andThen(addBraces).apply("name:jinzd");
        System.out.println(rst);
    }
}
```

运行上面例子看道如下结果：

```script
{name=jinzd}

Process finished with exit code 0
```

或者，我们修改上面程序为如下排版会更好些：

```java
package me.ereach.lambdas;

import java.util.function.Function;

public class Main06 {
    public static void main(String[] args) {
        Function<String, String> replaceColon = str -> str.replace(":", "=");
        Function<String, String> addBraces = str -> "{" + str + "}";

        var rst = replaceColon
                .andThen(addBraces)
                .apply("name:jinzd");
        
        System.out.println(rst);
    }
}
```

既然我们这一节讲的是组合函数接口，所以还有下面一种方式：

```java
package me.ereach.lambdas;

import java.util.function.Function;

public class Main06 {
    public static void main(String[] args) {
        Function<String, String> replaceColon = str -> str.replace(":", "=");
        Function<String, String> addBraces = str -> "{" + str + "}";

        var rst = replaceColon
                .andThen(addBraces)
                .apply("name:jinzd");

        System.out.println(rst);

        rst = addBraces
                .compose(replaceColon)
                .apply("name:jinzd");

        System.out.println(rst);
    }
}
```

### 断言接口(Pedicate Interface)

这节我们将介绍断言接口，它非常有用，它的形式是：boolean test(T t)其它形式还包括：

* public interface BiPredicate<T, U>即：test(T t, U u)

参考下面的例子:

```java
package me.ereach.lambdas;

import java.util.function.Predicate;

public class PredicateDemo {
    public static void main(String[] args) {
        Predicate<String> isLongerThan5 = str -> str.length() > 5;

        var rst = isLongerThan5.test("Hello, World.");
        System.out.println(rst);
    }
}
```

### 组合断言(Combining Predicates)

我们可以把多个断言组合起来使用形成组合断言，如下例子：

```java
package me.ereach.lambdas;

import java.util.function.Predicate;

public class CPredicateDemo {
    public static void main(String[] args) {
        Predicate<String> hasLeftBrace = str -> str.startsWith("{");
        Predicate<String> hasRightBrace = str -> str.endsWith("}");

        Predicate<String> hasLeftAndRightBraces = hasLeftBrace.and(hasRightBrace);

        var rst = hasLeftAndRightBraces.test("{name=jinzd}");
        System.out.println(rst);
    }
}
```

### 二元运算符接口(BinaryOperator)

之前我们已经提过```java.util.function```包，它包括了我们之前学习过的所有函数式接口，之前我们还说过这些函数式接口可以分为四类：Conssumer、Supplier、Function、Predicate，事实上所有函数式接口都可划归以上四个分类。这一节我们讨论二元运算符接口```Interface BinaryOperator<T>```它的函数形式是```BiFunction<T, T, T>```，它接收两个泛型参数同时返回一个同类型的结果，参看下面例子：

```java
package me.ereach.lambdas;

import java.util.function.BinaryOperator;

public class BinaryOperatorDemo {
    public static void main(String[] args) {
        BinaryOperator<Integer> add = (x, y) -> x + y;
        var rst = add.apply(1, 2);
        System.out.println(rst);
    }
}
```

我们也可组合下面例子：

```java
package me.ereach.lambdas;

import java.util.function.BinaryOperator;
import java.util.function.Function;

public class BinaryOperatorDemo {
    public static void main(String[] args) {
        BinaryOperator<Integer> add = (x, y) -> x + y;
        Function<Integer, Integer> square = x -> x * x;

        var rst = add.andThen(square).apply(1, 2);
        System.out.println(rst);
    }
}
```

### 一元运算符(UnaryOperator)

一元运算符接口是这一章的最后一个函数式接口，它的接口形式是```Interface UnaryOperator<T>```，它的函数形式是```Function<T, T>```，它接收一个参数并返回一个同类型的参数。

```java
package me.ereach.lambdas;

import java.util.function.UnaryOperator;

public class UnaryDemo {
    public static void main(String[] args) {
        UnaryOperator<Integer> square = n -> n * n;
        UnaryOperator<Integer> increment = n -> n + 1;

        var rst = increment.andThen(square).apply(1);
        System.out.println(rst);
    }
}
```

### 总结

这一章我们学习了以下内容：

* Lambda Expressions
* Functional Interfaces
* Java中我们有以下四类函数式接口：
  * Consumers
  * Suppliers
  * Functions
  * Predicates
* Composition提供我们对函数式接口的组合使用方法

## Streams

### 简介

流(streams)是Java的一个强大功能，它允许开发人员通过声明方式操作数据集，这与编写结构化查询有些类似，这一节我们将学习并演示这些特性，并通过多种流操作构建复杂的查询，例如：

* 映射(mapping)
* 过滤(filtering)
* 切片(slicing)
* 排序(sorting)
* 减少(reducing)
* 收集器(collectors)

### 命令式与函数式编程

在讨论流之前，我们先看看命令式编程及函数式编程试图解决的问题，我们有一个电影的列表对象，我们希望统计喜欢指数超过10个点的电影：

* 返回项目，创建```Movie```类，并实现下面属性及方法：

```java
package me.ereach.streams;

public class Movie {
    private String title;
    private int likes;

    public Movie(String title, int likes) {
        this.title = title;
        this.likes = likes;
    }

    public int getLikes() {
        return likes;
    }
}
```

* 返回主类并做如下修改

```java
package me.ereach.streams;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        // Imperative Programming 关注怎样实现 (How to)
        int count = 0;
        for (var movie : movies)
            if (movie.getLikes() > 10)
                count++;

        System.out.println(count);

        // Declarative Programming (函数式，What)
        var count2 = movies.stream()
                .filter(movie -> movie.getLikes() > 10)
                .count();

        System.out.println(count2);
    }
}
```

* 运行上面例子我们得到如下结果：

```
2
2

Process finished with exit code 0
```

从上面的例子可以看出：

* 命令式编程关注执行步骤，即告诉计算机一步一步如何实现
* 声明式编程关注做什么，但不具体指定要怎么去做

### 创建流

我们可以通过下面方式来创建有限流和无限流：

* 通过集合创建(collections)
* 通过数组创建(arrays)
* 通过任意数量的对象(arbitrary number of objects)

让我们看下面的例子：

* 返回主类并如下修改代码：

```java
package me.ereach.streams;

import java.util.Arrays;
import java.util.stream.Stream;

public class Main02CreatingStream {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5};
        Arrays.stream(nums)
                .forEach(x -> System.out.println(x));

        System.out.println("===");
        Stream.of(1, 2, 3, 4, 5)
                .forEach(x -> System.out.println(x));

        System.out.println("===");
        Stream.generate(() -> Math.random())
                .limit(5)
                .forEach(x -> System.out.println(x));

        System.out.println("===");
        Stream.iterate(1, n -> n + 1)
                .limit(5)
                .forEach(x -> System.out.println(x));
    }
}
```

* 运行上面代码看到如下结果：

```
1
2
3
4
5
===
1
2
3
4
5
===
0.2735417472080208
0.11658728008024433
0.42925975918204184
0.5448181675313459
0.021836172107764673
===
1
2
3
4
5

Process finished with exit code 0
```

### 映射元素(Mapping Elements)

这一节我们通过变换一个字符串值来演示映射，我们通过map()或flatMap()两个方法来实现。

* 首先我们返回Movie类并做如下修改(增加title的getter)：

```java
package me.ereach.streams;

public class Movie {
    private String title;
    private int likes;

    public Movie(String title, int likes) {
        this.title = title;
        this.likes = likes;
    }

    public int getLikes() {
        return likes;
    }

    public String getTitle() {
        return title;
    }
}
```

* 返回主类我们还是通过之前用过的Movie类来完成面演示：

```java
package me.ereach.streams;

import java.util.List;

public class Main03Mapping {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        // map method
        movies.stream()
                .map(movie -> movie.getTitle())
                .forEach(name -> System.out.println(name));

        System.out.println("===");
        // mapToInt
        movies.stream()
                .mapToInt(movie -> movie.getLikes())
                .forEach(val -> System.out.println(val));
    }
}
```

* 运行上面代码看到如下结果：

```
a
b
c
===
10
15
20

Process finished with exit code 0
```

下面我们再看flatMap()方法：

* 返回主类并做如下修改:

```java
package me.ereach.streams;

import java.util.List;
import java.util.stream.Stream;

public class Main04FlatMapping {
    public static void main(String[] args) {
        var stream = Stream.of(List.of(1, 2, 3), List.of(4, 5, 6));

        // flatMap method
        stream
                .flatMap(list -> list.stream())
                .forEach(n -> System.out.println(n));
    }
}
```

* 运行上面代码看到如下结果：

```
1
2
3
4
5
6

Process finished with exit code 0
```

### 过滤元素

* 返回主类并如下修改代码：

```java
package me.ereach.streams;

import java.util.List;

public class Main05FilteringElements {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        movies.stream()
                .filter(m -> m.getLikes() > 10)
                .forEach(m -> System.out.println(m.getTitle()));
    }
}
```

* 运行上面的代码看到如下结果：

```
b
c

Process finished with exit code 0
```

* 上面的代码，filter、map等方法也叫做中间操作，它们返回新的stream对象；而forEach等叫做最终操作，它们通常会输出一些结果。
* 上面的代码还可通过断言来实现，如下：

```java
package me.ereach.streams;

import java.util.List;
import java.util.function.Predicate;

public class Main05FilteringElements {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        Predicate<Movie> isPopular = m -> m.getLikes() > 10;

        movies.stream()
                .filter(isPopular)
                .forEach(m -> System.out.println(m.getTitle()));
    }
}
```

### 流切片(Slicing Stream)

通常，我们有4个方法可以对流进行切片操作，而之前的```limit(n)```就是其中一种；此外```skip(n)```也可以对流进行切片，这两个切片操作结合起来可以完成对数据的分页操作，如下：

* limit(n)
* skip(n)
* takeWhile(predicate)，当断言为真时将在流中握住这些元素
* dropWhile(predicate)，当断言为真时将在流中剔除这些元素

下面我们进一步演示这些切片方法：

* 返回主类方法，并做下面修改：

```java
package me.ereach.streams;

import java.util.List;

public class Main06SlicingStream {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        System.out.println("===limit");
        movies.stream()
                .limit(2)
                .forEach(m -> System.out.println(m.getTitle()));

        System.out.println("===skip");
        movies.stream()
                .skip(2)
                .forEach(m -> System.out.println(m.getTitle()));

        // for sorted stream,
        System.out.println("===takeWhile");
        movies.stream()
                .takeWhile(m -> m.getLikes() <= 15)
                .forEach(m -> System.out.println(m.getTitle()));

        System.out.println("===dropWhile");
        movies.stream()
                .dropWhile(m -> m.getLikes() <= 15)
                .forEach(m -> System.out.println(m.getTitle()));
    }
}
```

* 运行上面代码我们得到下面结果：

```
===limit
a
b
===skip
c
===takeWhile
a
b
===dropWhile
c

Process finished with exit code 0
```

思考：

* takeWhile和dropWhile与filter比较有什么差异
* 有1000部电影，每个页面显示10部电影
* 用切片实现第三页电影的显示

### 流排序

通常，流中元素的顺序依赖底层数据元的数据顺序，例如前面的movies的List，基于该列表创建的流的顺序与列表相同，如果需要我们也可排序这个流。

* 返回主类我们将做如下演示：

```java
package me.ereach.streams;

import java.util.Comparator;
import java.util.List;

public class Main07StreamSort {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("b", 10),
                new Movie("a", 15),
                new Movie("c", 20)
        );

        System.out.println("===compareTo");
        movies.stream()
                .sorted((a, b) -> a.getTitle().compareTo(b.getTitle()))
                .forEach(m -> System.out.println(m.getTitle()));

        System.out.println("===Comparator & Method References");
        movies.stream()
                .sorted(Comparator.comparing(Movie::getTitle))
                .forEach(m -> System.out.println(m.getTitle()));

        System.out.println("===Comparator & Method References with desc");
        movies.stream()
                .sorted(Comparator.comparing(Movie::getTitle).reversed())
                .forEach(m -> System.out.println(m.getTitle()));

    }
}
```

* 运行上面代码可看到如下排序后的结果：

```
===compareTo
a
b
c
===Comparator & Method References
a
b
c
===Comparator & Method References with desc
c
b
a

Process finished with exit code 0
```

### 获取唯一元素

* 基于列表的元素可能包含重复信息，而这些重复信息我们希望重流中滤除掉(distinct())，返回主类做如下操作修改：

```java
package me.ereach.streams;

import java.util.List;

public class Main08UniqueElements {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        System.out.println("===distinct based likes");
        movies.stream()
                .map(Movie::getLikes)
                .distinct()
                .forEach(System.out::println);

        System.out.println("===distinct base title");
        movies.stream()
                .map(Movie::getTitle)
                .distinct()
                .forEach(System.out::println);
    }
}
```

* 上面代码可看到如下结果：

```
===distinct based likes
10
15
20
===distinct base title
a
b
c

Process finished with exit code 0
```

### 拾取元素

* 当进行一些复杂的查询时，我们可能会碰到一些问题并导致错误的结果，针对这些问题我们可以用peek方法处理，返回主类并进行如下修改：

```java
package me.ereach.streams;

import java.util.List;

public class Main09Peeking {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        movies.stream()
                .filter(m -> m.getLikes() > 10)
                .peek(m -> System.out.println("filtered: " + m.getTitle()))
                .map(Movie::getTitle)
                .peek(t -> System.out.println("mapped: " + t))
                .forEach(System.out::println);
    }
}
```

* 上面代码peek用于调试过程中，检测过滤和映射可能引发的问题，结果如下：

```
filtered: b
mapped: b
b
filtered: c
mapped: c
c

Process finished with exit code 0
```

### 简单简化(Simple Reducers)

目前为止我们讨论了相关的中间操作符，通过这些中间操作符我们可以构造我们期望的管线操作，如下：

* map()/flatMap()
* filter()
* limit()/skip()
* sorted()
* distinct()
* peek()

下面几节我们将介绍一些简化操作(reducers)，它们的目的是把对象的流(stream)简化为单个对象，例如：

* count()，通过计数方法计算元素的个数
* anyMatch(predicate)，检测流中的任何元素是否满足条件
* allMatch(predicate)，检测流中的所有元素是否满足条件
* noneMatch(predicate)，检测流中是否没有元素满足条件
* findFirst()
* findAny()
* max(comparator)
* min(comparator)

以上这些简化操作都是最终操作(terminal operations)，它们产生相关的结果，所以在它们之后不能再调用其他操作，看下面的例子：

```java
package me.ereach.streams;

import java.util.Comparator;
import java.util.List;

public class Main10SimpleReducers {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        // get count of movies
        System.out.println("===Count");
        System.out.println(movies.stream().count());

        // check if exists for the predicate
        System.out.println("===anyMatch");
        var rst = movies.stream()
                .anyMatch(m -> m.getLikes() > 10);
        System.out.println(rst);

        // check if all match the predicate
        System.out.println("===allMatch");
        rst = movies.stream()
                .allMatch(m -> m.getLikes() > 10);
        System.out.println(rst);

        // check if none match the predicate
        System.out.println("===noneMatch");
        rst = movies.stream()
                .noneMatch(m -> m.getLikes() > 10);
        System.out.println(rst);

        // return the first movie obj
        System.out.println("===findFirst");
        var movie = movies.stream()
                .findFirst()
                .get();
        System.out.println(movie.getTitle());

        // return any movie obj
        System.out.println("===findAny");
        movie = movies.stream()
                .findAny()
                .get();
        System.out.println(movie.getTitle());

        System.out.println("===max");
        movie = movies.stream()
                .max(Comparator.comparing(Movie::getLikes))
                .get();
        System.out.println(movie.getTitle());

        System.out.println("===min");
        movie = movies.stream()
                .min(Comparator.comparing(Movie::getTitle))
                .get();
        System.out.println(movie.getTitle());
    }
}
```

运行上面代码看到如下输出：

```
===Count
3
===anyMatch
true
===allMatch
false
===noneMatch
false
===findFirst
a
===findAny
a
===max
c
===min
a

Process finished with exit code 0
```

### 简化一个流

上一节我们介绍了一些简单的简化器，这一节我们将介绍简化流更多的控制，它可以使我们更好的控制流简化为单个值，例如：

* 下面的例子实现所有电影的likes相加：

```java
package me.ereach.streams;

import java.util.List;
import java.util.Optional;

public class Main11ReducingAStream {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        // 调用map方法以映射电影的likes
        // 调用reduce方法以简化流为单值流
        // 可以看到reduce被重载过，第二个版本支持两个参数的累加器
        // 操作过程类似
        // [10, 15, 20]
        // [25, 20]
        // [45]
        // 它返回一个 Optional<Integer>，意思是或许是一个整数值
        Optional<Integer> sum = movies.stream()
                .map(m -> m.getLikes())
                .reduce((a, b) -> a + b);
        
        // 对于或许没有返回值的sum来说，使用orElse(0)
        // 表示有值则输出该值，无值则输出0
        System.out.println(sum.orElse(0));
    }
}
```

运行上面代码输出：

```
45

Process finished with exit code 0
```

* 还是上面的代码我们还可以使用引用方法进一步简化为：

```java
package me.ereach.streams;

import java.util.List;
import java.util.Optional;

public class Main11ReducingAStream {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        Optional<Integer> sum = movies.stream()
                .map(Movie::getLikes)
                .reduce(Integer::sum);
        System.out.println(sum.orElse(0));
    }
}
```

* 在就是上面Optional的写法有时显得很别扭，我们可以用下面方法取代：

```java
package me.ereach.streams;

import java.util.List;

public class Main11ReducingAStream {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        Integer sum = movies.stream()
                .map(Movie::getLikes)
                .reduce(0, Integer::sum);
        System.out.println(sum);
    }
}
```

### 收集器(Collectors)

到目前为止，我们已经尝试了流的forEach方法，另一方面我们也期望处理这些流的结果为某种数据结果，如List、Set、Map等等，看下面的例子：

* 返回主类并做如下处理：

```java
package me.ereach.streams;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main12Collectors {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        System.out.println("===toList");
        var rst1 = movies.stream()
                .collect(Collectors.toList());

        rst1.forEach(x -> System.out.println("Movie { title: " + x.getTitle() + ", likes: " + x.getLikes() + " }"));

        System.out.println("===toSet");
        var rst2 = movies.stream()
                .collect(Collectors.toSet());

        rst2.forEach(x -> System.out.println("Movie { title: " + x.getTitle() + ", likes: " + x.getLikes() + " }"));

        System.out.println("===toMap");
        var rst3 = movies.stream()
                .collect(Collectors.toMap(Movie::getTitle, Movie::getLikes));

        System.out.println(rst3);

        System.out.println("===toHashMap");
        var rst4 = movies.stream()
                .collect(Collectors.toMap(Movie::getTitle, m -> m));

        System.out.println(rst4);

        System.out.println("===toHashMap another type of m");
        var rst5 = movies.stream()
                .collect(Collectors.toMap(Movie::getTitle, Function.identity()));

        System.out.println(rst5);

        System.out.println("===summingInt");
        var rst6 = movies.stream()
                .collect(Collectors.summingInt(Movie::getLikes));

        System.out.println(rst6);

        System.out.println("===summarizingInt");
        var rst7 = movies.stream()
                .collect(Collectors.summarizingInt(Movie::getLikes));

        System.out.println(rst7);

        System.out.println("===joining");
        var rst8 = movies.stream()
                .filter(m -> m.getLikes() > 10)
                .map(Movie::getTitle)
                .collect(Collectors.joining(", "));

        System.out.println(rst8);

    }
}
```

* 运行上面代码得到下面结果：

```
===toList
Movie { title: a, likes: 10 }
Movie { title: b, likes: 15 }
Movie { title: c, likes: 20 }
===toSet
Movie { title: a, likes: 10 }
Movie { title: b, likes: 15 }
Movie { title: c, likes: 20 }
===toMap
{a=10, b=15, c=20}
===toHashMap
{a=me.ereach.streams.Movie@5b480cf9, b=me.ereach.streams.Movie@6f496d9f, c=me.ereach.streams.Movie@723279cf}
===toHashMap another type of m
{a=me.ereach.streams.Movie@5b480cf9, b=me.ereach.streams.Movie@6f496d9f, c=me.ereach.streams.Movie@723279cf}
===summingInt
45
===summarizingInt
IntSummaryStatistics{count=3, sum=45, min=10, average=15.000000, max=20}
===joining
b, c

Process finished with exit code 0
```

### 元素分组

有些时候我们需要对数据进行分组，我们还是以movies的例子来演示：

* 首先添加一个电影的分类枚举：

```java
package me.ereach.streams;

public enum Genre {
    COMEDY,
    ACTION,
    THRILLER
}
```

* 返回电影类做如下更新：

```java
package me.ereach.streams;

public class Movie {
    private String title;
    private int likes;
    private Genre genre;

    public Movie(String title, int likes, Genre genre) {
        this.title = title;
        this.likes = likes;
        this.genre = genre;
    }

    public Movie(String title, int likes) {
        this.title = title;
        this.likes = likes;
    }

    public int getLikes() {
        return likes;
    }

    public String getTitle() {
        return title;
    }

    public Genre getGenre() {
        return genre;
    }

    @Override
    public String toString() {
        return "Movie { title: " + title + ", likes: " + likes + " }";
    }
}
```

* 返回主类进行如下修改：

```java
package me.ereach.streams;

import java.util.List;
import java.util.stream.Collectors;

public class Main13Grouping {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10, Genre.THRILLER),
                new Movie("b", 15, Genre.ACTION),
                new Movie("c", 20, Genre.ACTION)
        );

        var rst = movies.stream()
                .collect(Collectors.groupingBy(Movie::getGenre));

        System.out.println(rst);
    }
}
```

* 完成后运行主类代码看到如下结果：

```
{ACTION=[Movie { title: b, likes: 15 }, Movie { title: c, likes: 20 }], THRILLER=[Movie { title: a, likes: 10 }]}

Process finished with exit code 0
```

* 如下继续更新主类：

```java
package me.ereach.streams;

import java.util.List;
import java.util.stream.Collectors;

public class Main13Grouping {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10, Genre.THRILLER),
                new Movie("b", 15, Genre.ACTION),
                new Movie("c", 20, Genre.ACTION)
        );

        System.out.println("===toSet");
        var rst = movies.stream()
                .collect(Collectors.groupingBy(Movie::getGenre, Collectors.toSet()));

        System.out.println(rst);

        System.out.println("===counting");
        var rst1 = movies.stream()
                .collect(Collectors.groupingBy(Movie::getGenre, Collectors.counting()));

        System.out.println(rst1);

        System.out.println("===counting");
        var rst2 = movies.stream()
                .collect(Collectors.groupingBy(
                        Movie::getGenre,
                        Collectors.mapping(
                                Movie::getTitle,
                                Collectors.joining(", "))));

        System.out.println(rst2);
    }
}
```

* 运行主类代码看到下面结果：

```
===toSet
{THRILLER=[Movie { title: a, likes: 10 }], ACTION=[Movie { title: b, likes: 15 }, Movie { title: c, likes: 20 }]}
===counting
{THRILLER=1, ACTION=2}
===counting
{THRILLER=a, ACTION=b, c}

Process finished with exit code 0
```

### 元素分区

有些时候，我们需要根据一些条件来分区我们的数据，例如：把电影分为两类，一类是likes小于15的，另一类是大约等于15的：

* 返回主类：

```java
package me.ereach.streams;

import java.util.List;
import java.util.stream.Collectors;

public class Main14PartitionElements {
    public static void main(String[] args) {
        List<Movie> movies = List.of(
                new Movie("a", 10, Genre.THRILLER),
                new Movie("b", 15, Genre.ACTION),
                new Movie("c", 20, Genre.ACTION)
        );

        var rst = movies.stream()
                .collect(Collectors.partitioningBy(
                        m -> m.getLikes() >= 15,
                        Collectors.mapping(Movie::getTitle,
                                Collectors.joining(", "))));
        
        System.out.println(rst);
    }
}
```

* 运行看到如下结果

```
{false=a, true=b, c}

Process finished with exit code 0
```

### 基于原生类型的流

现在我们已经了解Java的流是如何工作的，这一节我们讨论原生数据类型相关的流如long stream和double stream，如下：

* IntStream
* LongStream
* DoubleStream

返回主类，并做如下修改：

```java
package me.ereach.streams;

import java.util.stream.IntStream;

public class Main15PrimitriveStreams {
    public static void main(String[] args) {
        System.out.println("===rangeClosed");
        IntStream.rangeClosed(1, 5)
                .forEach(System.out::println);

        System.out.println("===range");
        IntStream.range(1, 5)
                .forEach(System.out::println);

    }
}
```

运行主类看到如下结果：

```
===rangeClosed
1
2
3
4
5
===range
1
2
3
4

Process finished with exit code 0
```

## 并发与多线程

### 简介

如今，多数计算机都有多个处理器核心，它们可以并发的执行多个任务，这一节我们介绍如何编写代码来利用这些特性，让应用程序更快并有更好的响应性。这一节包含以下内容：

* 术语和概念
* 使用线程，包括暂停和中断线程
* 并发问题，包括竞争条件(race conditions)以及能见度问题(visibility problems)，这些问题存在于所有并发系统中，以及防止 这些问题的策略。
* 同步(Synchronization)
* 易失性字段(Volatile Fields)
* 原子对象(Atomic Objects)

### 进程和线程

* 进程(Process)是程序(Program)或应用(Application)的实例。当你启动一个应用，比如打开代码编辑器或播放音乐，操作系统将把应用装载到一个进程当中，而进程则包含应用的执行代码和其他资源。每个进程都有它自己的地址空间，进程之间不会相互重叠。
* 操作系统在同一时刻可以执行多个进程，例如编辑代码的同时也在播放音乐。
* 一个进程可以包含多个线程(threads)，而每个线程可以响应不同的用户请求，例如一个Web应用响应多个用户的下载请求，这样的情况我们称为多线程(Multi-Threading)，而这样的应用也被称为多线程应用。

事实上，线程才是你应用程序执行的代码，一个进程至少有一个线程(也叫主线程)，我们还可创建其他更多的附加线程以执行更多并发任务。如果你的应用不是多线程应用，那么对于多核心的CPU资源来说就是一个浪费，看下面的例子：

```java
package me.ereach.concurrency;

public class Main {
    public static void main(String[] args) {
        System.out.println(Thread.activeCount());
        System.out.println(Runtime.getRuntime().availableProcessors());
    }
}
```

运行上面代码可看到如下相似结果，它显示了当前进程的线程数以及可利用的CPU核心数：

```
2
12

Process finished with exit code 0
```

上面的运行结果显示：

* 这个应用包括两个线程，其中一个是主线程(main thread)它正在运行我们的main方法
* 另一个线程是背景线程，它运行垃圾收集器，它用于移除内存中不再使用的对象

注释：

* 一个CPU核心可以跑两个线程，上面12核心的处理器可以跑24个线程

上面结果可以看出：

* 我们只有两个线程，因此，我们只用了一个核心。

所以针对多线程我们应该关注以下几个方面：

* 启动(start)线程
* 暂停(pause)线程
* 中断(interrupt)线程
* 在线程之间安全的共享数据

### 启动一个线程

Java中，我们需要使用```Thread```类创建一个线程，它在```java.lang```下，参看下面的例子：

* 创建类```DownloadFileTask```并实现```Runnable```接口如下：

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());
    }
}
```

* 创建主类并添加入口函数如下：

```java
package me.ereach.concurrency;

public class Main01StartingThread {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        Thread thread = new Thread(new DownloadFileTask());
        thread.start();
    }
}
```

* 运行主类看到如下结果：

```
main
Downloading a file: Thread-0

Process finished with exit code 0
```

* 针对主类我们做如下改动，让它启动更多的线程：

```java
package me.ereach.concurrency;

public class Main01StartingThread {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());

        for (var i = 0; i < 5; i++) {
            Thread thread = new Thread(new DownloadFileTask());
            thread.start();
        }
    }
}
```

* 运行该主类看到下面相似结果，它们是并发运行的：

```
main
Downloading a file: Thread-4
Downloading a file: Thread-1
Downloading a file: Thread-0
Downloading a file: Thread-2
Downloading a file: Thread-3

Process finished with exit code 0
```

### 暂停线程(sleep)

这部分我们将演示如何暂停一个正在运行的线程。

* 为演示一个线程的长时间运行情况，我们返回```DownloadFileTask```并进行如下修改：

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Download completed: " + Thread.currentThread().getName());
    }
}
```

* 返回主类并运行我们在大约3秒的时间可看到如下输出，而非单任务单线程下的15秒时间：

```
main
Downloading a file: Thread-3
Downloading a file: Thread-2
Downloading a file: Thread-0
Downloading a file: Thread-4
Downloading a file: Thread-1
Download completed: Thread-2
Download completed: Thread-3
Download completed: Thread-0
Download completed: Thread-1
Download completed: Thread-4

Process finished with exit code 0
```

针对线程Java虚拟机提供了线程调度器(Thread Scheduler)来管理这些线程的运行以及运行多长时间，当你有很多任务(Task)等待运行时，调度器将把CPU时间分成小片，并在不同的时间片上切换不同的任务，这种切换场景非常的快，以至于给我们一种并发执行的映像，这其实是软件级别的并行。

### 加入线程(Joining a Thread)

* 继续上面的例子，当完成下载任务之后，你可能希望触发另一个任务以扫描这个下载的文件是否包含病毒，你可能想到如下的改动：

```java
package me.ereach.concurrency;

public class Main01StartingThread {
    public static void main(String[] args) {
        Thread thread = new Thread(new DownloadFileTask());
        thread.start();
        System.out.println("scanning for virus");
    }
}
```

* 运行上面程序我们看到如下并非期望的结果，事实上我们也无法预测任务什么时候完成：

```
scanning for virus
Downloading a file: Thread-0
Download completed: Thread-0

Process finished with exit code 0
```

* 为解决上面问题，返回主类并如下修改代码：

```java
package me.ereach.concurrency;

public class Main01StartingThread {
    public static void main(String[] args) {
        Thread thread = new Thread(new DownloadFileTask());
        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("scanning for virus");
    }
}
```

* 再次运行可看到如下期望的结果：

```
Downloading a file: Thread-0
Download completed: Thread-0
scanning for virus

Process finished with exit code 0
```

* 上面的join方法告诉当前线程等待另一线程的完成才能继续其他任务，这就是join方法的工作机制

### 中断线程(Interrupting a Thread)

* 通常，在执行一个长时间任务的时候，我们期望能够中断任务的执行，因此，返回```DownloadFileTask```并做如下修改：

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());

        for (var i = 0; i < Integer.MAX_VALUE; i++)
            System.out.println("Downloading byte " + i);

        System.out.println("Download completed: " + Thread.currentThread().getName());
    }
}
```

* 返回主类，并做如下修改，我们希望1秒钟后下载任务的线程被中断：

```java
package me.ereach.concurrency;

public class Main01StartingThread {
    public static void main(String[] args) {
        Thread thread = new Thread(new DownloadFileTask());
        thread.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();
    }
}
```

* 执行上面代码，我们发现，下载线程并不会被中断，原因是```thread.interrupt()```只是标记线程的中断，并非直接中断执行的线程，因此我们还需要进一步修改下载线程的代码：

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());

        for (var i = 0; i < Integer.MAX_VALUE; i++) {
            if (Thread.currentThread().isInterrupted()) return;
            System.out.println("Downloading byte " + i);
        }

        System.out.println("Download completed: " + Thread.currentThread().getName());
    }
}
```

* 返回主类并继续执行我们将看到线程被正常中断；另一方面，如果一个线程正在sleeping，此时如果发送中断请求到线程，它将抛出一个异常，这也是sleep必须处理异常的原因。

### 并发问题

到目前为止，我们的下载任务是彼此孤立的。但是，在真是场景中，这些线程必须访问并修改相关的共享资源，例如：在下载过程中每个线程都必须报告该线程下载的字节数，针对这个被下载的对象，我们必须跟踪整个下载进程并随时报告给用户。此时，多个线程访问相同的数据对象可能导致一些问题，例如拿到错误的结果或应用程序直接崩溃，这种情况被称为“竞态条件(Race Condition)”；另一情况是当一个线程改变共享数据时，这个改变对其他线程而言时不可见的，这也是我们之前提到过的能见度问题。因此，在开发多线程代码的同时，我们必须明白并防止这些问题的发生，我们写的代码应该是线程安全的代码(Thread-safe Code)。另外，在Java文档中我们可以看到一些类是线程安全的，这也意味着这些类可以被多个线程安全的使用。

### 竞态条件(Race Conditions)

继续我们前面的例子(线程下载多个文件)，这一次，我们期望显示下载任务的字节总数，这个字节总数是一个共享的数据对象，而多线程竞争修改这个共享的数据资源，导致我们提到竞态条件问题。

* 返回项目，添加一个新类```DownloadStatus```，如下：

```java
package me.ereach.concurrency;

public class DownloadStatus {
    private int totalBytes;

    public int getTotalBytes() {
        return totalBytes;
    }

    public void incrementTotalBytes() {
        totalBytes++;
    }
}
```

* 返回```DownloadFileTask```并做如下更新(使用```DownloadStatus```记录相关的下载字节数)

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    private DownloadStatus status;

    public DownloadFileTask(DownloadStatus status) {
        this.status = status;
    }

    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());

        for (var i = 0; i < 10000; i++) {
            status.incrementTotalBytes();
        }

        System.out.println("Download completed: " + Thread.currentThread().getName());
    }
}
```

* 返回主类并作如下修改：

```java
package me.ereach.concurrency;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var status = new DownloadStatus();

        List<Thread> threads = new ArrayList<>();

        for (var i = 0; i < 5; i++) {
            var thread = new Thread(new DownloadFileTask(status));
            thread.start();
            threads.add(thread);
        }

        for (var thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(status.getTotalBytes());
    }
}
```

* 运行主类我们看到如下输出，而非期望的50000，这种情况就是所谓的静态条件:

```
Downloading a file: Thread-3
Downloading a file: Thread-4
Downloading a file: Thread-2
Downloading a file: Thread-1
Downloading a file: Thread-0
Download completed: Thread-3
Download completed: Thread-2
Download completed: Thread-4
Download completed: Thread-0
Download completed: Thread-1
15842

Process finished with exit code 0
```

造成上面问题的原因是多个线程竞争的去修改```DownloadStatus```对象中的```totalBytes```，这个操作包括三个步骤：

* 从主内存中读取```totalBytes```并把它存储到CPU
* 在CPU中完成增量操作
* 把新的```totalBytes```在次存储到主内存中

上面三步操作也被叫做非原子操作，因为它涉及多个步骤。作为对比，一个原子操作是不能被分割为多个步骤的。正是这种非原子操作导致了这个问题。

### 线程安全策略

Java编写线程安全的代码提供了一些策略，这些策略保证了编写的代码可在多线程间安全的执行，这些策略包括：

* 线程封闭(Thread Confinement)，最简单的线程安全策略，即不在线程间共享数据或限制每个线程访问它自己的数据，用前面的下载例子就是为每个线程提供它自己的```DownloadStatus```对象
* 不可变对象(Immutable/Unchangeable objects)，一个对象是不可变的，如果这个对象是日期时间或是数据，则当它被创建后它将不能再被改变。例如Java的String类，它就是不可变的，如果我们把它转换为大写的字符串对象，我们将得到一个新的字符串对象。所以在线程间共享不可变对象是一个不错的选择。
* 同步(Synchronization)，这一策略的目的是防止多线程访问相同的对象，通过协调器(coordinator)来保证任何时候访问该对象的线程只有一个，事实上这一过程是通过使用锁来完成的，同步强制代码按一定顺序执行，事实上，这有点反并发，而且实现同步是一个挑战也很容易出错，其中一个问题就是死锁(deadlock)，它发生于两个线程间的相互等待，它甚至引发程序崩溃，所以我们应尽量避免使用同步。
* 原子对象(Atomic Objects)，例如```AtomicInteger```该类运行我们不使用锁实现线程安全，针对```AtomicInteger```Java虚拟机可以实现一个原子级别的增量操作，它无需把前面的增量操作分解为三个更小的操作。
* 分区(Partitioning)把数据分区为不同的数据段以便不同线程并发的访问，Java提供了数种集合类支持使用分区，不同的线程访问不同的数据段。

### 线程封闭

这一节我们将使用线程封闭策略(Thread Confinement)防止竞态条件(Race Conditions)

* 返回```DownloadFileTask```类并更新代码如下：

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    private DownloadStatus status;

    public DownloadFileTask() {
        this.status = new DownloadStatus();
    }

    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());

        for (var i = 0; i < 10000; i++) {
            status.incrementTotalBytes();
        }

        System.out.println("Download completed: " + Thread.currentThread().getName());
    }

    public DownloadStatus getStatus() {
        return status;
    }
}
```

* 返回主类我们将做如下修改：

```java
package me.ereach.concurrency;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Thread> threads = new ArrayList<>();
        List<DownloadFileTask> tasks = new ArrayList<>();

        for (var i = 0; i < 5; i++) {
            var task = new DownloadFileTask();
            tasks.add(task);

            var thread = new Thread(task);
            thread.start();
            threads.add(thread);
        }

        for (var thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        var totalBytes = tasks.stream()
                .map(t -> t.getStatus().getTotalBytes())
                .reduce(Integer::sum);

        System.out.println(totalBytes);
    }
}
```

* 运行主类代码，我们总可以得到“50000”一致的结果：

```
Downloading a file: Thread-1
Downloading a file: Thread-3
Downloading a file: Thread-2
Downloading a file: Thread-0
Downloading a file: Thread-4
Download completed: Thread-1
Download completed: Thread-2
Download completed: Thread-4
Download completed: Thread-3
Download completed: Thread-0
Optional[50000]

Process finished with exit code 0
```

### 锁(Locks)

多线程情况下，另防止竞态条件和能见度问题的策略是防止线程同时访问一个对象，这种策略叫做同步(Synchronization)，我们实现同步的方式是使用锁，我们使用锁以阻止我们的代码在同一时刻运行于同一对象上，这种情况是由JVM来保证的，这也被称为临界区(Critical Section)，实际上就是让我们的代码顺序执行，这如同房间被锁住了，其他人要进入只能等待。

* 返回```DownloadStatus```并做如下修改：

```java
package me.ereach.concurrency;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DownloadStatus {
    private int totalBytes;
    private Lock lock = new ReentrantLock();

    public int getTotalBytes() {
        return totalBytes;
    }

    public void incrementTotalBytes() {
        lock.lock();
        try {
            totalBytes++;
        }
        finally {
            lock.unlock();
        }
    }
}
```

* 返回主类运行我们将得到“50000”一致的结果

### synchronized关键字

上一节我们通过```lock```和```unlock```实现了同步操作，这一节，我们将一种新的方法实现同步，就是```synchronized```关键字，通过这个关键字我们实现同样的操作，并且不再需要在程序中直言加锁和解锁，因此代码得到简化。

* 返回```DownloadStatus```类并做如下修改：

```java
package me.ereach.concurrency;

public class DownloadStatus {
    private int totalBytes;

    public int getTotalBytes() {
        return totalBytes;
    }

    public void incrementTotalBytes() {
        synchronized (this) {
            totalBytes++;
        }
    }
}
```

* 再次运行主类代码我们将得到一致的"50000"结果，上面的代码还可修改为下面的形式：

```java
package me.ereach.concurrency;

public class DownloadStatus {
    private int totalBytes;

    public int getTotalBytes() {
        return totalBytes;
    }

    public synchronized void incrementTotalBytes() {
        totalBytes++;
    }
}
```

### volatile关键字

Java给我们提供了另外一种方法编写线程安全的代码，而不需要我们前缀```synchronized```关键字，这就是```volatile```挥发关键字，它解决能见度问题但并不包括竞态条件，所以它不阻止两个线程同时修改一些数据。取而代之，当一个线程修改数据时，它确保另一个线程能够看到被修改的数据。

* 让我们再回到之前的```DownloadStatus```并做如下修改：

```java
package me.ereach.concurrency;

public class DownloadStatus {
    private boolean isDone;

    public synchronized boolean isDone() {
        return isDone;
    }

    public synchronized void done() {
        isDone = true;
    }

    private int totalBytes;

    public int getTotalBytes() {
        return totalBytes;
    }

    public void incrementTotalBytes() {
        totalBytes++;
    }
}
```

* 返回```DownloadFileTask``并做如下修改：

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    private DownloadStatus status;

    public DownloadFileTask(DownloadStatus status) {
        this.status = status;
    }

    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());

        for (var i = 0; i < 1000000; i++)
            status.incrementTotalBytes();

        status.done();

        System.out.println("Download completed: " + Thread.currentThread().getName());
    }

    public DownloadStatus getStatus() {
        return status;
    }
}
```

* 返回主类并做如下修改：

```java
package me.ereach.concurrency;

public class Main {
    public static void main(String[] args) {
        var status = new DownloadStatus();
        var thread1 = new Thread(new DownloadFileTask(status));
        var thread2 = new Thread(() -> {
            while (!status.isDone()) {}
            System.out.println(status.getTotalBytes());
        });

        thread1.start();
        thread2.start();
    }
}
```

* 运行上面代码我们看到如下结果：

```
Downloading a file: Thread-0
1000000
Download completed: Thread-0

Process finished with exit code 0
```

上面的代码由于```DownloadStatus```使用```synchronized```实现，对于并发来说这不是一件好事，因此我们建议下面的方法：

* 返回```DownloadStatus```移除代码中的```synchronized```关键字：

```java
package me.ereach.concurrency;

public class DownloadStatus {
    private boolean isDone;

    public boolean isDone() {
        return isDone;
    }

    public void done() {
        isDone = true;
    }

    private int totalBytes;

    public int getTotalBytes() {
        return totalBytes;
    }

    public void incrementTotalBytes() {
        totalBytes++;
    }
}
```

* 返回主类并运行代码我们看到代码由于无法看到isdone变化而无限等待，这是一个能见度问题，原因是CPU核心之间的缓存是无法在线程间看到的，除非写回主内存，为了解决这个问题我们需要把该属性声明为```volatile```(非稳定)，返回```DownloadStatus```并进行如下修改：

```java
package me.ereach.concurrency;

public class DownloadStatus {
    private volatile boolean isDone;

    public boolean isDone() {
        return isDone;
    }

    public void done() {
        isDone = true;
    }

    private int totalBytes;

    public int getTotalBytes() {
        return totalBytes;
    }

    public void incrementTotalBytes() {
        totalBytes++;
    }
}
```

* 完成上面修改后再次运行代码看到下面完全正确的结果：

```
Downloading a file: Thread-0
1000000
Download completed: Thread-0

Process finished with exit code 0
```

### 线程信号wait()和notify()

* 返回到前面的主类，看如下代码的```while```循环，当下载完成之前，这个循环将不断被执行并占用CPU资源：

```java
package me.ereach.concurrency;

public class Main {
    public static void main(String[] args) {
        var status = new DownloadStatus();
        var thread1 = new Thread(new DownloadFileTask(status));
        var thread2 = new Thread(() -> {
            while (!status.isDone()) {}
            System.out.println(status.getTotalBytes());
        });

        thread1.start();
        thread2.start();
    }
}
```

* 要解决上面代码循环的问题可以使用wait()和notify()这是Java对象都有的方法，wait将使线程进入sleep状态，直到其他线程的notify导致这个状态的改变，如下：

```java
package me.ereach.concurrency;

public class Main {
    public static void main(String[] args) {
        var status = new DownloadStatus();
        var thread1 = new Thread(new DownloadFileTask(status));
        var thread2 = new Thread(() -> {
            while (!status.isDone()) {
                synchronized (status) {
                    try {
                        status.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println(status.getTotalBytes());
        });

        thread1.start();
        thread2.start();
    }
}
```

* 返回```DownloadFileTask``并做如下修改：

```java
package me.ereach.concurrency;

public class DownloadFileTask implements Runnable {
    private DownloadStatus status;

    public DownloadFileTask(DownloadStatus status) {
        this.status = status;
    }

    @Override
    public void run() {
        System.out.println("Downloading a file: " + Thread.currentThread().getName());

        for (var i = 0; i < 1000000; i++)
            status.incrementTotalBytes();

        status.done();
        
        synchronized (status) {
            status.notifyAll();
        }

        System.out.println("Download completed: " + Thread.currentThread().getName());
    }

    public DownloadStatus getStatus() {
        return status;
    }
}
```

* 运行上面主类代码我们看到下面结果：

```
Downloading a file: Thread-0
1000000
Download completed: Thread-0

Process finished with exit code 0
```

回顾上面的例子：

* 为解决能见度问题，我们针对```DownloadStatus```中的属性```isDone```前增加了```volatile```关键字，使该属性在CPU缓存中的更新可以被技术的回写到主内存。
* 主类中的空循环会大量占用CPU时间片，为避免此种情况我们加入了```wait()```方法，让线程进入sleep暂停状态
* 而在```DownloadFileTask```类则在完成状态修改后，通过```notifyAll（）```方法通知线程重新进入状态检查

### 原子对象(Atomic Objects)

另一种实现线程安全的方法使使用Java的原子类(atomic classes in java.util.concurrent.atomic)，这些类包括:

* AtomicBoolean
* AtomicInteger
* AtomicIntegerArray
* 等待

使用这些类我们可以执行原子操作，比如之前我们实现的增量操作:

* 返回```DownloadStatus```类，并做如下修改：

```java
package me.ereach.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class DownloadStatus {
    private volatile boolean isDone;
    private AtomicInteger totalBytes = new AtomicInteger();

    public boolean isDone() {
        return isDone;
    }

    public void done() {
        isDone = true;
    }

    public int getTotalBytes() {
        return totalBytes.get();
    }

    public void incrementTotalBytes() {
        totalBytes.incrementAndGet();
    }
}
```

* 完成上面修改后，运行主类我可以看到下面结果：

```
Downloading a file: Thread-0
1000000
Download completed: Thread-0

Process finished with exit code 0
```

### 加法器

Java的原子对象用于实现计数器是很好的选择，但是，如果有多个线程频繁的更新一个值，那么，我们有一个比原子对象更快更好的选择，就是如下的加法器：LongAdder、DoubleAdder

* 返回```DownloadStatus```类并做如下代码更新：

```java
package me.ereach.concurrency;

import java.util.concurrent.atomic.LongAdder;

public class DownloadStatus {
    private volatile boolean isDone;
    private LongAdder totalBytes = new LongAdder();

    public boolean isDone() {
        return isDone;
    }

    public void done() {
        isDone = true;
    }

    public int getTotalBytes() {
        return totalBytes.intValue();
    }

    public void incrementTotalBytes() {
        totalBytes.increment();
    }
}
```

* 返回主类并再次运行我们看到如下正确的结果：

```
Downloading a file: Thread-0
1000000
Download completed: Thread-0

Process finished with exit code 0
```

### 同步集合(Synchronized Collections)

所谓同步集合是指在多线程之间共享一个集合。

* 返回主类并编写如下代码：

```java
package me.ereach.concurrency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Main02SyncCollections {
    public static void main(String[] args) {
        Collection<Integer> collection = new ArrayList<>();

        var thread1 = new Thread(() -> {
            collection.addAll(Arrays.asList(1, 2, 3));
        });

        var thread2 = new Thread(() -> {
            collection.addAll(Arrays.asList(4, 5, 6));
        });

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(collection);
    }
}
```

* 运行上面代码我们会得到下面类似的随机结果，或```[4, 5, 6]```：

```
[1, 2, 3]

Process finished with exit code 0
```

* 导致上面情况的原因是竞态条件(race condition)，对此我们需要使用```synchronizedCollection```，它返回一个线程安全的集合对象，如下：

```java
package me.ereach.concurrency;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Main02SyncCollections {
    public static void main(String[] args) {
        Collection<Integer> collection = Collections.synchronizedCollection(new ArrayList<>());

        var thread1 = new Thread(() -> {
            collection.addAll(Arrays.asList(1, 2, 3));
        });

        var thread2 = new Thread(() -> {
            collection.addAll(Arrays.asList(4, 5, 6));
        });

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(collection);
    }
}
```

* 再次运行该主类我们得到下面结果：

```
[1, 2, 3, 4, 5, 6]

Process finished with exit code 0
```

### 并发集合(Concurrent Collections)

如上一节，同步集合通过使用锁来实现线程安全，当一个线程访问同步集合时，整个集合都将被枷锁其他线程必须等待。针对大多数情况，这种方式除了性能方面没有什么负面影响！针对性能的影响，Java提供了并发集合(Concurrent Collections)以改善性能，它们包括：ConcurrentHashMap、ConcurrentLinkedQueue等等集合类支持：

* 下面的例子：

```java
package me.ereach.concurrency;

import java.util.HashMap;
import java.util.Map;

public class Main03ConCollections {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "a");
        System.out.println(map.get(1));
        map.remove(1);
    }
}
```

* 上面例子在非多线程情况下没有任何问题，但在多线程情况下则面临很多问题，下面代码就是解决方案：

```java
package me.ereach.concurrency;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Main03ConCollections {
    public static void main(String[] args) {
        Map<Integer, String> map = new ConcurrentHashMap<>();

        map.put(1, "a");
        System.out.println(map.get(1));
        map.remove(1);
    }
}
```

* 多线程情况下，上面代码在没有更多修改的情况下其性能将得到改善，原因是```Map```只是一个接口，而```HashMap```和```ConcurrentHashMap```都是它的两不同的实现。

### 总结

* 多线程(Multi-Threading)应用能在更少的时间内做更多的事情
* 多线程应用关键是要解决竞态条件(Race Condition)问题，否则我们会得到错误的结果或导致应用程序崩溃
* 能见度问题(Visibility Problems)是多线程应用需要处理的另一个问题，针对这个问题我们有几个策略：
  * 线程封闭(Thread Confinement)，目的是不在线程间共享数据
  * 线程同步(Synchronization)，它通过枷锁来保证线程的执行顺序，应尽量避免
  * 原子类型(Atomic Type)，使用原子类型而尽量避免使用锁
  * 数据分区(Partitioning)
  * 并发集合(Concurrent Collections)
