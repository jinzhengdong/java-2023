package me.ereach;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input your name: ");
        String name = scanner.next();
        System.out.println("Hello, " + name);
        System.out.print("Input your age: ");
        int age = scanner.nextInt();
        System.out.println("You are " + age + " years old.");
        System.out.print("Please input PI: ");
        double pi = scanner.nextDouble();
        System.out.println("Your pi = " + pi);
    }
}
