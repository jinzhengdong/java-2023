package com.xinhua;

public class Square extends Geometry {
    private float s;

    public Square(float s) {
        this.s = s;
    }

    public float getS() {
        return s;
    }

    public void setS(float s) {
        this.s = s;
    }

    @Override
    double getArea() {
        return s * s;
    }

    @Override
    double getPerimeter() {
        return s * 4;
    }
}
