package com.xinhua.l20231222pm2springmvc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoCtrl {
    @GetMapping("/hello")
    public String sayHello() {
        return "Hello, Spring";
    }
}
