package com.ynxinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyTest {
    @Test
    public void test04() {
        Set<Student> set = new HashSet<>();

        set.add(new Student("Jerry", "male", "202213"));
        set.add(new Student("Tom", "male", "202213"));

        set.stream().forEach(x -> System.out.println(x));
    }

    @Test
    public void test03() {
        List<Person> people = new ArrayList<>();

        Person person = new Person("Jerry", "male");
        Person person1 = new Person("Jerry", "male");

        people.add(new Person("Tom", "male"));
        people.add(new Person("Wendy", "female"));
        people.add(new Person("Wendy", "female"));
        people.add(person);
        people.add(person);
        people.add(person1);

        people.stream().forEach(x -> System.out.println(x));
    }

    @Test
    public void test02() {
        Set<Person> people = new HashSet<>();

        Person person = new Person("Jerry", "male");
        Person person1 = new Person("Jerry", "male");

        people.add(new Person("Tom", "male"));
        people.add(new Person("Wendy", "female"));
        people.add(new Person("Wendy", "female"));
        people.add(person);
        people.add(person);
        people.add(person1);

        people.stream().forEach(x -> System.out.println(x));
    }

    @Test
    public void test01() {
        Set<String> set = new HashSet<>();

        set.add("Apple");
        set.add("Banana");
        set.add("Apple");

        System.out.println(set);

        Set<Integer> set1 = new HashSet<>();

        set1.add(12);
        set1.add(17);
        set1.add(12);

        System.out.println(set1);
    }
}
