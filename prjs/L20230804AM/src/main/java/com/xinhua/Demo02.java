package com.xinhua;

import java.sql.*;

public class Demo02 {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // 加载数据库驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 建立数据库连接
            String url = "jdbc:mysql://localhost:3306/sakila?serverTimezone=Asia/Shanghai";
            String user = "root";
            String password = "123456";

            conn = DriverManager.getConnection(url, user, password);
            // 创建Statement对象
            stmt = conn.createStatement();
            // 执行查询语句
            String sql = "select * from actor";
            rs = stmt.executeQuery(sql);
            // 处理结果集
            while (rs.next()) {
                int actorId = rs.getInt("actor_id");
                String fname = rs.getString("first_name");
                String lname = rs.getString("last_name");
                System.out.println("actorId=" + actorId + ", fname=" + fname + ", lname=" + lname);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
