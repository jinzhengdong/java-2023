package com.xinhua;

import java.util.Arrays;

public class Demo4 {
    public static void main(String[] args) {
        byte[] b = {1, 2, 3};

        System.out.println(b);

        System.out.println(Arrays.toString(b));

        for (var x : b)
            System.out.println(x);

        System.out.println("求和");

        int t = 0;
        for (var x : b)
            t = t + x;

        System.out.println(t);

        System.out.println("===");

        int[] i = new int[3];
        i[0] = 10;
        i[1] = 9;
        i[2] = 18;

        System.out.println(i);
        System.out.println(Arrays.toString(i));

        for (var x : i)
            System.out.println(x);

        System.out.println("求和");

        int t2 = 0;
        for (var x : i)
            t2 = t2 + x;

        System.out.println(t2);
    }
}
