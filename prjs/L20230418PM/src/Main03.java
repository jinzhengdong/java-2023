public class Main03 {
    public static void main(String[] args) {
        //            0123456789012
        String str = "Hello, World.";
        int len = str.length();
        System.out.println(len);

        System.out.println(str.charAt(5));
        System.out.println(str.charAt(12));
        System.out.println(str.charAt(1));

        System.out.println(str.substring(7));
        System.out.println(str.substring(2));
        System.out.println(str.substring(3, 10));
    }
}
