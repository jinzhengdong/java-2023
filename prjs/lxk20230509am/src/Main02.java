import java.util.Scanner;

public class Main02 {
    public static void main(String[] args) {
        while(true){
            Scanner scanner = new Scanner(System.in);
            System.out.print("请输入贷款金额() : ");
            double 贷款金额 = scanner.nextDouble();
            System.out.print("请输入年利率");
            double 年利率 = scanner.nextDouble()/100.0;
            System.out.print("请输入贷款日期");
            int 贷款日期 = scanner.nextInt();

            int 贷款 = 贷款日期*12;
            double 月利率 = 年利率/12.0;
            double 月还款 = (月利率*贷款金额)/(1-Math.pow(1+月利率,-贷款));
            double 月利息 = 月还款*贷款;
            double 总利率 = 月利息-贷款金额;

            System.out.printf("每月产生利息为：%.2f\n",月还款);
            System.out.printf("总利率为：%.2f\n",总利率);
        }
    }
}
