package me.ereach;

public class Main {
    public static void main(String[] args) {
        Point2D p1 = new Point2D(2, 3);
        Point2D p2 = new Point2D(5, 7);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p1.getDistance(p2));
    }
}
