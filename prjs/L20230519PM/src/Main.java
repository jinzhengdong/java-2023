import me.ereach.Point;

public class Main {
    public static void main(String[] args) {
        Point p1 = new Point();
        Point p2 = new Point(3, 5);

        System.out.println(p1);
        System.out.println(p2);

//        System.out.println(p1.x);
//        System.out.println(p2.y);

        System.out.println(p1.getX());
        p1.setX(7);
        System.out.println(p1.getX());

        System.out.println(p2.getY());
    }
}