## 面向对象编程（OOP）

面向对象编程是Java的核心特性之一。在学习面向对象编程时，需要掌握以下几个方面：

### 类和对象

类和对象是面向对象编程的基本概念。类是描述对象的模板，而对象是类的一个实例。以下是Java类和对象的示例：

java

复制代码

```java
//类的定义
public class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}

//对象的创建和使用
Person person = new Person("Tom", 18);
String name = person.getName();
int age = person.getAge();
```

### 封装

封装是面向对象编程的一个重要特性，可以隐藏对象的实现细节。Java中可以使用访问修饰符来控制类中的属性和方法的访问权限。以下是Java封装的示例：

java

复制代码

```java
public class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
```

### 继承

继承是面向对象编程的另一个重要特性，可以通过继承父类来获得父类的属性和方法。Java中使用关键字`extends`来实现继承。以下是Java继承的示例：

java

复制代码

```java
//父类
public class Animal {
    public void eat() {
        System.out.println("Animal eat");
    }
}

//子类
public class Dog extends Animal {
    public void bark() {
        System.out.println("Dog bark");
    }
}

//创建对象
Dog dog = new Dog();
dog.eat();
dog.bark();
```

### 多态

多态是面向对象编程的另一个重要特性，可以让不同的子类对象调用相同的父类方法时产生不同的结果。在Java中，多态可以通过方法重写和方法重载来实现。以下是Java多态的示例：

java

复制代码

```java
//父类
public class Animal {
    public void eat() {
        System.out.println("Animal eat");
    }
}

//子类
public class Dog extends Animal {
    @Override
    public void eat() {
        System.out.println("Dog eat");
    }

    public void bark() {
        System.out.println("Dog bark");
    }
}

//多态示例
Animal animal = new Dog();
animal.eat();
```

### 接口

接口是面向对象编程中的另一个重要概念，可以用来描述对象的行为。Java中使用关键字`interface`来定义接口，使用关键字`implements`来实现接口。以下是Java接口的示例：

java

复制代码

```java
//接口
public interface Flyable {
    void fly();
}

//实现接口
public class Bird implements Flyable {
    @Override
    public void fly() {
        System.out.println("Bird fly");
    }
}
```

## 总结

以上是面向对象编程（OOP）的一些知识点和示例，需要不断地掌握和练习，以便在后续的Java开发中更加熟练地使用面向对象编程。