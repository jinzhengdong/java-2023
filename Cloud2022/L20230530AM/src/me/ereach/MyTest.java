package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(2, 3);
        System.out.println(p1 == p2);
        System.out.println(p1.equals(p2));
    }

    @Test
    public void test02() {
        java.awt.Point p1 = new java.awt.Point(2, 3);
        java.awt.Point p2 = new java.awt.Point(2, 3);
        System.out.println(p1 == p2);
        System.out.println(p1.equals(p2));
    }

    @Test
    public void test01() {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(2, 3);
        Point p3 = p1;
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p1 == p2);
        System.out.println(p3 == p1);
    }
}
