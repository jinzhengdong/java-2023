package me.ereach;

public class Circle {
    private Point o;
    private float r;

    public Circle(Point o, float r) {
        this.o = o;
        this.r = r;
    }

    public Point getO() {
        return o;
    }

    public void setO(Point o) {
        this.o = o;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "o=" + o +
                ", r=" + r +
                '}';
    }
}
