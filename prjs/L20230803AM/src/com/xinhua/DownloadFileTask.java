package com.xinhua;

public class DownloadFileTask implements Runnable {
    private DownloadStatus status;

    public DownloadFileTask(DownloadStatus status) {
        this.status = status;
    }

    @Override
    public void run() {
        System.out.println("Download a file: " + Thread.currentThread().getName());

        for (var i = 0; i < 10000; i++) {
            status.incrementTotalBytes();
        }

        System.out.println("Download a file: " + Thread.currentThread().getName());
    }
}
