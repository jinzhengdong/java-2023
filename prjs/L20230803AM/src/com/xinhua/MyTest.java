package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MyTest {
    @Test
    public void test05() {
        String value = null;
        Optional<String> optional = Optional.ofNullable(value);
        String result = optional.orElse("Default");
        System.out.println(result);
    }

    @Test
    public void test04() {
        String value = null;

        Optional<String> optional = Optional.ofNullable(value);
        System.out.println(optional);
    }

    @Test
    public void test03() {
        String value = "Hello";
        Optional<String> optional = Optional.of(value);
        System.out.println(optional);
        System.out.println(optional.get());
        System.out.println(optional.isPresent());
        System.out.println(optional.isEmpty());
    }

    @Test
    public void test02() {
        DownloadStatus[] status = new DownloadStatus[3];
        status[0] = new DownloadStatus();
        status[1] = new DownloadStatus();
        status[2] = new DownloadStatus();

        List<Thread> threads = new ArrayList<>();

        for (var i = 0; i < 3; i++) {
            var thread = new Thread(new DownloadFileTask(status[i]));

            thread.start();
            threads.add(thread);
        }

        for (var thread : threads) {
            try {
                thread.join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(status[0].getTotalBytes());
        System.out.println(status[1].getTotalBytes());
        System.out.println(status[2].getTotalBytes());
    }

    @Test
    public void test01() {
        var status = new DownloadStatus();

        List<Thread> threads = new ArrayList<>();

        for (var i = 0; i < 3; i++) {
            var thread = new Thread(new DownloadFileTask(status));

            thread.start();
            threads.add(thread);
        }

        for (var thread : threads) {
            try {
                thread.join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(status.getTotalBytes());
    }
}
