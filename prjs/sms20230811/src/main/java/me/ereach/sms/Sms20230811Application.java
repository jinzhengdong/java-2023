package me.ereach.sms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("me.ereach.sms.mapper")
@SpringBootApplication
public class Sms20230811Application {

    public static void main(String[] args) {
        SpringApplication.run(Sms20230811Application.class, args);
    }

}
