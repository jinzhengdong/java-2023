package com.xinhua.myset;

import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class MyTest {
    public void test02() {
        Set<String> set = new LinkedHashSet<>();

        set.add("Apple");
        set.add("Apple");
        set.add("Cherry");
        set.add("pear");

        System.out.println(set);

        set.remove("Cherry");
        System.out.println(set);
    }

    @Test
    public void test01() {
        Set<String> set = new HashSet<>();

        set.add("Apple");
        set.add("Apple");
        set.add("Cherry");
        set.add("pear");

        System.out.println(set);

        set.remove("Cherry");
        System.out.println(set);
    }
}
