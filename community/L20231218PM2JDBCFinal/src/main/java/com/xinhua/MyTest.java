package com.xinhua;

import org.junit.jupiter.api.Test;

import java.sql.*;

public class MyTest {
    @Test
    public void test01() {
        // JDBC URL, 用户名和密码
        String jdbcURL = "jdbc:mysql://localhost:3306/world";
        String username = "root";
        String password = "123456";
        // 车辆信息
        String name = "Shanghai";
        String countryCode = "AFG";
        String district = "Shanghai";
        int population = 24150000;
        // 加载JDBC驱动
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        // 创建数据库连接
        try (Connection connection = DriverManager.getConnection(jdbcURL, username, password)) {
            // SQL语句
            String sql = "INSERT INTO city (Name, CountryCode, District, Population) VALUES (?, ?, ?, ?)";
            // 创建PreparedStatement
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                // 设置参数
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, countryCode);
                preparedStatement.setString(3, district);
                preparedStatement.setInt(4, population);
                // 执行插入操作
                int rowsAffected = preparedStatement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("数据插入成功！");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
