public class Main06Array {
    public static void main(String[] args) {
        int[] intArray = new int[5];
        System.out.println(intArray);

        intArray[0] = 3;
        intArray[1] = 7;
        intArray[2] = 5;
        intArray[3] = 9;
        intArray[4] = 13;
        intArray[5] = 17;

        for (int i = 0; i < intArray.length; i++)
            System.out.println(intArray[i]);

//        for (int i = 0; i < intArray.length; i++)
//            intArray[i] = i;
//
//        for (int i = 0; i < intArray.length; i++)
//            System.out.println(intArray[i]);

    }
}
