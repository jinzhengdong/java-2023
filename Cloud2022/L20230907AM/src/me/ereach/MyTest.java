package me.ereach;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyTest {
    @Test
    public void test04() {
        try (FileReader reader = new FileReader("d:\\readmeaa.txt")) {
            int character;
            while ((character = reader.read()) != -1) {
                System.out.print((char) character);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("\nRead file finished.");
        }
    }

    @Test
    public void test03() {
        try {
            FileReader reader = new FileReader("d:\\readme.txt");
            int character;
            while ((character = reader.read()) != -1) {
                System.out.print((char) character);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test02() {
        FileReader reader = null;

        try {
            reader = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        int read = 0;

        while (read >= 0) {
            try {
                read = reader.read();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            System.out.println(read);
        }

        try {
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void test01() throws IOException {
        var reader = new FileReader("d:\\readme.txt");

        int read = 0;

        while (read >= 0) {
            read = reader.read();
            System.out.println(read);
        }

        reader.close();
    }
}
