package me.ereach;

import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyTest {
    @Test
    public void test05() {
        List<Movie> movies = List.of(
                new Movie("a", 10, Genre.THRILLER),
                new Movie("b", 15, Genre.ACTION),
                new Movie("c", 20, Genre.ACTION)
        );

        var rst = movies.stream()
                .collect(Collectors.partitioningBy(
                        m -> m.getLikes() <= 15,
                        Collectors.mapping(Movie::getTitle,
                                Collectors.joining(", "))));

        System.out.println(rst);
    }

    @Test
    public void test04() {
        List<Movie> movies = List.of(
                new Movie("a", 10, Genre.THRILLER),
                new Movie("b", 15, Genre.ACTION),
                new Movie("c", 20, Genre.ACTION)
        );

        var rst = movies.stream()
                .collect(Collectors.groupingBy(Movie::getGenre));

        System.out.println(rst);
    }

    @Test
    public void test03() {
        var movies = List.of(
                new Movie("a", 10),
                new Movie("b", 20),
                new Movie("c", 30)
        );

        boolean rst = movies.stream()
                .anyMatch(m -> m.getLikes() > 20);
        System.out.println(rst);

        rst = movies.stream()
                .allMatch(m -> m.getLikes() > 20);
        System.out.println(rst);
        System.out.println("===");
        var result = movies.stream()
                .findFirst()
                .get();
        System.out.println(result.getTitle());
        System.out.println("===");
        result = movies.stream()
                .findAny()
                .get();
        System.out.println(result.getTitle());
        System.out.println("===");
        Optional<Integer> sum = movies.stream()
                .map(Movie::getLikes)
                .reduce(Integer::sum);
        System.out.println(sum.orElse(0));

        System.out.println("===toList");
        var rst1 = movies.stream()
                .collect(Collectors.toList());

        rst1.forEach(x -> System.out.println("Movie { title: " + x.getTitle() + ", likes: " + x.getLikes() + " }"));
        System.out.println("===");

        rst1.stream().forEach(x -> System.out.println(x.getTitle()));
        System.out.println("===toHashMap");
        var rst4 = movies.stream()
                .collect(Collectors.toMap(Movie::getTitle, m -> m));

        System.out.println(rst4);
        System.out.println("===");
        System.out.println(rst4.get("a").getTitle());
    }

    @Test
    public void test02() {
        var movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        movies.stream()
                .filter(m -> m.getLikes() > 10)
                .peek(m -> System.out.println("Filtered: " + m.getTitle()))
                .map(Movie::getTitle)
                .peek(t -> System.out.println("Mapped: " + t))
                .forEach(System.out::println);
    }

    @Test
    public void test01() {
        var stream = Stream.generate(() -> Math.random());
        stream.forEach(n -> System.out.println(n));
    }
}
