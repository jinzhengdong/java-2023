package me.ereach;

public enum Genre {
    COMEDY,
    ACTION,
    THRILLER
}
