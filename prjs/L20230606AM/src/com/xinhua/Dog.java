package com.xinhua;

public class Dog extends Animal implements Behavior {
    @Override
    void sound() {
        System.out.println("wang wang wang");
    }

    @Override
    public void walk() {
        System.out.println("dog walking.");
    }
}
