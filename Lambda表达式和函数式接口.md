# 第四章：Lambda表达式和函数式接口

## 4.1 Lambda表达式的基本概念

Lambda表达式是Java 8引入的一种新的语法特性，用于简化函数式编程的代码编写。它可以将函数作为一种方法参数传递，或者用于定义函数式接口的实现。Lambda表达式的基本语法如下：

```
(parameter list) -> {lambda body}
```

其中，参数列表指定了Lambda表达式的输入参数，箭头符号"->"将参数列表和Lambda表达式的主体部分分隔开来，Lambda主体部分包含了具体的代码实现。

### 4.1.1 Lambda表达式的定义和语法

Lambda表达式的定义和语法包括以下几个要点：

- 参数列表：Lambda表达式可以有零个或多个参数，多个参数之间用逗号分隔。参数的类型可以显式声明，也可以根据上下文进行推断。
- 箭头符号：Lambda表达式的箭头符号"->"将参数列表和Lambda主体部分分隔开来。
- Lambda主体：Lambda主体部分可以是一个表达式，也可以是一段代码块。如果是表达式，则可以直接返回结果；如果是代码块，则需要使用大括号包围，并且需要使用return语句返回结果。

下面是一些Lambda表达式的示例：

- 无参数的Lambda表达式：`() -> System.out.println("Hello, Lambda!");`
- 单个参数的Lambda表达式：`(int x) -> x * x`
- 多个参数的Lambda表达式：`(int x, int y) -> x + y`
- 带有返回值的Lambda表达式：`(int x, int y) -> { return x + y; }`

### 4.1.2 Lambda表达式的优势和用途

Lambda表达式的优势和用途主要体现在以下几个方面：

- 简洁性：Lambda表达式可以大大简化代码的编写，尤其是对于函数式接口的使用，可以将复杂的代码逻辑用更简洁的方式表达。
- 可读性：Lambda表达式可以使代码更加清晰易读，尤其是对于一些简单的函数式操作，可以直接在Lambda表达式中表达出来，避免了繁琐的匿名内部类的编写。
- 并行处理：Lambda表达式可以方便地进行并行处理，通过使用Stream API结合Lambda表达式，可以实现更高效的并行计算。

Lambda表达式的应用场景非常广泛，包括但不限于以下几个方面：

- 集合操作：Lambda表达式可以方便地对集合进行遍历、过滤、映射等操作，简化了集合操作的代码编写。
- 事件处理：Lambda表达式可以简化事件处理的代码编写，特别是在GUI应用程序中，可以直接将事件处理逻辑以Lambda表达式的形式传递给事件监听器。
- 并行计算：Lambda表达式结合Stream API可以方便地进行并行计算，提高了计算效率。

## 4.2 函数式接口的介绍

函数式接口是Java中的一种特殊接口，它只包含一个抽象方法。函数式接口的引入是为了支持Lambda表达式的使用。函数式接口的定义和特点如下：

### 4.2.1 函数式接口的定义和特点

函数式接口的定义非常简单，只需要使用`@FunctionalInterface`注解来标记接口，并确保接口中只有一个抽象方法。函数式接口的特点包括：

- 只有一个抽象方法：函数式接口只能有一个抽象方法，但可以有多个默认方法或静态方法。
- Lambda表达式的支持：函数式接口可以被Lambda表达式直接赋值，从而简化代码的编写。
- 方法引用的支持：函数式接口可以被方法引用直接引用，进一步简化代码的编写。

### 4.2.2 常见的函数式接口及其使用方法

Java 8提供了一些常见的函数式接口，用于支持Lambda表达式的使用。以下是一些常见的函数式接口及其使用方法：

- `Consumer<T>`：接收一个参数并返回void，用于消费一个值。
- `Supplier<T>`：不接收参数，返回一个值，用于提供一个值。
- `Function<T, R>`：接收一个参数并返回一个结果，用于转换一个值。
- `Predicate<T>`：接收一个参数并返回一个布尔值，用于判断一个值是否满足某个条件。
- `UnaryOperator<T>`：接收一个参数并返回一个结果，用于对一个值进行一元操作。
- `BinaryOperator<T>`：接收两个参数并返回一个结果，用于对两个值进行二元操作。

使用这些函数式接口可以简化代码的编写，例如：

```java
Consumer<String> print = (str) -> System.out.println(str);
print.accept("Hello, Lambda!");

Supplier<Integer> random = () -> new Random().nextInt(100);
int num = random.get();

Function<Integer, String> convert = (num) -> String.valueOf(num);
String str = convert.apply(123);

Predicate<Integer> isEven = (num) -> num % 2 == 0;
boolean result = isEven.test(10);

UnaryOperator<Integer> square = (num) -> num * num;
int result = square.apply(5);

BinaryOperator<Integer> add = (a, b) -> a + b;
int result = add.apply(2, 3);
```

### 4.2.3 自定义函数式接口

除了使用Java提供的函数式接口，我们还可以自定义函数式接口来满足特定的需求。自定义函数式接口的步骤如下：

1. 使用`@FunctionalInterface`注解标记接口，确保接口只有一个抽象方法。
2. 定义接口的抽象方法，根据需求确定参数和返回类型。
3. 可选地定义默认方法或静态方法，以提供额外的功能。

例如，我们可以自定义一个函数式接口`Calculator`，用于执行数学运算：

```java
@FunctionalInterface
interface Calculator {
    int calculate(int a, int b);
}

// 使用自定义函数式接口
Calculator add = (a, b) -> a + b;
int result = add.calculate(2, 3);
```

## 4.3 Lambda表达式的应用

Lambda表达式在Java中的应用非常广泛，特别是在集合操作、并行计算和简化事件处理方面。下面将分别介绍这三个方面的应用。

### 4.3.1 使用Lambda表达式进行集合操作

Lambda表达式可以方便地对集合进行遍历、过滤、映射等操作，大大简化了集合操作的代码编写。以下是一些常见的集合操作示例：

- 遍历集合：

```java
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
names.forEach(name -> System.out.println(name));
```

- 过滤集合：

```java
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
List<Integer> evenNumbers = numbers.stream()
                                   .filter(n -> n % 2 == 0)
                                   .collect(Collectors.toList());
```

- 映射集合：

```java
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
List<Integer> nameLengths = names.stream()
                                 .map(name -> name.length())
                                 .collect(Collectors.toList());
```

### 4.3.2 使用Lambda表达式进行并行计算

Lambda表达式结合Stream API可以方便地进行并行计算，提高了计算效率。以下是一个并行计算的示例：

```java
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
int sum = numbers.parallelStream()
                 .reduce(0, (a, b) -> a + b);
```

在上面的示例中，使用`parallelStream()`方法将集合转换为并行流，然后使用`reduce()`方法对集合中的元素进行求和操作。

## 4.4 方法引用和构造器引用

方法引用和构造器引用是Lambda表达式的一种简化写法，用于直接引用已经存在的方法或构造器。方法引用和构造器引用可以使代码更加简洁和易读。下面将分别介绍方法引用和构造器引用的基本概念和用法，以及它们之间的区别和选择。

### 4.4.1 方法引用的基本概念和用法

方法引用是Lambda表达式的一种简化写法，用于直接引用已经存在的方法。方法引用的基本语法如下：

```
方法引用的类型::方法名
```

其中，方法引用的类型可以是以下几种：

- 静态方法引用：`类名::静态方法名`
- 实例方法引用：`实例::实例方法名`
- 对象方法引用：`类名::实例方法名`

以下是一些方法引用的示例：

```java
// 静态方法引用
Function<Integer, String> toString = Integer::toString;
String str = toString.apply(123);  // 输出："123"

// 实例方法引用
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
names.forEach(System.out::println);  // 输出："Alice" "Bob" "Charlie"

// 对象方法引用
StringJoiner joiner = new StringJoiner(", ");
names.forEach(joiner::add);
String result = joiner.toString();  // 输出："Alice, Bob, Charlie"
```

在上面的示例中，我们使用了不同类型的方法引用。静态方法引用直接引用了`Integer`类的`toString`方法，实例方法引用直接引用了`System.out`对象的`println`方法，对象方法引用直接引用了`StringJoiner`对象的`add`方法。

### 4.4.2 构造器引用的基本概念和用法

构造器引用是Lambda表达式的一种简化写法，用于直接引用已经存在的构造器。构造器引用的基本语法如下：

```
构造器引用的类型::new
```

其中，构造器引用的类型是要引用的构造器所属的类。

以下是一个构造器引用的示例：

```java
Function<String, Integer> toInteger = Integer::new;
Integer number = toInteger.apply("123");  // 输出：123
```

在上面的示例中，我们使用构造器引用直接引用了`Integer`类的构造器，将字符串转换为整数。

### 4.4.3 方法引用和构造器引用的区别和选择

方法引用和构造器引用都是Lambda表达式的简化写法，它们之间的区别和选择主要取决于以下几个因素：

- 引用的方法或构造器的参数列表和返回类型。
- Lambda表达式的参数列表和返回类型。
- 代码的可读性和简洁性。

通常情况下，如果Lambda表达式的主体部分只是简单地调用一个已经存在的方法或构造器，且参数列表和返回类型与Lambda表达式的参数列表和返回类型一致，那么可以考虑使用方法引用或构造器引用来简化代码。

## 4.5 Lambda表达式的注意事项和最佳实践

Lambda表达式是一种强大的语法特性，但在使用时需要注意一些事项，并遵循一些最佳实践，以确保代码的性能、可读性和维护性。下面将分别介绍Lambda表达式的性能考虑、可读性和维护性，以及适用场景和限制。

### 4.5.1 Lambda表达式的性能考虑

在使用Lambda表达式时，需要考虑以下几个性能方面的因素：

- 创建对象：Lambda表达式会创建一个匿名内部类的实例，因此会产生对象的创建和销毁开销。在频繁调用的场景中，可以考虑将Lambda表达式提取为静态变量或方法引用，以避免重复创建对象。
- 方法引用 vs Lambda表达式：方法引用通常比Lambda表达式更高效，因为方法引用直接引用已经存在的方法，而不需要创建额外的对象。在性能要求较高的场景中，可以优先考虑使用方法引用。

### 4.5.2 Lambda表达式的可读性和维护性

为了提高代码的可读性和维护性，可以遵循以下几个最佳实践：

- 明确Lambda表达式的参数类型：在Lambda表达式的参数列表中明确指定参数的类型，可以使代码更加清晰易懂。
- 使用大括号和return语句：对于复杂的Lambda表达式，可以使用大括号包围Lambda主体部分，并使用return语句返回结果，以提高代码的可读性。
- 添加注释：对于复杂的Lambda表达式，可以添加适当的注释，解释Lambda表达式的用途和实现逻辑，方便他人理解和维护代码。

### 4.5.3 Lambda表达式的适用场景和限制

Lambda表达式适用于以下场景：

- 函数式接口：Lambda表达式主要用于函数式接口的实现，可以简化函数式接口的使用。
- 集合操作：Lambda表达式可以方便地对集合进行遍历、过滤、映射等操作，简化了集合操作的代码编写。
- 并行计算：Lambda表达式结合Stream API可以方便地进行并行计算，提高了计算效率。

Lambda表达式的一些限制包括：

- 不能修改非final的局部变量：Lambda表达式中引用的局部变量必须是final或等效于final的（即实际上不可修改）。
- 不能引用this和super关键字：Lambda表达式中不能直接引用this和super关键字，因为它们与Lambda表达式的上下文不一致。

## 4.6 Lambda表达式和函数式接口开发实际应用

Lambda表达式和函数式接口的组合可以帮助我们简化代码，提高开发效率。下面将介绍两个实战案例，分别是使用Lambda表达式实现排序算法和使用Lambda表达式和函数式接口进行并发编程。

### 4.6.1 Lambda表达式实现排序算法

排序算法是计算机科学中的经典问题，我们可以使用Lambda表达式和函数式接口来实现不同的排序算法。以下是一个使用Lambda表达式实现快速排序算法的示例：

```java
import java.util.Arrays;
import java.util.Comparator;

public class SortingExample {
    public static void main(String[] args) {
        Integer[] numbers = {5, 2, 8, 1, 9, 3};

        // 使用Lambda表达式实现快速排序
        Arrays.sort(numbers, (a, b) -> a - b);

        // 输出排序结果
        System.out.println(Arrays.toString(numbers));  // 输出：[1, 2, 3, 5, 8, 9]
    }
}
```

在上面的示例中，我们使用`Arrays.sort`方法对整数数组进行排序，通过Lambda表达式 `(a, b) -> a - b` 实现了快速排序算法。Lambda表达式中的比较逻辑 `(a, b) -> a - b` 表示按照升序排序。

### 4.6.2 Lambda表达式和函数式接口进行并发编程

并发编程是现代应用程序开发中的重要问题，我们可以使用Lambda表达式和函数式接口结合Java的并发工具来简化并发编程。以下是一个使用Lambda表达式和函数式接口进行并发编程的示例：

```java
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConcurrencyExample {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(5);

        // 使用Lambda表达式提交任务
        for (int i = 0; i < 10; i++) {
            final int taskId = i;
            executor.submit(() -> {
                System.out.println("Task " + taskId + " is running.");
            });
        }

        // 关闭线程池
        executor.shutdown();
    }
}
```

在上面的示例中，我们使用`ExecutorService`和`Executors.newFixedThreadPool`创建一个固定大小的线程池，然后使用Lambda表达式提交任务。Lambda表达式 `() -> { System.out.println("Task " + taskId + " is running."); }` 表示要执行的任务逻辑。

通过使用Lambda表达式和函数式接口，我们可以简化并发编程的代码，提高代码的可读性和维护性。

## 4.7 Lambda表达式各应用场景

Lambda表达式在Java中有多种应用场景，可以简化代码的编写，提高开发效率。以下是一些Lambda表达式各应用场景的示例：

1. 集合操作：

```java
List<String> names = Arrays.asList("Alice", "Bob", "Charlie");

// 遍历集合
names.forEach(name -> System.out.println(name));

// 过滤集合
List<String> filteredNames = names.stream()
                                  .filter(name -> name.length() > 4)
                                  .collect(Collectors.toList());

// 映射集合
List<Integer> nameLengths = names.stream()
                                 .map(name -> name.length())
                                 .collect(Collectors.toList());
```

2. 文件操作：

```java
// 读取文件内容
try (BufferedReader reader = new BufferedReader(new FileReader("file.txt"))) {
    String line;
    while ((line = reader.readLine()) != null) {
        System.out.println(line);
    }
} catch (IOException e) {
    e.printStackTrace();
}

// 写入文件内容
try (BufferedWriter writer = new BufferedWriter(new FileWriter("file.txt"))) {
    writer.write("Hello, World!");
} catch (IOException e) {
    e.printStackTrace();
}
```

3. GUI应用程序：

```java
Button button = new Button();

// 使用Lambda表达式简化事件处理
button.addActionListener(event -> {
    System.out.println("Button clicked!");
});
```

4. 并发编程：

```java
ExecutorService executor = Executors.newFixedThreadPool(5);

// 使用Lambda表达式提交任务
for (int i = 0; i < 10; i++) {
    final int taskId = i;
    executor.submit(() -> {
        System.out.println("Task " + taskId + " is running.");
    });
}

// 关闭线程池
executor.shutdown();
```

5. 函数式接口的实现：

```java
// 自定义函数式接口
@FunctionalInterface
interface Calculator {
    int calculate(int a, int b);
}

// 使用Lambda表达式实现函数式接口
Calculator addition = (a, b) -> a + b;
Calculator subtraction = (a, b) -> a - b;

// 调用函数式接口的方法
int result1 = addition.calculate(5, 3);  // 输出：8
int result2 = subtraction.calculate(5, 3);  // 输出：2
```

6. 方法引用

排序算法是计算机科学中的经典问题，我们可以使用Lambda表达式和函数式接口来实现不同的排序算法。以下是一个使用Lambda表达式实现基于用户积分进行排序的示例：

```java
import java.util.Arrays;
import java.util.Comparator;

public class SortingExample {
    public static void main(String[] args) {
        User[] users = {
            new User("Alice", "Female", 100),
            new User("Bob", "Male", 80),
            new User("Charlie", "Male", 120)
        };

        // 使用Lambda表达式实现基于用户积分的排序
        Arrays.sort(users, Comparator.comparingInt(User::getPoints));

        // 输出排序结果
        for (User user : users) {
            System.out.println(user);
        }
    }
}

class User {
    private String name;
    private String gender;
    private int points;

    public User(String name, String gender, int points) {
        this.name = name;
        this.gender = gender;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", points=" + points +
                '}';
    }
}
```

在上面的示例中，我们定义了一个`User`类，包含了姓名、性别和积分属性。然后，我们使用Lambda表达式和`Comparator.comparingInt`方法实现了基于用户积分的排序。Lambda表达式 `Comparator.comparingInt(User::getPoints)` 表示按照用户积分进行排序。

最后，我们输出排序结果，展示了按照用户积分升序排序的结果。

# 第五章：Streams

## 5.1 Streams简介

Streams是Java 8引入的一种新的数据处理方式，它提供了一种高效且易于使用的方式来处理集合数据。Streams可以用于对集合进行过滤、映射、排序、归约等操作，以及进行并行计算。

### 5.1.1 什么是Streams

Streams是一种用于处理集合数据的抽象概念。它可以看作是一种高级的迭代器，可以对集合中的元素进行一系列的操作，而不需要显式地使用循环来遍历集合。

Streams提供了一种流式的操作方式，可以将多个操作连接起来形成一个流水线，每个操作都会对流中的元素进行处理，并将处理结果传递给下一个操作。这种流水线式的操作方式使得代码更加简洁、易读，并且可以提高代码的可维护性和可扩展性。

### 5.1.2 Streams的优势

Streams具有以下几个优势：

- 简洁性：Streams提供了一种简洁的操作方式，可以将多个操作连接起来形成一个流水线，使代码更加简洁、易读。
- 可读性：Streams的操作方式更加直观，可以更清晰地表达出数据处理的逻辑，提高代码的可读性。
- 并行计算：Streams可以方便地进行并行计算，通过使用并行Streams，可以充分利用多核处理器的计算能力，提高计算效率。

Streams的引入使得Java的集合操作更加强大和灵活，可以更方便地进行数据处理和计算。

## 5.2 创建Streams

在使用Streams之前，我们首先需要创建一个Stream对象。Java 8提供了多种方式来创建Streams，包括从集合、数组、文件以及其他方式创建。下面将分步骤讲解每种创建方式，并给出相应的示例。

### 5.2.1 从集合创建Streams

我们可以使用`stream()`方法从一个集合中创建一个Stream对象。下面是一个从List集合创建Stream的示例：

```java
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamCreationExample {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Alice");
        names.add("Bob");
        names.add("Charlie");

        // 从List集合创建Stream
        Stream<String> stream = names.stream();

        // 输出Stream中的元素
        stream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先创建了一个List集合`names`，然后使用`stream()`方法从该集合中创建了一个Stream对象`stream`。最后，我们使用`forEach`方法遍历Stream中的元素，并输出每个元素的值。

输出结果为：

```
Alice
Bob
Charlie
```

### 5.2.2 从数组创建Streams

我们可以使用`Arrays.stream()`方法从一个数组中创建一个Stream对象。下面是一个从数组创建Stream的示例：

```java
import java.util.Arrays;
import java.util.stream.IntStream;

public class StreamCreationExample {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};

        // 从数组创建IntStream
        IntStream stream = Arrays.stream(numbers);

        // 输出Stream中的元素
        stream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先创建了一个整数数组`numbers`，然后使用`Arrays.stream()`方法从该数组中创建了一个IntStream对象`stream`。最后，我们使用`forEach`方法遍历IntStream中的元素，并输出每个元素的值。

输出结果为：

```
1
2
3
4
5
```

### 5.2.3 从文件创建Streams

我们可以使用`Files.lines()`方法从一个文件中创建一个Stream对象。下面是一个从文件创建Stream的示例：

```java
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class StreamCreationExample {
    public static void main(String[] args) {
        String fileName = "data.txt";

        // 从文件创建Stream
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            // 输出Stream中的元素
            stream.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

在上面的示例中，我们首先指定了一个文件名`data.txt`，然后使用`Files.lines()`方法从该文件中创建了一个Stream对象`stream`。最后，我们使用`forEach`方法遍历Stream中的元素，并输出每个元素的值。

假设文件`data.txt`中的内容如下：

```
Alice
Bob
Charlie
```

输出结果为：

```
Alice
Bob
Charlie
```

### 5.2.4 其他方式创建Streams

除了上述方式，我们还可以使用其他方式来创建Streams，例如使用`Stream.of()`方法创建一个包含指定元素的Stream对象，或者使用`Stream.iterate()`方法创建一个包含指定序列的Stream对象。下面是一个使用`Stream.of()`方法创建Stream的示例：

```java
import java.util.stream.Stream;

public class StreamCreationExample {
    public static void main(String[] args) {
        // 使用Stream.of()方法创建Stream
        Stream<String> stream = Stream.of("Alice", "Bob", "Charlie");

        // 输出Stream中的元素
        stream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们使用`Stream.of()`方法创建了一个包含三个字符串元素的Stream对象`stream`。最后，我们使用`forEach`方法遍历Stream中的元素，并输出每个元素的值。

输出结果为：

```
Alice
Bob
Charlie
```

## 5.3 中间操作

在Streams中，中间操作用于对流中的元素进行处理和转换，可以进行过滤、映射、排序、去重、截断等操作。下面将分步骤讲解每种中间操作，并给出相应的示例。

### 5.3.1 过滤

过滤操作用于根据指定的条件筛选流中的元素。我们可以使用`filter()`方法来进行过滤操作。下面是一个对整数流进行过滤的示例：

```java
import java.util.stream.IntStream;

public class IntermediateOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 10);

        // 过滤偶数
        IntStream filteredStream = stream.filter(n -> n % 2 == 0);

        // 输出过滤后的元素
        filteredStream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到9的整数流`stream`。然后，我们使用`filter()`方法对流中的元素进行过滤，只保留偶数。最后，我们使用`forEach`方法遍历过滤后的流，并输出每个元素的值。

输出结果为：

```
2
4
6
8
```

### 5.3.2 映射

映射操作用于对流中的元素进行转换。我们可以使用`map()`方法来进行映射操作。下面是一个对字符串流进行映射的示例：

```java
import java.util.stream.Stream;

public class IntermediateOperationsExample {
    public static void main(String[] args) {
        // 创建一个字符串流
        Stream<String> stream = Stream.of("Alice", "Bob", "Charlie");

        // 将字符串转换为大写
        Stream<String> mappedStream = stream.map(String::toUpperCase);

        // 输出转换后的元素
        mappedStream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`Stream.of()`方法创建了一个包含三个字符串元素的流`stream`。然后，我们使用`map()`方法对流中的元素进行映射，将字符串转换为大写形式。最后，我们使用`forEach`方法遍历映射后的流，并输出每个元素的值。

输出结果为：

```
ALICE
BOB
CHARLIE
```

### 5.3.3 排序

排序操作用于对流中的元素进行排序。我们可以使用`sorted()`方法来进行排序操作。下面是一个对整数流进行排序的示例：

```java
import java.util.stream.IntStream;

public class IntermediateOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.of(5, 2, 8, 1, 9, 3);

        // 对流中的元素进行排序
        IntStream sortedStream = stream.sorted();

        // 输出排序后的元素
        sortedStream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`IntStream.of()`方法创建了一个整数流`stream`，包含了一些无序的整数元素。然后，我们使用`sorted()`方法对流中的元素进行排序。最后，我们使用`forEach`方法遍历排序后的流，并输出每个元素的值。

输出结果为：

```
1
2
3
5
8
9
```

### 5.3.4 去重

去重操作用于去除流中的重复元素。我们可以使用`distinct()`方法来进行去重操作。下面是一个对字符串流进行去重的示例：

```java
import java.util.stream.Stream;

public class IntermediateOperationsExample {
    public static void main(String[] args) {
        // 创建一个字符串流
        Stream<String> stream = Stream.of("Alice", "Bob", "Alice", "Charlie");

        // 去重
        Stream<String> distinctStream = stream.distinct();

        // 输出去重后的元素
        distinctStream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`Stream.of()`方法创建了一个包含四个字符串元素的流`stream`，其中包含了一些重复的元素。然后，我们使用`distinct()`方法对流中的元素进行去重操作。最后，我们使用`forEach`方法遍历去重后的流，并输出每个元素的值。

输出结果为：

```
Alice
Bob
Charlie
```

### 5.3.5 截断

截断操作用于对流中的元素进行截断，只保留指定数量的元素。我们可以使用`limit()`方法来进行截断操作。下面是一个对整数流进行截断的示例：

```java
import java.util.stream.IntStream;

public class IntermediateOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 10);

        // 截断前3个元素
        IntStream truncatedStream = stream.limit(3);

        // 输出截断后的元素
        truncatedStream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到9的整数流`stream`。然后，我们使用`limit()`方法对流中的元素进行截断，只保留前3个元素。最后，我们使用`forEach`方法遍历截断后的流，并输出每个元素的值。

输出结果为：

```
1
2
3
```

### 5.3.6 其他中间操作

除了上述中间操作，Streams还提供了其他一些中间操作，例如`skip()`方法用于跳过指定数量的元素，`peek()`方法用于对流中的元素进行查看操作，`flatMap()`方法用于将流中的每个元素转换为一个新的流，并将所有新的流合并为一个流等等。下面是完整的演示示例：

```java
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class IntermediateOperationsExample {
    public static void main(String[] args) {
        // 示例数据
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie");
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        // skip操作：跳过前两个元素
        Stream<String> skipStream = names.stream().skip(2);
        skipStream.forEach(System.out::println);

        // peek操作：查看每个元素的长度
        Stream<String> peekStream = names.stream().peek(name -> System.out.println("Length: " + name.length()));
        peekStream.forEach(System.out::println);

        // flatMap操作：将每个元素转换为一个新的流，并将所有新的流合并为一个流
        Stream<Integer> flatMapStream = numbers.stream().flatMap(number -> Stream.of(number, number * 2));
        flatMapStream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先创建了两个示例数据，一个是包含三个字符串元素的`names`列表，另一个是包含五个整数元素的`numbers`列表。

然后，我们分别演示了`skip()`、`peek()`和`flatMap()`这三个中间操作的使用。

- `skip()`操作用于跳过指定数量的元素。在示例中，我们使用`skip(2)`跳过了前两个元素，输出结果为："Charlie"。
- `peek()`操作用于查看每个元素的值，可以在查看的同时进行其他操作。在示例中，我们使用`peek(name -> System.out.println("Length: " + name.length()))`查看每个元素的长度，并输出结果为："Alice"、"Bob"、"Charlie"和每个元素的长度。
- `flatMap()`操作用于将每个元素转换为一个新的流，并将所有新的流合并为一个流。在示例中，我们使用`flatMap(number -> Stream.of(number, number * 2))`将每个整数元素和它的两倍转换为一个新的流，并输出结果为：1、2、2、4、3、6、4、8、5、10。

## 5.4 终端操作

在Streams中，终端操作用于对流进行最终的处理，产生最终的结果或副作用。终端操作是Streams的最后一步操作，一旦执行了终端操作，流就会被消耗，无法再进行其他操作。下面将分步骤讲解每种终端操作，并给出相应的示例。

### 5.4.1 遍历

遍历操作用于对流中的元素进行遍历，并对每个元素执行指定的操作。我们可以使用`forEach()`方法来进行遍历操作。下面是一个对整数流进行遍历的示例：

```java
import java.util.stream.IntStream;

public class TerminalOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 5);

        // 遍历流中的元素
        stream.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到4的整数流`stream`。然后，我们使用`forEach()`方法遍历流中的元素，并使用`System.out.println`打印每个元素的值。

输出结果为：

```
1
2
3
4
```

### 5.4.2 匹配

匹配操作用于判断流中的元素是否满足指定的条件。我们可以使用`allMatch()`、`anyMatch()`和`noneMatch()`等方法来进行匹配操作。下面是一个对整数流进行匹配的示例：

```java
import java.util.stream.IntStream;

public class TerminalOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 5);

        // 判断是否所有元素都大于0
        boolean allMatch = stream.allMatch(n -> n > 0);
        System.out.println("All match: " + allMatch);

        // 判断是否存在元素大于3
        boolean anyMatch = stream.anyMatch(n -> n > 3);
        System.out.println("Any match: " + anyMatch);

        // 判断是否所有元素都不小于0
        boolean noneMatch = stream.noneMatch(n -> n < 0);
        System.out.println("None match: " + noneMatch);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到4的整数流`stream`。然后，我们使用`allMatch()`方法判断流中的所有元素是否都大于0，使用`anyMatch()`方法判断流中是否存在元素大于3，使用`noneMatch()`方法判断流中的所有元素是否都不小于0。最后，我们使用`System.out.println`输出匹配结果。

输出结果为：

```
All match: true
Any match: true
None match: true
```

### 5.4.3 收集

收集操作用于将流中的元素收集到一个集合或其他数据结构中。我们可以使用`collect()`方法来进行收集操作。下面是一个对字符串流进行收集的示例：

```java
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TerminalOperationsExample {
    public static void main(String[] args) {
        // 创建一个字符串流
        Stream<String> stream = Stream.of("Alice", "Bob", "Charlie");

        // 收集流中的元素到List集合中
        List<String> collectedList = stream.collect(Collectors.toList());

        // 输出收集结果
        collectedList.forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`Stream.of()`方法创建了一个包含三个字符串元素的流`stream`。然后，我们使用`collect(Collectors.toList())`方法将流中的元素收集到一个List集合`collectedList`中。最后，我们使用`forEach`方法遍历收集结果，并输出每个元素的值。

输出结果为：

```
Alice
Bob
Charlie
```

### 5.4.4 归约

归约操作用于将流中的元素进行合并或计算，得到一个最终的结果。我们可以使用`reduce()`方法来进行归约操作。下面是一个对整数流进行归约的示例：

```java
import java.util.stream.IntStream;

public class TerminalOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 5);

        // 对流中的元素进行求和
        int sum = stream.reduce(0, (a, b) -> a + b);
        System.out.println("Sum: " + sum);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到4的整数流`stream`。然后，我们使用`reduce(0, (a, b) -> a + b)`方法对流中的元素进行求和，初始值为0。最后，我们使用`System.out.println`输出归约结果。

输出结果为：

```
Sum: 10
```

### 5.4.5 统计

统计操作用于对流中的元素进行统计，例如计算元素的个数、求和、平均值、最大值和最小值等。我们可以使用`count()`、`sum()`、`average()`、`max()`和`min()`等方法来进行统计操作。下面是一个对整数流进行统计的示例：

```java
import java.util.stream.IntStream;

public class TerminalOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 5);

        // 统计流中的元素个数
        long count = stream.count();
        System.out.println("Count: " + count);

        // 求和
        int sum = IntStream.range(1, 5).sum();
        System.out.println("Sum: " + sum);

        // 平均值
        double average = IntStream.range(1, 5).average().orElse(0);
        System.out.println("Average: " + average);

        // 最大值
        int max = IntStream.range(1, 5).max().orElse(0);
        System.out.println("Max: " + max);

        // 最小值
        int min = IntStream.range(1, 5).min().orElse(0);
        System.out.println("Min: " + min);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到4的整数流`stream`。然后，我们使用`count()`方法统计流中的元素个数，使用`sum()`方法求和，使用`average()`方法计算平均值，使用`max()`方法找到最大值，使用`min()`方法找到最小值。最后，我们使用`System.out.println`输出统计结果。

输出结果为：

```
Count: 4
Sum: 10
Average: 2.5
Max: 4
Min: 1
```

### 5.4.6 其他终端操作

除了上述终端操作，Streams还提供了其他一些终端操作，例如`findFirst()`方法用于找到流中的第一个元素，`findAny()`方法用于找到流中的任意一个元素，`toArray()`方法用于将流中的元素转换为数组等等。这些终端操作可以根据具体的需求进行选择和使用。

下面是完整的演示示例：

```java
import java.util.stream.Stream;

public class OtherTerminalOperationsExample {
    public static void main(String[] args) {
        // 创建一个字符串流
        Stream<String> stream = Stream.of("Alice", "Bob", "Charlie");

        // 找到流中的第一个元素
        String firstElement = stream.findFirst().orElse(null);
        System.out.println("First element: " + firstElement);

        // 找到流中的任意一个元素
        String anyElement = Stream.of("Alice", "Bob", "Charlie")
                .findAny()
                .orElse(null);
        System.out.println("Any element: " + anyElement);

        // 将流中的元素转换为数组
        String[] array = Stream.of("Alice", "Bob", "Charlie")
                .toArray(String[]::new);
        System.out.println("Array: " + Arrays.toString(array));
    }
}
```

在上面的示例中，我们首先使用`Stream.of()`方法创建了一个包含三个字符串元素的流`stream`。然后，我们使用`findFirst()`方法找到流中的第一个元素，并使用`orElse(null)`处理可能的空值情况。接着，我们使用`findAny()`方法找到流中的任意一个元素，并使用`orElse(null)`处理可能的空值情况。最后，我们使用`toArray(String[]::new)`方法将流中的元素转换为数组，并使用`Arrays.toString()`方法将数组转换为字符串进行输出。

输出结果为：

```
First element: Alice
Any element: Alice
Array: [Alice, Bob, Charlie]
```

## 5.5 并行Streams

### 5.5.1 并行Streams的优势

并行Streams是指在处理数据时，将数据分成多个部分并行处理，以提高处理速度和效率。并行Streams可以充分利用多核处理器的计算能力，加快数据处理的速度。在某些情况下，使用并行Streams可以显著提高程序的性能。

并行Streams的优势包括：

- 加速处理：并行Streams可以同时处理多个数据块，加快处理速度。
- 充分利用多核处理器：并行Streams可以充分利用多核处理器的计算能力，提高计算效率。
- 简化编程：使用并行Streams可以简化并行计算的编程过程，无需手动管理线程和同步。

然而，并行Streams并不适用于所有情况，有时候串行Streams的性能可能更好。在选择使用并行Streams时，需要根据具体的情况进行评估和测试。

### 5.5.2 使用并行Streams

在Java中，使用并行Streams非常简单，只需要在创建Streams时调用`parallel()`方法即可将流转换为并行流。下面是一个使用并行Streams的示例：

```java
import java.util.stream.IntStream;

public class ParallelStreamsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 10);

        // 使用并行Streams进行处理
        stream.parallel()
                .forEach(System.out::println);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到9的整数流`stream`。然后，我们使用`parallel()`方法将流转换为并行流，接着使用`forEach()`方法遍历并行流中的元素，并使用`System.out.println`打印每个元素的值。

输出结果为：

```
1
2
3
4
5
6
7
8
9
```

需要注意的是，并行Streams的处理顺序可能与元素的顺序不一致，因为并行处理会同时处理多个数据块。如果需要保持元素的顺序，可以使用`forEachOrdered()`方法代替`forEach()`方法。

## 5.6 自定义操作

### 5.6.1 自定义中间操作

在Streams中，我们可以自定义中间操作来满足特定的需求。自定义中间操作可以通过实现`Stream`接口或使用`StreamSupport.stream()`方法来实现。下面是一个自定义中间操作的示例：

```java
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CustomIntermediateOperationExample {
    public static void main(String[] args) {
        // 创建一个自定义中间操作的流
        Stream<String> stream = Stream.of("Alice", "Bob", "Charlie")
                .filter(name -> name.length() > 4)
                .map(String::toUpperCase)
                .customOperation();

        // 输出流中的元素
        stream.forEach(System.out::println);
    }
}

class CustomOperationStream<T> implements Stream<T> {
    private final Stream<T> stream;

    public CustomOperationStream(Stream<T> stream) {
        this.stream = stream;
    }

    @Override
    public Stream<T> filter(java.util.function.Predicate<? super T> predicate) {
        return stream.filter(predicate);
    }

    @Override
    public <R> Stream<R> map(java.util.function.Function<? super T, ? extends R> mapper) {
        return stream.map(mapper);
    }

    // 自定义中间操作
    public Stream<T> customOperation() {
        Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(stream.iterator(), 0);
        return StreamSupport.stream(spliterator, false)
                .map(this::performCustomOperation);
    }

    private T performCustomOperation(T element) {
        // 自定义操作逻辑
        return element;
    }

    // 其他Stream接口方法的实现...
}
```

在上面的示例中，我们首先创建了一个包含三个字符串元素的流`stream`，然后使用`filter()`方法对流中的元素进行过滤，使用`map()`方法对流中的元素进行映射。接着，我们使用自定义的中间操作`customOperation()`对流进行自定义操作。最后，我们使用`forEach`方法遍历流中的元素，并输出每个元素的值。

输出结果为：

```
ALICE
CHARLIE
```

### 5.6.2 自定义终端操作

在Streams中，我们也可以自定义终端操作来满足特定的需求。自定义终端操作可以通过实现`Stream`接口或使用`StreamSupport.stream()`方法来实现。下面是一个自定义终端操作的示例：

```java
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CustomTerminalOperationExample {
    public static void main(String[] args) {
        // 创建一个自定义终端操作的流
        Stream<String> stream = Stream.of("Alice", "Bob", "Charlie")
                .filter(name -> name.length() > 4)
                .map(String::toUpperCase)
                .customOperation();

        // 执行自定义终端操作
        stream.customTerminalOperation();
    }
}

class CustomOperationStream<T> implements Stream<T> {
    private final Stream<T> stream;

    public CustomOperationStream(Stream<T> stream) {
        this.stream = stream;
    }

    @Override
    public Stream<T> filter(java.util.function.Predicate<? super T> predicate) {
        return stream.filter(predicate);
    }

    @Override
    public <R> Stream<R> map(java.util.function.Function<? super T, ? extends R> mapper) {
        return stream.map(mapper);
    }

    // 其他Stream接口方法的实现...

    // 自定义终端操作
    public void customTerminalOperation() {
        Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(stream.iterator(), 0);
        StreamSupport.stream(spliterator, false)
                .forEach(this::performCustomTerminalOperation);
    }

    private void performCustomTerminalOperation(T element) {
        // 自定义操作逻辑
        System.out.println(element);
    }
}
```

在上面的示例中，我们首先创建了一个包含三个字符串元素的流`stream`，然后使用`filter()`方法对流中的元素进行过滤，使用`map()`方法对流中的元素进行映射。接着，我们使用自定义的终端操作`customTerminalOperation()`对流进行自定义操作。最后，我们执行自定义的终端操作，并输出每个元素的值。

输出结果为：

```
ALICE
CHARLIE
```

## 5.7 Streams的注意事项和最佳实践

### 5.7.1 避免过多的中间操作

在使用Streams时，应避免过多的中间操作。过多的中间操作可能会导致性能下降，因为每个中间操作都会产生一个新的流对象，增加了对象的创建和销毁的开销。应该尽量将多个中间操作合并为一个操作，以减少流对象的创建和销毁。

下面是一个示例，展示了避免过多中间操作的方法：

```java
import java.util.stream.IntStream;

public class AvoidExcessiveIntermediateOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 10);

        // 避免过多的中间操作
        int sum = stream.filter(n -> n % 2 == 0)
                .map(n -> n * 2)
                .sum();

        System.out.println("Sum: " + sum);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到9的整数流`stream`。然后，我们使用`filter()`方法对流中的元素进行过滤，只保留偶数。接着，我们使用`map()`方法对流中的元素进行映射，将每个元素乘以2。最后，我们使用`sum()`方法求和，得到最终的结果。

输出结果为：

```
Sum: 40
```

### 5.7.2 谨慎使用并行Streams

并行Streams可以提高处理速度和效率，但并不是在所有情况下都适用。在使用并行Streams时，需要谨慎评估和测试，确保并行处理能够带来性能的提升。

并行Streams适用于以下情况：

- 处理大量数据：并行Streams可以充分利用多核处理器的计算能力，加快处理速度。
- 处理独立的数据：并行Streams适用于处理独立的数据，每个数据之间没有依赖关系。

然而，并行Streams可能不适用于以下情况：

- 处理小规模数据：并行Streams的创建和管理会带来一定的开销，对于小规模数据，串行Streams可能更快。
- 处理有序数据：并行Streams的处理顺序可能与元素的顺序不一致，如果需要保持元素的顺序，应使用串行Streams。

下面是一个使用并行Streams的示例：

```java
import java.util.stream.IntStream;

public class UseParallelStreamsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 10);

        // 使用并行Streams进行处理
        int sum = stream.parallel()
                .filter(n -> n % 2 == 0)
                .map(n -> n * 2)
                .sum();

        System.out.println("Sum: " + sum);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到9的整数流`stream`。然后，我们使用`parallel()`方法将流转换为并行流，接着使用`filter()`方法对流中的元素进行过滤，使用`map()`方法对流中的元素进行映射。最后，我们使用`sum()`方法求和，得到最终的结果。

输出结果为：

```
Sum: 40
```

### 5.7.3 充分利用终端操作

在Streams中，终端操作是最后一步操作，会触发流的处理并产生最终的结果。在使用Streams时，应充分利用终端操作，避免在中间操作中进行复杂的逻辑处理。

下面是一个示例，展示了充分利用终端操作的方法：

```java
import java.util.stream.IntStream;

public class MakeFullUseOfTerminalOperationsExample {
    public static void main(String[] args) {
        // 创建一个整数流
        IntStream stream = IntStream.range(1, 10);

        // 充分利用终端操作
        long count = stream.filter(n -> n % 2 == 0)
                .peek(System.out::println)
                .count();

        System.out.println("Count: " + count);
    }
}
```

在上面的示例中，我们首先使用`IntStream.range()`方法创建了一个包含1到9的整数流`stream`。然后，我们使用`filter()`方法对流中的元素进行过滤，只保留偶数。接着，我们使用`peek()`方法查看每个元素的值，并使用`System.out.println`打印每个元素的值。最后，我们使用`count()`方法统计流中的元素个数，得到最终的结果。

输出结果为：

```
2
4
6
8
Count: 4
```

