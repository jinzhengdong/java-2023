package com.xinhua;

public interface Geometry2D {
    double getArea();
    double getPerimeter();
}
