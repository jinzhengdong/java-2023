public class Lesson2 {
    public static void main(String[] args) {
        int rst = add(3, 5);
        System.out.println(rst);

        rst = add(11, 35);
        System.out.println(rst);


        rst = sub(11, 3);
        System.out.println(rst);

        rst = sub(17, 4);
        System.out.println(rst);

    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {
        return a - b;
    }
}
