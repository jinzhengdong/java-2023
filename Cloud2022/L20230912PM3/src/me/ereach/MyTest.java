package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        GenericList<Integer> list1 = new GenericList<>();
        list1.add(1);
        list1.add(2);
        System.out.println(list1);

        GenericList<Double> list2 = new GenericList<>();
        list2.add(2.34d);
        list2.add(3.14d);
        System.out.println(list2);

        GenericList<Number> list3 = new GenericList<>();
        list3.add(12f);
        list3.add(12d);
        System.out.println(list3);
    }

    @Test
    public void test02() {
        GList<String> slist = new GList<>();
        slist.add("Hello");
        slist.add("World");
        System.out.println(slist);

        GList<Integer> iList = new GList<>();
        iList.add(1);
        iList.add(2);
        System.out.println(iList);
    }

    @Test
    public void test01() {
        ObjList list = new ObjList();

        list.add(1);
        list.add(2);

        list.add("Hello");
        list.add("World");

        System.out.println(list);
    }
}
