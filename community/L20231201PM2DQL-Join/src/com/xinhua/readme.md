```sql
-- 找出每个国家(country)的名称及大洲名称以及它的首都名称。
-- left 左
-- outer 外部的, inner 内部的
-- join 加入
select
	a.name,
    a.Continent,
	a.Capital,
    b.Name
from
	country as a
	left outer join city as b on a.Capital = b.ID; -- outer 保证 country 是完整的数据
    
select
	a.name,
    a.Continent,
	a.Capital,
    b.Name
from
	country as a
	left join city as b on a.Capital = b.ID; -- left outer join == left join
    
select
	a.name,
    a.Continent,
	a.Capital,
    b.Name
from
	country as a
	join city as b on a.Capital = b.ID; -- left outer join == left join
    

select
	a.name,
    a.Continent,
	a.Capital,
    b.Name
from
	country as a
	inner join city as b on a.Capital = b.ID; -- inner 保证 country, city 匹配的数据
    
select
	a.name,
    a.Continent,
	a.Capital,
    b.Name
from
	city as b
	right join country as a on a.Capital = b.ID; -- right
    
-- 找出每个国家的名称、所属的大洲、所用的官方语言
select
	a.name,
    a.Continent,
    b.Language
from
	country as a
    left join countrylanguage as b on a.Code = b.CountryCode and b.IsOfficial = true;
    
-- 找出每个国家的名称，所属的大洲，以及该国家的城市名称和城市人口
select
	a.name,
    a.continent,
    b.name,
    b.population
from
	country as a
    left join city as b on a.code = b.countrycode;
```