package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test04() {
        GenericList<String> genericList = new GenericList<>();
        genericList.add("Tod");
        genericList.add("Jerry");
        System.out.println(genericList);

        GenericList<Integer> genericList1 = new GenericList<>();
        genericList1.add(1);
        genericList1.add(2);
        System.out.println(genericList1);

        GenericList<User> genericList2 = new GenericList<>();
        genericList2.add(new User("Tod", "male"));
        genericList2.add(new User("wendy", "female"));
        System.out.println(genericList2);

    }

    @Test
    public void test03() {
        ObjList objList = new ObjList();

        objList.add(1);
        objList.add(new User("Tod", "male"));
        objList.add(new java.awt.Point(2, 3));
        objList.add("Hello, World!");

        for (int i = 0; i < objList.getCount(); i++) {
            System.out.println(objList.get(i));
        }
    }

    @Test
    public void test02() {
        UserList userList = new UserList();

        userList.add(new User("Jerry", "male"));
        userList.add(new User("Tod", "male"));

        System.out.println(userList);

        for (int i = 0; i < userList.getCount(); i++) {
            User user = userList.get(i);
            System.out.println(user);
        }
    }

    @Test
    public void test01() {
        List list = new List();

        list.add(1);
        list.add(2);

        System.out.println(list);
    }
}
