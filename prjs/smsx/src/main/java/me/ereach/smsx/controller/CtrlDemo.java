package me.ereach.smsx.controller;

import me.ereach.smsx.model.Subject;
import me.ereach.smsx.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CtrlDemo {
    @Autowired
    private SubjectService subjectService;

    @GetMapping("/sayhello")
    public String sayHello() {
        return "Hello, World.";
    }

    @GetMapping("/svcsubject")
    public List<Subject> getSvcSubject() {
        return subjectService.getSubjectList();
    }
}
