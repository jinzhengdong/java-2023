public class Main02 {
    public static void main(String[] args) {
        int num = 11;

        if (num % 2 == 0)
            System.out.println("是偶数");
        else
            System.out.println("是奇数");

        String alph = "E";

        if (alph == "a" || alph == "e" || alph == "i" || alph == "o" || alph == "u" || alph == "A" || alph == "E" ||
                alph == "I" || alph == "O" || alph == "U")
            System.out.println("是原音");
        else
            System.out.println("是辅音");
    }
}
