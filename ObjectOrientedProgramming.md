# 面向对象编程

前面的Java编程语言课程我们完成了从```过程化编程```到```函数式编程```的过度，从本节开始我们将介绍面向对象的编程过程。

## 开始

在开始学习Java面向对象的编程之前，我们应该掌握以下Java相关的编程知识：

* 数据类型
  * 原生数据类型：```byte short int long float double boolean char```
  * 引用类似：```Byte Short Integer Long Float Double Boolean String```
* 变量
  * 静态变量、实例变量、局部变量
* 流程控制
  * 条件语句：```if else```
  * 循环语句：```for loop```, ```while loop```, ```do while loop```
* 方法
  * 静态方法、实例方法

## 编程范式

所谓编程范式就是编程的模式、风格和方法，它包括：过程、函数、面向对象等。不同的编程范式之间没有绝对的好坏之分，事实上，各种编程方法常常混搭在一起使用。

## 面向对象编程的优势

* 降低的编程的复杂度
* 使代码更容易维护
* 代码有更好的可重用性
* 可以获得更高的开发效率

## 学习内容

* class 类，包括：Attributes(属性)、Constructors(构造函数)、Getters/Setters(获取/设置)、方法重载(Overloading)和重写(Overriding)
  * Encapsulation 封装
  * Abstraction 抽象
  * Inheritance 继承
  * Polymophism 多态
  * Interfaces 接口

## 类

* 创建类```Point```并更新为如下代码：

```java
package me.ereach;

public class Point {
    // 属性
    private float x;
    private float y;

    // 构成方法
    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    // 构造方法重载
    public Point() {
        x = 0;
        y = 0;
    }

    // getters & setters
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    // 重写父类(Object)的toString()方法
    @Override
    public String toString() {
        return "Point {" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    // 实例方法计算两个点之间的距离
    public double getDistance(Point other) {
        float w = this.x - other.x;
        float h = this.y - other.y;
        return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2));
    }
}
```

* 添加```Main```类测试```Point```类：

```java
package me.ereach;

import me.ereach.oop.Point;

public class Main {
  public static void main(String[] args) {
    Point point1 = new Point(2, 3);
    Point point2 = new Point(5, 7);
    System.out.println(point1);
    System.out.println(point2);
    System.out.println(point1.getDistance(point2));
  }
}
```

* 运行```main```函数可看到下面结果：

```
Point {x=2.0, y=3.0}
Point {x=5.0, y=7.0}
5.0

Process finished with exit code 0
```

### 属性

类中的变量也叫做类的属性或字段，对于公共属性我们可以通过类的实例进行访问，私有属性则通过getter和setter进行访问设置，如上面Point类包含下面两个私有属性：

* ```private float x;```
* ```private float y;```

### 构造函数

构造函数是与类同名且没有返回值的特殊方法，它可以有参数也可以没有参数，它也可以重载为多个版本，查看前面的Point类，我们可以看到两个版本的构造函数，下面是它们的声明：

* ```public Point(float x, float y)```
* ```public Point()```

构造函数是创建类实例时调用的特殊函数，假如你的类没有提供构造函数，那么Java编译器会为你的类创建一个缺省的构造函数，构造函数用于初始化类属性和设置必要的缺省值。

### getters和setters

在Java的类设计中，getters和setters被频繁使用，它们用于设置和访问类的属性，参考Point例子中的相关部分。

### 实例方法

Java类中，通过类实例进行访问的方法叫实例方法，例如Point类中的getDistance，它只能通过类实例进行访问计算两个点之间的距离

### 静态方法

在类中，通过关键字static修饰的方法，可以通过类名被直接访问，即使这个类没有生成任何实例，看下面例子：

* 创建类MyUtils并添加如下代码：

```java
package me.ereach;

public class MyUtils {
    public static double getCircleArea(float r) {
        return Math.PI * r * r;
    }

    public static double getCirclePerimeter(float r) {
        return Math.PI * r * 2;
    }
}
```

* 修改MyTest类对上面静态函数进行测试：

```java
package me.ereach;

import me.ereach.oop.MyUtils;
import org.junit.Test;

public class MyTest {
  @Test
  public void testMyUtils() {
    System.out.println(MyUtils.getCircleArea(7));
    System.out.println(MyUtils.getCirclePerimeter(7));
  }
}
```

* 运行测试代码我们看到如下输出：

```
153.93804002589985
43.982297150257104

Process finished with exit code 0
```

### 方法重载

所谓方法重载(overload)就是在同一个类中定义的多个同名方法，只要这些方法具备不同类型的参数或不同类型的返回值。Point例子中的构造函数就提供了两个不同的版本。

### 方法重写

Java类在缺省情况下都继承了Object类，当在子类中创建的方法与父类的方法具有相同的参数和返回值类型时，这个过程就叫作方法重写。
如Point类中的```toString```和```equals```就是针对Object类中的两个同名方法进行重写。

### 对象比较

* 创建一个```MyTest```类，添加并运行如下代码：

```java
package me.ereach;

import me.ereach.oop.Point;
import org.junit.Test;

public class MyTest {
  @Test
  public void testRefObjects() {
    Point point1 = new Point(2, 3);
    Point point2 = new Point(2, 3);
    System.out.println(point1.equals(point2));  // false
  }
}
```

* 上面代码输出```false```不是我们期望的结果，为此，返回Point类并重写父类的equals方法，如下：

```java
package me.ereach;

public class Point {
    // 属性
    private float x;
    private float y;

    // 构成方法
    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    // 构造方法重载
    public Point() {
        x = 0;
        y = 0;
    }

    // getters & setters
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    // 重写父类(Object)的toString()方法
    @Override
    public String toString() {
        return "Point {" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    // 重写父类的比较方法
    @Override
    public boolean equals(Object o) {
        if (o instanceof Point) {
            Point pt = (Point)o;
            return (x == pt.x) && (y == pt.y);
        }
        return super.equals(o);
    }

    // 实例方法计算两个点之间的距离
    public double getDistance(Point other) {
        float w = this.x - other.x;
        float h = this.y - other.y;
        return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2));
    }
}
```

* 完成Point类更新后返回MyTest类运行testRefObjects方法我们将看到期望的结果true

### 引用类型

* 创建Circle类并添加如下代码：

```java
package me.ereach;

public class Circle {
    private float r;

    public Circle(float r) {
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public double getArea() {
        return Math.PI * r * r;
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
```

* 返回MyTest类并更新测试代码如下：

```java
package me.ereach;

import me.ereach.oop.Circle;
import org.junit.Test;

public class MyTest {
  @Test
  public void testRefObjects() {
    Circle circle1 = new Circle(3);     // 创建新Circle对象（实例）circle1, Heap
    Circle circle2 = new Circle(3);     // 创建新Circle对象（实例）circle2, Heap
    Circle circle3 = circle1;           // 声明circle3是对circle1的引用, Stack

    System.out.println(circle1);        // me.ereach.oop.Circle@33e5ccce
    System.out.println(circle2);        // me.ereach.oop.Circle@5a42bbf4
    System.out.println(circle3);        // me.ereach.oop.Circle@33e5ccce 与 circle1 相同（引用）

    System.out.println(circle1 == circle2);     // false
    System.out.println(circle1 == circle3);     // true
  }
}
```

### 内存管理

Java管理着两种类型的内存区域：

* Heap，堆，它用于存放对象，这些对象包括你创建的类以及类的实例
* Stack，栈，它用于存放原生数据类型以及生命周期比较短的变量，另外，引用堆对象的变量也存储在这里。
* Java的内存是自动管理的，因此，不必像C++一样担心内存的回收
* 当Java在完成方法执行时，那些堆栈中的变量将被立即移除 
* 那些在堆中创建的对象，一个后台进程将会监视这些堆中被创建的对象，如果一个对象在一个相当的时间段内没有被使用，这个进程会把这个对象从堆中移除，这个过程叫做垃圾收集“Garbage Collection”这个组件也叫做垃圾收集器“Garbage Collector”

## 组合

在面向对象编程技术中，所谓组合并不是新概念，正如我们的类中使用的其它引用类型，如：String, Integer, Double等等的使用，下面我们通过之前创建的Point类来实现我们自己的组合类：

* 创建三角形类```Triangle```并组合我们的Point类，如下：

```java
package me.ereach;

import me.ereach.oop.Point;

public class Triangle {
  // 借助Point类构建我们三角形的三个定点
  private Point a;
  private Point b;
  private Point c;

  // 构造函数
  public Triangle(Point a, Point b, Point c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  // getters & setters
  public Point getA() {
    return a;
  }

  public void setA(Point a) {
    this.a = a;
  }

  public Point getB() {
    return b;
  }

  public void setB(Point b) {
    this.b = b;
  }

  public Point getC() {
    return c;
  }

  public void setC(Point c) {
    this.c = c;
  }

  // 重写父类toString方法
  @Override
  public String toString() {
    return "Triangle{" +
            "a=" + a +
            ", b=" + b +
            ", c=" + c +
            '}';
  }

  // 计算三角形周长
  public double getPerimeter() {
    return b.getDistance(c) + a.getDistance(c) + a.getDistance(b);
  }

  // 计算三角形面积
  public double getArea() {
    double p = getPerimeter() / 2;
    double la = b.getDistance(c);
    double lb = a.getDistance(c);
    double lc = a.getDistance(b);

    return Math.sqrt(p * (p - la) * (p - lb) * (p - lc));
  }
}
```

* 创建Triangle的测试方法如下：

```java
package me.ereach;

import me.ereach.oop.Point;
import me.ereach.oop.Triangle;
import org.junit.Test;

public class MyTest {
  @Test
  public void testTriangle() {
    Point p1 = new Point(2, 3);
    Point p2 = new Point(11, 5);
    Point p3 = new Point(7, 13);
    Triangle t1 = new Triangle(p1, p2, p3);
    System.out.println(t1);
    System.out.println(t1.getArea());
    System.out.println(t1.getPerimeter());
  }
}
```

* 运行上面测试代码看到下面结果：

```
Triangle{a=Point {x=2.0, y=3.0}, b=Point {x=11.0, y=5.0}, c=Point {x=7.0, y=13.0}}
40.0
29.344156254790995

Process finished with exit code 0
```

## 继承

继承的目的之一是代码的重用，那个被继承的类也叫作：基类(Base Class)、超类(Super Class)、父类(Parent Class)；而那个继承类也叫作：子类(Sub Class, Child Class)

* 基于之前的Circle类使它继承Point类作为其圆心，并修改相关代码如下：

```java
package me.ereach;

import me.ereach.oop.Point;

// Circle继承Point
public class Circle extends Point {
  private float r;

  // 构造函数
  public Circle(float r) {
    super(0, 0);    // 初始化圆心
    this.r = r;
  }

  // 重载构造函数
  public Circle(float x, float y, float r) {
    super(x, y);    // 初始化圆心
    this.r = r;
  }

  // getters & setters
  public float getR() {
    return r;
  }

  public void setR(float r) {
    this.r = r;
  }

  // 计算圆面积
  public double getArea() {
    return Math.PI * r * r;
  }

  // 计算圆周长
  public double getPerimeter() {
    return Math.PI * r * 2;
  }

  // 重写toString
  @Override
  public String toString() {
    return "Circle{" +
            "r=" + r +
            ", o=" + super.toString() +
            '}';
  }
}
```

* 针对Circle更新MyTest的测试代码如下：

```java
package me.ereach;

import me.ereach.oop.Circle;
import org.junit.Test;

public class MyTest {
  @Test
  public void testCircle() {
    Circle c1 = new Circle(7);
    System.out.println(c1);
    System.out.println(c1.getArea());
    System.out.println(c1.getPerimeter());

    Circle c2 = new Circle(3, 7, 5);
    System.out.println(c2);
    System.out.println(c2.getArea());
    System.out.println(c2.getPerimeter());

    System.out.println(c1.getDistance(c2));
  }
}
```

* 运行上面测试代码看到如下输出：

```
Circle{r=7.0, o=Point {x=0.0, y=0.0}}
153.93804002589985
43.982297150257104
Circle{r=5.0, o=Point {x=3.0, y=7.0}}
78.53981633974483
31.41592653589793
7.615773105863909

Process finished with exit code 0
```

## 多态

* 基于当前的Circle类(继承了Point)，我们修改相关的测试代码如下：

```java
package me.ereach;

import me.ereach.oop.Circle;
import me.ereach.oop.Point;
import org.junit.Test;

public class MyTest {
  @Test
  public void testPolymorphism() {
    Point[] points = {
            new Point(2, 3),
            new Circle(7),
            new Point(5, 7),
            new Circle(11, 13, 9)
    };

    for (Point x : points)
      System.out.println(x);
  }
}
```

* 运行上面代码看到如下输出：

```
Point {x=2.0, y=3.0}
Circle{r=7.0, o=Point {x=0.0, y=0.0}}
Point {x=5.0, y=7.0}
Circle{r=9.0, o=Point {x=11.0, y=13.0}}

Process finished with exit code 0
```

上面测试代码包含了向上类型转换(Upcasting)的概念，迭代过程中Circle包含一个向上的类型转换操作，它被转换为Point类型，即：
Point类型的对象同时具有Circle的形态，也就是所谓的多态(Polymorphism)

## abstract

Java抽象类的语法如下，需要注意的是抽象类中的抽象方法只有声明没有实现：

```
<abstract> class <class name> {
  <abstract> <return type> <method name>(parameter list);
}
```

这里abstract不同于我们前面说的抽象，这里的抽象类是指用abstract实现的类和描述的方法，而且抽象方法只能存在于抽象类中，抽象类存在的目的是被子类继承使用，
并在子类中实现相关的抽象方法，参看下面例子：

* 项目中添加一个抽象几何类```Geometry```及如下代码：

```java
package me.ereach.oopabstract;

abstract class Geometry {
  abstract double getArea();

  abstract double getPerimeter();
}
```

* 添加矩形类```Rect```并继承```Geometry```如下：

```java
package me.ereach.oopabstract;

public class Rect extends Geometry {
  public float h;
  public float w;

  public Rect(float h, float w) {
    this.h = h;
    this.w = w;
  }

  public float getH() {
    return h;
  }

  public void setH(float h) {
    this.h = h;
  }

  public float getW() {
    return w;
  }

  public void setW(float w) {
    this.w = w;
  }

  @Override
  double getArea() {
    return h * w;
  }

  @Override
  double getPerimeter() {
    return (h + w) * 2;
  }
}
```

* 添加```Circle```类并继承```Geometry```如下：

```java
package me.ereach.oopabstract;

public class Circle extends Geometry {
  private float r;

  public Circle(float r) {
    this.r = r;
  }

  public float getR() {
    return r;
  }

  public void setR(float r) {
    this.r = r;
  }

  @Override
  double getArea() {
    return Math.PI * r * r;
  }

  @Override
  double getPerimeter() {
    return Math.PI * r * 2;
  }
}
```

* 添加入口测试类及测试函数如下：

```java
package me.ereach.oopabstract;

public class Main {
  public static void main(String[] args) {
    Rect r1 = new Rect(10, 6);
    System.out.println(r1.getArea());
    System.out.println(r1.getPerimeter());

    Circle c1 = new Circle(7);
    System.out.println(c1.getArea());
    System.out.println(c1.getPerimeter());
  }
}
```

运行并看到如下结果，：

```
60.0
32.0
153.93804002589985
43.982297150257104

Process finished with exit code 0
```

## final

Java中final修饰符用于修饰类、类方法及属性，表示对象的最终形态，表示被修饰的类和方法不可改变！对于类来说就是不可被继承，对方方法而言就是不可再被重写，被修饰的属性即为常量，参考如下代码：

* 返回前面的```Rect```类把方法```getArea()```和```getPerimeter()```加上```final```修饰符，这样```Rect```的子类将不能重写这两个方法：

```java
package me.ereach.oopabstract;

public class Rect extends Geometry {
  public float h;
  public float w;

  public Rect(float h, float w) {
    this.h = h;
    this.w = w;
  }

  public float getH() {
    return h;
  }

  public void setH(float h) {
    this.h = h;
  }

  public float getW() {
    return w;
  }

  public void setW(float w) {
    this.w = w;
  }

  @Override
  final double getArea() {
    return h * w;
  }

  @Override
  final double getPerimeter() {
    return (h + w) * 2;
  }
}
```

* 类似的情况，如果```Rect```类加上```final```修饰符则该类将不能被继承，如下：

```java
package me.ereach.oopabstract;

public final class Rect extends Geometry {
  public float h;
  public float w;

  public Rect(float h, float w) {
    this.h = h;
    this.w = w;
  }

  public float getH() {
    return h;
  }

  public void setH(float h) {
    this.h = h;
  }

  public float getW() {
    return w;
  }

  public void setW(float w) {
    this.w = w;
  }

  @Override
  double getArea() {
    return h * w;
  }

  @Override
  double getPerimeter() {
    return (h + w) * 2;
  }
}
```

## 接口

Java中我们使用接口构建松散耦合、可扩展的应用。什么是耦合，如果一个类使用了另一个类，就可以说这两个类是相互耦合的，如下图两个紧耦合的类：

<img src='/images/img043.png' />

上图中的类，由于A使用了B，因此，在某些情况下B的改变可能导致A也必须改变，这种情况针对大型应用程序时时非常不利的，一个类的改变可能导致数百个类都需要重新编译，
这种情况时必须避免的我们需要更松散的耦合形式。为了实现松散耦合，在之前的章节中我们已经介绍了以下概念：

抽象Abstraction，它隐藏了实现的细节，这还不够，我们需要完全解耦，就是说，当B被改变时A不需要被重构，这就需要接口，如下图，我们通过接口实现A和B之间的解偶

<img src='/images/img044.png' />

通过接口，我们实现了A和B之间的解耦，即：

* 接口定义了类应该做什么
* 类则定义了怎么去做

### interface VS abstract

接口与abstract极其相似，下面我们将使用接口把之前的Geometry再实现一遍：

* 创建Geometry接口并定义相关的计算方法：

```java
package me.ereach.oopinterface;

interface Geometry {
    double getArea();
    double getPerimeter();
}
```

* 创建Circle类并实现Geometry:

```java
package me.ereach.oopinterface;

public class Circle implements Geometry {
    private float r;

    public Circle(float r) {
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public double getArea() {
        return Math.PI * r * r;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
```

* 创建Rect并实现Geometry:

```java
package me.ereach.oopinterface;

public class Rect implements Geometry {
    private float w;
    private float h;

    public Rect(float w, float h) {
        this.w = w;
        this.h = h;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    @Override
    public double getArea() {
        return w * h;
    }

    @Override
    public double getPerimeter() {
        return (w + h) * 2;
    }
}
```

### 解耦

这一节通过接口完成一个解耦的税后工资计算，假定总收入高于5000时超出部分需要上10%的个人所得税：

* 创建一个接口```ITax```添加税收计算方法```public float getTax(float income)```如下：

```java
package me.ereach.oopinterface;

public interface ITax {
    public float getTax(float income);
}
```

* 创建类```Tax````实现接口定义的方法：

```java
package me.ereach.oopinterface;

public class Tax implements ITax{
    @Override
    public float getTax(float income) {
        if (income > 5000)
            return (income - 5000) * 0.1f;

        return 0;
    }
}
```

* 创建Employee类并使用ITax接口声明属性：

```java
package me.ereach.oopinterface;

public class Employee {
    private String name;
    private float totalIncome;
    private ITax iTax;

    public Employee(String name, float totalIncome) {
        this.name = name;
        this.totalIncome = totalIncome;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(float totalIncome) {
        this.totalIncome = totalIncome;
    }

    public ITax getiTax() {
        return iTax;
    }

    public void setiTax(ITax iTax) {
        this.iTax = iTax;
    }

    public float getActualSalary() {
        return totalIncome - iTax.getTax(totalIncome);
    }
}
```

* 创建测MyTest类测试解耦的实现：

```java
package me.ereach.oopinterface;

import org.junit.Test;

public class MyTest {
    @Test
    public void testEmp() {
        Employee employee = new Employee("Tom", 6000);
        ITax iTax = new Tax();
        employee.setiTax(iTax);
        System.out.println(employee.getActualSalary());
    }
}
```

* 运行测试类看到如下结果：

```
5900.0

Process finished with exit code 0
```

上面的例子通过setiTax属性方法完成iTax实例的注入，从而实现解耦，类似，我们也可以通过构造函数或其它实例方法实现实例的注入。

### 实现多个接口

Java仅支持继承一个父类，但支持实现多个接口，因此Java接口应尽可能的小以便提供更灵活的接口实现，例如：

* 创建```Geometry2D```接口，如下：

```java
package me.ereach.multiinterfaces;

public interface Geometry2D {
    double getArea();
    double getPerimeter();
}
```

* 创建```Geometry3D```接口，如下：

```java
package me.ereach.multiinterfaces;

public interface Geometry3D {
    double getVolume();
}
```

* 创建类Square并实现Geometry2D接口:

```java
package me.ereach.multiinterfaces;

public class Square implements Geometry2D {
    private float s;

    public Square(float s) {
        this.s = s;
    }

    public float getS() {
        return s;
    }

    public void setS(float s) {
        this.s = s;
    }

    @Override
    public double getArea() {
        return s * s;
    }

    @Override
    public double getPerimeter() {
        return s * 4;
    }
}
```

* 创建Cube类，并实现Geometry2D和Geometry3D如下：

```java
package me.ereach.multiinterfaces;

public class Cube implements Geometry2D, Geometry3D {
    private float s;

    public Cube(float s) {
        this.s = s;
    }

    public float getS() {
        return s;
    }

    public void setS(float s) {
        this.s = s;
    }

    @Override
    public double getArea() {
        return s * s * 6;
    }

    @Override
    public double getPerimeter() {
        return s * 12;
    }

    @Override
    public double getVolume() {
        return s * s * s;
    }
}
```

* 添加MyTest测试类并完成下面测试：

```java
package me.ereach.multiinterfaces;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Square square = new Square(7);
        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());

        System.out.println("===");

        Cube cube = new Cube(7);
        System.out.println(cube.getArea());
        System.out.println(cube.getPerimeter());
        System.out.println(cube.getVolume());
    }
}
```