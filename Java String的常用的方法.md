Java包装类型String的常用的方法：

1. length()：返回字符串的长度。

```java
String str = "Hello World";
int len = str.length(); // len = 11
```

2. charAt(int index)：返回指定索引处的字符。

```java
String str = "Hello World";
char ch = str.charAt(1); // ch = 'e'
```

3. substring(int beginIndex, int endIndex)：返回从beginIndex到endIndex-1的子字符串。

```java
String str = "Hello World";
String subStr = str.substring(6, 11); // subStr = "World"
```

4. indexOf(String str)：返回str在字符串中第一次出现的位置，如果没有找到则返回-1。

```java
String str = "Hello World";
int index = str.indexOf("o"); // index = 4
```

5. lastIndexOf(String str)：返回str在字符串中最后一次出现的位置，如果没有找到则返回-1。

```java
String str = "Hello World";
int index = str.lastIndexOf("o"); // index = 7
```

6. equals(Object anObject)：比较字符串是否相等。

```java
String str1 = "Hello World";
String str2 = "Hello World";
boolean isEqual = str1.equals(str2); // isEqual = true
```

7. equalsIgnoreCase(String anotherString)：比较字符串是否相等，忽略大小写。

```java
String str1 = "Hello World";
String str2 = "hello world";
boolean isEqual = str1.equalsIgnoreCase(str2); // isEqual = true
```

8. startsWith(String prefix)：判断字符串是否以prefix开头。

```java
String str = "Hello World";
boolean isStartWith = str.startsWith("He"); // isStartWith = true
```

9. endsWith(String suffix)：判断字符串是否以suffix结尾。

```java
String str = "Hello World";
boolean isEndWith = str.endsWith("ld"); // isEndWith = true
```

10. toUpperCase()：将字符串转换为大写。

```java
String str = "Hello World";
String upperStr = str.toUpperCase(); // upperStr = "HELLO WORLD"
```

11. toLowerCase()：将字符串转换为小写。

```java
String str = "Hello World";
String lowerStr = str.toLowerCase(); // lowerStr = "hello world"
```

12. trim()：去除字符串两端的空格。

```java
String str = "  Hello World  ";
String trimStr = str.trim(); // trimStr = "Hello World"
```

13. replace(char oldChar, char newChar)：将字符串中的oldChar替换为newChar。

```java
String str = "Hello World";
String replaceStr = str.replace('o', 'a'); // replaceStr = "Hella Warld"
```

14. split(String regex)：将字符串按照regex分割成字符串数组。

```java
String str = "Hello,World";
String[] strArr = str.split(","); // strArr = {"Hello", "World"}
```

15. format(String format, Object... args)：使用指定的格式字符串和参数返回格式化的字符串。

```java
String str = String.format("Hello %s, your age is %d", "Tom", 20);
// str = "Hello Tom, your age is 20"
```

这些方法只是String类提供的一部分，还有很多其他的方法可以用来操作字符串。