package com.xinhua.edu;

public class Lesson2 {
    public static void main(String[] args) {
        int rst = add(4, 5);
        System.out.println(rst);

        rst = add(11, 13);
        System.out.println(rst);
    }

    public static int add(int a, int b) {
        return a + b;
    }

    // public static int sub(int a, int b) 减法
}
