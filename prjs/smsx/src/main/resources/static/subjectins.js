$(function () {
    $("#btnSave").on("click", function () {
        let myUrl = "http://localhost:8081/api/subject"

        let formData = new FormData()

        formData.append("subjectname", $("#subjectName").val())

        $.ajax({
            url: myUrl,
            type: 'POST',
            cache: false,
            data: formData,
            processData: false,
            contentType: false
        }).done(function (res) {
            debugger
            if (res != null)
                $("#message").text(`OK, id = ${res}`)
            else
                $("#message").text(`Failed.`)
        })
    })
})