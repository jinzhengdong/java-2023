package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() throws InvalidArgumentException {
        Account a1 = new Account();
        a1.deposit(100);
        System.out.println(a1);
        a1.withdraw(20);
        System.out.println(a1);

        System.out.println("===");
        Account a2 = new Account(200);
        a2.deposit(-20);
        System.out.println(a2);
    }
}
