package me.ereach.smsx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.smsx.model.Subject;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubjectMapper extends BaseMapper<Subject> {
}
