package me.ereach;

public class Circle {
    private float r;
    private Point p;

    public Circle(float r, Point p) {
        this.r = r;
        this.p = p;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public Point getP() {
        return p;
    }

    public void setP(Point p) {
        this.p = p;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "r=" + r +
                ", p=" + p +
                '}';
    }
}
