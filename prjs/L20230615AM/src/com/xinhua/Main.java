package com.xinhua;

public class Main {
    public static void main(String[] args) {
        sayHello("Jerry");
        System.out.println(3 / 0);
        sayHello(null);
    }

    public static void sayHello (String name) {
        System.out.println(name.toUpperCase());
    }
}
