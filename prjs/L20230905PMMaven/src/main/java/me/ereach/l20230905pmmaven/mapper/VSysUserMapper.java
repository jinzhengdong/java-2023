package me.ereach.l20230905pmmaven.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.l20230905pmmaven.viewmodel.VSysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface VSysUserMapper extends BaseMapper<VSysUser> {
    @Select("declare @username nvarchar(50) " +
            "declare @retvar int " +
            "declare @id int " +
            "exec @retvar = p_sys_user_login " +
            "   #{username}, " +
            "   #{password}, " +
            "   @id output " +
            "select * from v_sys_user where user_id = @id")
    public VSysUser login(String username, String password);
}
