package me.ereach;

public class Rect extends Geometry {
    private float h;
    private float w;

    public Rect(float h, float w) {
        this.h = h;
        this.w = w;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    @Override
    double getArea() {
        return h * w;
    }

    @Override
    double getPerimeter() {
        return (w + h) * 2;
    }
}
