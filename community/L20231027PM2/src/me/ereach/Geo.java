package me.ereach;

public interface Geo {
    double getArea();
    double getPerimeter();
}
