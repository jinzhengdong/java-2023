package com.xinhua.ab;

public class Rect extends Geometory {
    private float w;
    private float h;

    public Rect(float w, float h) {
        this.w = w;
        this.h = h;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    @Override
    protected double getArea() {
        return w * h;
    }

    @Override
    protected double getPerimeter() {
        return (w + h) * 2;
    }


}
