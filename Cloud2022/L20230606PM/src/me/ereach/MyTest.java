package me.ereach;

import org.junit.Test;

import java.text.DecimalFormat;
import java.util.Scanner;

public class MyTest {
    @Test
    public void test03() {
    }

    @Test
    public void test02() {
        System.out.println(String.format("%.2f", Math.PI));
        System.out.println(String.format("%.4f", Math.PI));

        int i = 1234567;
        System.out.println(String.format("%,d", i));
        System.out.println(String.format("%d", i));

        int l = 123_456_789;
        System.out.println(l);
//        System.out.println(String.format("%_d", l));
    }

    @Test
    public void test01() {
        DecimalFormat df = new DecimalFormat("##.00");
        String fn = df.format(Math.PI);
        System.out.println(fn);
    }
}
