package com.xinhua.demo20231202;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo20231202Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo20231202Application.class, args);
    }

}
