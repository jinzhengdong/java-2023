package me.ereach;

import me.ereach.edu.Employee;
import me.ereach.geo.Point3D;
import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        Employee s1 = new Employee(1, "Jerry", true, "13888000001", 6000D, 12, 30d);
        System.out.println(s1);
        System.out.println(s1.getTotalSalary());
    }

    @Test
    public void test01() {
        Point3D p1 = new Point3D();
        System.out.println(p1);
        Point3D p2 = new Point3D(2, 4, 5);
        System.out.println(Point3D.getDistanceStatic(p1, p2));
    }
}
