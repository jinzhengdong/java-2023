package me.ereach;

public class Utils {
    public static void print(int key, int value) {
        System.out.println(key + " = " + value);
    }

    public static <K, V, P> K genericTest(K key, V value, P p) {
        System.out.println(key + ", " + value + "," + p );
        return key;
    }

    public static <K, V> void genericPrint2(K key, V value) {
        System.out.println(key + " = " + value);
    }

    public static <T> void genericPrint (T key, int value) {
        System.out.println(key + " = " + value);
    }

    public String name;
    public String Description;

    public Utils(String name, String description) {
        this.name = name;
        Description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public static int max(int first, int second) {
        return (first > second) ? first : second;
    }

    public static float max(float first, float second) {
        return (first > second) ? first : second;
    }

    public static <T extends Comparable<T>> T genericMax(T first, T second) {
        return (first.compareTo(second) < 0) ? second : first;
    }

    @Override
    public String toString() {
        return "Utils{" +
                "name='" + name + '\'' +
                ", Description='" + Description + '\'' +
                '}';
    }
}
