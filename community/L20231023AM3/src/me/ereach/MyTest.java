package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Student s1 = new Student("Tom", 18, "xinhua");
        Student s2 = new Student("Jerry", 12, "xinhua");
        System.out.println(s1);
        System.out.println(s2);

        System.out.println(s1.sayHello());

        Teacher t1 = new Teacher("Cherry", 18, "xinhua", "java");
        Teacher t2 = new Teacher("wendy", 18, "xinhua", "java");
        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t2.sayHello());
    }
}
