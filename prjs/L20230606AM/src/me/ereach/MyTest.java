package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Circle c1 = new Circle(new Point(2, 3), 5);
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());

        Point p = new Point(5, 7);
        Circle c2 = new Circle(p, 11);
        System.out.println(c2);
        System.out.println(c2.getArea());
        System.out.println(c2.getPerimeter());
    }
}
