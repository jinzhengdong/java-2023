package com.xinhua;

public class Circle {
    private Poin2D o;
    private float r;

    public Circle(Poin2D o, float r) {
        this.o = o;
        this.r = r;
    }

    public Poin2D getO() {
        return o;
    }

    public void setO(Poin2D o) {
        this.o = o;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "o=" + o +
                ", r=" + r +
                '}';
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
