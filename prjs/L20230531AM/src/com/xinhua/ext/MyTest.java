package com.xinhua.ext;

import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        Point2D[] point2DS = {
                new Point2D(2, 3),
                new Point2D(5, 7),
                new Circle(7),
                new Point2D(11, 13),
                new Circle(3, 5, 11),
                new Point2D(11, 19),
        };

        for (Point2D x : point2DS)
            System.out.println(x);
    }

    @Test
    public void test01() {
        Circle c1 = new Circle(2, 3, 5);
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
    }
}
