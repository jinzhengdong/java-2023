package com.xinhua;

public class Student extends Person {
    private String className;
    private Integer age;

    public Student(String name, String gender, String className, Integer age) {
        super(name, gender);
        this.className = className;
        this.age = age;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "Person=" + super.toString() +
                ", className='" + className + '\'' +
                ", age=" + age +
                '}';
    }
}
