package com.xinhua;

abstract class Geometry {
    abstract double getArea();
    abstract double getPerimeter();

    public String sayHello(String name) {
        return "Hello, " + name;
    }
}
