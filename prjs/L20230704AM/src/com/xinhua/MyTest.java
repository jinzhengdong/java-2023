package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.*;

public class MyTest {
    @Test
    public void test03() {
        Function<String, String> replaceColon = str -> str.replace(":", "=");

        Function<String, String> addBraces = str -> "{" + str + "}";

        var rst = replaceColon
                .andThen(addBraces)
                .apply("name:jinzd");

        System.out.println(rst);

        rst = addBraces
                .compose(replaceColon)
                .apply("name:jinzd");

        System.out.println(rst);
    }

    @Test
    public void test02() {
        List<String> list = new ArrayList<>();

        list.add("a");
        list.add("b");
        list.add("c");

        Consumer<String> print = item -> System.out.println(item);
        Consumer<String> printUp = item -> System.out.println(item.toUpperCase());

        list.forEach(print.andThen(printUp));
    }

    @Test
    public void test01() {
        Consumer<String> myPrint = (str) -> System.out.println(str);
        myPrint.accept("Hello, World");

        Supplier<Integer> randInt = () -> new Random().nextInt(100);
        System.out.println(randInt.get());

        Function<String, String> convert = str -> "== " + str + " ==";
        System.out.println(convert.apply("Hello LXK"));

        Predicate<Integer> rst = num -> num % 2 == 0;
        System.out.println(rst.test(100));

        Function<Integer, Boolean> rstx = num -> num % 2 == 0;
        System.out.println(rstx.apply(100));

        UnaryOperator<Integer> square = num -> num * num;
        System.out.println(square.apply(5));

        BinaryOperator<Integer> add = (a, b) -> a + b;
        System.out.println(add.apply(3, 5));

        Calculator addxx = (a, b, c) -> a + b + c;
        System.out.println(addxx.calculate(1, 2, 3));
    }
}
