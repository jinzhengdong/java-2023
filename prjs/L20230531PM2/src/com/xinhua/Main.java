package com.xinhua;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello, World!");

        Point p1 = new Point(2, 3);
        Point p2 = new Point(5, 7);

        System.out.println(p1);
        System.out.println(p2);

        System.out.println("The distance from " + p1 + " to " + p2 + " is " + p1.getDistance(p2));
    }
}
