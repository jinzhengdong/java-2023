package me.ereach;

public class Circle extends Point {
    private float r;

    public Circle(float x, float y, float r) {
        super(x, y);
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public void setX(float x) {
        super.setX(x);
    }

    @Override
    public float getX() {
        return super.getX();
    }

    @Override
    public void setY(float y) {
        super.setY(y);
    }

    @Override
    public float getY() {
        return super.getY();
    }

    @Override
    public String toString() {
        return "Circle{" +
                "o=" + super.toString() +
                ", r=" + r +
                '}';
    }

    @Override
    public double getDistance(Point p) {
        return super.getDistance(p);
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
