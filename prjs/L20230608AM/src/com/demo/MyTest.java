package com.demo;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Emp emp1 = new Emp("Tod", 6000);
        ITax iTax = new Tax();
        emp1.setItax(iTax);
        System.out.println(emp1.getTax());
        System.out.println(emp1.getActualSalary());
        System.out.println(emp1);

        System.out.println("===");

        Emp emp2 = new Emp("Jerry", 7000, new Tax());
        System.out.println(emp2.getTax());
        System.out.println(emp2.getActualSalary());
        System.out.println(emp2);

        System.out.println("===");
        emp2.newSetEmp(9000, new Tax());
        System.out.println(emp2.getTax());
        System.out.println(emp2.getActualSalary());
        System.out.println(emp2);

    }
}
