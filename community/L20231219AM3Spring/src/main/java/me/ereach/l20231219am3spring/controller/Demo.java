package me.ereach.l20231219am3spring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Demo {
    @GetMapping("/hello")
    public String sayHello() {
        return "Hello, SpringBoot";
    }
}
