import me.ereach.Circle;
import me.ereach.CircleExt;
import me.ereach.Point2D;
import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        CircleExt c1 = new CircleExt(2, 3, 5);
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getArea());
        System.out.println(c1);
    }

    @Test
    public void test01() {
        Circle c1 = new Circle(new Point2D(2, 3), 7);
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());

        Circle c2 = new Circle(new Point2D(5, 7), 11);
        System.out.println(c2);
        System.out.println(c2.getArea());
        System.out.println(c2.getPerimeter());
        System.out.println("===");

        System.out.println(c1.getO().getDistance(c2.getO()));
    }
}
