package com.xinhua;

public class UserList {
    private User[] items = new User[10];
    private int count;

    public void add(User user) {
        items[count++] = user;
    }

    public User get(int index) {
        return items[index];
    }

    public int getCount() {
        return count;
    }
}
