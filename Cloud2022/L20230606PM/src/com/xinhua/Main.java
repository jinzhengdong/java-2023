package com.xinhua;

public class Main {
    public static void main(String[] args) {
        // + - * / % ()
        System.out.println(1 + 2);  // 3
        System.out.println(2 - 4);  // -2
        System.out.println(3 * 3);  // 9
        System.out.println(7 / 2);  // 3
        System.out.println(7 % 3);  // 1

        System.out.println((float)7 / 2);   // 3.5
        System.out.println(7 / (float)2);   // 3.5

        System.out.println((2 + 3) * 6);    // 30

        int a = 5;
        a += 1;     // a = a + 1
        System.out.println(a);  // 6
        a += 3;
        System.out.println(a);  // 9
        a *= 2;
        System.out.println(a);  // 18

        int b = a++;
        System.out.println(b);  // 18
        System.out.println(a);  // 19

        b = ++a;
        System.out.println(b);  // 20
        System.out.println(a);  // 20

        float c = 3.9f;
        System.out.println((int) c);    // 3
    }
}
