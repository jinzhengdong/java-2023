package com.xinhua;

public class Point2D {
    private float x;
    private float y;

    public Point2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point2D) {
            Point2D pt = (Point2D)obj;
            return (x == pt.x) && (y == pt.y);
        }

        return super.equals(obj);
    }

    public double getDistance(Point2D p) {
        float w = this.x - p.x;
        float h = this.y - p.y;

        return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2));
    }
}
