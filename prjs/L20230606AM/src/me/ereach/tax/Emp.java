package me.ereach.tax;

public class Emp {
    private String name;
    private float income;
    private ITax iTax;

    public Emp(String name, float income) {
        this.name = name;
        this.income = income;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getIncome() {
        return income;
    }

    public void setIncome(float income) {
        this.income = income;
    }

    public ITax getiTax() {
        return iTax;
    }

    public void setiTax(ITax iTax) {
        this.iTax = iTax;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", income=" + income +
                '}';
    }

    public float getActualIncome() {
        return income - iTax.getTax(income);
    }
}
