import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入一个正整数：");
        int num = scanner.nextInt();

        System.out.println(checkPrime(num));
    }

    public static String checkPrime(int num) {
        boolean flag = true;

        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                flag = false;
                break;
            }
        }

        if (flag)
            return "num " + num + " is a prime.";
        else
            return "num " + num + " is not a prime.";
    }
}