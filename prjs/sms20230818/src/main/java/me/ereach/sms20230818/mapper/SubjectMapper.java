package me.ereach.sms20230818.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.sms20230818.model.Subject;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectMapper extends BaseMapper<Subject> {
}
