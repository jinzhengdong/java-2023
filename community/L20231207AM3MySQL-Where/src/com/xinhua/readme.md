```SELsql
select
	*
from 
	film 
where 
	length > 120 
order by 
	length asc
limit 1;

select
	*
from 
	film 
where 
	length > 120 
order by 
	length desc
limit 1;

-- sum 求和
select 
	sum(replacement_cost) as total 
from 
	film 
where 
	replacement_cost >= 10.0;

select 
	sum(replacement_cost) as total 
from 
	film 
where 
	replacement_cost < 10.0;
    
select 
	sum(replacement_cost) as total 
from 
	film;
	
-- average 平均
select avg(replacement_cost) from film
where replacement_cost >= 10;

select avg(replacement_cost) from film
where replacement_cost < 10;

select avg(replacement_cost) from film;

-- count 计数
select count(film_id) from film
where replacement_cost >= 15;

select count(film_id) from film
where replacement_cost < 15;

select count(film_id) from film;

-- max 最大值
select max(replacement_cost) from film;

-- min 最小值
select min(replacement_cost) from film;

-- 查询 film 表的所有字段。 

-- 查询 film 表中 replacement_cost 大于 20 美元的所有字段。

-- 查询 film 表中 replacement_cost 的总和。sum
```