package com.xinhua;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入一个整数：");
        int temp = scanner.nextInt();

        int rst = temp % 2;

        if (rst == 0)
            System.out.println("是偶数");
        else
            System.out.println("不是偶数");
    }
}
