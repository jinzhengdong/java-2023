package me.ereach.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.sms.model.Student;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentMapper extends BaseMapper<Student> {
}