import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // 获取贷款金额、年利率和贷款期限
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("请输入贷款金额（元）：");
//        double loanAmount = scanner.nextDouble();
//        System.out.print("请输入年利率（%）：");
//        double annualInterestRate = scanner.nextDouble() / 100.0;
//        System.out.print("请输入贷款期限（年）：");
//        int loanTermInYears = scanner.nextInt();
//
//        // 计算每月还款金额和总支付利息
//        int numberOfPayments = loanTermInYears * 12;
//        double monthlyInterestRate = annualInterestRate / 12.0;
//        double monthlyPayment = (loanAmount * monthlyInterestRate) / (1 - Math.pow(1 + monthlyInterestRate, -numberOfPayments));
//        double totalPayment = monthlyPayment * numberOfPayments;
//        double totalInterest = totalPayment - loanAmount;
//
//        // 输出结果
//        System.out.printf("每月还款金额为：%.2f元\n", monthlyPayment);
//        System.out.printf("总支付利息为：%.2f元\n", totalInterest);

        int a = 3;
        int b = 5;
        System.out.printf("a = %d, b = %d, %d + %d = %d", a, b, a, b, a + b);

        float pi = 3.14f;
        int r = 3;
        System.out.printf("\ns = %d^2 * %f = %f", r, pi, Math.pow(r, 2) * pi);
        System.out.printf("\ns = %d^2 * %.2f = %.2f", r, pi, r * r * pi);
    }
}