package me.ereach;

import java.sql.*;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        System.out.println("Hello world!");

        Class.forName("com.mysql.cj.jdbc.Driver");

        String url = "jdbc:mysql://localhost:3306/sakila";
        String user = "root";
        String password = "123456";
        Connection conn = DriverManager.getConnection(url, user, password);

        Statement stmt = conn.createStatement();

        ResultSet rs = stmt.executeQuery("select * from actor");

        while (rs.next()) {
            System.out.print(rs.getInt("actor_id") + " ");
            System.out.print(rs.getString("first_name") + " ");
            System.out.println(rs.getString("last_name"));
        }
    }
}