```SQL
-- actor 演员
   -- actor_id 标识符
   -- first_name 名
   -- last_name 姓
   -- last_update 最后更新日期
-- address 地址
   -- address_id 标识符
   -- address 地址
   -- address2 地址2
   -- district 区域
   -- city_id 城市标识符
   -- postal_code 邮政编码
   -- phone 电话
   -- location 位置
   -- last_update 最后更新日期
-- category 门类，分类
-- city 城市
-- country 国家
-- customer 顾客，客户
-- film 电影
   -- film_id 标识符
   -- title 标题
   -- description 描述
   -- language_id 语言标识符
   -- original_language_id 原版语言
   -- rental_duration 租期
   -- rental_rate 租赁费用
   -- length 时长
   -- replacement_cost 更换成本
   -- rating 等级
   -- special_features 花絮
   -- last_update 最后更新
-- film_actor 电影演员
-- film_category 电影门类
-- film_text 电影文本，电影简述
-- inventory 音像店库存
-- language 语言
-- payment 支付
-- rental 租，租赁
-- staff 员工
-- store 商店，音像租赁店

create view v_film_actor as
select
	a.*,
    b.first_name,
    b.last_name,
    c.title
from
	film_actor as a
    left join actor as b on a.actor_id = b.actor_id
    left join film as c on a.film_id = c.film_id

```