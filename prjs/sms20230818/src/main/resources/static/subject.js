layui.use(['table', 'laypage', 'layer'], function () {
    let table = layui.table

    let tableCols = [
        { field: 'id', title: 'ID', width: 80, sort: true, align: 'center', hide: true },
        { field: 'subjectName', title: '课程名称' },
        { fixed: 'right', title: '操作', toolbar: '#colbar', width: 120, align: 'center' },
    ]

    // render 渲染
    // elem (element 元素)
    table.render({
        elem: "#subjectlist",
        url: 'http://localhost:8081' + "/api/subject",
        toolbar: "#titlebar",
        cols: [tableCols],
        page: true,
        even: true,
        limits: [10, 13, 16, 19],
        limit: 13,
        parseData: function (res) {
            debugger
            let rst

            if (this.page.curr) {
                rst = res.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr)
            }
            else {
                rst = res.slice(0, this.limit)
            }

            return {
                "code": 0,
                "data": rst,
                "count": res.length,
                "msg": res.message
            }
        },
    })

    table.on('tool(subjectlist)', function (obj) {
        switch (obj.event) {
            case 'editSubject':
                layer.open({
                    type: 2
                    , id: 'layerDemo' + 1
                    , content: `./subjectupdate.html?subjectid=${obj.data.subjectId}&subjectname=${obj.data.subjectName}&factor=${obj.data.factor}`
                    , btnAlign: 'c'
                    , shadeClose: true
                    , shade: 0.4
                    , area: ['580px', '360px']
                    , title: `添加科目`
                    , end: function (layero) {
                        $.getJSON(`${apiurl}/edu/edusubject/${obj.data.subjectId}`, function (rst) {
                            obj.update({
                                subjectId: rst.subjectId,
                                subjectName: rst.subjectName,
                                factor: rst.factor
                            })
                        })
                    }
                })
                break
            case 'delSubject':
                layer.confirm('您确定要删除选中的记录吗？', { icon: '&#xe702', title: '提示' }, function (index) {

                    let formData = new FormData();

                    formData.append("subjectid", obj.data.subjectId);

                    $.ajax({
                        url: 'http://localhost:8081' + '/edu/edusubject',
                        type: 'DELETE',
                        async: false,
                        cache: false,
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: (res) => {
                            obj.del()
                        },
                        error: (res) => {
                            alert("error: " + JSON.stringify(res))
                        }
                    })

                    layer.close(index)
                })
                break
        }
    })

    table.on('toolbar(subjectlist)', function (obj) {
        switch (obj.event) {
            case 'addCmd':
                layer.open({
                    type: 2
                    , id: 'layerDemo' + 1
                    , content: './subjectadd.html'
                    , btnAlign: 'c'
                    , shadeClose: true
                    , shade: 0.4
                    , area: ['580px', '320px']
                    , title: '添加新课程'
                    , yes: function () {
                        layer.closeAll();
                    }
                })
                break
            case 'qryCmd':
                let qrySubject = $("#qrySubject").val()
                debugger

                $.getJSON('http://localhost:8081' + "/api/subject", null, function (data) {
                    debugger
                    let fdata = data.filter((item) => {
                        return (
                            item.subjectName != null && item.subjectName.indexOf(qrySubject) >= 0
                        )
                    })

                    table.render({
                        elem: '#subjectlist',
                        toolbar: '#titlebar',
                        cols: [tableCols],
                        page: true,
                        even: true,
                        curr: 1,
                        limit: 13,
                        limits: [10, 13, 16, 19],
                        data: fdata
                    })

                    $("#qrySubject").val(qrySubject)
                })
                break
        }
    })

})