# Spring Security

## Create a Spring Boot and run it

* Access https://start.spring.io/
  * Project: Maven Project
  * Language: Java
  * Spring Boot: 2.2.0
  * Group: me.ereach
  * Artifact: demo
  * Name: demo
  * Description: Demo project for Spring Boot
  * Package name: me.ereach.demo
  * Packaging: jar
  * Java: 11
  * Dependencies
    * Spring Web

After above settings, click `Generate` button to download the project template.
And the settings can be changed later.

Extract `demo.zip` to a fold and open the pom.xml as a project, it includes following settings:

* the version of spring-boot-starter-parent is `2.2.0.RELEASE`
* `java.version` is 11

After project opened, run the project we can see the project is running under `localhost:80`

## Create RESTful API

* Goto main package `me.ereach.demo` and create `student` package under it.
* Under the `student` package create class `Student` as below:

```java
package me.ereach.demo.student;

public class Student {
    private final Integer studentId;
    private final String studentName;

    public Student(Integer studentId, String studentName) {
        this.studentId = studentId;
        this.studentName = studentName;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public String getStudentName() {
        return studentName;
    }
}
```

* Under the `student` package create class `StudentController` as below:

```java
package me.ereach.demo.student;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {
    private static final List<Student> STUDENTS = Arrays.asList(
            new Student(1, "James Bond"),
            new Student(2, "Jerry"),
            new Student(3, "Cherry")
    );

    @GetMapping(path = "/{studentId}")
    public Student getStudent(@PathVariable("studentId") Integer studentId) {
        return STUDENTS.stream().filter(student -> studentId.equals(student.getStudentId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Student " + studentId + " does not exists"));
    }
}
```

* Create below `rest.http`

```rest
###
GET http://localhost:8080/api/v1/students/1 HTTP/1.1
```

## Add Dependency to pom.xml

Add following contents to `pom.xml`:

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
    <version>2.7.7</version>
</dependency>
```

After that the `pom.xml` is below:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.0.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>me.ereach</groupId>
	<artifactId>demo</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>demo</name>
	<description>Demo project for Spring Boot</description>
	<properties>
		<java.version>11</java.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
			<version>2.7.7</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
```

After above step restart the spring boot application to do the same and check the password and use `user` with the `password`

## Spring Security Brief

* The url http://localhost:8080/login is used to access the default login page.
* The url http://localhost:8080/logout is used to logout.
* The url http://localhost:8080/api/v1/students/1

## Basic Authentication

<img src='./images/028.png' style='width:500px'>

* Go to the main package and create a new package `me.ereach.demo.security` and add new class `ApplicationSecurityConfig` under the package:

```java
package me.ereach.demo.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }
}
```

* After above updating, restart the application and access `http://localhost:8080/api/v1/students/1` we can see a popup login window
* For the basic security case we cannot logout since username and password are sent in every single request, so we cannot logout.

## Postman

start postman and do following tests:

* do http get with url "http://localhost:8080/api/v1/students/1" we got below error:

```json
{
    "timestamp": "2023-03-19T06:32:29.027+0000",
    "status": 401,
    "error": "Unauthorized",
    "message": "Unauthorized",
    "path": "/api/v1/students/1"
}
```

* within postman goto the `Authorization` tab and select `Basic Auth` then type `user` and `password` and submit the api call again.

```json
{
    "studentId": 1,
    "studentName": "James Bond"
}
```

* or 

```rest
###
get http://localhost:8080/api/v1/students/3 http/1.1
basic auth: user 2e66fa02-f1fe-4c68-9939-1c54bc2ed409
```

## Application User

After above cases, within browser attress enter http://localhost:8080 we got 404 error, for the issue goto the `/resources/static` and create `index.html` under it.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

</body>
</html>
```

then check 'http://localhost:8080' again, after user and password the page will be displayed.

now we go to the config and 

```java
package me.ereach.demo.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }
}
```

restart server and access http://localhost:8080

when we start spring boot security app there is a default user and password in the memory database.

在实际情况中，你可能拥有众多的用户，我们会把用户信息存储在数据库中。

for the spring security, we should prepare following:

* Username
* Password
* Roles
* Authorities
* and more...

## Create User

* Back to `ApplicationSecurityConfig` and do following updating:

```java
package me.ereach.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails jinzd = User.builder()
                .username("jinzd")
                .password("jinzd")
                .roles("Admin")
                .build();

        return new InMemoryUserDetailsManager(jinzd);
    }
}
```

* Starting the application and try `jinzd/jinzd` login, we cannot loign with the user.
* To solve the issue, go back to `security` package and add `PasswordConfig` class to the package:

```java
package me.ereach.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordConfig {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}
```

* Back to `ApplicationSecurityConfig` class and do following changes:

```java
package me.ereach.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails jinzd = User.builder()
                .username("jinzd")
                .password(passwordEncoder.encode("jinzd"))
                .roles("Admin")
                .build();

        return new InMemoryUserDetailsManager(jinzd);
    }
}
```

* Access following rest api:

```rest
get http://localhost:8080/api/v1/students/2 http/1.1
basic auth: jinzd jinzd
```

55'51"