public class Point2D {
    private double x;
    private double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public double getDistance(Point2D p) {
        double h = this.y - p.y;
        double w = this.x - p.x;
        return Math.sqrt(Math.pow(h, 2) + Math.pow(w, 2));
    }

    public static double getDistanceStatic(Point2D p1, Point2D p2) {
        double h = p1.y - p2.y;
        double w = p1.x - p2.x;
        return Math.sqrt(Math.pow(h, 2) + Math.pow(w, 2));
    }
}
