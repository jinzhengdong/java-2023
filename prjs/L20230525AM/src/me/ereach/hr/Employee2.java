package me.ereach.hr;

public class Employee2 {
    private  Integer id;
    private String name;
    private boolean xinbie;
    private double gongzi;
    private Integer jiaban;
    private  double jbgongzi;

    public Employee2(Integer id, String name, boolean xinbie, double gongzi, Integer jiaban, double jbgongzi) {
        this.id = id;
        this.name = name;
        this.xinbie = xinbie;
        this.gongzi = gongzi;
        this.jiaban = jiaban;
        this.jbgongzi = jbgongzi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isXinbie() {
        return xinbie;
    }

    public void setXinbie(boolean xinbie) {
        this.xinbie = xinbie;
    }

    public double getGongzi() {
        return gongzi;
    }

    public void setGongzi(double gongzi) {
        this.gongzi = gongzi;
    }

    public Integer getJiaban() {
        return jiaban;
    }

    public void setJiaban(Integer jiaban) {
        this.jiaban = jiaban;
    }

    public double getJbgongzi() {
        return jbgongzi;
    }

    public void setJbgongzi(double jbgongzi) {
        this.jbgongzi = jbgongzi;
    }

    @Override
    public String toString() {
        return "Employee2{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", xinbie=" + xinbie +
                ", gongzi=" + gongzi +
                ", jiaban=" + jiaban +
                ", jbgongzi=" + jbgongzi +
                '}';
    }
}
