import me.ereach.Point;
import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(5, 11);
        Point p3 = new Point();

        System.out.println(p1.getDistance(p2));
        System.out.println(p2.getDistance(p1));
        System.out.println("===");
        System.out.println(p3.getDistance(p1));
        System.out.println(p3.getDistance(p2));
    }
}
