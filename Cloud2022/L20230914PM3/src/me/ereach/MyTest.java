package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        GList<String> sList = new GList<>();

        sList.add("Hello");
        sList.add("World");

        for (int i = 0; i < sList.getCount(); i++) {
            System.out.println(sList.get(i));
        }

        System.out.println("===");

        for (var x : sList)
            System.out.println(x);
    }

    @Test
    public void test02() {
        GList<String> slist = new GList<>();

        slist.add("Hello");
        slist.add("World");
//        slist.add(1);

        System.out.println(slist);

        GList<Integer> iList = new GList<>();
        iList.add(1);
        iList.add(2);
        System.out.println(iList);

        GList<User> uList = new GList<>();
        uList.add(new User("Tom", "male"));
        uList.add(new User("Jerry", "male"));
        System.out.println(uList);
    }

    @Test
    public void test01() {
        ObjList objList = new ObjList();

        objList.add(1);
        objList.add(2);
        objList.add(new User("Tom", "male"));
        objList.add(new User("Jerry", "male"));
        objList.add("Hello");
        objList.add("World");

        System.out.println(objList);
    }
}
