public class Main {
    public static void main(String[] args) {
        String rst = sayHello("XinKai");
        System.out.println(rst);

        rst = sayHello("EnLai");
        System.out.println(rst);

        rst = sayHello("GuiJin");
        System.out.println(rst);
    }

    public static String sayHello(String name) {
        return "Hello, " + name + "!";
    }
}