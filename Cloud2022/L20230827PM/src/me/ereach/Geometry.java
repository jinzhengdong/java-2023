package me.ereach;

abstract class Geometry {
    abstract double getArea();
    abstract double getPerimeter();
}
