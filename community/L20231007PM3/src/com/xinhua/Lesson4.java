package com.xinhua;

public class Lesson4 {
    public static void main(String[] args) {
        int num = 3;

        if (num == 0)
            System.out.println("星期一");
        else if (num == 1)
            System.out.println("星期二");
        else if (num == 2)
            System.out.println("星期三");
        else if (num == 3)
            System.out.println("星期四");
        else if (num == 4)
            System.out.println("星期五");
        else if (num == 5)
            System.out.println("星期六");
        else if (num == 6)
            System.out.println("星期天");
        else
            System.out.println("未知");

        switch (num) {
            case 0:
                System.out.println("星期一");
                break;
            case 1:
                System.out.println("星期二");
                break;
            case 2:
                System.out.println("星期三");
                break;
            case 3:
                System.out.println("星期四");
                break;
            case 4:
                System.out.println("星期五");
                break;
            case 5:
                System.out.println("星期六");
                break;
            case 6:
                System.out.println("星期天");
                break;
            default:
                System.out.println("未知");
                break;
        }
    }
}
