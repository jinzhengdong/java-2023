package com.xinhua;

import org.junit.Test;

public class MyTest3 {
    @Test
    public void test01() {
        Object[] items = new Object[5];

        items[0] = "Hello";
        items[1] = 12;
        items[2] = new java.awt.Point(3, 5);
        items[3] = Math.PI;

        System.out.println(items);

        for (int i = 0; i < items.length; i++)
            System.out.println(items[i]);
    }
}
