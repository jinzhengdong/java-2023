package me.ereach;

import java.util.Arrays;

public class List {
    private int[] items = new int[10];
    private int count;

    public void add(int item) {
        items[count++] = item;
    }

    public int get(int index) {
        return items[index];
    }

    @Override
    public String toString() {
        return "List{" +
                "items=" + Arrays.toString(items) +
                ", count=" + count +
                '}';
    }
}
