package com.xinhua;

import org.junit.Test;

public class MyTest2 {
    @Test
    public void test02() {
        // byte Byte
        // short Short
        // int Integer
        // long Long
        // boolean Boolean
        // char Character
        // float Float
        // double Double
    }
    @Test
    public void test01() {
        byte b = 1;
        System.out.println(b);  // 1

        Byte B = 1;             // 有一个包装的过程
        System.out.println(B);  // 1 有一个解包的过车
        System.out.println(B.byteValue());  // 1 只输出它的值

        char c = 'c';
        System.out.println(c);

        Character C = 'C';
        System.out.println(C);
        System.out.println(C.hashCode());


    }
}
