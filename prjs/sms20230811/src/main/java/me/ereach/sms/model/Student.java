package me.ereach.sms.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("Student")
public class Student {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String fullName;
    private String gender;
    private Date birthdate;
    private Integer classId;

    @TableField(exist = false)
    private Class eduClass;
}