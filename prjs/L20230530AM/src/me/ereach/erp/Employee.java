package me.ereach.erp;

public class Employee {
    private Integer id;
    private String name;
    private float salary;
    private float hourlyRate;
    private int extraHours;

    public Employee(Integer id, String name, float salary, float hourlyRate, int extraHours) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.hourlyRate = hourlyRate;
        this.extraHours = extraHours;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public float getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getExtraHours() {
        return extraHours;
    }

    public void setExtraHours(int extraHours) {
        this.extraHours = extraHours;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", hourlyRate=" + hourlyRate +
                ", extraHours=" + extraHours +
                ", totalSalary=" + getTotalSalary() +
                ", Tax=" + getTax() +
                ", ActualSalary=" + getActualSalsry() +
                '}';
    }

    public float getTotalSalary() {
        return salary + hourlyRate * extraHours;
    }

    public float getTax() {
        if (getTotalSalary() <= 5000)
            return 0;

        return (getTotalSalary() - 5000) * 0.1f;
    }

    public float getActualSalsry() {
        return getTotalSalary() - getTax();
    }
}
