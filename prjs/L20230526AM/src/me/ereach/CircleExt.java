package me.ereach;

public class CircleExt extends Point2D {
    private float r;

    public CircleExt(float x, float y, float r) {
        super(x, y);
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "CircleExt{" +
                "o=" + super.toString() + ", " +
                "r=" + r +
                '}';
    }

    public Double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
