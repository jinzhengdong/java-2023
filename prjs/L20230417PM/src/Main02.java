import java.util.Date;

public class Main02 {
    public static void main(String[] args) {
        Person p1 = new Person("Lixinkai", "male", "2007-07-01");
        Person p2 = p1;
        System.out.println(p1);
        System.out.println(p2);

        Person p3 = new Person("Liu EnLai", "male", "2005-05-01");
        System.out.println(p3);
    }
}
