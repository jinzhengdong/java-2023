import java.awt.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int a = 10;
        int b = a;

        System.out.println(a);
        System.out.println(b);
        a = 20;
        System.out.println(a);
        System.out.println(b);

        Point p1 = new Point(10, 10);
        Point p2 = p1;
        System.out.println(p1);
        System.out.println(p2);
        p2.x = 50;
        p2.y = 70;
        System.out.println(p2);
        System.out.println(p1);

        p1.x = 3;
        p1.y = 5;
        System.out.println(p1);
        System.out.println(p2);

        Point p3 = new Point();
        p3 = p1;
        p1.x = 5;
        p1.y = 5;
        System.out.println(p3);
    }
}