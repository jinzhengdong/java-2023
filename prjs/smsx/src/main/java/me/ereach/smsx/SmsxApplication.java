package me.ereach.smsx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@MapperScan("me.ereach.smsx.mapper")
@EntityScan("me.ereach.smsx.model, me.ereach.smsx.viewmodel")
@SpringBootApplication
public class SmsxApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmsxApplication.class, args);
    }

}
