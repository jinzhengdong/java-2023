package com.lel;

public class Circle {
    private Point2D p;
    private float r;

    public Circle(Point2D p, float r) {
        this.p = p;
        this.r = r;
    }

    public Point2D getP() {
        return p;
    }

    public void setP(Point2D p) {
        this.p = p;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "p=" + p +
                ", r=" + r +
                '}';
    }

    public double yuan(){
        double s = Math.PI * Math.pow(r,2);
        return s;

    }
    public double yuanl(){
        double l = Math.PI * 2 * r;
        return l;
    }
}
