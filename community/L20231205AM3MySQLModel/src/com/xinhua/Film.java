package com.xinhua;

public class Film {
    private String filmName;
    private String Category;
    private String actors;
    // 时长
    private Integer length;
    // 票房
    private Integer marketing;

    public Film(String filmName, String category, String actors, Integer length, Integer marketing) {
        this.filmName = filmName;
        Category = category;
        this.actors = actors;
        this.length = length;
        this.marketing = marketing;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getMarketing() {
        return marketing;
    }

    public void setMarketing(Integer marketing) {
        this.marketing = marketing;
    }

    @Override
    public String toString() {
        return "Film{" +
                "filmName='" + filmName + '\'' +
                ", Category='" + Category + '\'' +
                ", actors='" + actors + '\'' +
                ", length=" + length +
                ", marketing=" + marketing +
                '}';
    }
}
