package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Point[] points = {
                new Point(2, 3),
                new Point(5, 7),
                new Circle(11, 13, 17),
                new Circle(4, 7, 6),
                new Point(6, 9)
        };

        for (var item : points)
            System.out.println(item);
    }
}
