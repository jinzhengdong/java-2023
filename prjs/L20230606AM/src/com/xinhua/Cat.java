package com.xinhua;

public class Cat extends Animal implements Behavior {
    @Override
    void sound() {
        System.out.println("miao miao miao");
    }

    @Override
    public void walk() {
        System.out.println("cat walking.");
    }
}
