package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class MyTest {
    @Test
    public void test07() {
        Predicate<Person> isFemale = x -> x.getGender() == "female";
        Predicate<Person> isMale = x -> x.getGender() == "male";
        Predicate<Person> isGreatThan18 = x -> x.getAge() > 18;

        Predicate<Person> isFemaleAndGreatThan18 = isFemale.and(isGreatThan18);
        Predicate<Person> isMaleAndGreatThan18 = isMale.and(isGreatThan18);

        List<Person> people = new ArrayList<>();

        people.add(new Person("Wendy", "female", 19));
        people.add(new Person("Cherry", "female", 16));
        people.add(new Person("Jerry", "male", 20));
        people.add(new Person("Tod", "male", 16));

        people.stream().forEach(x -> System.out.println(isFemaleAndGreatThan18.test(x)));
        System.out.println("===");
        people.stream().forEach(x -> System.out.println(isMaleAndGreatThan18.test(x)));

        System.out.println("===");

        people.forEach(person -> System.out.println(isMaleAndGreatThan18.test(person)));
        System.out.println("===");
        people.forEach(person -> System.out.println(isFemaleAndGreatThan18.test(person)));
    }
    @Test
    public void test06() {
        // 断言，判断是否有左花括号
        Predicate<String> hasLeftBrace = x -> x.startsWith("{");
        // 判断是否有右花括号
        Predicate<String> hasRightBrace = x -> x.endsWith("}");

        // 组合断言
        Predicate<String> hasLeftAndRightBraces = hasLeftBrace.and(hasRightBrace);

        System.out.println(hasLeftAndRightBraces.test("{name = 'Tod'}"));
        System.out.println(hasLeftAndRightBraces.test("name = 'Tod'"));
    }

    @Test
    public void test05() {
        // 断言
        Predicate<String> isLongThan5 = x -> x.length() > 5;

        Boolean rst = isLongThan5.test("Hello, World!");
        System.out.println(rst);

        System.out.println("===");
        System.out.println(isLongThan5.test("Tod"));
    }

    @Test
    public void test04() {
        // 替换冒号
        Function<String, String> replaceColon = x -> x.replace(":", "=");
        // 添加花括号
        Function<String, String> addBraces = x -> "{" + x + "}";

        String str = "name : 'Jerry'";
        System.out.println(replaceColon.andThen(addBraces).apply(str));
    }

    @Test
    public void test03() {
        // 供应商型
        Supplier<Double> getRandom = () -> Math.random();

        for (int i = 0; i < 10; i++)
            System.out.println(getRandom.get());
    }

    @Test
    public void test02() {
        List<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Cherry");
        list.add("Pear");

        Consumer<String> myPrint = x -> System.out.println(x);
        Consumer<String> myPrintUpper = x -> System.out.println(x.toUpperCase());

        list.forEach(myPrint.andThen(myPrintUpper));
    }

    @Test
    public void test01() {
        // 函数式接口，消费者
        Consumer<String> myPrint = x -> System.out.println(x);
        List<String> list = new ArrayList<>();
        list.add("Apple");
        list.add("Cherry");
        list.add("Pear");

        list.forEach(myPrint);

        System.out.println("===");
        Consumer<String> myPrint2 = (x) -> System.out.println(x);
        List<String> list2 = List.of("Apple", "Cherry", "Pear");
        list2.forEach(myPrint2);

        System.out.println("===");
        Consumer<String> myPrint3 = (String x) -> System.out.println(x);
        List<String> list3 = List.of("Apple", "Cherry", "Pear");
        list3.forEach(myPrint3);
    }
}
