package com.xinhua;

public class InvalidArgumentException extends Exception {
    public InvalidArgumentException() {
        super("Invalid argument in your account.");
    }

    public InvalidArgumentException(String message) {
        super(message);
    }
}
