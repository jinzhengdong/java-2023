package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MyTest {
    @Test
    public void test01() {
        List<String> list1 = new ArrayList<>();

        list1.add("Hello");
        list1.add("World");

        System.out.println(list1);

        List<String> list2 = new LinkedList<>();

        list2.add("Apple");
        list2.add("Banana");

        System.out.println(list2);
    }
}
