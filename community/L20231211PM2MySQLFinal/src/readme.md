-- rental 租赁
-- rental_id 租赁ID
-- rental_date 租赁日期
-- inventory_id 租赁商品
-- customer_id 客户ID
-- return_date 归还日期
-- staff_id 员工ID

```sql
select
    a.rental_id,
    a.return_date,
    a.inventory_id,
    a.customer_id,
    a.return_date,
    a.staff_id
from
    rental as a;

select
    count(电影名字), 电影名字
from
    v_rental
group by 电影名字
order by count(电影名字) desc;

select
    count(电影名字), 电影名字
from
    v_rental
group by 电影名字
order by count(电影名字) asc;
```