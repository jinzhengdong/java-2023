package com.xinhua;

public class Util {
//    public static int max(int a, int b) {
//        return a > b ? a : b;
//    }

    public static long max(long a, long b) {
        return a > b ? a : b;
    }

//    public static float max(float a, float b) {
//        return a > b ? a : b;
//    }

    public static double max(double a, double b) {
        return a > b ? a : b;
    }

    public static int min(int a, int b) {
        return a > b ? b : a;
    }
}
