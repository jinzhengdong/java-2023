public class Main04 {
    public static void main(String[] args) {
        int a = 10;
        int b;

        b = a++;
        System.out.println(b);
        System.out.println(a);

        b = ++a;
        System.out.println(b);
        System.out.println(a);
    }
}
