package com.xinhua;

public interface Geometry3D {
    double getVolume();
}
