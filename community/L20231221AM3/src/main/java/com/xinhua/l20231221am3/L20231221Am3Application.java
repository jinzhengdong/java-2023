package com.xinhua.l20231221am3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L20231221Am3Application {

    public static void main(String[] args) {
        SpringApplication.run(L20231221Am3Application.class, args);
    }

}
