package com.xinhua;

import org.junit.Test;

import java.util.*;
import java.util.stream.Stream;

public class MyTest {
    private List<Teacher> teachers = Arrays.asList(
            new Teacher("Mao WenTao", "female", "政治"),
            new Teacher("Liu ShangChao", "male", "C#"),
            new Teacher("Li YiLun", "male", "MySQL"),
            new Teacher("Jin ZhengDong", "male", "Java"),
            new Teacher("Sen XiaoMei", "female", "Web Page"),
            new Teacher("Fu YanRong", "female", "Network"),
            new Teacher("Ren SongLin", "male", "AR PR"),
            new Teacher("Wang XianQin", "female", "Office")
    );

    private List<Student> students = Arrays.asList(
            new Student("Zhang HaiShan", "male", "2021 Cloud", 19),
            new Student("Wang Deran", "male", "2021 Cloud", 18),
            new Student("Hu Long", "male", "2021 Cloud", 17),
            new Student("Du JinYu", "male", "2021 Cloud", 18),
            new Student("Zi YinHong", "male", "2021 Cloud", 16),
            new Student("Ma ZiXuan", "male", "2021 Cloud", 17),
            new Student("Li XinHao", "male", "2021 Cloud", 16),
            new Student("Bai YanWwei", "male", "2022 Cloud", 17),
            new Student("Guo Jian", "male", "2022 Cloud", 19),
            new Student("Zhou ShuangYang", "male", "2022 Cloud", 19),
            new Student("Chen RongLai", "male", "2022 Cloud", 17),
            new Student("Yu Yu", "male", "2022 Cloud", 18),
            new Student("Jiang FuYang", "male", "2022 Cloud", 17),
            new Student("Wan HaiChuan", "male", "2022 Cloud", 17),
            new Student("Lei ZhengYong", "male", "2022 Cloud", 16),
            new Student("Wang XiaoGgang", "male", "2022 Cloud", 17),
            new Student("Wu Ye", "male", "2022 Cloud", 17),
            new Student("Zeng HeRui", "female", "2022 Cloud", 18)
    );

    @Test
    public void test04() {
        System.out.println("The total age is " + students.stream().mapToDouble(Student::getAge).sum());
        System.out.println("The avg age is " + students.stream().mapToDouble(Student::getAge).average().orElse(0.0));
        System.out.println("There are " + students.stream().count() + " students.");
    }

    @Test
    public void test03() {
        Stream<Double> rnd = Stream.generate(Math::random);

        rnd.limit(5).forEach(System.out::println);
    }

    @Test
    public void test02() {
        double averageAge = students.stream()
                .mapToInt(Student::getAge)  // 将学生对象映射为年龄的整数流
                .average()  // 计算平均值
                .orElse(0.0);  // 如果没有学生或者年龄为空，则返回0.0

        System.out.println("Average age: " + averageAge);

        System.out.println("===");
        double totalAge = students.stream()
                .mapToInt(Student::getAge)
                .sum();
        System.out.println("Total age: " + totalAge);

        System.out.println("===");
        double maxAge = students.stream()
                .mapToInt(Student::getAge)
                .max()
                .orElse(0);
        System.out.println("max age: " + maxAge);

        System.out.println("===");
        double minAge = students.stream()
                .mapToInt(Student::getAge)
                .min()
                .orElse(0);
        System.out.println("min age" + minAge);

    }

    @Test
    public void test01() {
        Long cnt = students.stream().filter(x -> x.getClassName().equals("2022 Cloud")).count();
        System.out.println("Number of 2022 Cloud = " + cnt);
        System.out.println("===");
        cnt = students.stream().filter(x -> x.getClassName().equals("2021 Cloud")).count();
        System.out.println("Number of 2021 Cloud = " + cnt);
        System.out.println("===");
        System.out.println("Name starts with W = " + students.stream().filter(x -> x.getName().startsWith("W")).count());
    }
}
