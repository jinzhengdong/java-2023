package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void  test02() {
        Circle c1 = new Circle();
        Circle c2 = new Circle(7);

        System.out.println(c1);
        System.out.println(c2);

        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());

        System.out.println(c2.getArea());
        System.out.println(c2.getPerimeter());

        System.out.println(c2.getClassName());
    }

    @Test
    public void test01() {
        Point p1 = new Point();
        Point p2 = new Point(2, 3);

        System.out.println(p1);
        System.out.println(p2);

        p2.setX(2);
        p2.setY(3);

        System.out.println(p2);

        System.out.println(p1.getDistance(p2));
    }
}
