package me.ereach.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        // 获取UserService Bean
        UserService userService = context.getBean("userService", UserService.class);

        // 创建一个User对象
        User user = new User("John");

        // 调用UserService的保存用户方法
        userService.saveUser(user);
    }
}
