// 逻辑与运算符（如 &&）
// 逻辑或运算符（如 ||）
// 赋值运算符（如 =、+=、-=、*=、/=、%=）
public class Main03 {
    public static void main(String[] args) {
        // && 逻辑与 And
        System.out.println(true && true);   // true
        System.out.println(true && false);  // false
        System.out.println(false && true);  // false
        System.out.println(false && false); // false
        System.out.println("====");
        // || 逻辑或 Or
        System.out.println(true || true);   // true
        System.out.println(true || false);  // true
        System.out.println(false || true);  // true
        System.out.println(false || false); // false

        int age = 12;
        String gender = "male";

        //    false           true
        if (age > 18 && gender == "male")
            System.out.println("可以参军");
        else
            System.out.println("不可参军");

        System.out.println("===");

        if (age > 14 || gender == "female")
            System.out.println("可领32公斤粮食");
        else
            System.out.println("可领28公斤粮食");

    }
}
