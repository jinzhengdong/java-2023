package com.xinhua;

public class Demo2 {
    public static void main(String[] args) {
        int i1 = 10;
        int i2 = 12;

        System.out.println(i1 == i2);   // => false

        i2 = 10;

        System.out.println(i1 == i2);   // => true

        System.out.println(i1 > i2);    // false

        System.out.println(i1 <= i2);   // true

        System.out.println(i1 >= i2);   // true

        System.out.println(i1 != i2);   // false
    }
}
