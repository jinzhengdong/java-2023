# Spring Security最佳实践

## DB Layer

基于SQL Server的认证和权限系统，我们需要考虑以下几个方面：

* 用户表：用于存储所有的用户信息，包括用户名和密码等。
* 角色表：用于存储所有的角色信息，包括角色名称和角色描述等。
* 用户角色映射表：用于记录每个用户所属的角色。
* 权限表：用于存储所有的权限信息，包括权限名称和权限描述等。
* 角色权限映射表：用于记录每个角色所具有的权限。

基于上述需求，我们可以使用以下DDL语句来创建相应的表：

```sql
-- 用户表
CREATE TABLE [dbo].[Users] (
    [UserId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [Username] NVARCHAR(50) NOT NULL UNIQUE,
    [Password] NVARCHAR(50) NOT NULL
);

-- 角色表
CREATE TABLE [dbo].[Roles] (
    [RoleId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [RoleName] NVARCHAR(50) NOT NULL UNIQUE,
    [RoleDescription] NVARCHAR(100) NULL
);

-- 用户角色映射表
CREATE TABLE [dbo].[UserRoleMapping] (
    [UserId] INT NOT NULL,
    [RoleId] INT NOT NULL,
    CONSTRAINT [PK_UserRoleMapping] PRIMARY KEY ([UserId], [RoleId]),
    CONSTRAINT [FK_UserRoleMapping_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([UserId]),
    CONSTRAINT [FK_UserRoleMapping_Roles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles]([RoleId])
);

-- 权限表
CREATE TABLE [dbo].[Permissions] (
    [PermissionId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [PermissionName] NVARCHAR(50) NOT NULL UNIQUE,
    [PermissionDescription] NVARCHAR(100) NULL
);

-- 角色权限映射表
CREATE TABLE [dbo].[RolePermissionMapping] (
    [RoleId] INT NOT NULL,
    [PermissionId] INT NOT NULL,
    CONSTRAINT [PK_RolePermissionMapping] PRIMARY KEY ([RoleId], [PermissionId]),
    CONSTRAINT [FK_RolePermissionMapping_Roles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles]([RoleId]),
    CONSTRAINT [FK_RolePermissionMapping_Permissions] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permissions]([PermissionId])
);
```

在以上代码中，我们使用了CREATE TABLE语句来创建用户表、角色表、用户角色映射表、权限表和角色权限映射表。每个表都包括相应的字段和主键、外键等约束。其中，用户表的主键是UserId，角色表的主键是RoleId，权限表的主键是PermissionId，用户角色映射表和角色权限映射表的主键分别是UserId和RoleId、RoleId和PermissionId。同时，在用户表的Username字段上创建了一个UNIQUE约束，以确保每个用户名都是唯一的。

通过这些表的设计，我们可以实现基于SQL Server的认证和权限系统。例如，我们可以在用户登录时验证其用户名和密码，并在用户登录成功后根据其所属

## Java

### pom.xml

```xml
<dependencies>
    <!-- Spring Boot Web -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <!-- Spring Boot Security -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
    </dependency>

    <!-- MyBatis Plus -->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>3.4.3.1</version>
    </dependency>

    <!-- Lombok -->
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.22</version>
        <scope>provided</scope>
    </dependency>

    <!-- SQL Server JDBC Driver -->
    <dependency>
        <groupId>com.microsoft.sqlserver</groupId>
        <artifactId>mssql-jdbc</artifactId>
        <version>9.4.0.jre11</version>
    </dependency>
</dependencies>
```

这里我们添加了Spring Boot Web和Spring Boot Security依赖，用于创建RESTful API并实现认证和授权功能。MyBatis Plus用于处理数据库的CRUD操作，Lombok用于简化实体类的代码。最后，我们添加了SQL Server JDBC驱动程序，用于连接SQL Server数据库。

### 实体类

基于Java Spring Boot，Spring Security，MyBatis Plus，Lombox依赖的实体类代码：

* User 实体类

```java
package me.ereach.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @TableId(type = IdType.AUTO)
    private Integer userId;

    private String username;

    private String password;

}
```

* Role

```java
package me.ereach.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role {

    @TableId(type = IdType.AUTO)
    private Integer roleId;

    private String roleName;

    private String roleDescription;

}
```

* UserRoleMapping

```java
package me.ereach.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleMapping {

    @TableId
    private Integer userId;

    @TableId
    private Integer roleId;

}
```

* Permision

```java
package me.ereach.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Permission {

    @TableId(type = IdType.AUTO)
    private Integer permissionId;

    private String permissionName;

    private String permissionDescription;

}
```

* RolePermissionMapping

```java
package me.ereach.model;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolePermissionMapping {

    @TableId
    private Integer roleId;

    @TableId
    private Integer permissionId;

}
```

这里我们使用了Lombok来简化实体类的代码，MyBatis Plus用于处理数据库的CRUD操作，使用@TableId来标记实体类的主键字段，@IdType.AUTO表示主键自增长。这些实体类可以与数据库表进行一一对应，方便后续的数据库操作。

### Mapper

基于上面的实体类在me.ereach.mapper包下创建相关实体类的mapper接口，并继承BaseMapper的代码：

* UserMapper

```java
package me.ereach.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.model.User;

public interface UserMapper extends BaseMapper<User> {

}
```

* RoleMapper

```java
package me.ereach.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.model.Role;

public interface RoleMapper extends BaseMapper<Role> {

}
```

* UserRoleMappingMapper

```java
package me.ereach.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.model.UserRoleMapping;

public interface UserRoleMappingMapper extends BaseMapper<UserRoleMapping> {

}
```

* PermissionMapper

```java
package me.ereach.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.model.Permission;

public interface PermissionMapper extends BaseMapper<Permission> {

}
```

* RolePermissionMappingMapper

```java
package me.ereach.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.model.RolePermissionMapping;

public interface RolePermissionMappingMapper extends BaseMapper<RolePermissionMapping> {

}
```

这里我们使用MyBatis Plus的BaseMapper接口，可以自动实现基本的CRUD操作，同时我们还可以根据需要编写自定义的SQL语句。

### Controller

在me.ereach.controller包下实现一组RESTful API方法的代码：

```java
package me.ereach.controller;

import me.ereach.mapper.UserMapper;
import me.ereach.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @Secured("ROLE_Admin")
    @PostMapping("/user")
    public User addUser(@RequestBody User user) {
        userMapper.insert(user);
        return user;
    }
}
```

这里我们使用Spring Boot Web提供的@RestController和@RequestMapping注解定义路由和处理HTTP请求的方法。@Secured注解用于限制该方法只能由具有'Admin'角色的用户访问。@PostMapping注解定义HTTP POST请求的方法，并且请求的路径是/user。在方法内部，我们通过调用UserMapper的insert方法将用户添加到数据库中，并返回添加的用户对象。

### Config

以下是针对上面HTTP POST /api/user 方法添加相关拦截器配置的代码：

```java
package me.ereach.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import me.ereach.security.JwtAuthenticationEntryPoint;
import me.ereach.security.JwtAuthenticationFilter;
import me.ereach.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .authorizeRequests()
            .antMatchers("/api/user").hasRole("Admin")
            .antMatchers("/api/authenticate").permitAll()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
            .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(new JwtAuthenticationFilter(userDetailsService), UsernamePasswordAuthenticationFilter.class);
    }

}
```

这里我们使用了Spring Security的@EnableWebSecurity注解开启Web安全功能。在configureGlobal方法中，我们配置了UserDetailsServiceImpl和PasswordEncoder，用于实现用户的认证和密码加密。在configure方法中，我们设置了HTTP请求的授权规则，限制只有具有'Admin'角色的用户才能访问/api/user方法。同时，我们添加了一个JwtAuthenticationFilter拦截器，用于解析和验证JWT令牌，确保只有经过认证的用户才能访问受保护的资源。

### Entry Point

```java
package me.ereach;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("me.ereach.mapper")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
```

### Demo

以下是向用户系统中添加用户jinzd并授权的代码：

```java
package me.ereach;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import me.ereach.model.Role;
import me.ereach.model.User;
import me.ereach.service.RoleService;
import me.ereach.service.UserService;

@Component
public class DataInitializer implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        // 创建Admin角色
        Role adminRole = new Role();
        adminRole.setName("Admin");
        roleService.save(adminRole);

        // 创建jinzd用户
        User jinzd = new User();
        jinzd.setUsername("jinzd");
        jinzd.setPassword(passwordEncoder.encode("jinzd"));
        jinzd.setRoles(Arrays.asList(adminRole));
        userService.save(jinzd);
    }

}
```

这里我们创建了一个DataInitializer类，该类实现了CommandLineRunner接口，它会在Spring Boot启动时执行run方法，向用户系统中添加用户jinzd并授权。在run方法中，我们首先创建了一个Admin角色，并将其保存到数据库中。然后，我们创建一个jinzd用户，并设置其密码为jinzd，并将其授权为Admin角色。最后，我们使用UserService的save方法将该用户保存到数据库中。

### 测试

当本项目正常启动时，可以通过以下步骤进行用户添加操作，并检查是否能够成功登录：

* 启动项目后，在浏览器中访问<http://localhost:8080/login，跳转到登录页面。>
* 在登录页面中，输入用户名jinzd和密码jinzd，点击登录按钮。
* 如果登录成功，会跳转到应用首页。
* 在浏览器中访问<http://localhost:8080/api/user，此时应该会返回401> Unauthorized错误，因为该请求需要授权才能访问。
* 在请求头中添加Authorization信息，该信息的值为Bearer ${token}，其中${token}是在登录成功后返回的token值，格式如下：

```makefile
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqaW56ZCIsImlhdCI6MTYxODA1NjA4OCwiZXhwIjoxNjE4MDU5Njg4fQ.C2Mps5an5F5zeytl1rDQ2WEV7ZFFmxHY1uV9Gvyo13I
```

* 然后再次访问<http://localhost:8080/api/user，此时应该可以成功访问该API。>
* 发送一个POST请求，请求体中添加一个新用户的信息，如下所示：

```json
{
    "username": "test",
    "password": "test",
    "roles": [
        {
            "id": 1,
            "name": "Admin"
        }
    ]
}
```

* 如果添加成功，返回的状态码应该是201 Created，表示新用户已经成功添加到数据库中。
* 在数据库中检查，新用户test已经被成功添加到了用户系统中。

总的来说，通过以上步骤，我们可以完成用户添加的操作，并检查是否能够通过用户jinzd进行添加操作。

## Front-end

以下是基于jQuery, Ajax和Layui构建的前后端分离的前端项目示例，包含登录页面（login.html）和主页面（index.html）。

在/webapp目录下创建login.html文件，内容如下：

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Login</title>
  <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/layui/2.5.7/css/layui.css">
  <style>
    body {
      background-color: #F5F5F5;
    }

    .login-form {
      margin-top: 150px;
    }

    .login-form .layui-form-item {
      margin-bottom: 20px;
    }

    .login-form .layui-input {
      height: 38px;
    }

    .login-form .login-btn {
      width: 100%;
      height: 48px;
    }
  </style>
</head>
<body>
<div class="layui-container">
  <div class="layui-row">
    <div class="layui-col-md4 layui-col-md-offset4 login-form">
      <form class="layui-form" action="">
        <div class="layui-form-item">
          <label class="layui-form-label">Username</label>
          <div class="layui-input-block">
            <input type="text" name="username" lay-verify="required" placeholder="Enter username"
                   autocomplete="off"
                   class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <label class="layui-form-label">Password</label>
          <div class="layui-input-block">
            <input type="password" name="password" lay-verify="required" placeholder="Enter password"
                   autocomplete="off"
                   class="layui-input">
          </div>
        </div>
        <div class="layui-form-item">
          <button class="layui-btn login-btn" lay-submit lay-filter="login">Login</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.bootcdn.net/ajax/libs/layui/2.5.7/layui.all.js"></script>
<script>
  $(function () {
    layui.form.on('submit(login)', function (data) {
      $.ajax({
        type: 'POST',
        url: '/api/login',
        contentType: 'application/json',
        data: JSON.stringify(data.field),
        success: function (result) {
          if (result.success) {
            // 登录成功跳转到首页
            window.location.href = 'index.html';
          } else {
            layer.msg(result.message, {icon: 2});
          }
        }
      });
      return false;
    });
  });
</script>
</body>
</html>
```

对于系统登录后自动跳转到 index.html，可以通过 JavaScript 在登录成功后进行页面跳转。具体实现可以在登录成功的回调函数中添加以下代码：

```javascript
window.location.href = "index.html";
```

其中，window.location.href用于跳转页面，将其设置为 index.html 的地址即可。
