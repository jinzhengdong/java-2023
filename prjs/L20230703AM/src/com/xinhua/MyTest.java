package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        String temp = " >>>> ";

        greet((String message) -> { System.out.println(message);});
        greet(message -> System.out.println(message));
        System.out.println("===");
        greet(System.out::println);
        System.out.println("===");

        greet(message -> System.out.println(temp + message + temp));
    }

    @Test
    public void test02() {
        greet(new Printer() {
            @Override
            public void print(String message) {
                System.out.println("====" + message + "====");
            }
        });
    }

    @Test
    public void test01() {
        greet(new ConsolePrinter());
    }

    public void greet(Printer printer) {
        printer.print("Hello, World!");
    }
}
