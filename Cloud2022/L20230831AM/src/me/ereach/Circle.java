package me.ereach;

public class Circle extends Point {
    private float r;

    public Circle(float x, float y, float r) {
        super(x, y);
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "r=" + r +
                '}';
    }

    public double getArea() {
        return Math.PI * r * r;
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
