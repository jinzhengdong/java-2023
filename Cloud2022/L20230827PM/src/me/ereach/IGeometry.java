package me.ereach;

interface IGeometry {
    double getArea();
    double getPerimeter();
}
