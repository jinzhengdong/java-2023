package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        Rect r1 = new Rect(5, 3, new Point(2, 3));
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
    }

    @Test
    public void test01() {
        Circle c1 = new Circle(2, 3, 5);
        System.out.println(c1);

        Circle c2 = new Circle(7, 11, 13);
        System.out.println(c1.getDistance(c2));
    }
}
