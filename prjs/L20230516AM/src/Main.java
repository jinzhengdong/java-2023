public class Main {
    public static void main(String[] args) {
        Point p1 = new Point();
        Point p2 = new Point();

        System.out.println(p1);
        System.out.println(p2);

        System.out.println("===");

        p1.setX(1);
        p1.setY(2);
        p2.setX(5);
        p2.setY(7);

        System.out.println(p1);
        System.out.println(p2);

    }
}