## Java泛型测验



姓名：



日期：



### 填空题（共5题，每题5分）：

1. 泛型类的类型参数放在类名的（                               ）处。
   答案：尖括号（<>）。

2. 使用通配符"?"表示（                               ）。
   答案：未知类型。

3. 类型擦除是指在编译时将泛型类型转换为（                               ）。
   答案：原始类型。

4. 桥方法是在泛型类或泛型接口中为了解决（                                                             ）而生成的方法。
   答案：类型擦除带来的类型安全问题。

5. 通配符捕获是指将通配符类型赋值给（                               ）类型的过程。
   答案：具体类型。

### 单项选择题（共5题，每题5分）：

1. 泛型的主要作用是什么？
   a. 提高代码的可读性
   b. 提高代码的性能
   c. 提高代码的类型安全性
   d. 提高代码的复杂性
   答案：c

2. 下面哪个选项是泛型方法的正确定义方式？
   a. public void method<T>(T item)
   b. public <T> void method(T item)
   c. public void <T> method(T item)
   d. public void method(T item) <T>
   答案：b

3. 下面哪个选项是泛型接口的正确定义方式？
   a. interface MyInterface<T> {}
   b. interface MyInterface<> {}
   c. interface MyInterface {}
   d. interface MyInterface<T extends Object> {}
   答案：a

4. 下面哪个选项是泛型类的正确定义方式？
   a. class MyClass<T> {}
   b. class MyClass<> {}
   c. class MyClass {}
   d. class MyClass<T extends Object> {}
   答案：a

5. 下面哪个选项是泛型方法中的通配符的正确使用方式？
   a. List<?> list = new ArrayList<>();
   b. List<T> list = new ArrayList<?>();
   c. List<?> list = new ArrayList<T>();
   d. List<T> list = new ArrayList<>();
   答案：a

### 编程题（共5题，每题10分）：

1. 编写一个泛型方法`printArray()`，接受一个数组作为参数，并打印数组中的所有元素。要求使用泛型来实现。
```java
public class Main {
    public static <T> void printArray(T[] array) {
        for (T element : array) {
            System.out.println(element);
        }
    }

    public static void main(String[] args) {
        Integer[] intArray = {1, 2, 3, 4, 5};
        String[] stringArray = {"Hello", "World"};

        printArray(intArray);
        printArray(stringArray);
    }
}
```

2. 定义一个泛型类`Pair<K, V>`，表示键值对。类中包含两个私有属性`key`和`value`，以及相应的getter和setter方法。编写一个泛型方法`printPair()`，接受一个`Pair`对象作为参数，并打印出键值对的内容。
```java
public class Pair<K, V> {
    private K key;
    private V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}

public class Main {
    public static <K, V> void printPair(Pair<K, V> pair) {
        System.out.println("Key: " + pair.getKey());
        System.out.println("Value: " + pair.getValue());
    }

    public static void main(String[] args) {
        Pair<String, Integer> pair = new Pair<>("Java", 2023);
        printPair(pair);
    }
}
```

3. 定义一个泛型接口`Calculator<T>`，包含一个计算方法`T calculate(T a, T b)`，用于对两个参数进行计算并返回结果。编写一个实现类`AdditionCalculator<T>`，实现泛型接口并实现加法计算。
```java
public interface Calculator<T> {
    T calculate(T a, T b);
}

public class AdditionCalculator<T extends Number> implements Calculator<T> {
    @Override
    public T calculate(T a, T b) {
        if (a instanceof Integer) {
            return (T) Integer.valueOf(a.intValue() + b.intValue());
        } else if (a instanceof Double) {
            return (T) Double.valueOf(a.doubleValue() + b.doubleValue());
        } else if (a instanceof Float) {
            return (T) Float.valueOf(a.floatValue() + b.floatValue());
        } else {
            throw new IllegalArgumentException("Unsupported number type");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Calculator<Integer> intCalculator = new AdditionCalculator<>();
        System.out.println(intCalculator.calculate(2, 3));

        Calculator<Double> doubleCalculator = new AdditionCalculator<>();
        System.out.println(doubleCalculator.calculate(2.5, 3.7));
    }
}
```

4. 定义一个泛型方法`findMax()`，接受一个泛型列表作为参数，并返回列表中的最大值。要求列表中的元素必须实现`Comparable`接口。如果列表为空，则返回`null`。
```java
import java.util.List;

public class Main {
    public static <T extends Comparable<T>> T findMax(List<T> list) {
        if (list.isEmpty()) {
            return null;
        }

        T max = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            T current = list.get(i);
            if (current.compareTo(max) > 0) {
                max = current;
            }
        }

        return max;
    }

    public static void main(String[] args) {
        List<Integer> intList = List.of(2, 5, 1, 4, 3);
        System.out.println(findMax(intList));

        List<String> stringList = List.of("apple", "banana", "orange");
        System.out.println(findMax(stringList));
    }
}
```

5. 定义一个泛型类`Box<T>`，表示一个装箱子。类中包含一个私有属性`item`，以及相应的getter和setter方法。编写一个泛型方法`printBox()`，接受一个`Box`对象作为参数，并打印出箱子中的物品。
```java
public class Box<T> {
    private T item;

    public Box(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}

public class Main {
    public static <T> void printBox(Box<T> box) {
        System.out.println("Item: " + box.getItem());
    }

    public static void main(String[] args) {
        Box<String> stringBox = new Box<>("Book");
        printBox(stringBox);

        Box<Integer> intBox = new Box<>(123);
        printBox(intBox);
    }
}
```

## 类型擦除

在Java泛型中，类型擦除是指在编译时期将泛型类型转换为它们的原始类型。这是为了保持与旧版本Java代码的兼容性，因为泛型是在Java 5引入的。

类型擦除的过程包括以下几个步骤：

1. 替换类型参数：在泛型类或泛型方法的定义中，所有的类型参数都会被它们的限定类型或者Object类型所替换。例如，`List<T>`会被替换为`List<Object>`。

2. 擦除类型变量：在泛型类或泛型方法的定义中，所有的类型变量（即类型参数和类型参数的限定类型）都会被它们的第一个限定类型所替换。如果没有指定限定类型，则会被替换为Object类型。例如，`List<T extends Number>`会被替换为`List<Number>`。

3. 擦除泛型方法：在泛型方法中，类型参数的擦除过程与泛型类类似。泛型方法的类型参数会被替换为它们的限定类型或者Object类型。

4. 插入类型转换：在需要的地方，编译器会插入类型转换操作，以确保类型安全。这些类型转换操作是在类型擦除后进行的，以保持与原始泛型类型的兼容性。

下面是一个示例来说明类型擦除的概念：

```java
public class MyClass<T> {
    private T item;

    public MyClass(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public void printType() {
        System.out.println("Type: " + item.getClass().getSimpleName());
    }
}
```

在上述示例中，`MyClass`是一个泛型类，包含一个泛型类型参数`T`。它有一个私有属性`item`，以及相应的getter和setter方法。还有一个`printType()`方法，用于打印泛型类型的名称。

在编译时，类型擦除会将`MyClass<T>`转换为`MyClass<Object>`。这意味着所有的`T`都会被替换为`Object`。例如，`T item`会被替换为`Object item`。

此外，编译器会在需要的地方插入类型转换操作，以确保类型安全。在`printType()`方法中，`item.getClass().getSimpleName()`会被转换为`item.getClass().getSimpleName()`，因为类型擦除后，`item`的类型被擦除为`Object`，而`Object`类有一个`getClass()`方法。

下面是一个演示示例的代码：

```java
public class Main {
    public static void main(String[] args) {
        MyClass<String> stringClass = new MyClass<>("Hello");
        stringClass.printType();  // 输出：Type: String

        MyClass<Integer> intClass = new MyClass<>(123);
        intClass.printType();  // 输出：Type: Integer
    }
}
```

在上述示例中，我们创建了一个`MyClass<String>`对象和一个`MyClass<Integer>`对象。在运行时，`printType()`方法会打印出实际的泛型类型名称。由于类型擦除，`T`被替换为`Object`，所以输出结果分别为`Type: String`和`Type: Integer`。

这个示例演示了Java泛型中的类型擦除概念，即在编译时将泛型类型转换为它们的原始类型，并在需要的地方插入类型转换操作以保持类型安全。

## 桥方法

桥方法是在泛型类或泛型接口中为了解决类型擦除带来的类型安全问题而生成的方法。在Java中，泛型是通过类型擦除来实现的，即在编译时期，泛型类型会被擦除为它们的原始类型。这样做是为了保持与旧版本Java代码的兼容性，因为泛型是在Java 5引入的。

类型擦除会导致一个问题，即在泛型类或泛型接口中，如果存在重写（覆盖）的方法，由于类型擦除，它们的方法签名会变得相同，这可能会导致类型安全问题。为了解决这个问题，编译器会自动生成桥方法。

桥方法的作用是在类型擦除后，保留原始方法的签名，以确保类型安全。它们会在字节码中生成，并且会调用原始方法，以便在类型擦除后仍然能够正确处理泛型类型。

下面是一个示例来说明桥方法的概念：

```java
public class MyClass<T> {
    public void print(T item) {
        System.out.println(item.toString());
    }
}

public class StringClass extends MyClass<String> {
    @Override
    public void print(String item) {
        System.out.println("String: " + item);
    }
}
```

在上述示例中，`MyClass`是一个泛型类，`StringClass`继承自`MyClass`并指定了具体的类型参数为`String`。`StringClass`重写了`print()`方法，并添加了额外的逻辑。

在编译时，由于类型擦除，`StringClass`的`print()`方法的签名与父类的`print()`方法相同，都是`void print(Object item)`。为了解决这个问题，编译器会自动生成一个桥方法，保留原始方法的签名。在字节码中，会生成以下两个方法：

```java
// 桥方法
public void print(Object item) {
    print((String) item);
}

// 原始方法
public void print(String item) {
    System.out.println("String: " + item);
}
```

通过生成桥方法，编译器确保了在类型擦除后，仍然能够正确调用`StringClass`中的`print()`方法，并处理`String`类型的参数。

总结来说，桥方法是为了解决泛型类或泛型接口中类型擦除带来的类型安全问题而生成的方法。它们保留了原始方法的签名，在类型擦除后仍然能够正确处理泛型类型。
