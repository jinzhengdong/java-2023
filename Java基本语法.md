## Java基本语法

Java基本语法是学习Java编程的第一步。在学习Java语法时，需要掌握以下几个方面：

### Java基本语句

Java基本语句是Java代码的基本构建块。Java基本语句包括声明、赋值、条件、循环等语句。以下是一些常见的Java基本语句：

java

复制代码

```java
//声明语句
int a;
double b = 3.14;

//赋值语句
a = 1;
b = 2.0;

//条件语句
if (a == 1) {
    System.out.println("a is 1");
} else {
    System.out.println("a is not 1");
}

//循环语句
for (int i = 0; i < 10; i++) {
    System.out.println(i);
}
```

### Java数据类型

Java数据类型分为基本数据类型和引用数据类型。Java基本数据类型包括整型、浮点型、布尔型和字符型。以下是Java基本数据类型的示例：

java

复制代码

```java
//整型数据类型
int a = 1;
long b = 2L;

//浮点型数据类型
float c = 3.14f;
double d = 3.14;

//布尔型数据类型
boolean e = true;
boolean f = false;

//字符型数据类型
char g = 'a';
```

### Java运算符

Java运算符包括算术运算符、关系运算符、逻辑运算符、位运算符等。以下是Java运算符的示例：

java

复制代码

```java
//算术运算符
int a = 1 + 2;
int b = 3 - 4;
int c = 5 * 6;
int d = 7 / 8;
int e = 9 % 10;

//关系运算符
boolean f = 1 == 2;
boolean g = 3 != 4;
boolean h = 5 > 6;
boolean i = 7 < 8;
boolean j = 9 >= 10;
boolean k = 11 <= 12;

//逻辑运算符
boolean l = true && false;
boolean m = true || false;
boolean n = !true;

//位运算符
int o = 1 & 2;
int p = 3 | 4;
int q = 5 ^ 6;
int r = ~7;
```

### Java控制语句

Java控制语句包括条件语句和循环语句。以下是Java控制语句的示例：

java

复制代码

```java
//条件语句
int a = 1;
if (a == 1) {
    System.out.println("a is 1");
} else {
    System.out.println("a is not 1");
}

//循环语句
for (int i = 0; i < 10; i++) {
    System.out.println(i);
}

//while循环
int j = 0;
while (j < 10) {
    System.out.println(j);
    j++;
}
```

### Java方法

Java方法是一段可重用的代码块。Java方法可以接受参数，也可以返回值。以下是Java方法的示例：

java

复制代码

```java
//方法的定义和调用
public int add(int a, int b) {
    return a + b;
}
int c = add(1, 2);

//方法的重载
public int add(int a, int b, int c) {
    return a + b + c;
}
int d = add(1, 2, 3);
```

## 总结

以上是Java基本语法的一些知识点和示例，需要不断地掌握和练习，以便在后续的Java开发中更加熟练地使用Java语言。