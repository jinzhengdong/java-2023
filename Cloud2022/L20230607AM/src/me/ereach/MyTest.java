package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        int temp = 20;

        if (temp > 30) {
            if (temp > 35) {
                if (temp > 40) {
                    System.out.println("很热，多喝水，不要外出");
                }
                else {
                    System.out.println("很热，多喝水");
                }
            }
            else {
                System.out.println("有点热");
            }
        }
        else {
            System.out.println("天气暖和");
        }
    }

    @Test
    public void test02() {
        int temp = 18;

        if (temp >= 40) {
            System.out.println("天太热，多喝水，不要外出！");
        }
        else if (temp > 35) {
            System.out.println("天太热，多喝水");
        }
        else if (temp > 30) {
            System.out.println("有点热");
        } else if (temp > 25) {
            System.out.println("天气很好");
        } else if (temp > 20) {
            System.out.println("天气凉爽");
        }
        else {
            System.out.println("有点凉");
        }
    }

    @Test
    public void test01() {
        int age = 19;
        String gender = "female";

        if (age > 18 && gender == "male") {
            System.out.println("可以参军");
        }
        else {
            System.out.println("不可参军");
        }
    }
}
