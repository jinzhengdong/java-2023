package com.xinhua;

public class Account {
    private float balance;

    public Account(float balance) {
        this.balance = balance;
    }

    public Account() {
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "balance=" + balance +
                '}';
    }

    public void deposit(float value) throws InvalidArgumentException {
        if (value <= 0)
            throw new InvalidArgumentException("�����������");

        balance += value;
    }

    public void withdraw(float value) throws InvalidArgumentException {
        if (value <= 0 || value > balance)
            throw new InvalidArgumentException();

        balance -= value;
    }
}
