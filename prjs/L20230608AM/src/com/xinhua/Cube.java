package com.xinhua;

public class Cube implements Geometry2D, Geometry3D {
    private float s;

    public Cube(float s) {
        this.s = s;
    }

    public float getS() {
        return s;
    }

    public void setS(float s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return "Cube{" +
                "s=" + s +
                '}';
    }

    @Override
    public double getArea() {
        return s * s * 6;
    }

    @Override
    public double getPerimeter() {
        return s * 12;
    }

    @Override
    public double getVolume() {
        return s * s * s;
    }
}
