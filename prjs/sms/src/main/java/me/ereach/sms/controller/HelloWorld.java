package me.ereach.sms.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class HelloWorld {
    @GetMapping("/sayhello")
    public String sayHello() {
        return "Hello, World";
    }
}
