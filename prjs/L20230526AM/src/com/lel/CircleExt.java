package com.lel;

public class CircleExt extends Point2D{
    private float r;

    public CircleExt(float x, float y, float r) {
        super(x, y);
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "CircleExt{" +
                super.toString() + "," +
                "r=" + r +
                '}';
    }
    public double yuans(){
        double s = Math.PI * Math.pow(r,2);
        return s;
    }
    public double yuanl(){
        double l = Math.PI * 2 * r;
        return l;
    }
}
