package me.ereach;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MyTest {
    @Test
    public void test04() {
// 使用Stream.of()方法创建Stream
        Stream<String> stream = Stream.of("Alice", "Bob", "Charlie");

        // 输出Stream中的元素
        stream.forEach(System.out::println);
    }

    @Test
    public void test03() {
        String fileName = "d:\\readme.txt";

        // 从文件创建Stream
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            // 输出Stream中的元素
            stream.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test02() {
        int[] numbers = {1, 2, 3, 4, 5};

        // 从数组创建IntStream
        IntStream stream = Arrays.stream(numbers);

        // 输出Stream中的元素
        stream.forEach(System.out::println);
    }

    @Test
    public void test01() {
        List<String> names = new ArrayList<>();

        names.add("Alice");
        names.add("Bob");
        names.add("Charlie");

        System.out.println("===4");

        names.stream().forEach(System.out::println);

        System.out.println("===3");

        names.stream().forEach(x -> System.out.println(x));

        System.out.println("===3==");

        names.stream().forEach((String item) -> {
            System.out.println(item);
        });

        System.out.println("===2");

        // 从List集合创建Stream
        Stream<String> stream = names.stream();

        // 输出Stream中的元素
        stream.forEach(System.out::println);

        System.out.println("===1");
        names.forEach(x -> System.out.println(x));

        System.out.println("===1==");

        for (var x : names)
            System.out.println(x);
    }
}
