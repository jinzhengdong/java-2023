package com.xinhua;

import org.junit.Test;

import java.awt.*;

public class MyTest {
    @Test
    public void test03() {
        Circle c1 = new Circle(7, new Point2D(3, 5));
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getArea());
        System.out.println(c1);
    }

    @Test
    public void test02() {
        Point2D p1 = new Point2D(2, 3);
        Point2D p2 = new Point2D(2, 3);

        System.out.println(p1 == p2);   // false
        System.out.println(p1.equals(p2));  // true

        Point2D p3 = p1;
        System.out.println(p1 == p3);   // true
        System.out.println(p1);
        System.out.println(p3);
    }

    @Test
    public void test01() {
        java.awt.Point p1 = new Point(2, 3);
        java.awt.Point p2 = new Point(2, 3);

        System.out.println(p1 == p2);   // false
        System.out.println(p1.equals(p2));  // true
    }
}
