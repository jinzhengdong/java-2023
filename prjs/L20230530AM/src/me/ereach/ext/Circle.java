package me.ereach.ext;

import me.ereach.Point2D;

public class Circle extends Point2D {
    private float r;

    public Circle(float x, float y, float r) {
        super(x, y);
        this.r = r;
    }

    public float getX() {
        return super.getX();
    }

    public void setX(float x) {
        super.setX(x);
    }

    public float getY() {
        return super.getY();
    }

    public void setY(float y) {
        super.setY(y);
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "o=" + super.toString() + "," +
                "r=" + r +
                '}';
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
