public class Point3D {
    private double x;
    private double y;
    private double z;

    public Point3D() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Point3D(double x, double y) {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    public double getDistance(Point3D p) {
        double w = this.x - p.x;
        double h = this.y - p.y;
        double s = this.z - p.z;
        return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2) + Math.pow(s, 2));
    }

    public static double getDistanceStatic(Point3D p1, Point3D p2) {
        double w = p1.x - p2.x;
        double h = p1.y - p2.y;
        double s = p1.z - p2.z;
        return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2) + Math.pow(s, 2));
    }
}
