package com.xinhua;

public interface Geo {
    double getArea();
    double getPerimeter();
}
