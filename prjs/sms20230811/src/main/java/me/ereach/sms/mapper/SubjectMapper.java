package me.ereach.sms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.ereach.sms.model.Subject;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectMapper extends BaseMapper<Subject> {
}
