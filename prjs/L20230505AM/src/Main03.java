public class Main03 {
    public static void main1(String[] args) {
        int num = 12;
        boolean flag = true;

//        for (int i = 2; i < num; i++) {
//            if (num % i == 0) {
//                flag = false;
//                break;
//            }
//        }

        for (int i = 2; i < num; i++)
            if (num % i == 0) {
                flag = false;
                break;
            }

//        if (flag) {
//            System.out.println("the num " + num + " is prime");
//        }
//        else
//        {
//            System.out.println("the num " + num + " is not prime");
//        }

        if (flag)
            System.out.println("the num " + num + " is prime");
        else
            System.out.println("the num " + num + " is not prime");

    }

    public static void main(String[] args) {
        int num = 13;
        System.out.println(checkPrime(num));

        num = 16;
        System.out.println(checkPrime(num));
    }

    public static String checkPrime(int num) {
        boolean flag = true;

        for (int i = 2; i < num; i++)
            if (num % i == 0) {
                flag = false;
                break;
            }

        if (flag)
            return "the num " + num + " is prime.";
        else
            return "the num " + num + " is not prime.";
    }
}
