import me.ereach.geo.Point3D;

public class Main {
    public static void main(String[] args) {
        Point3D p1 = new Point3D();
        System.out.println(p1);
        Point3D p2 = new Point3D(2, 3, 5);
        System.out.println(p2);
    }
}