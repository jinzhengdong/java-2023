package com.xinhua.edu;

public class Lesson3 {
    public static void main(String[] args) {
        System.out.println(mul(3, 8));
        System.out.println(mul(5, 7));

        System.out.println(div(8, 4));
        System.out.println(div(19, 2));
    }

    public static int mul(int a, int b) {
        return a * b;
    }

    public static int div(int a, int b) {
        return a / b;
    }
}
