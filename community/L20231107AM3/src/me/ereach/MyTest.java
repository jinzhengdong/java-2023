package me.ereach;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class MyTest {
    @Test
    public void test01() {
        List<Teacher> list = new LinkedList<>();

        list.add(new Teacher("Tom", "male", "Java"));
        list.add(new Teacher("Jerry", "male", "C#"));
        list.add(new Teacher("Cherry", "female", "Python"));

        System.out.println(list);

        list.stream().filter(x -> x.getName() == "Jerry").forEach(x -> System.out.println("qry: " + x));

        list.remove(1);

        System.out.println(list);
    }
}
