package com.xinhua;

// 几何形状接口
public interface Geometry {
    // 计算面积
    double getArea();

    // 计算周长
    double getPerimeter();
}
