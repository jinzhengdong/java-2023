import java.util.Arrays;

public class Main02 {
    public static void main(String[] args) {
        int[] intArray = {1, 2, 7, 3};
        System.out.println(intArray);
        System.out.println(Arrays.toString(intArray));

        for (int i : intArray)
            if (i == 7)
                System.out.println(true);
            else
                System.out.println(false);
    }
}
