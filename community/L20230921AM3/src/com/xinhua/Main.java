package com.xinhua;

public class Main {
    public static void main(String[] args) {
        byte b = 12;
        System.out.println(b);
        b = 18;
        System.out.println(b);
        b = 127;
        System.out.println(b);

//        b = 128;
        b = -128;
        System.out.println(b);
//        b = -129;

        int i = 128;
        System.out.println(i);
        i = 1234567;
        System.out.println(i);

        short s = 345;
        System.out.println(s);

        int i1 = 327;
        System.out.println(i1);

        long l = 34567;
        System.out.println(l);

    }
}
