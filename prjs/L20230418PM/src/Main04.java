public class Main04 {
    public static void main(String[] args) {
        System.out.println("abcdefg\nabcdefg\n1234567890");

        // table \t
        System.out.println("c1\tc2\tc3\n1\t2\t3\n4\t5\t6");

        System.out.println("c1\tc2\tc3");
        System.out.println("1\t2\t3");
        System.out.println("4\t5\t6");
    }
}
