package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MyTest {
    private List<Teacher> teachers = Arrays.asList(
            new Teacher("Mao WenTao", "female", "政治"),
            new Teacher("Liu ShangChao", "male", "C#"),
            new Teacher("Li YiLun", "male", "MySQL"),
            new Teacher("Jin ZhengDong", "male", "Java"),
            new Teacher("Sen XiaoMei", "female", "Web Page"),
            new Teacher("Fu YanRong", "female", "Network"),
            new Teacher("Ren SongLin", "male", "AR PR"),
            new Teacher("Wang XianQin", "female", "Office")
    );

    private List<Student> students = Arrays.asList(
            new Student("Zhang HaiShan", "male", "2021 Cloud"),
            new Student("Wang Deran", "male", "2021 Cloud"),
            new Student("Hu Long", "male", "2021 Cloud"),
            new Student("Du JinYu", "male", "2021 Cloud"),
            new Student("Zi YinHong", "male", "2021 Cloud"),
            new Student("Ma ZiXuan", "male", "2021 Cloud"),
            new Student("Li XinHao", "male", "2021 Cloud"),
            new Student("Bai YanWwei", "male", "2022 Cloud"),
            new Student("Guo Jian", "male", "2022 Cloud"),
            new Student("Zhou ShuangYang", "male", "2022 Cloud"),
            new Student("Chen RongLai", "male", "2022 Cloud"),
            new Student("Yu Yu", "male", "2022 Cloud"),
            new Student("Jiang FuYang", "male", "2022 Cloud"),
            new Student("Wan HaiChuan", "male", "2022 Cloud"),
            new Student("Lei ZhengYong", "male", "2022 Cloud"),
            new Student("Wang XiaoGgang", "male", "2022 Cloud"),
            new Student("Wu Ye", "male", "2022 Cloud"),
            new Student("Zeng HeRui", "female", "2022 Cloud")
    );

    @Test
    public void test05() {
        List<Student> plist = students.parallelStream()
                .filter(x -> x.getName().startsWith("Z"))
                .collect(Collectors.toList());

        plist.forEach(System.out::println);
    }

    @Test
    public void test04() {
        List<Student> sortedList = students.stream()
                .sorted(Comparator.comparing(Student::getName))
                .collect(Collectors.toList());

        sortedList.forEach(System.out::println);
    }

    @Test
    public void test03() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Integer i = list.stream().reduce(0, Integer::sum);
        System.out.println(i);

        System.out.println("===");
        Integer max = list.stream().reduce(0, Integer::max);
        System.out.println(max);
    }

    @Test
    public void test02() {
        List<Student> s1 = students.stream().filter(x -> x.getClassName() == "2022 Cloud")
                .collect(Collectors.toList());

        s1.stream().forEach(x -> System.out.println(x));
        System.out.println("===");
        List<Student> s2 = students.stream().filter(x -> x.getClassName() == "2021 Cloud")
                .collect(Collectors.toList());

        s2.stream().forEach(x -> System.out.println(x));
        System.out.println("===");

        List<Teacher> t1 = teachers.stream().filter(x -> x.getGender() == "female")
                .collect(Collectors.toList());

        t1.stream().forEach(System.out::println);

        System.out.println("===");
        List<Teacher> t2 = teachers.stream().filter(x -> x.getGender() == "male")
                .collect(Collectors.toList());

        t2.stream().forEach(System.out::println);

        System.out.println("===");
        List<Integer> s3 = students.stream()
                .map(x -> x.getName().length())
                .collect(Collectors.toList());

        s3.stream().forEach(System.out::println);

        System.out.println("===");
        List<String> t3 = teachers.stream()
                .map((x -> x.getName()))
                .collect(Collectors.toList());

        t3.stream().forEach(System.out::println);
    }
    @Test
    public void test01() {
        List<Integer> list = Arrays.asList(6, 7, 4, 5, 8, 9, 10, 1, 2, 3);
        System.out.println(list);
        System.out.println("===");

        System.out.println(list.stream().filter(x -> x >= 5)
                .collect(Collectors.toList()));
        System.out.println("===");

        List<Integer> list2 = list.parallelStream().sorted()
                .collect(Collectors.toList());
        System.out.println(list2);

        System.out.println("===");

        List<Integer> list3 = list.stream().sorted().
                collect(Collectors.toList());
        System.out.println(list3);
    }
}
