package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test04() {
        Thread thread = new Thread(new DownloadFileTask());
        thread.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread.interrupt();
    }

    @Test
    public void test03() {
        System.out.println(Thread.currentThread().getName());

        Thread[] threads = new Thread[5];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new DownloadFileTask());
            threads[i].start();
        }

        for (Thread thread : threads) {
            try {
                thread.join(); // 等待每个线程完成
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("All downloads completed.");
    }


    @Test
    public void test02() {
        System.out.println(Thread.currentThread().getName());
        Thread thread = new Thread(new DownloadFileTask());
        thread.start();
    }

    @Test
    public void test01() {
        System.out.println(Thread.activeCount());
        System.out.println(Runtime.getRuntime().availableProcessors());
    }
}
