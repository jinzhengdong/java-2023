package com.demo;

public class Emp {
    private String name;
    private float income;
    private ITax itax;

    public Emp(String name, float income, ITax itax) {
        this.name = name;
        this.income = income;
        this.itax = itax;
    }

    public Emp(String name, float income) {
        this.name = name;
        this.income = income;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getIncome() {
        return income;
    }

    public void setIncome(float income) {
        this.income = income;
    }

    public ITax getItax() {
        return itax;
    }

    public void setItax(ITax itax) {
        this.itax = itax;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", income=" + income +
                ", itax=" + itax +
                '}';
    }

    public float getTax() {
        return itax.getTax(income);
    }

    public float getActualSalary() {
        return income - getTax();
    }

    public void newSetEmp(float income, ITax itax) {
        this.income = income;
        this.itax = itax;
    }
}
