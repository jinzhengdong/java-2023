package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MyTest {
    List<Film> filmList = Arrays.asList(
            new Film("星球大战一", "科幻", "哈里森福特，天行者卢克", 170, 200000000),
            new Film("星球大战前传", "科幻", "哈里森福特，天行者卢克", 170, 200000000),
            new Film("饥饿游戏", "动作", "大表姐，...", 90, 100000000)
    );

    @Test
    public void test01() {
        filmList.stream().forEach(System.out::println);
    }
}
