package me.ereach;

public interface IGeometry3D {
    double getVolume();
}
