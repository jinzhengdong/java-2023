package me.ereach.l005p01.rest;

import org.springframework.web.bind.annotation.*;

// http://localhost:8080/api/greeting
@RestController
@RequestMapping("/api")
public class UserResource {
    @GetMapping("/greeting")
    public String greeting() {
        return "Hello, World!";
    }

    @PostMapping("/greeting")
    public String makeGreeting(@RequestParam String name) {
        return "Hello, World, " + name;
    }
}
