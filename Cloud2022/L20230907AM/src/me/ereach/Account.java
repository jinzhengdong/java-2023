package me.ereach;

public class Account {
    private float balance;

    public void deposit(float value) throws InsufficientFundsException {
        if (value <= 0)
            throw new InsufficientFundsException("存款必修大于0");

        balance += value;
    }

    public void withdraw(float value) throws InsufficientFundsException {
        if (value > balance)
            throw new InsufficientFundsException("取款大于存款");

        balance -= value;
    }
}
