package com.xinhua;

public class Lesson4 {
    public static void main(String[] args) {
        int sum = 0;

        for (int i = 1; i <= 100; i++) {
            if (i == 51)
                break;

            sum += i;
        }

        System.out.println(sum);
    }
}
