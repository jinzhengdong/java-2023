import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (true){
            System.out.println("请输入一个正整数");
            String tmp=scanner.next();

            tmp=tmp.toLowerCase();

            if (tmp.equals("exit"))
                break;
            long num=Long.parseLong(tmp);
            System.out.println(SCP(num));
        }
        System.out.println("完成");
    }

    public static String SCP(long num){
        boolean flag=true;
        if (num==2){
            return num + "是质数";
        }
        if (num % 2 ==0)
            return  num+"不是质数";

        for (int i=2;i < num /2;i++){
            if (num%i==0){
                return  num+"不是质数";
            }

        }
     if (flag){
         return num+"是质数";
     }else
         return num+"不是质数";
    }

}