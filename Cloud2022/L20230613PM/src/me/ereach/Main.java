package me.ereach;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 读取贷款总额
        System.out.print("请输入贷款总额（万元）：");
        double totalLoan = scanner.nextDouble() * 10000;
        // 读取贷款期限
        System.out.print("请输入贷款期限（年）：");
        int loanTerm = scanner.nextInt();
        // 读取年利率
        System.out.print("请输入年利率（%）：");
        double annualInterestRate = scanner.nextDouble() / 100;
        // 计算每月还款金额和还款总额
        double[] result = calculate(totalLoan, loanTerm, annualInterestRate);
        // 输出计算结果
        System.out.printf("每月还款金额为：%.2f 元%n", result[0]);
        System.out.printf("还款总额为：%.2f 元%n", result[1]);
    }

    /**
     * 计算每月还款金额和还款总额
     *
     * @param totalLoan 贷款总额
     * @param loanTerm  贷款期限（年）
     * @param annualInterestRate 年利率
     * @return double[]，第一个元素是每月还款金额，第二个元素是还款总额
     */
    public static double[] calculate(double totalLoan, int loanTerm, double annualInterestRate) {
        double monthlyInterestRate = annualInterestRate / 12;
        int totalPayments = loanTerm * 12;
        double monthlyPayment = (totalLoan * monthlyInterestRate * Math.pow(1 + monthlyInterestRate, totalPayments))
                / (Math.pow(1 + monthlyInterestRate, totalPayments) - 1);
        double totalPayment = monthlyPayment * totalPayments;
        return new double[]{monthlyPayment, totalPayment};
    }
}
