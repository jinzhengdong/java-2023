package me.ereach.edu;

public class Employee {
    private Integer id;
    private String name;
    private Boolean gender;
    private String mobile;
    private Double salary;
    private Integer extraHours;
    private Double hourlyRate;

    public Employee(Integer id, String name, Boolean gender, String mobile, Double salary, Integer extraHours, Double hourlyRate) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.mobile = mobile;
        this.salary = salary;
        this.extraHours = extraHours;
        this.hourlyRate = hourlyRate;
    }


    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", mobile='" + mobile + '\'' +
                ", salary=" + salary +
                ", extraHours=" + extraHours +
                ", hourlyRate=" + hourlyRate +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Integer getExtraHours() {
        return extraHours;
    }

    public void setExtraHours(Integer extraHours) {
        this.extraHours = extraHours;
    }

    public Double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public Double getTotalSalary() {
        return salary + extraHours * hourlyRate;
    }
}
