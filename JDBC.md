# JDBC

## 一、JDBC简介

JDBC（Java Database Connectivity）是Java语言中用于操作关系型数据库的API，它提供了一种标准的方法来访问数据库。通过JDBC，我们可以使用Java程序连接到数据库，执行SQL语句，获取结果集等。

## 二、JDBC的使用步骤

使用JDBC连接数据库的一般步骤如下：

1. 加载数据库驱动程序
2. 建立数据库连接
3. 创建Statement对象
4. 执行SQL语句
5. 处理结果集
6. 关闭数据库连接

下面我们将详细介绍每个步骤的具体实现。

### 1. 加载数据库驱动程序

在使用JDBC之前，我们需要先加载数据库驱动程序。不同的数据库有不同的驱动程序，例如MySQL的驱动程序是com.mysql.jdbc.Driver，Oracle的驱动程序是oracle.jdbc.driver.OracleDriver等。

加载驱动程序的方式有两种：

#### （1）使用Class.forName()方法加载驱动程序

```java
Class.forName("com.mysql.jdbc.Driver");
```

#### （2）使用DriverManager.registerDriver()方法注册驱动程序

```java
DriverManager.registerDriver(new com.mysql.jdbc.Driver());
```

### 2. 建立数据库连接

在加载驱动程序之后，我们需要建立数据库连接。连接数据库需要指定数据库的URL、用户名和密码等信息。

```java
String url = "jdbc:mysql://localhost:3306/test";
String user = "root";
String password = "123456";
Connection conn = DriverManager.getConnection(url, user, password);
```

### 3. 创建Statement对象

建立数据库连接之后，我们需要创建Statement对象。Statement对象用于执行SQL语句。

```java
Statement stmt = conn.createStatement();
```

### 4. 执行SQL语句

创建Statement对象之后，我们可以执行SQL语句。执行SQL语句有三种方式：

#### （1）执行查询语句

```java
String sql = "select * from user";
ResultSet rs = stmt.executeQuery(sql);
```

#### （2）执行更新语句

```java
String sql = "update user set name='张三' where id=1";
int rows = stmt.executeUpdate(sql);
```

#### （3）执行批处理语句

```java
String sql1 = "insert into user(name, age) values('张三', 20)";
String sql2 = "insert into user(name, age) values('李四', 22)";
String sql3 = "insert into user(name, age) values('王五', 25)";
stmt.addBatch(sql1);
stmt.addBatch(sql2);
stmt.addBatch(sql3);
int[] rows = stmt.executeBatch();
```

### 5. 处理结果集

执行查询语句之后，我们需要处理结果集。ResultSet对象用于存储查询结果。

```java
while (rs.next()) {
    int id = rs.getInt("id");
    String name = rs.getString("name");
    int age = rs.getInt("age");
    System.out.println("id=" + id + ", name=" + name + ", age=" + age);
}
```

### 6. 关闭数据库连接

最后，我们需要关闭数据库连接。

```java
rs.close();
stmt.close();
conn.close();
```

## 三、JDBC的示例代码

下面是一个使用JDBC连接MySQL数据库的示例代码：

```java
import java.sql.*;

public class JdbcDemo {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            // 加载数据库驱动程序
            Class.forName("com.mysql.jdbc.Driver");
            // 建立数据库连接
            String url = "jdbc:mysql://localhost:3306/test";
            String user = "root";
            String password = "123456";
            conn = DriverManager.getConnection(url, user, password);
            // 创建Statement对象
            stmt = conn.createStatement();
            // 执行查询语句
            String sql = "select * from user";
            rs = stmt.executeQuery(sql);
            // 处理结果集
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                System.out.println("id=" + id + ", name=" + name + ", age=" + age);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```

