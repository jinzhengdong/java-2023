package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        GenericList<Number> genericList = new GenericList<>();

        genericList.add(1);
        genericList.add(2.34F);
        genericList.add(3.14D);

        System.out.println(genericList);
    }

    @Test
    public void test01() {
        GenericList<Integer> genericList = new GenericList<>();
        genericList.add(1);
        genericList.add(2);
//        genericList.add(2.34D);
        System.out.println(genericList);
    }
}
