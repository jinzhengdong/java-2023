package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test04() {
        GList<String> list = new GList<>();
        list.add("hello");
        list.add("world");
//        list.add(3);
        System.out.println(list);

        GList<Integer> list2 = new GList<>();
        list2.add(10);
        list2.add(20);
        System.out.println(list2);
    }

    @Test
    public void test03() {
        var list = new ObjList();

        list.add(1);
        list.add(2);

        list.add("Hello");
        list.add("world");

        list.add(new User("Tom", "male"));
        list.add(new User("Jerry", "male"));

        System.out.println(list);
    }

    @Test
    public void test02() {
        var list = new StringList();

        list.add("hello");
        list.add("world");
//        list.add(20);

        System.out.println(list);
    }

    @Test
    public void test01() {
        var list = new List();

        list.add(10);
        list.add(20);
//        list.add("hello");

        System.out.println(list);
    }
}
