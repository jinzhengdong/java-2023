package me.ereach.tax;

public class Main {
    public static void main(String[] args) {
        Emp emp = new Emp("Tom", 6000);
        ITax iTax = new Tax();
        emp.setiTax(iTax);
        System.out.println(emp.getActualIncome());
    }
}
