package com.xinhua.ab;

public class Circle extends Geometory {
    private float r;

    public Circle(float r) {
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    protected double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    @Override
    double getPerimeter() {
        return Math.PI * r * 2;
    }
}
