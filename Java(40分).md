## 一、单选题 (40分)

1. 下列哪个不是Java的基本数据类型？ A. int B. float C. double D. string
2. 下列哪个是Java的包装类型？ A. int B. float C. Double D. String
3. 下列哪个运算符不属于算术运算符？ A. + B. - C. * D. &
4. 下列哪个运算符不属于逻辑运算符？ A. && B. || C. ! D. +
5. 下列哪个运算符不属于比较运算符？ A. < B. > C. <= D. &

## 二、多选题 (40分)

1. 下列哪些是Java的引用类型？ A. String B. int C. Double D. Integer
2. 下列哪些是合法的算术运算符？ A. % B. ^ C. / D. <<
3. 下列哪些是合法的逻辑运算符？ A. & B. | C. $ D. ^
4. 下列哪些是合法的比较运算符？ A. == B. != C. <=> D. >=

## 三、编程题 (20分)

请编写一个Java程序，实现以下功能：

1. 提示用户输入两个整数。
2. 对这两个数进行加、减、乘、除运算。
3. 输出运算结果。

```java
javaCopy codeimport java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("请输入第一个整数：");
        int num1 = input.nextInt();
        System.out.print("请输入第二个整数：");
        int num2 = input.nextInt();
        System.out.println(num1 + " + " + num2 + " = " + (num1 + num2));
        System.out.println(num1 + " - " + num2 + " = " + (num1 - num2));
        System.out.println(num1 + " * " + num2 + " = " + (num1 * num2));
        System.out.println(num1 + " / " + num2 + " = " + (num1 / num2));
    }
}
```