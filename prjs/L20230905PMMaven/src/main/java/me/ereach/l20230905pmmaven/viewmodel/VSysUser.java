package me.ereach.l20230905pmmaven.viewmodel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("v_sys_user")
public class VSysUser {
    @TableId(type = IdType.AUTO)
    private Integer userId;
    private String userName;
    private Byte flag;
    private Integer employeeId;
    private String employeeName;
}
