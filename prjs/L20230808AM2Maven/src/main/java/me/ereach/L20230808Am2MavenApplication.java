package me.ereach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class L20230808Am2MavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(L20230808Am2MavenApplication.class, args);
    }

}
