package com.example.demo20231201;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo20231201Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo20231201Application.class, args);
    }

}
