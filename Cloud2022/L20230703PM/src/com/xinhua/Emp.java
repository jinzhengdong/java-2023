package com.xinhua;

public class Emp {
    private String name;
    private String gender;
    private float baseSalary;
    private int extraHours;
    private float hourlyRate;

    public Emp(String name, String gender, float baseSalary, int extraHours, float hourlyRate) {
        this.name = name;
        this.gender = gender;
        this.baseSalary = baseSalary;
        this.extraHours = extraHours;
        this.hourlyRate = hourlyRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(float baseSalary) {
        this.baseSalary = baseSalary;
    }

    public int getExtraHours() {
        return extraHours;
    }

    public void setExtraHours(int extraHours) {
        this.extraHours = extraHours;
    }

    public float getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", baseSalary=" + baseSalary +
                ", extraHours=" + extraHours +
                ", hourlyRate=" + hourlyRate +
                '}';
    }

    public float getTotalSalary() {
        return baseSalary + extraHours * hourlyRate;
    }
}
