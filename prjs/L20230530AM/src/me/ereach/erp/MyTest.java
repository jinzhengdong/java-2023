package me.ereach.erp;

import org.junit.Test;

import java.util.Arrays;

public class MyTest {
    Employee e1 = new Employee(1, "Tom", 6000, 30, 8);
    Employee e2 = new Employee(2, "Jerry", 5200, 25, 12);

    Employee[] emps = {
            new Employee(1, "Tom", 6000, 30, 8),
            new Employee(2, "Jerry", 5200, 25, 12),
            new Employee(2, "Tod", 4800, 20, 16)
    };

    @Test
    public void test03() {
        System.out.println(emps);
        System.out.println("===");
        System.out.println(Arrays.deepToString(emps));
    }

    @Test
    public void test04() {
        for (int i = 0; i < emps.length; i++)
            System.out.println(emps[i]);
    }

    @Test
    public void test02() {
        float totalActualSalary = 0;
        float totalSalary = 0;
        float totalTax = 0;

        for (var emp : emps) {
            System.out.println(emp);
            totalSalary += emp.getTotalSalary();
            totalTax += emp.getTax();
            totalActualSalary += emp.getActualSalsry();
        }

        System.out.println("===");
        System.out.println("Total Salary = " + totalSalary);
        System.out.println("Total Tax = " + totalTax);
        System.out.println("Total ActualSalary = " + totalActualSalary);
    }

    @Test
    public void test01() {
        System.out.println(e1);
        System.out.println(e2);
    }
}
