package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test07() {
        int total = 0;
        int cnt = 1;

        do {
            total += cnt;
            cnt++;
        } while (cnt <= 100);

        System.out.println(total);
    }

    @Test
    public void test06() {
        int total = 0;
        int cnt = 1;

        while (true) {
            total += cnt;
            cnt++;

            if (cnt > 100)
                break;
        }

        System.out.println(total);
    }

    @Test
    public void test05() {
        int total = 0;
        int cnt = 1;

        while (cnt <= 50) {
            total += cnt;
            cnt++;
        }

        System.out.println(total);
    }

    @Test
    public void test04() {
        int[] ary = {1, 2, 3, 5, 7, 9};
        int total = 0;

        for (int i = 0; i < ary.length; i++) {
            total += ary[i];
        }
        System.out.println(total);
        System.out.println("===");
        total = 0;
        for (int x : ary) {
            total += x;
        }
        System.out.println(total);
        System.out.println("===");
        total = 0;
        for (var x : ary) {
            total += x;
        }
        System.out.println(total);
    }

    @Test
    public void test03() {
        int total = 0;

        for (int i = 1; i <= 327; i++) {
            total += i;
        }

        System.out.println(total);
    }

    @Test
    public void test02() {
        int dayOfWeek = 3;

        switch (dayOfWeek) {
            case 1 -> System.out.println("Mon");
            case 2 -> System.out.println("Tue");
            case 3 -> System.out.println("Wed");
            case 4 -> System.out.println("Thr");
            case 5 -> System.out.println("Fri");
            case 6 -> System.out.println("Sat");
            case 7 -> System.out.println("Sun");
            default -> System.out.println("Any");
        }
    }

    @Test
    public void test01() {
        int dayOfWeek = 100;

        switch (dayOfWeek) {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wedensday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Sat");
                break;
            case 7:
                System.out.println("Sun");
                break;
            default:
                System.out.println("Any");
                break;
        }
    }
}
