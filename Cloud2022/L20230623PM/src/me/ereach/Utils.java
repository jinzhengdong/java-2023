package me.ereach;

public class Utils {
    public static int max(int a, int b) {
        if (a > b)
            return a;
        else
            return b;
    }

    public static float max(float a, float b) {
        if (a > b)
            return a;
        else
            return b;
    }

    public static float max(int a, float b) {
        if (a > b)
            return a;
        else
            return b;
    }

    public static int min(int a, int b) {
        return (a > b) ? b : a;
    }
}
