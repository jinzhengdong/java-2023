package me.ereach;

public class Rect implements Geometry {
    private float w;
    private float h;
    private Point a;

    public Rect(float w, float h, Point a) {
        this.w = w;
        this.h = h;
        this.a = a;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "Rect{" +
                "w=" + w +
                ", h=" + h +
                ", a=" + a +
                ", Point C (" + (a.getX() + w) + ", " + (a.getY() + h) + ")" +
                '}';
    }

    @Override
    public double getArea() {
        return w * h;
    }

    @Override
    public double getPerimeter() {
        return (w + h) * 2;
    }
}
