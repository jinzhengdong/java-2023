package me.ereach;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        // 加载Spring配置类
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        // 获取UserService Bean
        UserService userService = context.getBean(UserService.class);

        // 创建一个User对象
        User user = new User("John");

        // 调用UserService的保存用户方法
        userService.saveUser(user);
    }
}
