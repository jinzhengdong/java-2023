package me.ereach.comp;

import me.ereach.Point2D;
import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Circle c1 = new Circle(7, new Point2D(3, 5));
        Circle c2 = new Circle(11, new Point2D(2, 3));
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c1.getArea());
        System.out.println(c2.getArea());
    }
}
