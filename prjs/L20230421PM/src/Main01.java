import java.awt.*;

public class Main01 {
    public static void main(String[] args) {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(2, 3);

        Point p3 = p1;

        System.out.println(p1 == p2);   // true

        System.out.println(p1 == p3);   // true
    }
}
