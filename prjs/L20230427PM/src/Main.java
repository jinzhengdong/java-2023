public class Main {
    public static void main(String[] args) {
        int x = 41;     // 气温

        if (x > 30) {
            if (x > 36) {
                if (x > 40) {
                    System.out.println("非常热");
                }
                else {
                    System.out.println("太热了");
                    System.out.println("请多喝水");
                }
            }
            else
                System.out.println("有点热");
        }
        else if (x > 25) {
            System.out.println("天气很暖和");
        }
        else if (x > 20) {
            System.out.println("天气很凉爽");
        }
        else if (x > 10) {
            System.out.println("有点凉");
        }
        else {
            System.out.println("太冷了");
        }
    }
}