package com.xinhua;

public class Class {
    private Integer id;
    private String className;
    private String description;
    private Integer enterYear;
    private Integer finishYear;
    private Integer flag;

    public Class(Integer id, String className, String description, Integer enterYear, Integer finishYear, Integer flag) {
        this.id = id;
        this.className = className;
        this.description = description;
        this.enterYear = enterYear;
        this.finishYear = finishYear;
        this.flag = flag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEnterYear() {
        return enterYear;
    }

    public void setEnterYear(Integer enterYear) {
        this.enterYear = enterYear;
    }

    public Integer getFinishYear() {
        return finishYear;
    }

    public void setFinishYear(Integer finishYear) {
        this.finishYear = finishYear;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Class{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", description='" + description + '\'' +
                ", enterYear=" + enterYear +
                ", finishYear=" + finishYear +
                ", flag=" + flag +
                '}';
    }
}
