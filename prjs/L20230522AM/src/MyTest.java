import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Point2D p1 = new Point2D(3, 5);
        Point2D p2 = new Point2D(7, 11);
        System.out.println(p1.getDistance(p2));
        System.out.println(Point2D.getDistanceStatic(p1, p2));

        System.out.println("===");

        Point3D p3 = new Point3D(3, 5);
        Point3D p4 = new Point3D(7, 11);
        System.out.println(p3.getDistance(p4));
        System.out.println(Point3D.getDistanceStatic(p3, p4));

        System.out.println("===");

        Point3D p5 = new Point3D(3, 5, 7);
        Point3D p6 = new Point3D(11, 13, 17);
        System.out.println(p5.getDistance(p6));
        System.out.println(Point3D.getDistanceStatic(p5, p6));
    }
}
