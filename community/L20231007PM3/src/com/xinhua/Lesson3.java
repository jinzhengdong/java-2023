package com.xinhua;

public class Lesson3 {
    public static void main(String[] args) {
        int t = 37;

        if (t > 30) {
            if (t > 36)
                System.out.println("很热");
            else
                System.out.println("有点热");
        }
        else if (t > 26) {
            System.out.println("不太热");
        }
        else if (t > 20) {
            System.out.println("很凉爽");
        }
        else {
            System.out.println("有点冷");
        }
    }
}
