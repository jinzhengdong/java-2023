package com.ynxinhua;


import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MyTest {
    @Test
    public void test04() {
        Person p1 = new Person("Tom", "male");
        Person p2 = new Person("Wendy", "female");

        Map<String, Person> map = new HashMap<>();

        map.put(p1.getName(), p1);
        map.put(p2.getName(), p2);

        map.forEach((k, v) -> System.out.println(k + " => " + v));
    }

    @Test
    public void test03() {
        Map<Circle, Integer> map = new HashMap<>();

        map.put(new Circle(3), 1);
        map.put(new Circle(5), 2);

        map.forEach((k, v) -> System.out.println(k + ", " + v + ", Area=" + k.getArea()));
    }

    @Test
    public void test02() {
        Map<String, Integer> map = new LinkedHashMap<>();
        map.put("Apple", 1);
        map.put("Banana", 2);

        System.out.println(map);

        System.out.println("===");

        map.forEach((k, v) -> System.out.println(k + "=" + v));
    }
    @Test
    public void test01() {
        Map<String, Integer> map = new HashMap<>();
        map.put("Apple", 1);
        map.put("Banana", 2);

        System.out.println(map);

        System.out.println("===");

        map.forEach((k, v) -> System.out.println(k + "=" + v));
    }
}
