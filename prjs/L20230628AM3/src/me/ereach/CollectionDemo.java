package me.ereach;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CollectionDemo {
    @Test
    public void test02() {
        Collection<String> collection = new ArrayList<>();

        Collections.addAll(collection, "a", "b", "c");

        for (var x : collection)
            System.out.println(x);

        System.out.println("---println(collection)---");
        System.out.println(collection);
    }

    @Test
    public void test01() {
        Collection<String> collection = new ArrayList<>();
        collection.add("a");
        collection.add("b");
        collection.add("c");

        for (var x : collection)
            System.out.println(x);

        System.out.println(collection);
    }
}
