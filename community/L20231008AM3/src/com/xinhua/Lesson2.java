package com.xinhua;

public class Lesson2 {
    public static void main(String[] args) {
        int sum = 0;
        int count = 1;

        while (count <= 100) {
            sum += count;   // sum = sum + count
            count++;        // count = count + 1
        }

        System.out.println(sum);
    }
}
