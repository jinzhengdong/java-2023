package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test04() {
        Rect r1 = new Rect(3, 7);
        System.out.println(r1.getPerimeter());
        System.out.println(r1.getArea());

        r1.setH(17);
        System.out.println(r1.getPerimeter());
        System.out.println(r1.getArea());

    }

    @Test
    public void test03() {
       Point[] px = {
               new Point(2, 3),
               new CircleExt(2, 7, 10),
               new Point(4, 7),
               new CircleExt(11, 13, 17)
       };

       for (var x : px)
           System.out.println(x);
    }

    @Test
    public void test02() {
        CircleExt ce = new CircleExt(2, 3, 7);
        System.out.println(ce);
    }

    @Test
    public void test01() {
        Point p = new Point(2, 3);
        Circle c = new Circle(7, p);
        System.out.println(c);
    }
}
