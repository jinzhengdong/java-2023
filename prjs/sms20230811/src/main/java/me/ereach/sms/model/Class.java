package me.ereach.sms.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("Class")
public class Class {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String className;
    private Integer grade;

    @TableField(exist = false)
    private Student student;
}