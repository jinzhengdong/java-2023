import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        System.out.println(sayHello("Su"));
        System.out.println(sayHello("Dong"));
        System.out.println(sayHello("Liu"));

        System.out.println(add(3, 5));
    }

    @Test
    public void test02() {
        System.out.println(sayHello("Tod"));
        System.out.println(add(100, 200));
    }

    public Integer add(Integer a, Integer b) {
        return a + b;
    }

    private String sayHello(String name) {
        return "Hello, " + name + "!";
    }
}
