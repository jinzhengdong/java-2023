package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        Shape c1 = new Circle();
        Shape r1 = new Rectangle();

        c1.draw();
        System.out.println("====");
        r1.draw();
    }

    @Test
    public void test01() {
//        Shape s1 = new Shape();
//        s1.draw();
//
        Circle c1 = new Circle();
        c1.draw();

        Rectangle r1 = new Rectangle();
        r1.draw();
    }
}
