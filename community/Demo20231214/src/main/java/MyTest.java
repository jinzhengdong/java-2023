import org.junit.Test;

import java.sql.*;

public class MyTest {
    
    @Test
    public void test01() {
        // 数据库连接信息
        String jdbcUrl = "jdbc:mysql://localhost:3306/world";
        String username = "root";
        String password = "123456";

        // 声明数据库连接对象、Statement对象和结果集对象
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // 加载数据库驱动
            Class.forName("com.mysql.cj.jdbc.Driver");

            // 建立数据库连接
            conn = DriverManager.getConnection(jdbcUrl, username, password);
            System.out.println("数据库连接成功！");

            // 创建Statement对象
            stmt = conn.createStatement();

            // 执行查询语句
            String sql = "SELECT * FROM city";
            rs = stmt.executeQuery(sql);

            // 处理结果集
            while (rs.next()) {
                int id = rs.getInt("ID");
                String name = rs.getString("Name");
                String countryCode = rs.getString("CountryCode");
                String district = rs.getString("District");
                int population = rs.getInt("Population");

                System.out.println("ID: " + id);
                System.out.println("Name: " + name);
                System.out.println("CountryCode: " + countryCode);
                System.out.println("District: " + district);
                System.out.println("Population: " + population);
                System.out.println("--------------------");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭结果集、Statement和数据库连接
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
