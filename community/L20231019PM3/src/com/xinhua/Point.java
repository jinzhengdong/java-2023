package com.xinhua;

public class Point {
    // 属性
    private float x;
    private float y;

    // 构造方法
    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    // 构造重载
    public Point() {
        x = 0;
        y = 0;
    }

    // get & set 方法
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    // 重写方法
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public double getDistance(Point o) {
        float s = this.x - o.x;
        float h = this.y - o.y;

        return Math.sqrt(h * h + s * s);
    }
}
