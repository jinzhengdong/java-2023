package me.ereach;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class MyTest {
    @Test
    public void test06() {
        try (var reader = new FileReader("d:\\readme.txt")) {
            int value = 0;
            while (value != -1) {
                value = reader.read();
                System.out.print((char) value);
            }
        }
        catch (IOException e) {
            System.out.println("could not read");
        }
        finally {
            System.out.println("\nFinally step");
        }
    }

    @Test
    public void test05() {
        FileReader reader = null;

        try {
            reader = new FileReader("d:\\readme.txt");
            int read = 0;
            while (read != -1) {
                read = reader.read();
                System.out.print((char) read);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            try {
                reader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Test
    public void test04() {
        try {
            int read = 0;

            var reader = new FileReader("d:\\readme.txt");

            while (read != -1) {
                read = reader.read();
                System.out.print((char) read);
            }

            new SimpleDateFormat().parse("");

        } catch (IOException | ParseException e) {
//            e.printStackTrace();
            throw new RuntimeException(e);
//            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("finally");
        }

        System.out.println("finished.");
    }

    @Test
    public void test03() {
        try {
            int read = 0;
            var reader = new FileReader("d:\\xreadme.txt");

            while (read != -1) {
                read = reader.read();
                System.out.print((char) read);
            }

        } catch (IOException e) {
//            throw new RuntimeException(e);
            System.out.println(e.getMessage());
        }
//        catch (FileNotFoundException  e) {
////            throw new RuntimeException(e);
//            System.out.println(e.getMessage());
//        }

        System.out.println("\nfinished.");
    }

    @Test
    public void test02() {
        try {
            var reader = new FileReader("d:\\xreadme.txt");
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//            System.out.println(Arrays.deepToString(e.getStackTrace()));
//            System.out.println(e.fillInStackTrace());
//            System.out.println(e.getCause());
//            System.out.println(e.getMessage());
//            throw new RuntimeException(e);
        }

        System.out.println("finished");
    }
    @Test
    public void test01() throws FileNotFoundException {
        var reader = new FileReader("d:\\xreadme.txt");
        System.out.println("finished");
    }
}
