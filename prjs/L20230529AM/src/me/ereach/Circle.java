package me.ereach;

public class Circle {
    private float r;
    private Point2D o;

    public Circle(float r, Point2D o) {
        this.r = r;
        this.o = o;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public Point2D getO() {
        return o;
    }

    public void setO(Point2D o) {
        this.o = o;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "r=" + r +
                ", o=" + o +
                '}';
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
