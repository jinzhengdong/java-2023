package com.xinhua;

import org.junit.Test;

public class MyTest {

    @Test
    public void test02() {
        User user1 = new User("Jerry", "male", 12);
        User user2 = new User("Tod", "male", 12);
        System.out.println(user1.compareTo(user2));
    }

    @Test
    public void test01() {
//        GenericList<String> genericList1 = new GenericList<>();
//        genericList1.add("Hello");
//        genericList1.add("World");
//        System.out.println(genericList1);
//
//        System.out.println("===");
//        GenericList<Integer> genericList2 = new GenericList<>();
//        genericList2.add(2);
//        genericList2.add(5);
//        System.out.println(genericList2);

        GenericList<User> genericList3 = new GenericList<>();
        genericList3.add(new User("Jerry", "male", 12));
        genericList3.add(new User("Tod", "male", 13));
        genericList3.add(new User("Wendy", "female", 12));

        System.out.println(genericList3);
    }
}
