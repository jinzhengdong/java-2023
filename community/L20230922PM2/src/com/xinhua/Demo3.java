package com.xinhua;

public class Demo3 {
    public static void main(String[] args) {
        int gender = 1;
        int age = 17;

        System.out.println(age > 18);   // 20 > 18 ? true
        System.out.println(age > 30);   // 20 > 30 ? false

        System.out.println(gender == 1);    // gender == 1 ? true
        System.out.println(gender == 0);    // gender == 0 ? false

        System.out.println(age >= 18 && gender == 1);   // true

        if (age >= 18 && gender == 1)
            System.out.println("可以参军");
        else
            System.out.println("不可以参军");

        System.out.println("===&& 并且");
        System.out.println(true && true);   // true
        System.out.println(true && false);   // false
        System.out.println(false && true);   // false
        System.out.println(false && false);   // false

        System.out.println("===|| 或者");
        System.out.println(true || true);   // true
        System.out.println(true || false);   // true
        System.out.println(false || true);   // true
        System.out.println(false || false);   // false

        System.out.println("=== ！ 非 取反");
        System.out.println(! true); // false
        System.out.println(! false);    // true




    }
}
