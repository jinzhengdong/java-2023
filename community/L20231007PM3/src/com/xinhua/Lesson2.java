package com.xinhua;

public class Lesson2 {
    public static void main(String[] args) {
        int age = 10;
        String gender = "male";

        // true && true => true
        if (age > 18 && gender == "male")
            System.out.println("可以参军");
        else
            System.out.println("不可参军");

    }
}
