package com.xinhua;

import org.junit.Test;

import java.awt.*;

public class MyTest {
    @Test
    public void test06() {
        Poin2D[] poin2DS = {
                new Poin2D(2, 3),
                new Poin2D(5, 7),
                new Circle2D(3, 5, 7),
                new Circle2D(11, 13, 17)
        };

        for (var item : poin2DS)
            System.out.println(item);
    }

    @Test
    public void test05() {
        Circle2D c1 = new Circle2D(2, 3, 5);
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println("===");
        Circle2D c2 = new Circle2D(7);
        System.out.println(c2);
        System.out.println(c2.getArea());
        System.out.println(c2.getPerimeter());
        System.out.println("===");
        System.out.println(c1.getDistance(c2));

        System.out.println("===");
        c1.setX(11);
        c1.setY(13);
        System.out.println(c1);
    }

    @Test
    public void test04() {
        Circle c1 = new Circle(new Poin2D(2, 3), 7);
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println("====");
        Circle c2 = new Circle(new Poin2D(7, 11), 9);
        System.out.println(c2);
        System.out.println(c2.getArea());
        System.out.println(c2.getPerimeter());

        System.out.println("===");
        System.out.println(c1.getO().getDistance(c2.getO()));
    }

    @Test
    public void test03() {
        Poin2D p1 = new Poin2D(2, 3);
        Poin2D p2 = new Poin2D(2, 3);

        System.out.println(p1);
        System.out.println(p2);

        System.out.println(p1 == p2);
        System.out.println(p1.equals(p2));

        System.out.println("===");

        java.awt.Point p3 = new Point(2, 3);
        System.out.println(p1.equals(p3));
    }

    @Test
    public void test02() {
        System.out.println(Util.max(2l, 5l));
        System.out.println(Util.max(2.5f, 3.5f));
        System.out.println(Util.max(2.5d, 3.5d));
        System.out.println("====");
        System.out.println(Util.min(2, 5));
    }

    @Test
    public void test01() {
        Poin2D p1 = new Poin2D();
        System.out.println(p1);

        Poin2D p2 = new Poin2D(3, 5);
        System.out.println(p2);

        p1.setX(7);
        p1.setY(11);
        System.out.println(p1);

        System.out.println("this distance from p1 to p2 is " + p1.getDistance(p2));
    }
}
