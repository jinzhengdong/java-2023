package com.xinhua;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyTest {
    @Test
    public void test04() {
        try (var fileReader = new FileReader("d:\\readme.txt")) {
            System.out.println(fileReader);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("finished.");
        }
    }
    @Test
    public void test03() {
        FileReader fileReader = null;

        try {
            fileReader = new FileReader("d:\\readmess.txt");
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            System.out.println("finished.");
        }
    }

    @Test
    public void test02() {
        try {
            FileReader fileReader = new FileReader("d:\\readmess.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            System.out.println("finished.");
        }
    }

    @Test
    public void test01() throws FileNotFoundException {
        var fileReader = new FileReader("d:\\readme.txt");
        System.out.println("finished.");
    }
}
