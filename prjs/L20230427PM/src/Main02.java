public class Main02 {
    public static void main(String[] args) {
        int age = 18;
        String gender = "female";

        // && and 并且
        if (age > 18 && gender == "male")
            System.out.println("可以参军");
        else
            System.out.println("不可以参军");

        if (age % 2 != 0)
            System.out.println("age 是奇数");
        else
            System.out.println("age 是偶数");

        if (age >= 19 || gender == "male")
            System.out.println("OK");
        else
            System.out.println("not ok");
    }
}
