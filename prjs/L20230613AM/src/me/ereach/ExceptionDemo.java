package me.ereach;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;

public class ExceptionDemo {
    @Test
    public void test06() {
        FileReader reader = null;

        try {
            reader = new FileReader("d:\\readme.txt");
            var value = reader.read();
            System.out.println(value);
            new SimpleDateFormat().parse("");
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
        finally {
            try {
                reader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ;
        }
    }

    @Test
    public void test05() {
        try {
            var reader = new FileReader("d:/readme.txt");
            System.out.println("file opened.");
            try {
                int value = 0;
                while (true) {
                    value = reader.read();

                    if (value == -1)
                        break;

                    System.out.println(value);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (FileNotFoundException e) {
//            System.out.println("�ļ�û�ҵ�");
//            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Test
    public void test04() throws Exception {
        var reader = new FileReader("d:\\readme.txe");
        System.out.println("file opened.");
    }

    @Test
    public void test03() {
        try {
            var reader = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        System.out.println("file opened.");

    }
    @Test
    public void test02() throws IOException {
        var reader = new FileReader("d:\\readme.txe");
        System.out.println("file opened.");
    }

    @Test
    public void test01() {
        sayHello("jerry");
        show();
    }

    public void show() {
        sayHello(null);
    }

    public void sayHello(String name) {
        System.out.println(name.toUpperCase());
    }
}
