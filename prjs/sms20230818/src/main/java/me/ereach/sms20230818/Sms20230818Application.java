package me.ereach.sms20230818;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("me.ereach.sms20230818.mapper")
public class Sms20230818Application {

    public static void main(String[] args) {
        SpringApplication.run(Sms20230818Application.class, args);
    }

}
