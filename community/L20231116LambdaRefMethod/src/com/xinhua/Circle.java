package com.xinhua;

public class Circle implements Geo {
    private float r;

    public Circle(float r) {
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "r=" + r +
                ", Area=" + getArea() +
                ", Perimeter=" + getPerimeter() +
                '}';
    }

    @Override
    public double getArea() {
        return Math.PI * r * r;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
