package me.ereach;

public class Circle {
    private Point2D o;
    private float r;

    public Circle(Point2D o, float r) {
        this.o = o;
        this.r = r;
    }

    public Point2D getO() {
        return o;
    }

    public void setO(Point2D o) {
        this.o = o;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "o=" + o +
                ", r=" + r +
                '}';
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }
}
