## Java异常处理

Java异常处理是Java开发中的重要部分，可以帮助我们处理程序中的错误情况。以下是Java异常处理的一些知识点和示例：

### 异常类

Java中提供了众多的异常类，包括`RuntimeException`、`IOException`、`NullPointerException`等。以下是Java异常类的示例：

java

复制代码

```java
//RuntimeException
int a = 1 / 0;

//IOException
try {
    FileWriter writer = new FileWriter("data.txt");
    writer.write("Hello World");
    writer.close();
} catch (IOException e) {
    e.printStackTrace();
}

//NullPointerException
String str = null;
int length = str.length();
```

### 异常处理

在Java中，我们可以使用`try-catch`语句处理异常。`try`代码块中包含可能会抛出异常的代码，如果有异常发生，会跳转到`catch`代码块中进行处理。以下是Java异常处理的示例：

java

复制代码

```java
try {
    int a = 1 / 0;
} catch (ArithmeticException e) {
    System.out.println("发生异常：" + e.getMessage());
}
```

### 异常链

在处理异常时，有时候我们需要将多个异常链在一起。Java中提供了`Throwable`类来实现异常链。以下是Java异常链的示例：

java

复制代码

```java
try {
    FileReader reader = new FileReader("data.txt");
    reader.read();
} catch (FileNotFoundException e) {
    throw new RuntimeException("文件不存在", e);
} catch (IOException e) {
    throw new RuntimeException("读取文件失败", e);
}
```

### 自定义异常

除了使用Java提供的异常类，我们还可以自定义异常类来处理特定的异常情况。以下是Java自定义异常的示例：

java

复制代码

```java
public class MyException extends RuntimeException {
    public MyException(String message) {
        super(message);
    }
}

try {
    throw new MyException("自定义异常");
} catch (MyException e) {
    System.out.println("发生异常：" + e.getMessage());
}
```

## 总结

以上是Java异常处理的一些知识点和示例。在Java开发中，异常处理是一个重要的环节，需要不断地掌握和练习，以便在后续的Java开发中更加熟练地处理各种异常情况。