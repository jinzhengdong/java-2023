import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Main02 {
    public static void main(String[] args) {
        double num = 77881234567.896798;
        DecimalFormat df = new DecimalFormat("#,###.0000");
        String formattedNum = df.format(num);
        System.out.println(formattedNum); // 输出为 1,234,567.89

        int num1 = 1234;
        double num2 = 22123.457899;
        String formattedNum1 = String.format("%,d", num1);
        String formattedNum2 = String.format("%,.3f", num2);
        System.out.println(formattedNum1); // 输出为 1234
        System.out.println(formattedNum2); // 输出为 22,123.458

        double numx = 0.12347;
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMaximumFractionDigits(2);
        String formattedNumx = nf.format(numx);
        System.out.println(formattedNumx); // 输出为 12.34%
    }
}
