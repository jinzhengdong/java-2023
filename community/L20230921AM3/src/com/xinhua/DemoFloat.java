package com.xinhua;

public class DemoFloat {
    public static void main(String[] args) {
        float f1 = 12.34f;
        System.out.println(f1);

        double d1 = 123.456;
        System.out.println(d1);

        char c = 'C';
        System.out.println(c);
//        char c1 = 'Hello';
        String s = "Hello, World";
        System.out.println(s);

        boolean b = true;
        System.out.println(b);

        b = false;
        System.out.println(b);
    }
}
