package com.demo;

public interface ITax {
    public float getTax(float income);
}
