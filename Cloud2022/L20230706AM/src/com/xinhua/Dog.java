package com.xinhua;

public class Dog extends Animal {
    public void bark() {
        System.out.println("Dog is barking.");
    }

    @Override
    public void eat() {
        System.out.println("Dog is eating.");
    }

    @Override
    public void makeSount() {
        System.out.println("Dog is making a sound");
    }
}
