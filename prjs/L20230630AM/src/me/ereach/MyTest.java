package me.ereach;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class MyTest {
    @Test
    public void test01() {
        Map<String, User> map = new HashMap<>();

        var u1 = new User("Tom", "male", "e1", 19);
        var u2 = new User("Jerry", "male", "e2", 18);
        var u3 = new User("Cherry", "female", "e3", 20);
        var u4 = new User("Wendy", "female", "e4", 21);

        map.put(u1.getEmail(), u1);
        map.put(u2.getEmail(), u2);
        map.put(u3.getEmail(), u3);
        map.put(u4.getEmail(), u4);

        var u = map.get("e3");
        System.out.println(u);

        var flag = map.containsKey("e2");
        System.out.println(flag);
    }
}
