package me.ereach.tax;

interface ITax {
    float getTax(float income);
}
