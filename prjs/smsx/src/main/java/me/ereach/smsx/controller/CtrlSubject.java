package me.ereach.smsx.controller;

import me.ereach.smsx.mapper.SubjectMapper;
import me.ereach.smsx.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CtrlSubject {
    @Autowired
    private SubjectMapper subjectMapper;

    @GetMapping("/subject")
    public List<Subject> getSubject() {
        return subjectMapper.selectList(null);
    }

    @GetMapping("/subject/{id}")
    public Subject getSubjectById(
            @PathVariable("id") int id
    ) {
        return subjectMapper.selectById(id);
    }

    // http://localhost:8081/api/subject2?id=1
    @GetMapping("/subject2")
    public Subject getSubjectById2(
            @RequestParam(value = "id") Integer id
    ) {
        return subjectMapper.selectById(id);
    }

    @PostMapping("/subject")
    public Integer insertSubject(
            @RequestParam("subjectname") String subjectName
    ) {
        Subject subject = new Subject();

        subject.setSubjectName(subjectName);

        return subjectMapper.insert(subject);
    }

    @PutMapping("/subject")
    public Integer updateSubject(
            @RequestParam("id") int id,
            @RequestParam("subjectname") String subjectName
    ) {
        Subject subject = new Subject();
        subject.setId(id);
        subject.setSubjectName(subjectName);

        return subjectMapper.updateById(subject);
    }

    @DeleteMapping("/subject")
    public Integer deleteSubject(
            @RequestParam("id") int id
    ) {
        Subject subject = new Subject();
        subject.setId(id);

        return subjectMapper.deleteById(subject);
    }
}
