package me.ereach;

public class Triangle implements IGeometry {
    private Point pa;
    private Point pb;
    private Point pc;

    public Triangle(Point pa, Point pb, Point pc) {
        this.pa = pa;
        this.pb = pb;
        this.pc = pc;
    }

    public Point getPa() {
        return pa;
    }

    public void setPa(Point pa) {
        this.pa = pa;
    }

    public Point getPb() {
        return pb;
    }

    public void setPb(Point pb) {
        this.pb = pb;
    }

    public Point getPc() {
        return pc;
    }

    public void setPc(Point pc) {
        this.pc = pc;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "pa=" + pa +
                ", pb=" + pb +
                ", pc=" + pc +
                '}';
    }

    @Override
    public double getArea() {
        return 100D;
    }

    @Override
    public double getPerimeter() {
        return 120D;
    }
}
