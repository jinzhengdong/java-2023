package com.xinhua;

public class Lesson3 {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= 10; i++) {
            sum += i;
        }
        System.out.println(sum);

        sum = 0;
        int count = 1;
        while (count <= 10) {
            sum += count;
            count++;
        }
        System.out.println(sum);

        sum = 0;
        count = 1;
        do {
            sum += count;
            count++;
        } while (count <= 10);
        System.out.println(sum);
    }
}
