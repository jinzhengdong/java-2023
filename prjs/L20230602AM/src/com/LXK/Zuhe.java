package com.LXK;

public class Zuhe {
    private point a;
    private point b;
    private point c;

    public Zuhe(point a, point b, point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public point getA() {
        return a;
    }

    public void setA(point a) {
        this.a = a;
    }

    public point getB() {
        return b;
    }

    public void setB(point b) {
        this.b = b;
    }

    public point getC() {
        return c;
    }

    public void setC(point c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "Zuhe{" +
                ",jl=" +jl()+
                ",lx=" +lx()+
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
    public double jl(){
        return  a.juli(c)+a.juli(b)+b.juli(c);
    }
    public double lx(){
        double e = jl()/2;
        double r = a.juli(c);
        double t = a.juli(b);
        double g = b.juli(c);
        return Math.sqrt(e*(a.juli(b)*(a.juli(c)*(b.juli(c)))));
    }
}
