package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        int a = 3;
        int b = 5;
        System.out.println(a + b);

        short c = 1;
        int d = 12;
        System.out.println(c + d);

        Integer e = 17;
        int f = 18;
        System.out.println(e + f);

        Number x = 12;
        Number y = 17.98;
        System.out.println((Integer) x + (Double) y);
    }

    @Test
    public void test02() {
        GenericList<Integer> genericList1 = new GenericList<>();
        genericList1.add(1);
        genericList1.add(100);
//        genericList1.add("sss");

        for (int i = 0; i < genericList1.getCount(); i++)
            System.out.println(genericList1.get(i));

        System.out.println("===");

        GenericList<Double> genericList2 = new GenericList<>();
        genericList2.add(2.34);
        genericList2.add(3.14);

        for (int i = 0; i < genericList2.getCount(); i++)
            System.out.println(genericList2.get(i));

    }

    @Test
    public void test01() {
        ObjList objList = new ObjList();

        objList.add(1);
        objList.add(2);
        objList.add("Hello, World!");
        objList.add(new java.awt.Point(2, 3));
        objList.add(new java.awt.Point(5, 7));

        for (int i = 0; i < objList.getCount(); i++)
            System.out.println(objList.get(i));
    }
}
