package me.ereach.hr;

public class Emp {
    private Integer id;
    private String name;
    private Float salary;
    private Integer extraHours;
    private Float hourlyRate;

    public Emp(Integer id, String name, Float salary, Integer extraHours, Float hourlyRate) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.extraHours = extraHours;
        this.hourlyRate = hourlyRate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public Integer getExtraHours() {
        return extraHours;
    }

    public void setExtraHours(Integer extraHours) {
        this.extraHours = extraHours;
    }

    public Float getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", extraHours=" + extraHours +
                ", hourlyRate=" + hourlyRate +
                '}';
    }

    public Float getTotalSalary() {
        Float s = salary + hourlyRate * extraHours;
        Float t = (s - 5000) * 0.1f;
        return s - t;
    }
}
