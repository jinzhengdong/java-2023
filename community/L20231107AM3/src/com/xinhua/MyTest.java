package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MyTest {
    @Test
    public void test01() {
        List<Person> list = new ArrayList<>();

        // add 添加
        list.add(new Person("Tom", "male"));
        list.add(new Person("Wendy", "female"));
        list.add(new Person("Wendy", "male"));

        // 输出全部
        System.out.println(list);

        // 查询
        list.stream().filter(x -> x.getName() == "Wendy").forEach(x -> System.out.println("qry:" + x));

        // get 获取
        System.out.println(list.get(0));

        // remove 删除
        list.remove(0);

        System.out.println(list);
    }
}
