```sql
select actor_id id, last_name 姓, first_name 名 from actor;
select actor_id as id, last_name as 姓, first_name as 名 from actor;
select actor_id as 'id', last_name as '姓', first_name as '名' from actor;
SELECT actor_id id, CONCAT(first_name, ' ', last_name) fullName FROM actor;

-- address 地址
select
    a.address, a.address2, a.district, a.city_id,
    b.city
from
    address as a
    left join city as b on a.city_id = b.city_id

select * from category;
-- children 儿童片
-- Classics 经典
-- Comedy 悲剧
-- Documentary 纪录片
-- Family 家庭剧

-- city 城市
-- country 国家
select
    a.*,
    b.country
from
    city as a
    left join country as b on a.country_id = b.country_id;

-- fullname 全名
-- customer 顾客
select
    concat(a.first_name, ' ', a.last_name) fullname,
    b.address
from
    customer as a
    left join address as b on a.address_id = b.address_id
    
-- rental 租赁表
-- rental 租赁日期
-- inventory_id 库存产品id
-- customer_id 顾客id
-- return_date 归还日期
-- staff_id 员工id
select
    a.rental_id id,
    a.rental_date 租赁日期,
    -- a.inventory_id 音像ID,
    -- b.film_id '电影id',
    c.title,
    -- a.customer_id,
    concat(d.first_name, ' ', d.last_name) custname
from
    rental as a
    left join inventory as b on a.inventory_id = b.inventory_id
    left join film as c on c.film_id = b.film_id
    left join customer as d on a.customer_id = d.customer_id
```