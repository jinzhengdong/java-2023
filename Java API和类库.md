## Java API和类库

Java API和类库是Java开发中的重要组成部分，提供了各种各样的类和方法，帮助开发者快速构建应用程序。以下是Java API和类库中的一些重要知识点和示例：

### 字符串处理

Java中提供了丰富的字符串处理类和方法，例如`String`、`StringBuilder`、`StringBuffer`等。以下是Java字符串处理的示例：

java

复制代码

```java
//字符串拼接
String str1 = "hello";
String str2 = "world";
String result = str1 + " " + str2;

//字符串查找
String str = "hello world";
int index = str.indexOf("world");

//字符串替换
String str = "hello world";
String result = str.replace("world", "Java");
```

### 集合框架

Java的集合框架提供了各种各样的集合类和数据结构，例如`List`、`Set`、`Map`等。以下是Java集合框架的示例：

java

复制代码

```java
//List的使用
List<String> list = new ArrayList<>();
list.add("Apple");
list.add("Banana");
list.add("Orange");
String fruit = list.get(0);

//Set的使用
Set<Integer> set = new HashSet<>();
set.add(1);
set.add(2);
set.add(3);
boolean contains = set.contains(2);

//Map的使用
Map<String, Integer> map = new HashMap<>();
map.put("Apple", 1);
map.put("Banana", 2);
map.put("Orange", 3);
int value = map.get("Apple");
```

### 日期和时间处理

Java中提供了`java.util.Date`和`java.time`等类来处理日期和时间。以下是Java日期和时间处理的示例：

java

复制代码

```java
//获取当前时间
Date now = new Date();

//日期格式化
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
String formattedDate = sdf.format(now);

//LocalDateTime使用
LocalDateTime now = LocalDateTime.now();
int year = now.getYear();
int month = now.getMonthValue();
int day = now.getDayOfMonth();
```

### 输入输出流

Java中提供了各种各样的输入输出流类，例如`InputStream`、`OutputStream`、`Reader`、`Writer`等。以下是Java输入输出流的示例：

java

复制代码

```java
//写入文件
try (FileWriter writer = new FileWriter("data.txt")) {
    writer.write("Hello World");
}

//读取文件
try (FileReader reader = new FileReader("data.txt")) {
    int data = reader.read();
    while (data != -1) {
        System.out.println((char)data);
        data = reader.read();
    }
}
```

## 总结

以上是Java API和类库中的一些重要知识点和示例。Java API和类库是Java开发中的重要组成部分，需要不断地掌握和练习，以便在后续的Java开发中更加熟练地使用各种API和类库。