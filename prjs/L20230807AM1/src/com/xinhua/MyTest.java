package com.xinhua;

import org.junit.Test;

import java.sql.*;

public class MyTest {
    @Test
    public void test03() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // 加载数据库驱动程序
//            Class.forName("com.mysql.cj.jdbc.Driver");
            // 建立数据库连接
            String url = "jdbc:mysql://localhost:3306/sakila?serverTimezone=Asia/Shanghai";
            String user = "root";
            String password = "123456";

            conn = DriverManager.getConnection(url, user, password);
            // 创建Statement对象
            stmt = conn.createStatement();

            int retvar = stmt.executeUpdate("insert into actor (first_name, last_name) values ('三', '张')");
            System.out.println(retvar);

//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test02() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // 加载数据库驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 建立数据库连接
            String url = "jdbc:mysql://localhost:3306/sakila?serverTimezone=Asia/Shanghai";
            String user = "root";
            String password = "123456";

            conn = DriverManager.getConnection(url, user, password);
            // 创建Statement对象
            stmt = conn.createStatement();

            int retvar = stmt.executeUpdate("insert into actor (first_name, last_name) values ('三', '张')");
            System.out.println(retvar);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test01() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // 加载数据库驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");
            // 建立数据库连接
            String url = "jdbc:mysql://localhost:3306/sakila?serverTimezone=Asia/Shanghai";
            String user = "root";
            String password = "123456";

            conn = DriverManager.getConnection(url, user, password);
            // 创建Statement对象
            stmt = conn.createStatement();

            int retvar = stmt.executeUpdate("update actor set first_name = 'ED G' where actor_id = 3");
            System.out.println(retvar);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
