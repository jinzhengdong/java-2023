package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        Cube c = new Cube(7);
        System.out.println(c);
        System.out.println(c.getVolume());
        System.out.println(c.getArea());
        System.out.println(c.getPerimeter());
    }

    @Test
    public void test02() {
        Rectangle r = new Rectangle(3, 7);
        System.out.println(r.getArea());
        System.out.println(r.getPerimeter());
    }

    @Test
    public void test01() {
        Circle c = new Circle(2, 3, 5);
        System.out.println(c.getArea());
        System.out.println(c.getPerimeter());
    }
}
