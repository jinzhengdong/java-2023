package me.ereach;

import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

public class MyTest {
    @Test
    public void test02() {
        String[] fruits = {"Apple", "Orange", "Banana", "Pear", "Ch", "bb"};

        Arrays.sort(fruits);

        for (String fruit : fruits)
            System.out.println(fruit);
    }

    @Test
    public void test01() {
        String[] fruits = {"Apple", "Orange", "Banana", "Pear", "ch"};

        Comparator<String> comparator = (String f1, String f2) -> f1.length() - f2.length();

        Arrays.sort(fruits, comparator);

        for (String fruit : fruits)
            System.out.println(fruit);
    }
}
