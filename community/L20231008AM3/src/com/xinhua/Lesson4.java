package com.xinhua;

public class Lesson4 {
    public static void main(String[] args) {
        int num = 0;
        int count = 1;

        do {
            num += count;
            count++;
        } while (count <= 100);

        System.out.println(num);

        num = 0;
        count = 1;
        do {
            num += count;
            count += 2;
        } while (count <= 100);

        System.out.println(num);

        num = 0;
        count = 0;
        do {
            num += count;
            count += 2;
        } while (count <= 100);

        System.out.println(num);

    }
}
