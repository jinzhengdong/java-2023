package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MyTest {
    private List<Teacher> teachers = Arrays.asList(
            new Teacher("Mao WenTao", "female", "政治"),
            new Teacher("Liu ShangChao", "male", "C#"),
            new Teacher("Li YiLun", "male", "MySQL"),
            new Teacher("Jin ZhengDong", "male", "Java"),
            new Teacher("Sen XiaoMei", "female", "Web Page"),
            new Teacher("Fu YanRong", "female", "Network"),
            new Teacher("Ren SongLin", "male", "AR PR"),
            new Teacher("Wang XianQin", "female", "Office")
    );

    private List<Student> students = Arrays.asList(
            new Student("Zhang HaiShan", "male", "2021 Cloud"),
            new Student("Wang Deran", "male", "2021 Cloud"),
            new Student("Hu Long", "male", "2021 Cloud"),
            new Student("Du JinYu", "male", "2021 Cloud"),
            new Student("Zi YinHong", "male", "2021 Cloud"),
            new Student("Ma ZiXuan", "male", "2021 Cloud"),
            new Student("Li XinHao", "male", "2021 Cloud"),
            new Student("Bai YanWwei", "male", "2022 Cloud"),
            new Student("Guo Jian", "male", "2022 Cloud"),
            new Student("Zhou ShuangYang", "male", "2022 Cloud"),
            new Student("Chen RongLai", "male", "2022 Cloud"),
            new Student("Yu Yu", "male", "2022 Cloud"),
            new Student("Jiang FuYang", "male", "2022 Cloud"),
            new Student("Wan HaiChuan", "male", "2022 Cloud"),
            new Student("Lei ZhengYong", "male", "2022 Cloud"),
            new Student("Wang XiaoGgang", "male", "2022 Cloud"),
            new Student("Wu Ye", "male", "2022 Cloud"),
            new Student("Zeng HeRui", "female", "2022 Cloud")
    );

    @Test
    public void test02() {
        students.stream()
                .filter(x -> x.getGender() == "female")
                .forEach(System.out::println);
        System.out.println("===");
        students.stream()
                .filter(x -> x.getClassName().startsWith("2021"))
                .forEach(System.out::println);
        System.out.println("===");
        students.stream()
                .filter(x -> x.getClassName().startsWith("2022"))
                .forEach(System.out::println);
        System.out.println("===");
        students.stream()
                .filter(x -> x.getName().startsWith("Zhou"))
                .forEach(System.out::println);
        System.out.println("===");
        List<Student> list = students.stream()
                .filter(x -> x.getGender() == "female")
                .collect(Collectors.toList());

        Integer cnt = 0;
        for (Student x : list)
            cnt += 1;

        System.out.println(cnt);
    }
    @Test
    public void test01() {
        List<Circle> list1 = Arrays.asList(
                new Circle(2),
                new Circle(3),
                new Circle(5),
                new Circle(7)
        );

        list1.stream().forEach(System.out::println);

        System.out.println("===");

        List<Circle> list2 = list1.stream()
                .filter(x -> x.getR() > 3)
                .collect(Collectors.toList());

        list2.stream().forEach(System.out::println);

        System.out.println("===");

        list1.stream()
                .filter(x -> x.getArea() < 78)
                .forEach(System.out::println);
    }
}
