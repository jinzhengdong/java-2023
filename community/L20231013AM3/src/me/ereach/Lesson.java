package me.ereach;

import org.junit.Test;

public class Lesson {
    @Test
    public void test01() {
        System.out.println("Hello, World!");
    }

    @Test
    public void test02() {
        int[] arr = new int[5];

        arr[0] = 1;
        arr[1] = 2;
        arr[2] = 3;
        arr[3] = 4;
        arr[4] = 5;

        System.out.println(arr);

        for (int i = 0; i < arr.length; i++)
            System.out.println(arr[i]);

        System.out.println("===");

        printArray(arr);
    }

    @Test
    public void test03() {
        String[] arr = new String[5];

        arr[0] = "Hello";
        arr[1] = "World";

        int n = 0;

        while (n < arr.length) {
            System.out.println(arr[n]);
            n++;
        }

        System.out.println("===");

        printArray(arr);
    }

    @Test
    public void test04() {
        String[] arr1 = {"Hello", "World", "Tom", "Jerry", "Tod"};
        // 方法的标识符：方法名字 + 参数
        printArray(arr1);

        int[] arr2 = {1, 2, 3, 4, 5};
        printArray(arr2);
    }

    @Test
    public void test05() {
        int[] arr = {64, 38, 99, 12};

        printArray(arr);
        bubbleSort(arr);
        printArray(arr);
    }

    public void bubbleSort(int[] arr) {
        int n = arr.length;

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public void printArray(int[] a) {
        int n = a.length;

        for (int i = 0; i < n; i++)
            System.out.print(a[i] + " ");

        System.out.println();
    }

    // 方法的重载
    public void printArray(String[] a) {
        int n = a.length;

        for (int i = 0; i < n; i++)
            System.out.print(a[i] + " ");

        System.out.println();
    }
}
