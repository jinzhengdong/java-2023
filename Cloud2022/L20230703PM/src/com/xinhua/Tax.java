package com.xinhua;

public class Tax implements ITax {
    @Override
    public float getTax(float totalSalary) {
        if (totalSalary > 5000) {
            return (totalSalary - 5000) * 0.1f;
        }
        else
            return 0;
    }
}
