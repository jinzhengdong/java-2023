package me.ereach;

public class Movie {
    private String title;
    private Integer likes;

    public Movie(String title, Integer likes) {
        this.title = title;
        this.likes = likes;
    }

    public String getTitle() {
        return title;
    }

    public Integer getLikes() {
        return likes;
    }
}
