# Java包装类型

Java的包装类型是指将基本数据类型封装成对象的类型，它们分别是：Byte、Short、Integer、Long、Float、Double、Character、Boolean。这些包装类型都是在java.lang包中定义的，因此在使用时无需导入其他包。

包装类型的主要作用是将基本数据类型转换成对象，从而可以在对象上调用一些方法，比如toString()、equals()等。此外，包装类型还可以用于泛型、集合等需要对象类型的场合。

下面是一个简单的示例，演示了如何使用包装类型：

```
public class Test {
    public static void main(String[] args) {
        // 定义一个Integer对象
        Integer i = new Integer(10);
        
        // 调用Integer对象的方法
        System.out.println("i的值为：" + i.intValue());
        System.out.println("i的二进制表示为：" + Integer.toBinaryString(i));
        
        // 将Integer对象转换成int类型
        int j = i.intValue();
        System.out.println("j的值为：" + j);
        
        // 将字符串转换成Integer对象
        String str = "20";
        Integer k = Integer.valueOf(str);
        System.out.println("k的值为：" + k);
        
        // 将Integer对象转换成字符串
        String str2 = k.toString();
        System.out.println("str2的值为：" + str2);
    }
}
```

在上面的示例中，我们定义了一个Integer对象i，并调用了它的一些方法，比如intValue()、toBinaryString()等。同时，我们还演示了如何将Integer对象转换成int类型，以及如何将字符串转换成Integer对象。最后，我们将Integer对象转换成字符串，并输出它的值。

由于包装类型是对象，因此它们可以为null。在使用包装类型时，需要注意空指针异常的情况。