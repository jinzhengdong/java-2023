$(function () {
    let ck = new CookieSettings()
    let pathvars = ck.getUrlVars()

    debugger

    $("#subjectName").val(decodeURIComponent(pathvars.subjectname.replace(/\+/g, " ")))

    $("#btnSave").on("click", function () {

        let formData = new FormData()

        formData.append("id", pathvars.id)
        formData.append("subjectname", $("#subjectName").val())

        $.ajax({
            url: "http://localhost:8081/api/subject",
            type: 'PUT',
            cache: false,
            data: formData,
            processData: false,
            contentType: false
        }).done(function (res) {
            debugger
            if (res != null)
                $("#message").text(`OK, id = ${res}`)
            else
                $("#message").text(`failed.`)
        })
    })
})