package com.xinhua;

public class Subject {
    private Integer id;
    private String subjectName;
    private float factor;
    private String description;

    public Subject(Integer id, String subjectName, float factor, String description) {
        this.id = id;
        this.subjectName = subjectName;
        this.factor = factor;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public float getFactor() {
        return factor;
    }

    public void setFactor(float factor) {
        this.factor = factor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", subjectName='" + subjectName + '\'' +
                ", factor=" + factor +
                ", description='" + description + '\'' +
                '}';
    }
}
