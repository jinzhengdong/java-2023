import java.util.Arrays;

public class Main01 {
    public static void main(String[] args) {
        int[] intArray = {1, 2, 3};
        System.out.println(Arrays.toString(intArray));

        System.out.println("===");
        /*
        * [[1, 2, 3],
        *  [4, 5, 6],
        *  [7, 8, 9]]
        * */
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        System.out.println(Arrays.toString(matrix));
        System.out.println(Arrays.deepToString(matrix));
    }
}
