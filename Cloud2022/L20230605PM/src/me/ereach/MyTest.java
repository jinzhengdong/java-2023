package me.ereach;

import org.junit.Test;

import java.util.Arrays;

public class MyTest {
    @Test
    public void test05() {
        int a = 10;
        System.out.println(a);
        a = 15;
        System.out.println(a);

        final int C = 10;
        System.out.println(C);
    }

    @Test
    public void test04() {
        int[] iarray =  {1, 2, 3, 4};
        System.out.println(Arrays.toString(iarray));
        System.out.println("===");
        int[][] arry = {{1, 2, 3}, {4, 5, 6}};
        System.out.println(Arrays.toString(arry));
        System.out.println("===");
        System.out.println(Arrays.deepToString(arry));
    }

    @Test
    public void test03() {
        int[] iary = new int[2];
        iary[0] = 1;
        iary[1] = 2;

        System.out.println(iary);

        for (int i = 0; i < iary.length; i++) {
            System.out.println(iary[i]);
        }

        System.out.println("===");

        for (var x : iary)
            System.out.println(x);

        System.out.println("===");
        System.out.println(Arrays.toString(iary));
    }

    @Test
    public void test02() {
        System.out.println("Are\t\tyou OK?");
        System.out.println("He said: \"Are you OK?\"");
        System.out.println("He said: 'Are you OK?'");
        System.out.println("He said: \'Are you OK?\'");
        System.out.println("Hello, \\World");
    }

    @Test
    public void test01() {
        System.out.println("Hello, \nWorld!");
    }
}
