package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test02() {
        Teacher t1 = new Teacher("Wendy", 18, "xinhua");
        System.out.println(t1);

        t1.setName("Tod");
        System.out.println(t1);
    }

    @Test
    public void test01() {
        Student s1 = new Student("Tom", 18, "xinhua");
        Student s2 = new Student("Jerry", 12, "xinhua");
        System.out.println(s1);
        System.out.println(s2);

        s1.setName("Cherry");
        System.out.println(s1);
    }
}
