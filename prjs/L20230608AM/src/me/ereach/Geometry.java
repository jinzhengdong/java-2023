package me.ereach;

public interface Geometry {
    double getArea();
    double getPerimeter();
    double getVolume();
}
