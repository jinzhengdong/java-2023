import me.ereach.geo.Point2D;
import me.ereach.hr.Emp;
import me.ereach.hr.Employee;
import me.ereach.hr.Employee2;
import org.junit.Test;

import java.awt.*;

public class MyTest {
    @Test
    public void test01() {
        Point2D p1 = new Point2D(2, 3);
        Point2D p2 = new Point2D(2, 3);
        System.out.println(p1.equals(p2));

        String str = "Hello, World";
//        System.out.println(p1.equals(str));

        Point p3 = new Point(2, 3);
        Point p4 = new Point(2, 3);
        System.out.println(p3.equals(p4));
//        System.out.println(p3.equals(str));
    }
    @Test
    public void test02(){
        Employee emp1 = new Employee(01,"Tom",true,"1987894561",7000D,10,20D);
        System.out.println(emp1.sjgz(emp1));
    }
    @Test
    public void test03(){
        Employee2 P3 = new Employee2(1,"lxk",true,8000d,50,20);
        double p4 = P3.getGongzi()+ P3.getJiaban()* P3.getJbgongzi();
        double p5 = ((P3.getGongzi()+P3.getJiaban()* P3.getJbgongzi())-5000)*0.1;
        double p6 = p4-p5;
        System.out.println(p4);
        System.out.println(p5);
        System.out.println(p6);
    }

    @Test
    public void test04() {
        Emp e0 = new Emp(1, "Tod", 6000f, 20, 30f);
        Emp e1 = new Emp(1, "Tod", 6000f, 20, 30f);
        System.out.println(e1.getTotalSalary());
    }
}
