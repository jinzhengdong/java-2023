// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        byte a = -128; // 1 byte 11111111
        byte a1 = 127; // 1 byte 01111111

        short b = 2;   // 2 bytes
        int c = 100;    // 4 bytes
        long d = 5000;  // 8 bytes
        float e = 3.141592653589F;   // 4 bytes
        double f = 3.141592653589D;   // 8 bytes

        char g = 'a';
        char h = '2';
        char i = '4';

        System.out.println(e);
        System.out.println(f);
        System.out.println(g);

        System.out.println(h + i);

        byte x1 = 2;
        byte x2 = 4;
        System.out.println(x1 + x2);

        byte x3 = '4';
        char x4 = '4';
        System.out.println(x3 + x4);
    }
}