填空题：

1. 在Java中，所有的类都默认继承自 _______ 类。
答案：Object

2. 抽象类中可以有非抽象方法，但是抽象方法必须在抽象类中声明为 _______。
答案：抽象方法

3. 在Java中，接口中的方法默认为 _______。
答案：抽象方法

4. 在Java中，使用 _______ 关键字可以防止子类对父类中的方法进行重写。
答案：final

5. 在Java中，使用 _______ 关键字可以防止子类对父类中的方法进行重载。
答案：static

6. 在Java中，使用 _______ 关键字可以使一个类只能被实例化一次。
答案：singleton

7. 在Java中，使用 _______ 关键字可以使一个类不被继承。
答案：final

8. 在Java中，使用 _______ 关键字可以使一个方法只能被同一个类中的其他方法调用。
答案：private

9. 在Java中，使用 _______ 关键字可以使一个方法在子类中被重写。
答案：protected

10. 在Java中，使用 _______ 关键字可以使一个类在同一个包中的其他类访问。
答案：default

选择题：

1. 下列哪个关键字可以使一个类不被继承？
A. abstract
B. final
C. static
D. private
答案：B

2. 下列哪个关键字可以使一个方法在子类中被重写？
A. private
B. final
C. protected
D. static
答案：C

3. 下列哪个关键字可以使一个方法只能被同一个类中的其他方法调用？
A. private
B. final
C. protected
D. static
答案：A

4. 下列哪个关键字可以使一个类只能被实例化一次？
A. singleton
B. final
C. static
D. private
答案：A

5. 下列哪个关键字可以防止子类对父类中的方法进行重载？
A. private
B. final
C. protected
D. static
答案：B

6. 下列哪个关键字可以防止子类对父类中的方法进行重写？
A. private
B. final
C. protected
D. static
答案：B

7. 下列哪个关键字可以使一个类在同一个包中的其他类访问？
A. default
B. final
C. static
D. private
答案：A

8. 下列哪个关键字可以使一个类不被实例化？
A. singleton
B. final
C. static
D. private
答案：B

9. 下列哪个关键字可以使一个方法同时被同一个类中的其他方法和子类中的方法调用？
A. private
B. final
C. protected
D. public
答案：C

10. 下列哪个关键字可以使一个方法默认为抽象方法？
A. abstract
B. final
C. protected
D. static
答案：A

编程题：

1. 编写一个类，实现单例模式。
答案：

public class Singleton {
    private static Singleton instance = new Singleton();
    private Singleton() {}
    public static Singleton getInstance() {
        return instance;
    }
}

2. 编写一个抽象类Animal，包含一个抽象方法eat()，并实现两个子类Dog和Cat，分别重写eat()方法。
答案：

public abstract class Animal {
    public abstract void eat();
}

public class Dog extends Animal {
    @Override
    public void eat() {
        System.out.println("狗吃骨头");
    }
}

public class Cat extends Animal {
    @Override
    public void eat() {
        System.out.println("猫吃鱼");
    }
}

3. 编写一个接口Shape，包含一个方法area()，并实现两个类Circle和Rectangle，分别重写area()方法。
答案：

public interface Shape {
    double area();
}

public class Circle implements Shape {
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    @Override
    public double area() {
        return Math.PI * radius * radius;
    }
}

public class Rectangle implements Shape {
    private double width;
    private double height;
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    @Override
    public double area() {
        return width * height;
    }
}

4. 编写一个类Student，包含属性name和age，并实现Comparable接口，按照年龄从小到大排序。
答案：

public class Student implements Comparable<Student> {
    private String name;
    private int age;
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    @Override
    public int compareTo(Student o) {
        return this.age - o.age;
    }
}