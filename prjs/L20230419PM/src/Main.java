import java.util.Arrays;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int[] intArray = {2, 3, 1};
        String[] strArray = {"Hello, ", "World", "!"};

        System.out.println(intArray.length);

        int[] intObj = intArray.clone();

        System.out.println(intObj.length);

        // forEach
        for(var i : intObj) {
            System.out.println(i);
        }

        System.out.println("===");
        Arrays.stream(intArray).sorted().forEach(a -> System.out.println(a));

        Arrays.sort(intArray);
        System.out.println("===");

        for (var i : intArray)
            System.out.println(i);

    }
}