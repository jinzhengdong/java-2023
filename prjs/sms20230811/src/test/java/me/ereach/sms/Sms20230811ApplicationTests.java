package me.ereach.sms;

import me.ereach.sms.mapper.SubjectMapper;
import me.ereach.sms.model.Subject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Sms20230811ApplicationTests {
    @Autowired
    private SubjectMapper subjectMapper;

    @Test
    void deleteSubject() {
        Subject subject = new Subject();

        subject.setId(3);

        int retvar = subjectMapper.deleteById(subject);

        System.out.println(retvar);
    }

    @Test
    void putSubject() {
        Subject subject = new Subject();

        subject.setId(3);
        subject.setSubjectName("C++ Programming Language");

        int retvar = subjectMapper.updateById(subject);

        System.out.println(retvar);
    }

    @Test
    void postSubject() {
        Subject subject = new Subject();

        subject.setId(3);
        subject.setSubjectName("JavaScript Programming Language");

        int retvar = subjectMapper.insert(subject);

        System.out.println(retvar);
    }

    @Test
    void contextLoads() {
        List<Subject> list = subjectMapper.selectList(null);
        list.stream().forEach(x -> System.out.println(x));
    }

}
