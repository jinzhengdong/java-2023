public class Main03 {
    public static void main(String[] args) {
        int i = 1;

//        while (i <= 10) {
//            System.out.println(i);
//            i++;
//        }

//        while (i <= 10) {
//            System.out.println(i);
//        }

//        while (i <= 10) {
//            if (i % 2 == 1) {
//                System.out.println(i);
//            }
//
//            i++;
//        }

//        while (i <= 10) {
//            if (i % 2 == 1) {
//                i++;
//                continue;
//            }
//
//            System.out.println(i);
//            i++;
//        }

        while (i <= 10) {
            if (i % 3 == 2) {
                i++;
                System.out.println("cur i = " + i);
                break;
            }

            System.out.println(i);
            i++;
        }

        System.out.println("final step.");
    }
}
