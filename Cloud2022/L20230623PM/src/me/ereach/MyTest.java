package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test03() {
        System.out.println(Utils.max(7, 4));    // 7
        System.out.println(Utils.max(7.6f, 23.5f));     // 23.5
        System.out.println(Utils.max(7, 12.57f));   // 12.57
        System.out.println(Utils.max(1.23f, 7));    // 7
    }

    @Test
    public void test02() {
        System.out.println(Utils.max(20, 17.17f));
        System.out.println(Utils.max(22.23f, 13.33f));

        System.out.println(Utils.min(3, 8));
        System.out.println(Utils.min(17, 4));
    }

    @Test
    public void test01() {
        Point p1 = new Point();
        Point p2 = new Point(2, 3);
        System.out.println(p1);
        System.out.println(p2);

        p1.setX(5);
        p1.setY(5);
        System.out.println(p1);

        System.out.println(p2.getX());
        System.out.println(p2.getY());

        System.out.println(p1.getDistance(p2));
    }
}
