package me.ereach;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MyTest {
    @Test
    public void test02() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");

        System.out.println(list);

        list.add(1, "cde");

        System.out.println(list);
    }

    @Test
    public void test01() {
        Collection<String> collection = new ArrayList<>();

        collection.add("a");
        collection.add("b");
        collection.add("c");

        for (var x : collection)
            System.out.println(x);

        System.out.println("===");
        Collections.addAll(collection, "d", "e", "f");
        System.out.println(collection);

        System.out.println("===second");
        var objArray = collection.toArray();

        for (int i = 0; i < objArray.length; i++)
            System.out.println(objArray[i]);

        System.out.println("===end");
        System.out.println(objArray.getClass());
        System.out.println(">>>>");
        for (var x : objArray)
            System.out.println(x.getClass());

    }
}
