package com.xinhua;

public class Poin2D {
    private float x;
    private float y;

    public Poin2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Poin2D() {
        x = 0;
        y = 0;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public double getDistance(Poin2D p) {
        float w = this.x - p.x;
        float h = this.y - p.y;

        return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2));
    }

    @Override
    public String toString() {
        return "Poin2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Poin2D) {
            Poin2D p = (Poin2D) obj;
            return p.x == this.x && p.y == this.y;
        }
        return false;
    }
}
