package me.ereach;

public class User implements Comparable<User> {
    private String name;
    private String gender;
    private int points;

    public User(String name, String gender, int points) {
        this.name = name;
        this.gender = gender;
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public int compareTo(User o) {
        return this.points - o.points;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", points=" + points +
                '}';
    }
}
