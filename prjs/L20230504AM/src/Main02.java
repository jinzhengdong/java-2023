public class Main02 {
    public static void main(String[] args) {
        int[] intArray = {1, 3, 5, 7, 9};

//        for (var x : intArray)
//            System.out.println(x);

        for (int x : intArray) {
            System.out.println(x);
            System.out.println("Hello, World!");
        }

//        for (int i = 0; i < intArray.length; i++) {
//            System.out.println(intArray[i]);
//        }

    }
}
