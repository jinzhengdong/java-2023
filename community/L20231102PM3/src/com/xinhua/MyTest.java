package com.xinhua;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MyTest {
    @Test
    public void test03() {
        List<Employee> list = new ArrayList<>();

        list.add(new Employee("Tom", "male", 5000, 4, 20));
        list.add(new Employee("Jerry", "male", 4000, 8, 18));

        System.out.println(list);
        System.out.println("===");

        for (Employee emp : list)
            System.out.println(emp);
        System.out.println("===");

        list.stream().forEach(x -> System.out.println(x));
    }

    @Test
    public void test02() {
        Employee[] employees = new Employee[5];

        employees[0] = new Employee("Tom", "male", 5000, 4, 20);
        employees[1] = new Employee("Jerry", "male", 4000, 8, 18);

        System.out.println(employees);

        for (int i = 0;i < employees.length; i++)
            System.out.println(employees[i]);
    }
    @Test
    public void test01() {
        Employee[] employees = {
                new Employee("Tom", "male", 5000, 4, 20),
                new Employee("Jerry", "male", 4000, 8, 18)
        };

        System.out.println(employees);

        for (int i = 0;i < employees.length; i++)
            System.out.println(employees[i]);
    }
}
