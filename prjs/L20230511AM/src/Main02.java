import java.util.Arrays;

public class Main02 {
    public static void main(String[] args) {
        int[][][] arr = new int[2][3][4];

        // 初始化数组
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                for (int k = 0; k < arr[i][j].length; k++) {
                    arr[i][j][k] = i + j + k;
                }
            }
        }

        // 使用toString输出
        System.out.println("使用toString输出：");
        System.out.println(Arrays.toString(arr));

        // 使用deepToString输出
        System.out.println("使用deepToString输出：");
        System.out.println(Arrays.deepToString(arr));
    }
}
