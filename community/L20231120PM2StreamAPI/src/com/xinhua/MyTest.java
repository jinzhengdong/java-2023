package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MyTest {
    @Test
    public void test02() {
        List<Geo> list = Arrays.asList(
                new Circle(7),
                new Circle(5),
                new Square(9),
                new Square(6)
        );

        list.forEach(System.out::println);
    }

    @Test
    public void test01() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        System.out.println(list.stream()
                .reduce(0, Integer::sum));
        System.out.println("===");
        System.out.println(list.stream()
                .reduce(0, Integer::max));
        System.out.println("===");
        System.out.println(list.stream()
                .reduce(5, Integer::min));
    }
}
