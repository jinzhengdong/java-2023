public class Main02 {
    public static void main(String[] args) {
        float pi = 3.14f;
        System.out.println(pi);
        pi = 3.1415f;
        System.out.println(pi);

        final float PI = 3.14F;
        System.out.println(PI);
//        PI = 3.1415f;

        final float E = 2.71828F;
    }
}
