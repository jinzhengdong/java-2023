public class Main05 {
    public static void main(String[] args) {
        int a = 10, b = 5, c = 2;
        int result = a + b * c; // 优先计算乘法运算符，然后再计算加法运算符
        System.out.println(result); // 输出20

        result = (a + b) * c; // 使用括号改变计算次序
        System.out.println(result); // 输出30

        System.out.println(8 / 3);  // 2

        System.out.println((float) 8 / 3);  // 2

        System.out.println(8 / (float)3);  // 2

        System.out.println((float) 8 / (float)3);  // 2
    }
}
