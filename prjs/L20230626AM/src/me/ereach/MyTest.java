package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test05() {
        User usr1 = new Instructor("Jerry", "male", 12);
        System.out.println(usr1);
        User usr2 = new Instructor("Tod", "male", 16);
        System.out.println(usr2);

        System.out.println(usr1.compareTo(usr2));
    }

    @Test
    public void test04() {
        KeyValuePair<String, Integer> usr = new KeyValuePair<>("age", 12);
        System.out.println(usr);

        KeyValuePair<String, User> kv = new KeyValuePair<>("User", new User("Jerry", "male", 12));
        System.out.println(kv);
    }

    @Test
    public void test03() {
        Utils.print("name", "Wendy");
        Utils.print("age", 12);
    }

    @Test
    public void test02() {
        User u1 = new User("Jerry", "male", 20);
        User u2 = new User("Tom", "male", 18);

        System.out.println(Utils.max(u1, u2));
    }

    @Test
    public void test01() {
        // int Integer
        System.out.println(Utils.max(23D, 17.23D));
    }
}
