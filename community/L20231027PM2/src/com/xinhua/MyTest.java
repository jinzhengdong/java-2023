package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Circle c = new Circle(7);
        System.out.println(c);
        System.out.println(c.getArea());
        System.out.println(c.getPerimeter());
        System.out.println("===");
        Square s = new Square(7);
        System.out.println(s);
        System.out.println(s.getArea());
        System.out.println(s.getPerimeter());
    }
}
