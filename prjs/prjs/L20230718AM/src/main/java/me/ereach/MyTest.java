package me.ereach;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyTest {
    @Test
    public void test07() {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        Integer sum = movies.stream()
                .map(Movie::getLikes)
                .reduce(10, Integer::sum);
        System.out.println(sum);
    }

    @Test
    public void test06() {
        List<Movie> movies = List.of(
                new Movie("a", 10),
                new Movie("b", 15),
                new Movie("c", 20)
        );

        // 调用map方法以映射电影的likes
        // 调用reduce方法以简化流为单值流
        // 可以看到reduce被重载过，第二个版本支持两个参数的累加器
        // 操作过程类似
        // [10, 15, 20]
        // [25, 20]
        // [45]
        // 它返回一个 Optional<Integer>，意思是或许是一个整数值
        Optional<Integer> sum = movies.stream()
                .map(m -> m.getLikes())
                .reduce((a, b) -> a + b);

        // 对于或许没有返回值的sum来说，使用orElse(0)
        // 表示有值则输出该值，无值则输出0
        System.out.println(sum.orElse(0));
    }

    @Test
    public void test05() {
        List<Integer> list = List.of(1,2, 4, 7, 10);

        list.stream().skip(20)
                .forEach(System.out::println);
    }

    @Test
    public void test04() {
        Stream.iterate(1, n -> n + 1)
                .limit(10)
                .forEach(System.out::println);

        System.out.println("===");

        var stream = Stream.of(List.of(1, 2, 3), List.of(4, 5, 6));
        stream.forEach(list -> System.out.println(list));
    }

    @Test
    public void test03() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 7);

        // 使用forEach打印每个元素
        numbers.parallelStream()
                .forEach(System.out::println);
        System.out.println("===");;

        // 使用collect将元素收集到一个List中
        List<Integer> evenNumbers = numbers.stream()
                .filter(number -> number % 2 == 0)
                .collect(Collectors.toList());

        for (var x : evenNumbers)
            System.out.println(x);
        System.out.println("===");
        // 使用count统计元素个数
        long count = numbers.stream()
                .count();
        System.out.println(count);
        System.out.println("===");

        // 使用max找到最大元素
        int max = numbers.stream()
                .max(Integer::compare)
                .orElse(0);
        System.out.println(max);
        System.out.println("===");
        // 使用min找到最小元素
        int min = numbers.stream()
                .min(Integer::compare)
                .orElse(0);
        System.out.println(min);
    }

    @Test
    public void test02() {
        List<Integer> numbers = Arrays.asList(9, 1, 2, 3, 3, 4, 4, 5, 7);

        // 使用filter过滤偶数
        // select * from table where
        Stream<Integer> evenNumbers = numbers.stream()
                .filter(number -> number % 2 == 0);
        evenNumbers.forEach(System.out::println);
        System.out.println("===");
        // 使用map将数字映射为它们的平方
        // select <expression> from table
        Stream<Integer> squares = numbers.stream()
                .map(number -> number * number);
        squares.forEach(System.out::println);
        System.out.println("===");
        // 使用sorted对数字进行排序
        // select * from table order by <field list>
        Stream<Integer> sortedNumbers = numbers.stream()
                .sorted();
        sortedNumbers.forEach(System.out::println);
        System.out.println("===");
        // 使用distinct去除重复的数字
        // select dictinct * from table
        Stream<Integer> distinctNumbers = numbers.stream()
                .distinct();
        distinctNumbers.forEach(System.out::println);
    }

    @Test
    public void test01() {
        // 从集合创建Stream
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        Stream<Integer> stream1 = numbers.stream();
        stream1.forEach(System.out::println);

        // 从数组创建Stream
        int[] array = {1, 2, 3, 4, 5};
        Stream<Integer> stream2 = Arrays.stream(array).boxed();
        stream2.forEach(System.out::println);

        // 从I/O通道创建Stream
        try (Stream<String> stream3 = Files.lines(Paths.get("d:\\readme.txt"))) {
            stream3.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
