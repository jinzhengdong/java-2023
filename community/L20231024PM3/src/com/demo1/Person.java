package com.demo1;

public class Person {
    private String name;
    private int age;

    public Person(String naem, int age) {
        this.name = naem;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "naem='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
