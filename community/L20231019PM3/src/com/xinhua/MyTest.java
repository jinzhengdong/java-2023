package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Point p1 = new Point();
        Point p2 = new Point(2, 3);

        System.out.println(p1);
        System.out.println(p2);

        System.out.println(p1.getDistance(p2));
        System.out.println(p2.getDistance(p1));
    }
}
