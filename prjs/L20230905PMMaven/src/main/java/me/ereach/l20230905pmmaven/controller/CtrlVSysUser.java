package me.ereach.l20230905pmmaven.controller;

import me.ereach.l20230905pmmaven.mapper.VSysUserMapper;
import me.ereach.l20230905pmmaven.viewmodel.VSysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CtrlVSysUser {
    @Autowired
    private VSysUserMapper vSysUserMapper;

    @PostMapping("/login")
    public VSysUser login(
            @RequestParam("username") String username,
            @RequestParam("password") String password) {
        return vSysUserMapper.login(username, password);
    }
}
