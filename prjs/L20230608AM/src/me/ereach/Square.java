package me.ereach;

public class Square implements Geometry {
    private float s;

    public Square(float s) {
        this.s = s;
    }

    public float getS() {
        return s;
    }

    public void setS(float s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return "Square{" +
                "s=" + s +
                '}';
    }

    @Override
    public double getArea() {
        return s * s;
    }

    @Override
    public double getPerimeter() {
        return s * 4;
    }

    @Override
    public double getVolume() {
        return 0;
    }
}
