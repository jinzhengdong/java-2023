package com.xinhua;

public class MathDemo {
    public static void main(String[] args) {
        System.out.println(Math.abs(-10));  // 10
        System.out.println(Math.abs(-10.23));   // 10.23

        System.out.println(Math.sqrt(2));   // 1.4142135
        System.out.println(Math.sqrt(3));   // 1.732

        System.out.println(Math.pow(2, 3)); // 8
        System.out.println(Math.pow(3, 7)); // 2187

        System.out.println(Math.sin(30));   // -0.98
        System.out.println(Math.sin(Math.PI / 6));  // 0.5

        System.out.println(Math.exp(1));    // 2.71828...

        System.out.println(Math.ceil(2.3)); // 3
        System.out.println(Math.floor(2.9));    // 2

        System.out.println(Math.round(3.4));    // 3
        System.out.println(Math.round(3.7));    // 4

        double pi = 3.14159265;
        // p2 = 3.14
        // p3 = 3.1416

        System.out.println(Math.random());

        // 编写一个猜数游戏，产生1 到 100随机数，猜。 AI


    }
}
