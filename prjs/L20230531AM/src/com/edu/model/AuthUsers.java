package com.edu.model;

public class AuthUsers {
    private Integer id;
    private Integer usercode;
    private String userpass;
    private String email;

    public AuthUsers(Integer id, Integer usercode, String userpass, String email) {
        this.id = id;
        this.usercode = usercode;
        this.userpass = userpass;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUsercode() {
        return usercode;
    }

    public void setUsercode(Integer usercode) {
        this.usercode = usercode;
    }

    public String getUserpass() {
        return userpass;
    }

    public void setUserpass(String userpass) {
        this.userpass = userpass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "AuthUsers{" +
                "id=" + id +
                ", usercode=" + usercode +
                ", userpass='" + userpass + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
