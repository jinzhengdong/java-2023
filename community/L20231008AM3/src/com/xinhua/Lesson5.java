package com.xinhua;

public class Lesson5 {
    public static void main(String[] args) {
        int sum = 0;
        int count = 1000;

        while (count <= 100) {
            sum += count;
            count++;
        }

        System.out.println(sum);

        sum = 0;
        count = 1000;

        do {
            sum += count;
            count++;
        } while (count <= 100);

        System.out.println(sum);
    }
}
