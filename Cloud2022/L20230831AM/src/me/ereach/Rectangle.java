package me.ereach;

public class Rectangle implements IGeometry {
    private float h;
    private float w;

    public Rectangle(float h, float w) {
        this.h = h;
        this.w = w;
    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "h=" + h +
                ", w=" + w +
                '}';
    }

    @Override
    public double getArea() {
        return h * w;
    }

    @Override
    public double getPerimeter() {
        return (h + w) * 2;
    }
}
