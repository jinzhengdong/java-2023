* world (世界，数据库)
  * Tables (表集合)
    * city (城市，表)
      * Columns (字段，列)
        * id (ID, 标识符)
        * name (名称，国家名称)
        * countrycode (国家代码)
        * district (行政区)
        * population (人口)
      * Indexes (索引) 
        * Primary (主键索引)
        * CountryCode (普通索引)
      * ForeignKeys (外键)
        * city_ibfk_1 (外键索引)
      * Triggers (触发器)
    * country (国家，表)
    * countrylanguage (国家语言，表)