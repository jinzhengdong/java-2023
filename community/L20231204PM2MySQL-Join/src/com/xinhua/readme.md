```sql
select a.*, b.name from city as a left join country as b on a.CountryCode = b.Code;

-- 选择 a.代码, a.国家名字, b.国家语言 来自 国家 别名 a 
-- 左加入 国家语言 别名 b 条件 a.代码 = b.国家代码
select a.code, a.name, b.Language from country as a left join countrylanguage as b on a.Code = b.CountryCode;

-- actor 演员
-- actor_id 标识符
-- first_name 名
-- last_name 姓
-- last_update 最后更新日期
-- address 地址
-- address_id 标识符
-- address 地址
-- address2 地址2
-- district 区域
-- city_id 城市标识符
-- postal_code 邮政编码
-- phone 电话
-- location 位置
-- last_update 最后更新日期
-- category 门类，分类
-- city 城市
-- country 国家
-- customer 顾客，客户
-- film 电影
-- film_actor 电影演员
-- film_category 电影门类
-- film_text 电影文本，电影简述
-- inventory 音像店库存
-- language 语言
-- payment 支付
-- rental 租，租赁
-- staff 员工
-- store 商店，音像租赁店
```