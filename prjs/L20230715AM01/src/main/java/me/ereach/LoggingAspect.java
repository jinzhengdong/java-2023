package me.ereach;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
    @Before("execution(* me.ereach.example.UserService.saveUser(..))")
    public void beforeSaveUser() {
        System.out.println("Before saving user...");
    }
}
