package me.ereach;

import org.junit.Test;

public class MyTest {
    @Test
    public void test04() {
        Utils.print(3, 5);

        Utils.genericPrint("age", 12);
        Utils.genericPrint2("Hello", 2.37f);
        String a = Utils.genericTest("Hello", 2.3f, new java.awt.Point(3, 5));
        System.out.println(a);  // Hello
    }

    @Test
    public void test03() {
        System.out.println(Utils.max(7, 15));
        System.out.println(Utils.max(21, 13));
        System.out.println(Utils.max(2.3f, 3.7f));

        System.out.println("===");
        System.out.println(Utils.genericMax(2.3f, 3.7f));

        Utils utils = new Utils("Tools", "获取最大最小值");
        System.out.println(utils.toString());
        System.out.println(utils.getName());
        System.out.println(utils.getDescription());
    }

    @Test
    public void test02() {
        User user1 = new User("Jerry", "male", 12);
        User user2 = new User("Wendy", "female", 12);
        System.out.println(user2.compareTo(user1));

        System.out.println(user1.equals(user2));
    }

    @Test
    public void test01() {
        GenericList<User> genericList = new GenericList<>();
        genericList.add(new User("Jerry", "male", 12));
        genericList.add(new User("Tod", "male", 13));
        genericList.add(new User("Wendy", "female", 12));

        System.out.println(genericList.get(0).compareTo(genericList.get(1)));
        System.out.println(genericList.get(0).compareTo(genericList.get(2)));
    }
}
