package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.function.BiFunction;

public class MyTest {
    @Test
    public void test03() {
        Runnable runnable = () -> System.out.println("Hello");
        runnable.run();

        System.out.println("===");

        BiFunction<Integer, Integer, Integer> add = (a, b) -> a + b;
        System.out.println(add.apply(5, 7));
    }
    @Test
    public void test02() {
        Person[] people = {
                new Person("Tom", "male", 18),
                new Person("Wendy", "female", 12),
                new Person("Jerry", "male", 16),
                new Person("Tod", "male", 11)
        };

        for (Person p : people)
            System.out.println(p);

        System.out.println("===");

        Comparator<Person> comparatorAge = (Person p1, Person p2) -> p1.getAge() - p2.getAge();

        Arrays.sort(people, comparatorAge);

        for (Person p : people)
            System.out.println(p);

        System.out.println("===");

        Comparator<Person> comparatorNameLength = (Person p1, Person p2) -> p1.getName().length() - p2.getName().length();

        Arrays.sort(people, comparatorNameLength);

        for (Person p : people)
            System.out.println(p);

    }

    @Test
    public void test01() {
        String[] fruits = {"Apple", "Orange", "Banana", "Pear"};

        Comparator<String> comparator = (String f1, String f2) -> f1.length() - f2.length();

        for(String item : fruits)
            System.out.println(item);

        System.out.println("===");

        Arrays.sort(fruits, comparator);

        for(String item : fruits)
            System.out.println(item);

        System.out.println("===");

        Arrays.sort(fruits);

        for(String item : fruits)
            System.out.println(item);
    }
}
