package com.xinhua;

public class Main {
    public static void main(String[] args) {
        int a = 10;
        int b = 12;

        System.out.println(a == b);     // flase
        System.out.println(a != b);     // true
        System.out.println(a < b);      // true
        System.out.println(a > b);      // false
        System.out.println(a <= b);     // true
        System.out.println(a >= b);     // false
        System.out.println("===");
        System.out.println(true && true);   // true
        System.out.println(true && false);  // false
        System.out.println(false && true);  // talse
        System.out.println(false && false); // false
        System.out.println(false && false && true); // false
        System.out.println("===");
        System.out.println(true || true);   // true
        System.out.println(true || false);  // true
        System.out.println(false || true);  // true
        System.out.println(false || false); // false
        System.out.println("===");
        System.out.println(! true);
        System.out.println(! false);
    }
}
