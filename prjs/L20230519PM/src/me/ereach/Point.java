package me.ereach;

public class Point extends Object {
    private float x;
    private float y;

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    // get 获取
    // distance 距离
    // getDistance 获取两点之间的距离
    public double getDistance(Point p) {
        float h = this.x - p.x;
        float v = this.y - p.y;
        return Math.sqrt(Math.pow(h, 2) + Math.pow(v, 2));
    }
}
