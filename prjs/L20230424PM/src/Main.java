public class Main {
    public static void main(String[] args) {
        // 绝对值
        System.out.println(Math.abs(-10));  // 10
        System.out.println(Math.abs(10));   // 10
        System.out.println(Math.abs(0));    // 0

        // 平方根
        System.out.println(Math.sqrt(2));   // 1.4142135
        System.out.println(1.4142135D * 1.4142135D);
        System.out.println(Math.sqrt(3));   // 1.732

        System.out.println(Math.sqrt(6));
        System.out.println(Math.sqrt(2) * Math.sqrt(3));

        // 指数运算 pow => power
        System.out.println(Math.pow(2, 3)); // 2 * 2 * 2 = 8
        System.out.println(Math.pow(2, 4)); // 2 * 2 * 2 * 2 = 16
        System.out.println(Math.pow(2.71828D, 3));

        // exp 自然数的指数运算
        // e^x
        System.out.println(Math.exp(1));    // 2.71828^1
        System.out.println(Math.exp(2));    // 2.71828^2

        // ln(x) = a
        // 2.71828^a = x
        System.out.println(Math.log(2.71828));
        System.out.println(Math.log10(2));  // 0.3010

        System.out.println(Math.ceil(2.1));     // 3
        System.out.println(Math.floor(2.9));    // 2

        System.out.println(Math.round(2.4));
        System.out.println(Math.round(2.6));

        System.out.println(Math.random());
    }
}