package com.xinhua;

public class Circle2D extends Poin2D {
    private float r;

    public Circle2D(float x, float y, float r) {
        super(x, y);
        this.r = r;
    }

    public Circle2D(float r) {
        super(0, 0);
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public float getX() {
        return super.getX();
    }

    public void setX(float x) {
        super.setX(x);
    }

    public float getY() {
        return super.getY();
    }

    public void setY(float y) {
        super.setY(y);
    }

    @Override
    public String toString() {
        return "Circle2D{" +
                "o" + super.toString() + ", " +
                "r=" + r +
                '}';
    }

    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }

}
