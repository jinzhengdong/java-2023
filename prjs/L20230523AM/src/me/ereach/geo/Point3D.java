package me.ereach.geo;

public class Point3D {
    private double x;
    private double y;
    private double z;

    public Point3D() {
        x = 0;
        y = 0;
        z = 0;
    }

    // this 是类似于指向Point3D
    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    public static double getDistanceStatic(Point3D p1, Point3D p2) {
        double h = p1.y - p2.y;
        double w = p1.x - p2.x;
        double z = p1.z - p2.z;

        return Math.sqrt(Math.pow(h, 2) + Math.pow(w, 2) + Math.pow(z, 2));
    }
}
