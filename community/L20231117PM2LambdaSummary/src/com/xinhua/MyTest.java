package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MyTest {
    @Test
    public void test01() {
        List<Teacher> teachers = Arrays.asList(
                new Teacher("Mao WT", "female", "政治"),
                new Teacher("Liu SC", "male", "C#"),
                new Teacher("Li YL", "male", "MySQL"),
                new Teacher("Jin ZD", "male", "Java"),
                new Teacher("Sen XM", "female", "Web Page"),
                new Teacher("Fu YR", "female", "Network"),
                new Teacher("Ren SL", "male", "AR PR"),
                new Teacher("Wang XQ", "female", "Office")
        );

        List<Student> students = Arrays.asList(
                new Student("Zhang HS", "male", "2021 Cloud"),
                new Student("Wang Dr", "male", "2021 Cloud"),
                new Student("Hu L", "male", "2021 Cloud"),
                new Student("Du JY", "male", "2021 Cloud"),
                new Student("Zi YH", "male", "2021 Cloud"),
                new Student("Ma ZX", "male", "2021 Cloud"),
                new Student("Li XH", "male", "2021 Cloud"),
                new Student("Bai YW", "male", "2022 Cloud"),
                new Student("Guo J", "male", "2022 Cloud"),
                new Student("Zhou SY", "male", "2022 Cloud"),
                new Student("Chen RL", "male", "2022 Cloud"),
                new Student("Yu Y", "male", "2022 Cloud"),
                new Student("Jiang FY", "male", "2022 Cloud"),
                new Student("Wan HC", "male", "2022 Cloud"),
                new Student("Lei ZY", "male", "2022 Cloud"),
                new Student("Wang XG", "male", "2022 Cloud"),
                new Student("Wu Y", "male", "2022 Cloud")
        );

        // 过来
        students.stream().filter(x -> x.getClassName() == "2022 Cloud").forEach(System.out::println);
        System.out.println("===");
        students.stream().filter(x -> x.getClassName() == "2021 Cloud").forEach(System.out::println);

        // 总人数
        System.out.println(students.stream().count());

        // 2022 Cloud 人数
        System.out.println(students.stream().filter(x -> x.getClassName() == "2022 Cloud").count());

        // 2021 Cloud 人数
        System.out.println(students.stream().filter(x -> x.getClassName() == "2021 Cloud").count());

        // 有多少老师
        System.out.println(teachers.stream().count());

        // 有几位女老师
        System.out.println(teachers.stream().filter(x -> x.getGender() == "female").count());

        // 有几位男老师
        System.out.println(teachers.stream().filter(x -> x.getGender() == "male").count());


    }
}
