import javax.swing.plaf.PanelUI;

public class Main02 {
    public static Byte b;
    public static Short s;
    public static Integer i = 100;
    public static Long l;
    public static Float f;
    public static Double d;
    public static String str;
    public static Boolean bl;
    public static void main(String[] args) {
        System.out.println(b);
        System.out.println(s);
        System.out.println(i);
        System.out.println(i / 3);
        System.out.println(i.doubleValue() / 3.0);
        System.out.println(l);
        System.out.println(f);
        System.out.println(d);
        System.out.println(str);
        System.out.println(bl);
    }
}
