# 第一章 关于这门课你需要知道的

## 第一讲

### 简介

* 课程包括两个主题：认证，解决你是谁的问题；授权，解决你能做什么的问题。
* 普遍性：几乎所有的企业级应用场景都有权限需求。
* 必要性：认证和授权作为整体构架的基础部分，它直接决定产品的安全性和稳定性。
* 可持续性：针对系统的攻击随技术不断进步变化，这促使安全技术和策略也不断进化。
* 面试必考：求职面试中必考一点。
* 为什么是Spring Security?
  * 首先，它是最流行的安全框架；
  * 其次，它是Spring全家桶的重要成员；
  * 丰富全面的安全相关特性，业务隔离度好，是事实标准；
* 学习路径是从单体项目到微服务的学习路径，如下图：

<img src="./images/001.png" width="673px">

### 课程章节

* 第一章
  * 课程介绍
  * 环境配置
* 第二章
  * 认证和授权
  * 过滤器和过滤器链
  * HTTP请求的结构
  * HTTP Basic Auth
  * 安全配置
  * 定制登录页
  * csrf等的设置
  * 登录成功及失败的处理
  * 自定义Filter
* 第三章
  * 密码进化史
  * 密码编码器
  * 验证注解和自定义验证注解
  * 密码的验证和自定义注解和验证器
  * 验证消息的国际化
  * 异常处理
  * 多个安全配置共存
* 第四章
  * 核心组件 SecurityContext
  * UserDetails、UserDetailsService
  * 定制化数据库
  * UserDetails 和 GrantedAuthority
  * 环境和环境变量
  * 自动化测试
* 第五章
  * 认证流程和源码解析
  * LDAP配置和多认证源
  * JWT的概念和创建以及解析
  * 访问令牌和刷新令牌
  * 创建JwtFilter
  * 实现登录接口和刷新令牌接口
  * 完成注册接口
* 第六章
  * 多因子认证和TOTP
  * 云服务和多因子认证流程
  * 短信发送服务
  * Email发送服务
  * 多因子认证整体逻辑
  * 使用Redis缓存
  * 选择发送方式和验证
  * 前后端多工程配置
  * CORS跨域处理
* 第七章  
  * 授权机制
  * 角色和权限的概念
  * Spring Security授权
  * 使用注解完成简单角色授权
  * 在注解中使用权限表达式
  * 角色分级
  * 划分权限
  * 自定义权限表达式
  * 高级权限表达式的用法
* 第八章
  * Oauth 2.0 简介
  * 常用的授权模式和流程
  * 授权和资源服务的概念
  * 实授权服务器和资源服务器
  * 资源服务器验证JWT Claims
  * 第三方登录
  * GitHub登录，微博登录
  * 单点登录SSO
  * 单页应用如何在OAuth2模式下工作
* 第九章
  * 和Keycloak的集成
  * 课程总结

### 学习效果

* 独立设计一个安全的单体应用
* 为微服务开发UAA或集成第三方
* 前后端配合时常见的问题处理

### 技术栈

* Spring Security 5.x
* Java 11 & Spring Boot 2.x

### 先决条件

* Java Programming Language
* Spring & Spring Boot
* Java 函数式编程 Lambda
* MySQL

## 第二讲

### 课程更新

* Spring Boot from 2.3 to 2.7，配置文件的修改
* Spring Security from 5.2 to 5.7，使用Bean简化原来的继承类实现抽象方法
* Spring Authorization Server 0.4-M1

## 第三讲

### 前端环境搭建

* 安装Node.js（v12或14）熟练使用npm/cnpm（v6.x）
* Vue.js及Vue CLI，前端工程使用vue cli创建

### 后端环境的搭建

* Java SDK (Open JDK 11)
* Maven
* Intellij IDEA，并安装以下插件：
  * envfile，用于加载环境变量
  * Lombok，基于注解生成实体类的相关代码
  * HTTP Client，可以快速咋文本编辑器中写request
* IDEA的热启动配置如下图：

<img src="./images/006.png" width="400px">

* 完成上述步骤后如下图双击shift:

<img src="./images/007.png" width="300px">

* 弹出的对话框中输入“registry”，并点击“actions”，如下图：

<img src="./images/008.png" width="400px">

* 点击上图“registry...”进入下图所示界面：

<img src="./images/009.png" width="600px">

* 上图勾选“compiler.automake.allow.when.app.running”选项，然后关闭窗口。
* 针对VSCode推荐安装以下插件：
  * Debugger for Chrome
  * REST Client
  * Vue VS Code Extension Pack
  * Java Extension Pack
* Chrome的开发工具如下：

<img src="./images/010.png" width="800px">

* 针对Chrome还可安装“Vue DevTools”插件

## 第四讲

### 工程结构

项目文件存放在```/res/prjs/spring-security/```下，如下图：

<img src="./images/011.png" width="200px">

上图中，自第二章到第五章的目录只包含一个工程；自第六章开始，目录中有两个工程（如下图，其中，uaa-ui目录是前端项目）：

<img src="./images/012.png" width="200px">

自第八章开始，项目中有更多的工程用于演示OAuth2.0在多服务器角色环境下的工程结构，如下图：

<img src="./images/013.png" width="120px">

第九章演示Keycloak集成的服务器单点登录，还包括docker形式启动的部署，建议安装并熟悉docker的使用，如下图：

<img src="./images/014.png" width="120px">

另外，对于一些私有信息的设置我们通过环境变量来实现，例如：您把下图所示文件```.env.sample```更名为```.env```:

<img src="./images/015.png" width="200px">

并完成下面设置：

```script
SMTP_HOST=smtp.sendgrid.net
SMTP_PORT=587
SMTP_USERNAME=apikey
SMTP_PASSWORD=你的 SendGrid API KEY
EMAIL_API_KEY=你的 SendGrid API KEY
SMS_PROVIDER_NAME=ali
SMS_API_URL=dysmsapi.aliyuncs.com
ALI_API_KEY=你的阿里云 API KEY
ALI_API_SECRET=你的阿里云 API SECRET
LEAN_CLOUD_APP_ID=你的 LeanCloud APP ID
LEAN_CLOUD_APP_KEY=你的 LeanCloud APP KEY
JWT_KEY=2tQq2AeomJX1sCPBON5jRdkpET5DNN9Hpzjv888xTBOP83LS2qbCoq0vuYTwLTIqXL3GUOvEyUrda7B1zKe+Hw==
JWT_REFRESH_KEY=AgbMd/alAL6gUAPE55xh9AgcH+Aq3fyy8X0TKy02L49O7mmLAzgF2D7TsEKfB/VtePujY6sQ4SCzC/sovDCFpg==
```

针对多工程项目，在项目根目录下的```pom.xml```，依赖版本号都记录在这里，它是标准的多模块的配置方式。

针对单体工程项目，IDEA选择Open，然后选择```pom.xml```并作为项目打开可以打开该项目。完成项目的装载后，我们可在java项目下找到包含```main```函数的启动入口，运行该函数可启动项目。如果启动失败可能是因为环境配置的问题导致！针对这种情况可进行如下操作：

* 点击“Edit Configurations...”，如下图：

<img src="./images/016.png" width="240px">

* 如下选中```EnvFile```标签：

<img src="./images/017.png" width="640px">

* 上界面滑动到底部添加环境文件```.env```
* 针对测试，我们可以单独测试各用例，也可整体测试。

# 第二章 初识 Spring Security

## 第五讲

### 认证和授权的概念

本章的内容包括：

* 认证和授权的概念
  * 什么是认证（Authentication），回答我是谁的问题
  * 什么式授权（Authorization），回答我能做什么的问题
* Filter和FilterChain
  * Spring Security实现认证和授权的底层机制
* Http
  * 熟悉Http的请求/响应结构
  * Filter和客户端交互（获取数据，返回数据）是通过请求/响应中的字段完成的
* 实战小项目
  * 新建工程
  * 添加依赖
  * 安全配置
  * 定制化登录页面
  * CSRF攻击和保护
  * 登录成功和失败后的处理

### 实战项目（L005P01）

* 首先我们创建一个spring boot项目
  * JDK 11
  * Spring Security 5.7
  * Spring Boot 2.7
* 创建一个不受保护的api（rest controller）
* 添加spring security依赖，此时它是一个最小化的安全配置，提供内建的资源保护
  * 提供一个默认的登录页
  * 在安全页中加入授权的匹配，如果授权不匹配则不能访问该服务

注意：

* 从```gitee.com```上克隆该项目，并在```/prjs/L005P01/L005P01```下找到该项目。
* 项目完成创建后，在添加```Spring Security```前，完成下面微服务代码：

```java
package me.ereach.l005p01.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserResource {
    @GetMapping("/greeting")
    public String greeting() {
        return "Hello, World!";
    }
}
```

* 完成上面代码后，启动项目并测试服务地址： <http://localhost:8080/api/greeting>
* 上面地址任何人都可访问，不是我们希望看到的情况，因此我们添加下面依赖到```pom.xml```，而且不需要指定版本号：

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

* 完成上面步骤后，请刷新Maven，并且新的```pom.xml```如下所示：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.7.8</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>com.example</groupId>
    <artifactId>L005P01</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>L005P01</name>
    <description>L005P01</description>
    <properties>
        <java.version>11</java.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>RELEASE</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>
```

* 重新运行项目并继续访问原先的greeting服务，我们看到登录页面如下，从运行窗口找到相关密码：

<img src="./images/019.png" width="320px">

* 完成登录后我们的api再次可以访问，这就是我们的认证过程。
* 接下来我们将完成一个最简单的授权过程，添加一个package叫config；在config下创建一个类叫"SecurityConfig"，并添加如下代码：

```java
package me.ereach.l005p01.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests(req -> req.mvcMatchers("/api/greeting").hasRole("ADMIN"));
    }
}
```

* 上面代码要求```/api/greeting```的操作人员具备“ADMIN”角色，因此，再次访问该api时将报403错误（没有权限），如下：

<img src="./images/020.png" width="520px">

* 继续修改“SecurityConfig”为如下代码，并启动服务：

```java
package me.ereach.l005p01.config;

import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin(Customizer.withDefaults())
                .authorizeRequests(req -> req.mvcMatchers("/api/greeting").authenticated());
    }
}
```

* 再次登录后可正常访问该“/api/greeting”服务，原因是上面代码指示只需登录就可访问该服务（authenticated）

## 第六讲

### 过滤器和过滤器链

任何Spring Web应用本质上只是一个servlet。Security Filter在HTTP请求到达你的Controller之前过滤每一个传人的HTTP请求。如下图：

<img src="./images/021.png" width="800px">

上图中，未认证就是401，而未授权则是403。

一般来说，过滤器的操作包括以下几个步骤：

1. 首先，过滤器需要从请求中提取用户名/密码。它可以通过HTTP头、表单字段、Cookie等处获得。
2. 然后，过滤器要对用户名和密码进行组合验证，比如数据库。
3. 在验证成功后，过滤器需要检查用户是否被授权访问请求的URI。
4. 如果请求通过了这些检查，那么过滤器就可以让请求通过你的“DispatcherServlet”后重新定向到“@Controllers”或者“@RestController”。

参看下面代码片段：

<img src="./images/022.png" width="800px">

在Spring框架中，认证及授权是通过一系列的小过滤器形成一个过滤器链的形式，如下图：

<img src="./images/023.png" width="800px">

现在返回项目，并按下面提示完成相关步骤：

* 返回```L005P01```项目，并修改```application.properties```为如下内容：

```
logging.level.org.springframework.security=DEBUG
server.port=8080
```

* 启动服务并访问：<http://localhost:8080/api/greeting>
* 从控制台我们可以看到下面输出信息：

<img src="./images/024.png" width="500px">

* 我们看到，在未完成用户登录前操作被导航到登录页面。
* 在“application.properties”中，我们页可以定义日志的输出格式，如下：

```
logging.pattern.console='%clr(%d{E HH:mm:ss.SSS}){blue} %clr(%-5p) %clr(${PID}){faint} %clr(---){faint}%clr([%8.15t]){cyan} %clr(%-40.40logger{0}){blue} %clr(:){red} %clr(%m){faint}%n'
logging.level.org.springframework.security=DEBUG
server.port=8080
```

* 完成上面更新后，启动应用我们就可以看到如下不同的输出结果，其中包括```DefaultSecurityFilterChain```:

<img src="./images/025.png" width="600px">

* 返回到集成环境中，打开```SecurityConfig.java```并移除其中内容如下：

```java
package me.ereach.l005p01.config;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
}
```

* 重新启动服务并访问api我们可看到错误信息，并转到登录界面，完成登录后api被成功执行。

### 常见的内置过滤器

<img src="./images/026.png" width="800px">

## 第七讲

### HTTP请求结构

<img src="./images/027.png" width="800px">

如果vscode安装了rest client插件，则可在vscode中创建一个`rest.http`的请求文件，如下：

```rest
###
GET http://localhost:8080/api/greeting HTTP/1.1
```