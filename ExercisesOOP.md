# Java OOP编程练习

### class Point

* 创建类Point
  * 私有属性：
    * float x
    * float y
  * 构造方法：
    * public Point(float x, float y)
  * Getters & Setters
  * 重写方法：
    * toString()
  * 公共实例方法：
    * public double getDistance(Point other)

### class Circle

* 创建类Circle并组合Point类
  * 私有属性：
    * float r 半径
    * Point o 圆心
  * 构造方法：
    * public Circle(float r, Point o)
  * Getters & Setters
  * 重写方法:
    * toString()
  * 公共实例方法：
    * public double getArea()
    * public double getPerimeter()
* 创建CircleExt继承Point
  * 私有属性：
    * float r 半径
  * 构造方法：
    * public Circle(float x, float y, float r)
  * Getters & Setters
  * 重写方法：
    * toString()
  * 公共实例方法：
    * public double getArea()
    * public double getPerimeter()

### class Triangle

* 创建Triangle并组合Point
  * 私有属性:
    * Point a
    * Point b
    * Point c
  * 构造方法:
    * public Triangle(Point a, Point b, Point c)
  * Getters & Setters
  * 重写方法：
    * toString()
  * 公共实例方法：
    * public double getArea()
    * public double getPerimeter()

### abstract

* 创建抽象类Shape
  * 抽象方法：
    * abstract double getArea()
    * abstract double getPerimeter()
* 创建矩形类Rect并继承(extends)抽象类Shape
  * 私有属性：
    * float w
    * float h
  * 构造函数：
    * public Rect(float w, float h)
  * Getters & Setters
  * 重写toString()方法
  * 实现抽象类Shape中定义的抽象方法

### final

继承是面向对象编程的重要特性之一，以final修饰的类无法被继承；同样，以final修饰的方法不能被重写，尝试以下实验：

* 重复abstract一节的作业
* 创建Square(正方形)类并继承Rect(矩形)
  * 构造函数
    * public Square(float s)
  * 方法重写
    * getArea()
    * getPerimeter()
* 修改父类Rect
  * 下面两个方法分别加上修饰符final，完成后查看Square的相关错误信息
    * getArea()
    * getPerimeter
  * 修改Rect类加上修饰符final，完成后查看Square的相关错误信息

### interface

学习接口编程方法，对比abstract有何不同(抽象使用继承关键字extends，而接口使用implements)：

* 创建Shape接口
  * 接口方法
    * double getArea()
    * double getPerimeter()
* 创建类Rect并实现Shape接口
  * 私有属性
    * float w
    * float h
  * 构造方法
    * public Rect(float w, float h)
  * 实现接口方法(implements)
* 创建类Circle并实现Shape接口
  * 私有属性
    * float r
  * 构造方法
    * public Circle(float r)
  * 实现接口方法(implements)

### 解耦

通过接口实现对象的依赖注入从而达到解耦的目的。

#### 点

基于前面章节传统的点类Point，以解耦的方式实现getDistance方法的依赖注入：

* 创建接口IGeometry并添加距离计算方法声明如下：

```java
package me.ereach.di;

interface IGeometry {
    double getDistance(Point p1, Point p2);
}
```

* 添加IGeometry的实现类为如下代码：

```java
package me.ereach.di;

public class Geometry implements IGeometry {
    @Override
    public double getDistance(Point p1, Point p2) {
        float w = p1.getX() - p2.getX();
        float h = p1.getY() - p2.getY();
        return Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2));
    }
}
```

* 创建点Point类为如下代码：

```java
package me.ereach.di;

public class Point {
    private float x;
    private float y;
    IGeometry iGeometry;

    public Point(float x, float y) {
        this.x = x;
        this.y = y;
    }

    // 这里为构造注入点
    public Point(float x, float y, IGeometry iGeometry) {
        this.x = x;
        this.y = y;
        this.iGeometry = iGeometry;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public IGeometry getiGeometry() {
        return iGeometry;
    }

    // 这里为属性注入点
    public void setiGeometry(IGeometry iGeometry) {
        this.iGeometry = iGeometry;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    // 私有化减少细节
    private double getDistance(Point p1, Point p2) {
        return iGeometry.getDistance(p1, p2);
    }

    public double getDistance(Point other) {
        return this.getDistance(this, other);
    }
}
```

* 创建测试类MyTest:

```java
package me.ereach.di;

import org.junit.Test;

public class MyTest {
    @Test
    public void testPoint() {
        Point p1 = new Point(2, 3);
        Point p2 = new Point(5, 7);
        Geometry geometry = new Geometry();

        // 注入geometry实例
        p1.setiGeometry(geometry);
        System.out.println(p1.getDistance(p2));
    }
}
```

#### Employee

* 创建接口ITax，个税类
  * float getTax(float totalSalary) 计算个税
* 创建ITax实现类Tax
  * 个税计算标准
    * 工资总额高于5000者超额部分上税10%
* 创建类Employee
  * 属性
    * String name
    * float baseSalary
    * float hourlyRate
    * float extraHours
    * ITax iTax
  * 构造函数
    * 要求两个构造函数版本其中一个实现个税实例的注入
  * Getters & Setters
    * 要求包含属性个税实例注入
  * toString()
  * 方法
    * float getTotalSalary() 获取总工资
    * float getTax() 完成注入个税计算
    * float getActualSalary() 税后工资计算
