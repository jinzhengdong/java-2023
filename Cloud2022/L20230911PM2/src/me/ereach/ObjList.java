package me.ereach;

import java.util.Arrays;

public class ObjList {
    private Object[] list = new Object[10];
    private int count;

    public void add(Object item) {
        list[count++] = item;
    }

    public Object getList(int index) {
        return list[index];
    }

    @Override
    public String toString() {
        return "ObjList{" +
                "list=" + Arrays.toString(list) +
                ", count=" + count +
                '}';
    }
}
