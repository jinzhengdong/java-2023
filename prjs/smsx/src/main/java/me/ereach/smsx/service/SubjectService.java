package me.ereach.smsx.service;

import me.ereach.smsx.mapper.SubjectMapper;
import me.ereach.smsx.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {
    @Autowired
    private SubjectMapper subjectMapper;

    @Bean
    public List<Subject> getSubjectList() {
        return subjectMapper.selectList(null);
    }
}
