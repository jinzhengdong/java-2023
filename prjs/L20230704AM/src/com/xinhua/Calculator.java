package com.xinhua;

@FunctionalInterface
public interface Calculator {
    int calculate(int a, int b, int c);
}
