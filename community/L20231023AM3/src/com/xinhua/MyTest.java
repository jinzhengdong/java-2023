package com.xinhua;

import org.junit.Test;

public class MyTest {
    @Test
    public void test01() {
        Student s1 = new Student("Xinhua");
        System.out.println(s1);
        System.out.println(s1.sayHello());

        Student s2 = new Student("Tom", 18, "Xinhua");
        System.out.println(s2);
        System.out.println(s2.sayHello());
    }
}
