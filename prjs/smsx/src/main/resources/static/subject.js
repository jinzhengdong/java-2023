layui.use(['table', 'laypage', 'layer'], function () {
    let table = layui.table

    let tableCols = [
        { field: 'id', title: 'ID', width: 80, sort: true, align: 'center', hide: true },
        { field: 'subjectName', title: 'Subject' },
        { fixed: 'right', title: 'Operation', toolbar: '#colbar', width: 120, align: 'center' },
    ]

    // render ��Ⱦ
    // elem (element Ԫ��)
    table.render({
        elem: "#subjectlist",
        url: 'http://localhost:8081' + "/api/subject",
        toolbar: "#titlebar",
        cols: [tableCols],
        page: true,
        even: true,
        limits: [10, 13, 16, 19],
        limit: 13,
        parseData: function (res) {
            debugger
            let rst

            if (this.page.curr) {
                rst = res.slice(this.limit * (this.page.curr - 1), this.limit * this.page.curr)
            }
            else {
                rst = res.slice(0, this.limit)
            }

            return {
                "code": 0,
                "data": rst,
                "count": res.length,
                "msg": res.message
            }
        },
    })

    table.on('tool(subjectlist)', function (obj) {
        switch (obj.event) {
            case 'editSubject':
                layer.open({
                    type: 2
                    , id: 'layerDemo' + 1
                    , content: `./subjectupdate.html?id=${obj.data.id}&subjectname=${obj.data.subjectName}`
                    , btnAlign: 'c'
                    , shadeClose: true
                    , shade: 0.4
                    , area: ['580px', '300px']
                    , title: `Update`
                    , end: function (layero) {
                        $.getJSON(`http://localhost:8081/api/subject/${obj.data.id}`, function (rst) {
                            obj.update({
                                id: rst.id,
                                subjectName: rst.subjectName
                            })
                        })
                    }
                })
                break
            case 'delSubject':
                layer.confirm('Are you sure?', { icon: '&#xe702', title: 'Delete' }, function (index) {

                    let formData = new FormData();

                    formData.append("id", obj.data.id);

                    $.ajax({
                        url: 'http://localhost:8081/api/subject',
                        type: 'DELETE',
                        async: false,
                        cache: false,
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: (res) => {
                            debugger
                            obj.del()
                        },
                        error: (res) => {
                            debugger
                            alert("error: " + JSON.stringify(res))
                        }
                    })

                    layer.close(index)
                })
                break
        }
    })

    table.on('toolbar(subjectlist)', function (obj) {
        switch (obj.event) {
            case 'addCmd':
                layer.open({
                    type: 2
                    , id: 'layerDemo' + 1
                    , content: './subjectins.html'
                    , btnAlign: 'c'
                    , shadeClose: true
                    , shade: 0.4
                    , area: ['580px', '260px']
                    , title: 'Add Subject'
                    , yes: function () {
                        layer.closeAll();
                    }
                })
                break
            case 'qryCmd':
                let qrySubject = $("#qrySubject").val()
                debugger

                $.getJSON('http://localhost:8081/api/subject', null, function (data) {
                    debugger
                    let fdata = data.filter((item) => {
                        return (
                            item.subjectName != null && item.subjectName.indexOf(qrySubject) >= 0
                        )
                    })

                    table.render({
                        elem: '#subjectlist',
                        toolbar: '#titlebar',
                        cols: [tableCols],
                        page: true,
                        even: true,
                        curr: 1,
                        limit: 13,
                        limits: [10, 13, 16, 19],
                        data: fdata
                    })

                    $("#qrySubject").val(qrySubject)
                })
                break
        }
    })

})