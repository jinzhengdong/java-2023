package com.xinhua.ab;

public abstract class Geometory {
    protected abstract double getArea();
    protected abstract double getPerimeter();
}
