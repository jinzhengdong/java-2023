package com.xinhua;

public class Student {
    private Integer id;
    private String name;
    private String gender;
    private String birthDate;
    private Integer classId;
    private Integer employeeId;
    private Integer enterYear;
    private String mobilePhone;

    public Student(Integer id, String name, String gender, String birthDate, Integer classId, Integer employeeId, Integer enterYear, String mobilePhone) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
        this.classId = classId;
        this.employeeId = employeeId;
        this.enterYear = enterYear;
        this.mobilePhone = mobilePhone;
    }

    public Student() {
        this.id = 0;
        this.name = "";
        this.gender = "";
        this.birthDate = "";
        this.classId = 0;
        this.employeeId = 0;
        this.enterYear = 0;
        this.mobilePhone = "";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getEnterYear() {
        return enterYear;
    }

    public void setEnterYear(Integer enterYear) {
        this.enterYear = enterYear;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", classId=" + classId +
                ", employeeId=" + employeeId +
                ", enterYear=" + enterYear +
                ", mobilePhone='" + mobilePhone + '\'' +
                '}';
    }
}
