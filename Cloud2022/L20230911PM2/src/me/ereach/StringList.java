package me.ereach;

import java.util.Arrays;

public class StringList {
    private String[] list = new String[10];
    private int count;

    public void add(String item) {
        list[count++] = item;
    }

    public String get(int index) {
        return list[index];
    }

    @Override
    public String toString() {
        return "StringList{" +
                "list=" + Arrays.toString(list) +
                ", count=" + count +
                '}';
    }
}
