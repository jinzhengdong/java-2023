package com.xinhua;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MyTest {
    List<ClassRoom> classRoomList = Arrays.asList(
            new ClassRoom(1, "理实05", "教学主楼2楼", 22),
            new ClassRoom(2, "理实06", "教学主楼2楼", 22),
            new ClassRoom(3, "理实07", "教学主楼2楼", null),
            new ClassRoom(4, "理实08", "教学主楼2楼", null),
            new ClassRoom(5, "理实09", "教学主楼2楼", null),
            new ClassRoom(6, "理实10", "教学主楼2楼", null),
            new ClassRoom(7, "理实11", "教学主楼3楼", null),
            new ClassRoom(8, "理实12", "教学主楼3楼", null)
    );
    List<Subject> subjectList = Arrays.asList(
            new Subject(1, "Java", 1.4f, "语言基础、面向对象、高级主题"),
            new Subject(2, "Python", 1.4f, "语言基础、面向对象、数据处理"),
            new Subject(3, "MySQL", 1.2f, "DDL、DQL、DML、Proc、Func、Trigger")
    );

    @Test
    public void test02() {
        System.out.println("Total class room: " + classRoomList.stream().count());
        System.out.println("The 3rd floor total: " + classRoomList.stream().filter(x -> x.getLocation().equals("教学主楼3楼")).count());
        classRoomList.stream().filter(x -> x.getLocation().equals("教学主楼2楼")).forEach(System.out::println);
    }

    @Test
    public void test01() {
        System.out.println("Total subjects: " + subjectList.stream().count());
        subjectList.stream().forEach(System.out::println);
        System.out.println("Average factor: " + subjectList.stream().mapToDouble(Subject::getFactor).average());
    }
}
