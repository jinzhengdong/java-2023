package me.ereach.geo;

import java.awt.*;

public class Point2D {
    private double x;
    private double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

//    @Override
//    public boolean equals(Object obj) {
//        Point2D pt = (Point2D) obj;
//        return this.x == pt.x && this.y == pt.y;
//    }

    // instance 实例
    // instance of, 什么 是 什么的 实例吗？
    // obj 是 Point2D 这个类的实例吗？
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point2D) {
            Point2D pt = (Point2D) obj;
            return (x == pt.x) && (y == pt.y);
        }
        return super.equals(obj);
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj instanceof Point2D) {
//            Point2D pt = (Point2D) obj;
//            return (x == pt.x) && (y == pt.y);
//        }
//        return false;
//    }

}
