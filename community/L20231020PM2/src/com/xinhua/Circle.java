package com.xinhua;

public class Circle {
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    public Circle() {
        r = 0;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "r=" + r +
                '}';
    }

    protected double getArea() {
        return Math.PI * r * r;
    }

    public double getPerimeter() {
        return Math.PI * r * 2;
    }

    String getClassName() {
        return "Circle";
    }
}
