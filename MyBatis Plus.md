# MyBatis Plus

## 什么是MyBatis

MyBatis 是一个优秀的持久层框架，它支持定制化 SQL、存储过程以及高级映射。MyBatis 避免了几乎所有的 JDBC 代码和手动设置参数以及获取结果集的过程。MyBatis 可以使用简单的 XML 或注解进行配置，以映射原生信息，将接口和 Java 的 POJOs（Plain Old Java Objects，普通的 Java 对象）映射为数据库中的记录。

以下是MyBatis的一些主要特性：

1. **自定义SQL查询:** MyBatis 最大的优点之一就是它允许你编写自定义的 SQL 语句。你可以完全控制 SQL 语句，以满足你的具体需求。

2. **映射:** MyBatis 提供了强大的映射功能，包括映射查询结果到 Java 对象和 Java 对象到数据库表。

3. **事务管理:** MyBatis 对 JDBC 的事务进行了抽象，你可以选择使用 MyBatis 的事务管理，也可以通过 Spring 等其他框架进行事务管理。

4. **存储过程和函数:** MyBatis 提供了很好的对存储过程和函数的支持。

5. **连接池:** MyBatis 内置了一个很简单的连接池，但也可以集成其他的连接池。

6. **延迟加载:** MyBatis 支持延迟加载，你可以配置哪些属性需要进行延迟加载。

MyBatis 是一个灵活、强大且易于使用的持久层框架，它可以大大简化数据库操作的代码，提高开发效率。

## 什么是MyBatis Plus如何安装

MyBatis Plus（简称MP）是一个MyBatis的增强工具，在MyBatis的基础上只做增强，不做改变，为简化开发、提高效率而生。

以下是MyBatis Plus的一些主要特性和优势：

1. **无侵入：** MyBatis Plus只是在MyBatis的基础上进行增强，没有改变MyBatis的原有功能，你可以想用就用，不想用就抛弃。

2. **便捷的CRUD操作：** MyBatis Plus提供了一些通用Mapper接口和实现，大大简化了单表的增删改查操作，无需编写SQL。

3. **条件构造器：** 提供强大的条件构造器Wrapper，可以方便地编写各种复杂的查询条件。

4. **多种主键策略：** 支持多种数据库主键生成策略，并默认自动处理。

5. **分页插件：** 提供简单的分页功能，无需关心具体的分页SQL。

6. **性能分析插件：** MyBatis Plus提供性能分析插件，可以用来输出SqlStatement，找出慢查询。

7. **全局配置：** 允许全局配置通用功能，比如主键生成策略、表名前缀等，一次配置，全局使用。

8. **代码生成器：** 提供代码生成器，可以快速生成Entity、Mapper、Mapper XML、Service、Controller等代码，提高开发效率。

9. **多租户SQL解析器：** 对原查询进行改写，实现对数据进行隔离，保证数据安全。

10. **动态表名注入：** 实现对全局动态表的实时更换。

MyBatis Plus的优势在于它大大简化了开发人员的工作量，提高了代码的可维护性，使开发更专注于业务逻辑的实现，而无需过多关注数据层的操作。

在项目中引入MyBatis Plus，主要包括以下步骤：

1. **添加依赖：** 首先，在你的项目的pom.xml中添加MyBatis Plus的依赖：

```xml
<dependencies>
    <!-- 引入mybatis-plus-boot-starter -->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>最新版本</version>
    </dependency>

    <!-- 如果你的项目中还没有引入mybatis和spring-boot的相关依赖，也需要引入 -->
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>最新版本</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
    </dependency>
</dependencies>
```

请确保使用的是MyBatis Plus的最新版本。你可以在[MyBatis Plus的官方网站](https://mybatis.plus/)上找到最新的版本信息。

2. **配置MyBatis Plus：** 在你的Spring Boot的application.properties（或application.yml）文件中，配置MyBatis Plus的相关信息，如mapper文件的位置、别名包的路径等：

```properties
mybatis-plus.mapper-locations=classpath:mapper/*.xml
mybatis-plus.type-aliases-package=com.your.package.entity
```

3. **使用MyBatis Plus：** 现在，你就可以在你的项目中使用MyBatis Plus了。比如，你可以创建一个继承自`BaseMapper`接口的Mapper接口：

```java
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.your.package.entity.YourEntity;

public interface YourEntityMapper extends BaseMapper<YourEntity> {

}
```

这样，你就可以使用`BaseMapper`接口中的通用方法了，如`insert()`、`deleteById()`、`selectById()`等。

以上就是如何在项目中引入MyBatis Plus的基本步骤。注意，具体的版本号和配置可能需要根据你的实际项目情况进行调整。

## MyBatis Plus CURD操作

MyBatis Plus 是一个在 MyBatis 基础上的增强工具，它简化了 MyBatis 的使用，并提供了一些常用的 CRUD（增删改查）操作的方法。下面我将详细介绍并演示 MyBatis Plus 的 CURD 操作。

首先，你需要在项目中引入 MyBatis Plus 的依赖。可以使用 Maven 或者 Gradle 进行引入，具体可以参考 MyBatis Plus 的官方文档获取最新版本的依赖配置。

在介绍 CURD 操作之前，假设你已经创建了一个数据库表，并生成了对应的实体类。

### 添加数据（Create）

首先，我们需要创建一个实体类对应数据库表中的一条记录。假设我们有一个 `User` 实体类，具有 `id`、`name` 和 `age` 属性。

```java
public class User {
    private Long id;
    private String name;
    private Integer age;
    // 省略构造方法、getter 和 setter 方法
}
```

要添加一条记录，我们可以使用 MyBatis Plus 提供的 `insert` 方法。

```java
User user = new User();
user.setName("Alice");
user.setAge(25);
userMapper.insert(user);
```

这样就可以将一个用户信息插入到数据库中。

### 查询数据（Retrieve）

接下来，我们来演示如何从数据库中查询数据。

#### 根据主键查询

如果你知道记录的主键，可以使用 MyBatis Plus 的 `selectById` 方法进行查询。

```java
Long userId = 1L;
User user = userMapper.selectById(userId);
System.out.println(user);
```

这样就可以根据主键查询到对应的用户信息。

#### 条件查询

如果你需要根据条件查询数据，可以使用 MyBatis Plus 提供的 `selectList` 方法。

```java
// 使用 QueryWrapper 构建查询条件
QueryWrapper<User> queryWrapper = new QueryWrapper<>();
queryWrapper.eq("name", "Alice").gt("age", 20);

List<User> userList = userMapper.selectList(queryWrapper);
for (User user : userList) {
    System.out.println(user);
}
```

这样就可以根据条件查询到满足条件的用户列表。

### 更新数据（Update）

如果你需要更新数据库中的数据，可以使用 MyBatis Plus 的 `updateById` 方法。

```java
User user = userMapper.selectById(1L);
user.setName("Bob");
userMapper.updateById(user);
```

这样就可以更新指定主键的记录。

### 删除数据（Delete）

如果你需要删除数据库中的数据，可以使用 MyBatis Plus 的 `deleteById` 方法。

```java
Long userId = 1L;
userMapper.deleteById(userId);
```

这样就可以根据主键删除对应的记录。

以上就是 MyBatis Plus 的 CURD 操作的简要介绍和演示。当然，MyBatis Plus 还提供了更多的方法和功能，例如分页查询、批量操作等。你可以参考 MyBatis Plus 的官方文档来了解更多详细信息。

### AR模式

MyBatis Plus 的 AR 模式是一种简化数据库操作的方式，它将实体类和数据库表映射起来，使得我们可以通过实体类的方法来进行数据库的增删改查操作。下面是一个示例：

假设我们有一个 User 实体类，对应数据库中的 user 表，表结构如下：

```
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```

我们可以定义一个 User 类，继承 MyBatis Plus 提供的 Model 类，这样就可以使用 AR 模式了：

```java
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

@TableName("user")
public class User extends Model<User> {
    private Long id;
    private String name;
    private Integer age;
    private String email;

    // getter 和 setter 略

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
}
```

在这个类中，我们使用了 `@TableName` 注解来指定对应的数据库表名，继承了 MyBatis Plus 提供的 `Model` 类，并定义了实体类的属性和方法。

接下来，我们可以使用 AR 模式进行数据库操作。例如，插入一条数据：

```java
User user = new User();
user.setName("Tom");
user.setAge(20);
user.setEmail("tom@example.com");
user.insert();
```

这里我们创建了一个 User 对象，设置了属性值，然后调用 `insert()` 方法将数据插入到数据库中。

查询数据也很简单，例如，查询所有数据：

```java
List<User> userList = new User().selectAll();
for (User user : userList) {
    System.out.println(user);
}
```

这里我们创建了一个空的 User 对象，调用 `selectAll()` 方法查询所有数据，并遍历输出。

更新和删除数据也类似，例如，更新一条数据：

```java
User user = new User();
user.setId(1L);
user.setName("Tom");
user.setAge(21);
user.setEmail("tom@example.com");
user.updateById();
```

这里我们创建了一个 User 对象，设置了属性值和主键 id，然后调用 `updateById()` 方法更新数据。

删除一条数据也很简单：

```java
new User().setId(1L).deleteById();
```

这里我们创建了一个空的 User 对象，设置了主键 id，然后调用 `deleteById()` 方法删除数据。

总之，使用 MyBatis Plus 的 AR 模式可以大大简化数据库操作，提高开发效率。除了上述示例，MyBatis Plus 还提供了很多其他的 AR 操作方法，可以根据具体需求进行使用。

## 条件构造器

### Wrapper

`MyBatis Plus`的条件构造器`Wrapper`是一个强大的工具，可以帮助我们构建复杂的查询条件。它提供了一系列的方法，可以用来构建查询条件，例如`eq`、`ne`、`like`、`in`、`between`等等。下面是一个简单的示例，演示如何使用`Wrapper`构建查询条件：

```java
Wrapper<User> wrapper = new QueryWrapper<>();
wrapper.eq("name", "张三").or().eq("name", "李四");
List<User> userList = userMapper.selectList(wrapper);
```

在这个示例中，我们创建了一个`QueryWrapper`对象，并使用`eq`方法添加了两个查询条件，分别是`name`等于`张三`和`name`等于`李四`。我们还使用了`or`方法将这两个条件组合成一个`OR`条件。最后，我们使用`selectList`方法执行查询，并将结果保存在`userList`中。

除了`eq`和`or`方法之外，`Wrapper`还提供了许多其他的方法，可以用来构建不同类型的查询条件。例如，`like`方法可以用来构建模糊查询条件，`in`方法可以用来构建`IN`条件，`between`方法可以用来构建`BETWEEN`条件等等。下面是一个更复杂的示例，演示如何使用`Wrapper`构建一个包含多个查询条件的查询：

```java
Wrapper<User> wrapper = new QueryWrapper<>();
wrapper.like("name", "张").and(w -> w.lt("age", 30).or().isNotNull("email"));
List<User> userList = userMapper.selectList(wrapper);
```

在这个示例中，我们创建了一个`QueryWrapper`对象，并使用`like`方法添加了一个模糊查询条件，查询`name`包含`张`的用户。然后，我们使用`and`方法添加了一个`AND`条件，这个条件包含两个子条件：`age`小于`30`和`email`不为空。我们使用`lt`方法添加了一个小于条件，使用`or`方法将其与`isNotNull`方法组合成一个`OR`条件。最后，我们使用`selectList`方法执行查询，并将结果保存在`userList`中。

`MyBatis Plus`的条件构造器`Wrapper`是一个非常强大的工具，可以帮助我们构建复杂的查询条件。通过使用不同的方法，我们可以构建出各种类型的查询条件，从而实现更加灵活和精确的数据查询。

### QueryWrapper

`MyBatis Plus`的条件构造器`QueryWrapper`是一个强大的工具，可以帮助我们构建复杂的查询条件。它提供了许多方法来构造各种类型的查询条件，例如等于、不等于、大于、小于、模糊查询等。下面是一些常用的方法及其示例：

1. `eq`方法：等于

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.eq("name", "张三");
List<User> userList = userMapper.selectList(wrapper);
```

2. `ne`方法：不等于

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.ne("age", 18);
List<User> userList = userMapper.selectList(wrapper);
```

3. `gt`方法：大于

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.gt("age", 18);
List<User> userList = userMapper.selectList(wrapper);
```

4. `ge`方法：大于等于

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.ge("age", 18);
List<User> userList = userMapper.selectList(wrapper);
```

5. `lt`方法：小于

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.lt("age", 18);
List<User> userList = userMapper.selectList(wrapper);
```

6. `le`方法：小于等于

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.le("age", 18);
List<User> userList = userMapper.selectList(wrapper);
```

7. `like`方法：模糊查询

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.like("name", "张");
List<User> userList = userMapper.selectList(wrapper);
```

8. `in`方法：包含

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.in("age", Arrays.asList(18, 20, 22));
List<User> userList = userMapper.selectList(wrapper);
```

9. `notIn`方法：不包含

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.notIn("age", Arrays.asList(18, 20, 22));
List<User> userList = userMapper.selectList(wrapper);
```

10. `isNull`方法：为空

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.isNull("email");
List<User> userList = userMapper.selectList(wrapper);
```

11. `isNotNull`方法：不为空

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.isNotNull("email");
List<User> userList = userMapper.selectList(wrapper);
```

12. `orderBy`方法：排序

```java
QueryWrapper<User> wrapper = new QueryWrapper<>();
wrapper.orderByDesc("age");
List<User> userList = userMapper.selectList(wrapper);
```

以上是一些常用的方法及其示例，`QueryWrapper`还提供了许多其他方法，可以根据具体需求选择使用。

### UpdateWrapper

`UpdateWrapper`是MyBatis Plus中的一个条件构造器，用于构建更新操作的条件。它提供了一系列方法，可以方便地构建各种复杂的更新条件。

下面是一个示例，假设我们有一个`User`实体类，包含`id`、`name`和`age`三个属性，对应数据库中的`user`表。我们想要更新`name`为`Tom`的用户的年龄为`20`，可以使用`UpdateWrapper`来构建更新条件：

```java
UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
updateWrapper.eq("name", "Tom").set("age", 20);
userMapper.update(null, updateWrapper);
```

上面的代码中，我们首先创建了一个`UpdateWrapper`对象，然后使用`eq`方法指定了`name`为`Tom`的用户，使用`set`方法将其年龄更新为`20`。最后，我们调用`userMapper.update`方法执行更新操作，第一个参数为更新的实体对象，这里传入`null`表示只更新指定的字段，不更新其他字段。

除了`eq`和`set`方法，`UpdateWrapper`还提供了许多其他方法，可以用于构建各种复杂的更新条件。例如，我们可以使用`like`方法来模糊匹配某个字段，使用`in`方法来匹配多个值，使用`between`方法来匹配某个范围内的值等等。下面是一些示例：

```java
// 更新所有年龄大于等于18岁的用户的姓名为"John"
UpdateWrapper<User> updateWrapper1 = new UpdateWrapper<>();
updateWrapper1.ge("age", 18).set("name", "John");
userMapper.update(null, updateWrapper1);

// 更新所有姓名以"Tom"开头的用户的年龄为25
UpdateWrapper<User> updateWrapper2 = new UpdateWrapper<>();
updateWrapper2.likeRight("name", "Tom").set("age", 25);
userMapper.update(null, updateWrapper2);

// 更新所有年龄在18到25岁之间的用户的姓名为"Mike"
UpdateWrapper<User> updateWrapper3 = new UpdateWrapper<>();
updateWrapper3.between("age", 18, 25).set("name", "Mike");
userMapper.update(null, updateWrapper3);

// 更新所有id在1、3、5、7、9的用户的年龄为30
UpdateWrapper<User> updateWrapper4 = new UpdateWrapper<>();
updateWrapper4.in("id", 1, 3, 5, 7, 9).set("age", 30);
userMapper.update(null, updateWrapper4);
```

上面的代码中，我们分别使用了`ge`、`likeRight`、`between`和`in`方法来构建更新条件，然后使用`set`方法更新指定的字段。这些方法都可以根据需要进行组合，构建更加复杂的更新条件。

## 自定义SQL

在`MyBatis Plus`中，我们可以在Mapper接口中定义方法，然后使用注解或XML编写自定义SQL语句。下面我将演示如何在Mapper接口中定义方法，编写自定义SQL。

首先，我们需要定义一个Mapper接口，例如：

```java
public interface UserMapper extends BaseMapper<User> {
    @Select("SELECT * FROM user WHERE age > #{age}")
    List<User> selectByAge(@Param("age") Integer age);
}
```

在这个例子中，我们定义了一个`selectByAge`方法，使用`@Select`注解指定了自定义的SQL语句。这个SQL语句会查询`user`表中所有`age`大于传入参数`age`的记录，并返回一个`List<User>`类型的结果集。

接下来，我们可以在代码中使用这个方法，例如：

```java
UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
List<User> userList = userMapper.selectByAge(18);
```

这个例子中，我们获取了一个`UserMapper`对象，并调用了`selectByAge`方法，传入参数`18`。`MyBatis Plus`会自动将这个参数传递给SQL语句中的`#{age}`占位符，并执行查询操作。最后，我们得到了一个`List<User>`类型的结果集。

除了使用注解，我们还可以使用XML文件编写自定义SQL语句。例如，我们可以在`UserMapper.xml`文件中编写如下内容：

```xml
<mapper namespace="com.example.mapper.UserMapper">
    <select id="selectByAge" resultType="com.example.entity.User">
        SELECT * FROM user WHERE age > #{age}
    </select>
</mapper>
```

这个XML文件中定义了一个`selectByAge`方法，使用`<select>`标签指定了自定义的SQL语句。这个SQL语句与之前的例子相同，查询`user`表中所有`age`大于传入参数`age`的记录，并返回一个`com.example.entity.User`类型的结果集。

在代码中使用这个方法的方式与之前相同，例如：

```java
UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
List<User> userList = userMapper.selectByAge(18);
```

这个例子中，我们获取了一个`UserMapper`对象，并调用了`selectByAge`方法，传入参数`18`。`MyBatis Plus`会自动将这个参数传递给SQL语句中的`#{age}`占位符，并执行查询操作。最后，我们得到了一个`List<User>`类型的结果集。

使用`MyBatis Plus`在Mapper接口中定义方法，编写自定义SQL语句非常方便。我们可以使用注解或XML文件来编写SQL语句，然后在代码中直接调用这些方法，完成数据库操作。

## 插件

### 分页插件

下面将详细讲解并演示`MyBatis Plus`中的分页插件。

`MyBatis Plus`中的分页插件是一个非常强大的工具，它可以帮助我们轻松地实现分页查询功能。下面我将演示如何使用分页插件。

首先，我们需要在`pom.xml`文件中添加以下依赖：

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.4.3.1</version>
</dependency>
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-extension</artifactId>
    <version>3.4.3.1</version>
</dependency>
```

然后，在我们的`Mapper`接口中定义一个方法，用于查询数据并返回分页结果。例如：

```java
public interface UserMapper extends BaseMapper<User> {
    IPage<User> selectUserPage(Page<User> page, @Param("name") String name);
}
```

在这个方法中，我们使用了`Page`和`IPage`两个类。`Page`类表示分页查询的参数，包括当前页码、每页显示的记录数等信息。`IPage`类表示分页查询的结果，包括查询到的数据列表、总记录数等信息。

接下来，我们需要在`application.yml`文件中配置分页插件：

```yaml
mybatis-plus:
  configuration:
    # 开启驼峰命名转换
    map-underscore-to-camel-case: true
    # 配置分页插件
    plugins:
      - com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor
```

最后，在我们的业务代码中调用`selectUserPage`方法即可实现分页查询。例如：

```java
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public IPage<User> getUserPage(Page<User> page, String name) {
        return userMapper.selectUserPage(page, name);
    }
}
```

在这个方法中，我们调用了`selectUserPage`方法，并传入了一个`Page`对象和一个`name`参数。`Page`对象表示分页查询的参数，`name`参数表示查询条件。`selectUserPage`方法会返回一个`IPage`对象，其中包含了查询到的数据列表、总记录数等信息。

以上就是使用`MyBatis Plus`分页插件的基本步骤和示例代码。使用分页插件可以大大简化分页查询的代码，提高开发效率。

### 性能分析插件

MyBatis Plus的性能分析插件可以帮助我们分析SQL语句的执行情况，包括执行时间、执行的SQL语句等信息，从而帮助我们优化SQL语句的性能。

下面是使用MyBatis Plus的性能分析插件的步骤：

1. 在`application.properties`文件中添加配置：

```
# 开启SQL语句性能分析插件
mybatis-plus.configuration.log-impl=org.apache.ibatis.logging.stdout.StdOutImpl
```

2. 在代码中执行SQL语句，例如：

```java
List<User> userList = userMapper.selectList(null);
```

3. 执行完SQL语句后，在控制台中可以看到类似下面的输出：

```
2021-08-10 16:22:22.222  INFO 12345 --- [           main] com.baomidou.mybatisplus.core.MybatisPlus   : Total: 1
==>  Preparing: SELECT id,name,age,email,create_time,update_time FROM user
==> Parameters: 
<==    Columns: id, name, age, email, create_time, update_time
<==        Row: 1, Jone, 18, jone@mybatisplus.com, 2021-08-10 16:22:22, 2021-08-10 16:22:22
<==      Total: 1
```

其中，`Preparing`表示准备执行的SQL语句，`Parameters`表示SQL语句的参数，`Columns`表示查询结果的列名，`Row`表示查询结果的一行数据，`Total`表示查询结果的总数。

通过这些信息，我们可以了解SQL语句的执行情况，从而进行优化。例如，如果发现SQL语句执行时间过长，可以考虑添加索引或者优化SQL语句的写法。

需要注意的是，性能分析插件会输出所有执行的SQL语句，包括一些系统自动生成的SQL语句，因此在生产环境中应该关闭该插件，以避免泄露敏感信息。

## 代码生成器

MyBatis Plus的代码生成器是一个非常强大的工具，可以帮助我们快速生成基于数据库表的Java实体类、Mapper接口和XML映射文件。下面我将详细讲解并演示如何使用代码生成器。

1. 首先，我们需要在项目中引入MyBatis Plus的代码生成器依赖。在Maven项目中，可以在pom.xml文件中添加以下依赖：

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.4.2</version>
</dependency>
```

2. 接下来，我们需要在项目中创建一个配置类，用于配置代码生成器的参数。以下是一个示例配置类：

```java
package com.example.demo.config;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class CodeGeneratorConfig {

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(System.getProperty("user.dir") + "/src/main/java");
        gc.setAuthor("your name");
        gc.setOpen(false);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.example.demo");
        pc.setModuleName("module");
        mpg.setPackageInfo(pc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);
        strategy.setInclude("user"); // 需要生成的表名
        mpg.setStrategy(strategy);

        // 执行生成
        mpg.execute();
    }

}
```

在这个配置类中，我们可以设置全局配置、数据源配置、包配置和策略配置。其中，全局配置包括输出目录、作者、是否打开输出目录等；数据源配置包括数据库连接信息；包配置包括父包名和模块名；策略配置包括命名策略、是否使用Lombok、是否使用RestController等。需要注意的是，我们需要在策略配置中指定需要生成的表名。

3. 最后，我们只需要运行这个配置类的main方法，就可以生成Java实体类、Mapper接口和XML映射文件了。生成的文件会保存在指定的输出目录中。

代码生成器只是一个工具，生成的代码可能需要根据实际情况进行修改和优化。同时，我们也需要注意数据库表的设计，遵循良好的数据库设计原则，才能生成高质量的Java代码。
