# 异常

* 完成下面代码，然后酌情优化：

```java
package me.ereach.advanced.exceptions;

import org.junit.Test;

import java.io.FileReader;
import java.io.IOException;

public class MyTests {
    @Test
    public void testFile() {
        try {
            FileReader f = new FileReader("d:\\readme.txt");
            int read = 0;
            while (true) {
                read = f.read();
                if (read == -1)
                    break;
                System.out.print((char) read);
            }
            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```