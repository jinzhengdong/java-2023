package com.xinhua;

public class GenericList<T extends Comparable<T>> {
    private T[] items;
    private int count;
    @SuppressWarnings("unchecked")
    public GenericList() {
        items = (T[]) new Comparable[10];
    }
    public void add(T item) {
        items[count++] = item;
    }
    public T get(int index) {
        return items[index];
    }
    public int getCount() {
        return count;
    }
}