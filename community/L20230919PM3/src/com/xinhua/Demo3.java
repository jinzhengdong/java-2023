package com.xinhua;

public class Demo3 {
    public static void main(String[] args) {
        // 整数变量声明
        byte a = 12;
        short b = 278;
        int c = 300;
        long d = 4000;

        // 浮点数变量声明
        float e = 23.5f;
        float e1 = 23.45F;
        double f = 34.56;
        double f1 = 34.567d;
        double f2 = 34.567D;

        // 字符变量
        char c1 = 'A';
        char c2 = 'a';

        // 字符串
        String str = "Hello, World!";

        // 布尔类型
        boolean b1 = true;  // 真
        boolean b2 = false; // 假

        System.out.println("===");
        System.out.println(e);
        e = a + b;
        System.out.println(e);

        byte b3;
        b3 = 12;
        System.out.println(b3);

        int i = 100;
        System.out.println(i);
    }
}
