```sql
-- Tables 多个表的节点；Views 多个视图的节点；
-- Store Procedures 存储过程节点；Functions 函数节点
-- city 城市；countryName 国家；CountryLanguage 国家语言
-- 找出人口最多的国家
-- select 选择；* 所有字段；from 来自；country 国家
-- order by 按...排序；Population 人口；asc 正序
-- desc 倒序；limit 限制 1 条；2 限制 2 条
-- limit 3 限制 3 条
-- SQL S: Structure 结构；Q: Query 查询；L: Language 语言
-- 结构化查询语言（声明式编程语言）
select * from country order by Population asc;
select * from country order by Population desc;
select * from country order by Population desc limit 1;
select * from country order by Population desc limit 2;
select * from country order by Population desc limit 3;
select name, population from country order by population desc;
select
	name, 
    population 
from 
	country 
order by 
	population desc;
-- 找出人口最少的国家
select
	name, 
    population 
from 
	country 
order by 
	population asc limit 5;
-- 找出位于亚洲(Continent = 'Asia')的国家
-- where 在那里; Continent 大陆 
-- 选择 所有字段 来自 国家表 在那里 大陆 = 亚洲
select * from country where Continent = 'Asia';
select 
	*
from 
	country 
where 
	Continent = 'Asia';
-- 找出使用英语(Language = 'English')
-- 官方语言(IsOfficial = true)的国家
select
	*
from
	countrylanguage
where
	Language = 'English' and 
    IsOfficial = true;
-- 找出人口(Population > 100000000)过亿的国家
select * from country;
-- 找出人口超过10000000的城市
select * from city where Population > 10000000;
-- 找出使用中文(Chinese)为官方语言的国家
select * from countrylanguage;
-- 找出法语(French)为官方语言的国家

-- 找出非洲(Continent = Africa)大陆的国家
select * from country;
-- 找出人口最多的城市所属的国家

-- 找出每个国家的首都和它所属的大洲
-- 找出每个国家的名称、所属大洲和使用的官方语言
-- 找出每个城市的名称、所属国家的名称以及所属国家的人口数量。
-- 找找每个国家的名称、所属大洲以及最大的城市名称和人口数量

```