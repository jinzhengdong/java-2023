package me.ereach;

public class CircleExt extends Point {
    private float r;

    public CircleExt(float x, float y, float r) {
        super(x, y);
        this.r = r;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    @Override
    public String toString() {
        return "CircleExt{" +
                "r=" + r +
                ", p=" + super.toString() +
                '}';
    }
}
