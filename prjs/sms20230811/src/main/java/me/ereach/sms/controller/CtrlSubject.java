package me.ereach.sms.controller;

import me.ereach.sms.mapper.SubjectMapper;
import me.ereach.sms.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin
@RequestMapping("/api")
public class CtrlSubject {
    @Autowired
    private SubjectMapper subjectMapper;

    @GetMapping("/subject")
    public List<Subject> getSubject() {
        return subjectMapper.selectList(null);
    }

    @PostMapping("/subject")
    public Integer insertSubject(
            @RequestParam("subjectname") String subjectName
    ) {
        System.out.println("===post subject===");
        Subject subject = new Subject();
        subject.setId(3);
        subject.setSubjectName(subjectName);

        Integer retvar = subjectMapper.insert(subject);

        return retvar;
    }
}
