public class Main {
    public static void main(String[] args) {
        Point3D p0 = new Point3D();
        Point3D p1 = new Point3D(3, 5, 7);
        System.out.println(p0);
        System.out.println(p1);

        System.out.println(p0.getDistance(p1));
        System.out.println(Point3D.getDistanceStatic(p0, p1));
//        Point3D.getDistanceStatic()
    }
}