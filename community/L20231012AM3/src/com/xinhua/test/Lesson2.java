package com.xinhua.test;

import org.junit.Test;

import java.util.Random;

public class Lesson2 {
    @Test
    public void test01() {
        System.out.println("Hello, world, test01");
    }

    @Test
    public void test02() {
        System.out.println(isPrime(17));
        System.out.println(isPrime(8));
    }

    @Test
    public void test03() {
        System.out.println(calculatePi(1000000));
    }

    public double calculatePi(int nPoints) {
        Random rand = new Random();
        int inside = 0;

        for (int i = 0; i < nPoints; i++) {
            double x = rand.nextDouble();
            double y = rand.nextDouble();

            double distance = x * x + y * y;

            if (distance <= 1)
                inside++;
        }

        return  4D * inside / nPoints;
    }

    public boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }

        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }


}
