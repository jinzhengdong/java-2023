// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        byte a, b, c;
        short d = 11, e = 30;
        int f = 99;
        long a2, b2;
        float c3, f3;
        double d4, f4;

        c3 = 45f;
        d4 = 58d;

        System.out.println(c3);
        System.out.println(d4);

        c3 = 123.48F;
        d4 = 78.56D;

        System.out.println(c3);
        System.out.println(d4);

        int I1 = 12;
        int i1 = 14;

        System.out.println(I1);
        System.out.println(i1);

    }
}