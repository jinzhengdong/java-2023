package com.xinhua;

public class ClassRoom {
    private Integer id;
    private String classroomName;
    private String location;
    private Integer employeeId;

    public ClassRoom(Integer id, String classroomName, String location, Integer employeeId) {
        this.id = id;
        this.classroomName = classroomName;
        this.location = location;
        this.employeeId = employeeId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassroomName() {
        return classroomName;
    }

    public void setClassroomName(String classroomName) {
        this.classroomName = classroomName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "ClassRoom{" +
                "id=" + id +
                ", classroomName='" + classroomName + '\'' +
                ", location='" + location + '\'' +
                ", employeeId=" + employeeId +
                '}';
    }
}
