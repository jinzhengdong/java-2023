package me.ereach;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class MyTest {
    @Test
    public void test03() {
        try {
            var reader  = new FileReader("d:\\readme.txt");
        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
            System.out.println(e.getMessage());
        }
    }`

    @Test
    public void test02() throws FileNotFoundException {
        var reader = new FileReader("d:\\readme.txt");
        System.out.println(reader);
    }

    @Test
    public void test01() {
        sayHello("Tom");
        sayHello(null);
        System.out.println("Finished.");
    }

    public void sayHello(String name) {
        try {
            System.out.println("Hello, " + name.toUpperCase());
        }
        catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
