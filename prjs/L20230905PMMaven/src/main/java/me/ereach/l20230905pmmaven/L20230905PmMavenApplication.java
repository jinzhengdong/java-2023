package me.ereach.l20230905pmmaven;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@MapperScan("me.ereach.l20230905pmmaven.mapper")
@SpringBootApplication
public class L20230905PmMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(L20230905PmMavenApplication.class, args);
	}

}
